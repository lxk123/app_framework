package cn.com.dreamtouch.magicbox_repository.datasource.local;

import android.content.Context;
import android.support.annotation.NonNull;

import cn.com.dreamtouch.common.util.SPEditor;
import cn.com.dreamtouch.magicbox_common.util.CommonConstant;

/**
 * Created by MuHan on 17/2/20.
 */
public class UserLocalData {
    private static UserLocalData INSTANCE;
    private SPEditor spEditor;
    private Context mContext;

    private UserLocalData(@NonNull Context context) {
        spEditor = SPEditor.getInstance(context);
        mContext = context;
    }

    public static UserLocalData getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new UserLocalData(context);
        }
        return INSTANCE;
    }


    public String getSid() {
        return spEditor.loadStringInfo(CommonConstant.SPKey.SID);
    }


    public void setSid(String sid) {
        spEditor.saveStringInfo(CommonConstant.SPKey.SID, sid);
    }

    public void setShopID(String shopID) {
        spEditor.saveStringInfo(CommonConstant.SPKey.SHOPID, shopID);
    }

    public String getShopID() {
        return spEditor.loadStringInfo(CommonConstant.SPKey.SHOPID);
    }
    public void setShopType(String shopType) {
        spEditor.saveStringInfo(CommonConstant.SPKey.SHOP_TYPE, shopType);
    }

    public String getShopType() {
        return spEditor.loadStringInfo(CommonConstant.SPKey.SHOP_TYPE);
    }

    public String getEmployeeID() {
        return spEditor.loadStringInfo(CommonConstant.SPKey.EMPLOYID);
    }

    public void setEmployeeID(String employeeID) {
        spEditor.saveStringInfo(CommonConstant.SPKey.EMPLOYID, employeeID);
    }

    public void clear() {
        spEditor.saveStringInfo(CommonConstant.SPKey.SID, "");
        spEditor.saveStringInfo(CommonConstant.SPKey.EMPLOYID, "");
        spEditor.saveStringInfo(CommonConstant.SPKey.SHOPID, "");
        spEditor.saveStringInfo(CommonConstant.SPKey.SHOP_TYPE, "");

    }
}
