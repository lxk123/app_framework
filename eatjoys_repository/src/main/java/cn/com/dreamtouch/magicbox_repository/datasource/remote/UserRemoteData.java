package cn.com.dreamtouch.magicbox_repository.datasource.remote;

import android.content.Context;
import android.text.TextUtils;

import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_http_client.network.HttpClientHelper;
import cn.com.dreamtouch.magicbox_http_client.network.model.CancelOrderRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.CancelOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangePasswordRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangePasswordResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangeTableRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangeTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChearTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ClearTableRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllPayMethodResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetDishDetailRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetDishDetailResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetMessageRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetMessageResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetOrderListResopnse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetOrderListResquest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.IsNeedTableFeeRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.IsNeedTableFeeResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.LoginRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.LoginResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.OpenTableRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.OpenTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.OrderParam;
import cn.com.dreamtouch.magicbox_http_client.network.model.OrderStrBody;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayOrderRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayOrderStrBody;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayParam;
import cn.com.dreamtouch.magicbox_http_client.network.model.PostOrderRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.PostOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PrintOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushIsOnOffRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushIsOnOffResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushMessageRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushMessageResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.QrCodeTableRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.QrCodeTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.RefundDishes;
import cn.com.dreamtouch.magicbox_http_client.network.model.ReprintOrderRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.RetreatOrderRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.RetreatOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.SendVaildateCodeRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.SendVaildateCodeResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.UpdateDinnerPeopleRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.UpdateDinnerPeopleResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.WaitConfirmRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.WaitConfirmResponse;
import rx.Observable;

/**
 * Created by MuHan on 17/2/21.
 */

public class UserRemoteData {
    private static UserRemoteData INSTANCE;
    private Context context;

    private UserRemoteData(Context context) {
        this.context = context;

    }

    public static UserRemoteData getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new UserRemoteData(context);
        }
        return INSTANCE;
    }


    public Observable<LoginResponse> login(String username, String password) {
        LoginRequest request = new LoginRequest();
        request.setUsername(username);
        request.setPassword(password);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context)).login(request);
    }

    public Observable<GetAllTableResponse> getAllTable(String sid, String shopID,String updateFlag) {
        GetAllTableRequest request = new GetAllTableRequest();
        request.setSid(sid);
        request.setShopID(shopID);
        request.setUpdateFlag(updateFlag);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context), sid)
                .getAllTable(request);
    }

    public Observable<OpenTableResponse> openTable(String tableID, String shopID,
                                                   String peopleNumber, String remark) {
        OpenTableRequest request = new OpenTableRequest();
        request.setShopID(shopID);
        request.setTableID(tableID);
        request.setPeopleNumber(peopleNumber);
        request.setRemark(remark);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .openTable(request);
    }

    public Observable<ChangeTableResponse> changeTable( String oldTableID,
                                                       String tableID, String shopID, String orderID) {
        ChangeTableRequest request = new ChangeTableRequest();
        request.setTableID(tableID);
        request.setOldTableID(oldTableID);
        request.setShopID(shopID);
        request.setOrderID(orderID);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .changeTable(request);
    }

    public Observable<ChearTableResponse> cleanTable(String tableID, String shopID) {
        ClearTableRequest request = new ClearTableRequest();
        request.setTableID(tableID);
        request.setShopID(shopID);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .cleanTable(request);
    }

    public Observable<GetDishDetailResponse> getDishDetail(String shopID, String shopDishID) {
        GetDishDetailRequest request = new GetDishDetailRequest();
        request.setShopDishID(shopDishID);
        request.setShopID(shopID);
        return new HttpClientHelper(CommonConstant.API.url)
                .getDishDetail(request);
    }

    public Observable<GetCustomOrderInfoResponse> getCustomOrderInfo(String shopID, String orderID) {
        GetCustomOrderInfoRequest request = new GetCustomOrderInfoRequest();
        request.setOrderID(orderID);
        request.setShopID(shopID);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .getCustomOrderInfo(request);
    }

    public Observable<CancelOrderResponse> cancelOrder(String shopID, String orderID) {
        CancelOrderRequest request = new CancelOrderRequest();
        request.setOrderID(orderID);
        request.setShopID(shopID);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .cancelOrder(request);
    }

    public Observable<QrCodeTableResponse> qrCodeTable(String shopID, String qrcode) {
        QrCodeTableRequest request = new QrCodeTableRequest();
        request.setQrcode(qrcode);
        request.setShopID(shopID);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .qrCodeTable(request);
    }

    public Observable<GetOrderListResopnse> getOrderList(String date, String shopID, String pageSize, String pageIndex) {
        GetOrderListResquest request = new GetOrderListResquest();
        request.setDate(date);
        request.setShopID(shopID);
        request.setPageSize(pageSize);
        request.setPageIndex(pageIndex);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .getOrderList(request);
    }

    public Observable<UpdateDinnerPeopleResponse> updateDinnerPeople(String shopID, String tableID,
                                                                     String orderID, String peopleNum) {
        UpdateDinnerPeopleRequest request = new UpdateDinnerPeopleRequest();
        request.setShopID(shopID);
        request.setTableID(tableID);
        if(!orderID.equals("0") ){
            request.setOrderID(orderID);
        }
        request.setPeopleNum(peopleNum);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .updateDinnerPeople(request);
    }

    public Observable<ChangePasswordResponse> changePassword(String mobile, String pwd,
                                                             String code) {
        ChangePasswordRequest request = new ChangePasswordRequest();
        request.setMobile(mobile);
        request.setPwd(pwd);
        request.setCode(code);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .changePassword(request);
    }
  public Observable<WaitConfirmResponse> waiterConfirmOrder(String shopId, String orderID,String confirm,
                                                            String remark) {
        WaitConfirmRequest request = new WaitConfirmRequest();
        request.setShopID(shopId);
        request.setOrderID(orderID);
        request.setConfirm(confirm);
        request.setRemark(remark);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .waiterConfirmOrder(request);
    }

    public Observable<SendVaildateCodeResponse> sendValidateCode(String mobile) {
        SendVaildateCodeRequest request = new SendVaildateCodeRequest();
        request.setMobile(mobile);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .sendValidateCode(request);
    }

    public Observable<PushMessageResponse> pushMessage(String shopSerialNumber, String shopID) {
        PushMessageRequest request = new PushMessageRequest();
        request.setShopID(shopID);
        request.setShopSerialNumber(shopSerialNumber);
        return new HttpClientHelper(CommonConstant.API.url)
                .pushMessage(request);
    }

    public Observable<PushIsOnOffResponse> pullIsOnOff(String employeeID) {
        PushIsOnOffRequest request = new PushIsOnOffRequest();
        request.setEmployeeID(employeeID);
        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .pullIsOnOff(request);
    }

    public Observable<IsNeedTableFeeResponse> isNeedTableFee(String tableID, String shopID) {
        IsNeedTableFeeRequest request = new IsNeedTableFeeRequest();
        request.setShopID(shopID);
        request.setTableID(tableID);
        return new HttpClientHelper(CommonConstant.API.url)
                .isNeedTableFee(request);
    }
    public Observable<PrintOrderResponse> reprintOrder(String orderID, String shopID) {
        ReprintOrderRequest request = new ReprintOrderRequest();
        request.setShopID(shopID);
        request.setOrderID(orderID);
        return new HttpClientHelper(CommonConstant.API.url)
                .reprintOrder(request);
    }

    public Observable<GetMessageResponse> getMessage(String shopID, String page) {
        GetMessageRequest request = new GetMessageRequest();
        request.setShopID(shopID);
        request.setPage(page);
        return new HttpClientHelper(CommonConstant.API.url)
                .getMessage(request);
    }

    public Observable<GetShopDishResponse> getShopDish() {

        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .getShopDish();
    }

    public Observable<GetAllPayMethodResponse> getAllPayMethod() {

        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .getAllPayMethod();
    }

    public Observable<PostOrderResponse> postOrder(OrderParam orderParam) {
        PostOrderRequest request = new PostOrderRequest();
        request.setOrderParam(orderParam);

        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .postOrder(request);
    }

    public Observable<PayOrderResponse> payOrder( PayParam payOrderStrBody) {
        PayOrderRequest request = new PayOrderRequest();
        request.setPayOrderStrBody(payOrderStrBody);

        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .payOrder(request);
    }

    public Observable<RetreatOrderResponse> retreatOrder( RefundDishes orderStrBody) {
        RetreatOrderRequest request = new RetreatOrderRequest();
        request.setOrderStrBody(orderStrBody);

        return HttpClientHelper.getInstance(CommonConstant.API.getServerHost(context))
                .retreatOrder(request);
    }

}
