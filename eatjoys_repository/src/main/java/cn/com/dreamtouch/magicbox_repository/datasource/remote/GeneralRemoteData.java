package cn.com.dreamtouch.magicbox_repository.datasource.remote;


import android.content.Context;

/**
 * Created by MuHan on 17/2/21.
 */

public class GeneralRemoteData {
    private static GeneralRemoteData INSTANCE;
    private Context context;
    private GeneralRemoteData(Context context) {
   this.context=context;
    }

    public static GeneralRemoteData getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new GeneralRemoteData(context);
        }
        return INSTANCE;
    }
}
