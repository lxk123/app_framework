package cn.com.dreamtouch.magicbox_repository.datasource.local;

import android.content.Context;
import android.support.annotation.NonNull;

/**
 * Created by MuHan on 17/2/21.
 */

public class GeneralLocalData {
    private static GeneralLocalData INSTANCE;

    private GeneralLocalData(@NonNull Context context) {

    }

    public static GeneralLocalData getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new GeneralLocalData(context);
        }
        return INSTANCE;

    }
}
