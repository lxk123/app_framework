package cn.com.dreamtouch.magicbox_repository.repository;

import android.support.annotation.NonNull;

import cn.com.dreamtouch.magicbox_http_client.network.model.CancelOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangePasswordResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangeTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChearTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllPayMethodResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetDishDetailResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetMessageResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetOrderListResopnse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.IsNeedTableFeeResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.LoginResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.OpenTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.OrderParam;
import cn.com.dreamtouch.magicbox_http_client.network.model.OrderStrBody;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayOrderStrBody;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayParam;
import cn.com.dreamtouch.magicbox_http_client.network.model.PostOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PrintOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushIsOnOffResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushMessageResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.QrCodeTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.RefundDishes;
import cn.com.dreamtouch.magicbox_http_client.network.model.RetreatOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.SendVaildateCodeResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.UpdateDinnerPeopleResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.WaitConfirmResponse;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.UserRemoteData;
import rx.Observable;

/**
 * Created by MuHan on 17/2/9.
 */

public class UserRepository {
    private UserRemoteData mUserRemoteData;
    private UserLocalData mUserLocalData;
    private static UserRepository INSTANCE;

    public static UserRepository getInstance(@NonNull UserLocalData userLocalData, @NonNull UserRemoteData userRemoteData) {
        if (INSTANCE == null) {
            INSTANCE = new UserRepository(userLocalData, userRemoteData);
        }
        return INSTANCE;
    }

    private UserRepository(@NonNull UserLocalData userLocalData, @NonNull UserRemoteData userRemoteData) {
        mUserLocalData = userLocalData;
        mUserRemoteData = userRemoteData;
    }


    public Observable<LoginResponse> login(String username, String password) {
        return mUserRemoteData.login(username, password);
    }

    public Observable<GetAllTableResponse> getAllTable(String sid, String shopID,String updateFlag) {
        return mUserRemoteData.getAllTable(sid, shopID,updateFlag);
    }

    public Observable<OpenTableResponse> openTable(String tableID, String shopID ,
                                                   String peopleNumber, String remark) {
        return mUserRemoteData.openTable(tableID, shopID, peopleNumber, remark);
    }

    public Observable<ChangeTableResponse> changeTable( String oldTableID,
                                                       String tableID, String shopID, String orderID) {
        return mUserRemoteData.changeTable( oldTableID, tableID, shopID, orderID);
    }

    public Observable<ChearTableResponse> cleanTable(String tableID, String shopID) {
        return mUserRemoteData.cleanTable(tableID, shopID);
    }

    public Observable<GetDishDetailResponse> getDishDetail(String shopID, String shopDishID) {
        return mUserRemoteData.getDishDetail(shopID, shopDishID);
    }
    public Observable<GetCustomOrderInfoResponse> getCustomOrderInfo(String shopID, String orderID) {
        return mUserRemoteData.getCustomOrderInfo(shopID, orderID);
    }
    public Observable<CancelOrderResponse> cancelOrder(String shopID, String orderID) {
        return mUserRemoteData.cancelOrder(shopID, orderID);
    }
    public Observable<QrCodeTableResponse> qrCodeTable(String shopID, String qrcode) {
        return mUserRemoteData.qrCodeTable(shopID, qrcode);
    }
    public Observable<GetOrderListResopnse> getOrderList(String date, String shopID,String pageSize,String pageIndex) {
        return mUserRemoteData.getOrderList(date, shopID,pageSize,pageIndex);
    }
    public Observable<UpdateDinnerPeopleResponse> updateDinnerPeople(String shopID, String tableID,
                                                                     String orderID, String peopleNum) {
        return mUserRemoteData.updateDinnerPeople(shopID, tableID,orderID,peopleNum);
    }
    public Observable<ChangePasswordResponse> changePassword(String mobile, String pwd,
                                                             String code) {
        return mUserRemoteData.changePassword(mobile,pwd,code);
    }
    public Observable<WaitConfirmResponse> waiterConfirmOrder(String shopId, String orderID,String confirm,
                                                              String remark) {
        return mUserRemoteData.waiterConfirmOrder(shopId,orderID,confirm,remark);
    }
    public Observable<PushMessageResponse> pushMessage(String shopSerialNumber, String shopID) {
        return mUserRemoteData.pushMessage(shopSerialNumber,shopID);
    }
    public Observable<PushIsOnOffResponse> pullIsOnOff(String employeeID) {
        return mUserRemoteData.pullIsOnOff(employeeID);
    }
    public Observable<IsNeedTableFeeResponse> isNeedTableFee(String tableID, String shopID) {
        return mUserRemoteData.isNeedTableFee(tableID,shopID);
    }
    public Observable<PrintOrderResponse> reprintOrder(String orderID, String shopID) {
        return mUserRemoteData.reprintOrder(orderID,shopID);
    }
    public Observable<GetMessageResponse> getMessage(String shopID, String page) {
        return mUserRemoteData.getMessage(shopID,page);
    }
    public Observable<SendVaildateCodeResponse> sendValidateCode(String mobile) {
        return mUserRemoteData.sendValidateCode(mobile);
    }
    public Observable<GetShopDishResponse> getShopDish() {
        return mUserRemoteData.getShopDish();
    }
    public Observable<GetAllPayMethodResponse> getAllPayMethod() {
        return mUserRemoteData.getAllPayMethod();
    }

    public Observable<PayOrderResponse> payOrder( PayParam payOrderStrBody) {
        return mUserRemoteData.payOrder( payOrderStrBody);
    }
    public Observable<RetreatOrderResponse> retreatOrder( RefundDishes orderStrBody) {
        return mUserRemoteData.retreatOrder( orderStrBody);
    }

    public Observable<PostOrderResponse> postOrder(OrderParam orderParam) {
        return mUserRemoteData.postOrder( orderParam);
    }

}
