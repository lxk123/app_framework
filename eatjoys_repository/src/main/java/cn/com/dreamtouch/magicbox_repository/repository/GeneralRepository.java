package cn.com.dreamtouch.magicbox_repository.repository;

import android.support.annotation.NonNull;

import cn.com.dreamtouch.magicbox_repository.datasource.local.GeneralLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.GeneralRemoteData;

/**
 * Created by MuHan on 17/2/9.
 */

public class GeneralRepository {
    private GeneralRemoteData mGeneralRemoteData;
    private GeneralLocalData mGeneralLocalData;
    private static GeneralRepository INSTANCE;

    public static GeneralRepository getInstance(@NonNull GeneralLocalData generalLocalData, @NonNull GeneralRemoteData generalRemoteData) {
        if (INSTANCE == null) {
            INSTANCE = new GeneralRepository(generalLocalData, generalRemoteData);
        }
        return INSTANCE;
    }

    private GeneralRepository(@NonNull GeneralLocalData generalLocalData, @NonNull GeneralRemoteData generalRemoteData) {
        mGeneralLocalData = generalLocalData;
        mGeneralRemoteData = generalRemoteData;
    }
}
