package cn.com.dreamtouch.magicbox_repository;

import android.content.Context;
import android.support.annotation.NonNull;

import cn.com.dreamtouch.magicbox_repository.datasource.local.GeneralLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.GeneralRemoteData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.UserRemoteData;
import cn.com.dreamtouch.magicbox_repository.repository.GeneralRepository;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;

/**
 * Created by MuHan on 17/2/23.
 */

public class Injection {
    /**
     * 用户信息相关业务
     *
     * @param context
     * @return
     */

    public static UserRepository provideUserRepository(@NonNull Context context) {
        return UserRepository.getInstance(UserLocalData.getInstance(context.getApplicationContext()), UserRemoteData.getInstance(context.getApplicationContext()));
    }

    /**
     * 通用业务
     * @param context
     * @return
     */
    public static GeneralRepository provideGeneralRepository(@NonNull Context context) {
        return GeneralRepository.getInstance(GeneralLocalData.getInstance(context.getApplicationContext()), GeneralRemoteData.getInstance(context.getApplicationContext()));
    }


}
