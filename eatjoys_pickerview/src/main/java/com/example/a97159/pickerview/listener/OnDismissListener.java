package com.example.a97159.pickerview.listener;

/**
 * Created by tiger on 16/7/14.
 */
public interface OnDismissListener {
    void onDismiss(Object o);
}
