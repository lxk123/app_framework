package com.example.a97159.pickerview;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.a97159.pickerview.view.BasePickerView;
import com.example.a97159.pickerview.view.WheelPeriod;

import java.util.ArrayList;

/**
 * Created by tiger on 2017/4/24.
 */

public class PeriodPickerView<T> extends BasePickerView implements View.OnClickListener {
    WheelPeriod<T> wheelPeriod;
    private View btnSubmit, btnCancel;
    private TextView tvTitle;
    private OnPeriodSelectListener periodSelectListener;
    private static final String TAG_SUBMIT = "submit";
    private static final String TAG_CANCEL = "cancel";
    public PeriodPickerView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.pickerview_period, contentContainer);
        // -----确定和取消按钮
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setTag(TAG_SUBMIT);
        btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setTag(TAG_CANCEL);
        btnSubmit.setOnClickListener(this);
        btnCancel.setOnClickListener(this);
        //顶部标题
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        // ----转轮
        final View options_picker = findViewById(R.id.periodpicker);
        wheelPeriod = new WheelPeriod<>(options_picker);
    }
    public void setPicker(ArrayList<T> options1Items,
                          ArrayList<T> options2Items,
                          ArrayList<T> options3Items,
                          ArrayList<T> options4Items) {
        wheelPeriod.setPicker(options1Items, options2Items, options3Items, options4Items);
    }
    /**
     * 设置选中的item位置
     * @param option1 位置
     * @param option2 位置
     * @param option3 位置
     * @param option4 位置
     */
    public void setSelectOptions(int option1, int option2, int option3, int option4){
        wheelPeriod.setCurrentItems(option1, option2, option3, option4);
    }
    /**
     * 设置选项的单位
     * @param label1 单位
     * @param label2 单位
     * @param label3 单位
     * @param label4 单位
     */
    public void setLabels(String label1, String label2, String label3, String label4){
        wheelPeriod.setLabels(label1, label2, label3,label4);
    }
    /**
     * 分别设置第一二三级是否循环滚动
     * @param cyclic1,cyclic2,cyclic3 是否循环
     */
    public void setCyclic(boolean cyclic1,boolean cyclic2,boolean cyclic3,boolean cyclic4) {
        wheelPeriod.setCyclic(cyclic1,cyclic2,cyclic3,cyclic4);
    }
    @Override
    public void onClick(View v)
    {
        String tag=(String) v.getTag();
        if(tag.equals(TAG_CANCEL))
        {
            dismiss();
            return;
        }
        else
        {
            if(periodSelectListener!=null)
            {
                int[] optionsCurrentItems=wheelPeriod.getCurrentItems();
                periodSelectListener.onPeriodSelect(optionsCurrentItems[0], optionsCurrentItems[1],optionsCurrentItems[2], optionsCurrentItems[3]);
            }
            dismiss();
            return;
        }
    }

    public interface OnPeriodSelectListener {
        void onPeriodSelect(int options1, int options2,int options3, int options4);
    }

    public void setOnPeriodSelectListener(OnPeriodSelectListener periodSelectListener) {
        this.periodSelectListener = periodSelectListener;
    }

    public void setTitle(String title){
        tvTitle.setText(title);
    }
}
