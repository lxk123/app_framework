package com.example.a97159.pickerview.listener;


public interface OnItemSelectedListener {
    void onItemSelected(int index);
}
