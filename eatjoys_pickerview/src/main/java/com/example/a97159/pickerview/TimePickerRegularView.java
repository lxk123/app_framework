package com.example.a97159.pickerview;

import android.content.Context;
import android.support.annotation.ColorRes;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.example.a97159.pickerview.view.WheelTime;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

import static com.example.a97159.pickerview.TimePickerView.Type.HOURS_MIN_SEC;

/**
 * Created by Zhoutie on 2017/5/3.
 */

public class TimePickerRegularView extends LinearLayout {
    private OnTimeSelectListener timeSelectListener;

    WheelTime wheelTime;
    private View timepickerview;

    public TimePickerRegularView(Context context) {
        super(context);

    }
    public TimePickerRegularView(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater mInflater = LayoutInflater.from(context);
        View myView = mInflater.inflate(R.layout.pickerview_time_regular, this);

        // ----时间转轮
       timepickerview = myView.findViewById(R.id.timepicker);
        wheelTime = new WheelTime(timepickerview, HOURS_MIN_SEC);

        //默认选中当前时间
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        wheelTime.setPicker(year, month, day, hours, minute, second);
    }

    public void setViewBackgroundResource(@ColorRes int colorRes){
        if (timepickerview!=null){
            timepickerview.setBackgroundResource(colorRes);
        }
    }

    /**
     * 设置可以选择的时间范围
     * 要在setTime之前调用才有效果
     *
     * @param startYear 开始年份
     * @param endYear   结束年份
     */
    public void setRange(int startYear, int endYear) {
        wheelTime.setStartYear(startYear);
        wheelTime.setEndYear(endYear);
    }

    /**
     * 设置选中时间
     *
     * @param date 时间
     */
    public void setTime(Date date) {
        Calendar calendar = Calendar.getInstance();
        if (date == null)
            calendar.setTimeInMillis(System.currentTimeMillis());
        else
            calendar.setTime(date);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);
        wheelTime.setPicker(year, month, day, hours, minute, second);
    }
   public WheelTime getWheelTime()
   {
       return wheelTime;
   }
    /**
     * 设置是否循环滚动
     *
     * @param cyclic 是否循环
     */
    public void setCyclic(boolean cyclic) {
        wheelTime.setCyclic(cyclic);
    }

    public void onClick(View v) {
        if (timeSelectListener != null) {
            try {
                Date date = WheelTime.dateFormat.parse(wheelTime.getTime());
                timeSelectListener.onTimeSelect(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return;
    }

    public interface OnTimeSelectListener {
        void onTimeSelect(Date date);

    }

    public void setOnTimeSelectListener(OnTimeSelectListener timeSelectListener) {
        this.timeSelectListener = timeSelectListener;
    }
}
