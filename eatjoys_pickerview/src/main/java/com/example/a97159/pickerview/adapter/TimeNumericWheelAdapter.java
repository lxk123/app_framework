package com.example.a97159.pickerview.adapter;

/**
 * Created by Daisy on 2017/12/4.
 */

public class TimeNumericWheelAdapter implements WheelAdapter {

    /** The default min value */
    public static final int DEFAULT_MAX_VALUE = 9;

    /** The default max value */
    private static final int DEFAULT_MIN_VALUE = 0;

    // Values
    private int minValue;
    private int maxValue;

    /**
     * Default constructor
     */
    public TimeNumericWheelAdapter() {
        this(DEFAULT_MIN_VALUE, DEFAULT_MAX_VALUE);
    }

    /**
     * Constructor
     * @param minValue the wheel min value
     * @param maxValue the wheel max value
     */
    public TimeNumericWheelAdapter(int minValue, int maxValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    @Override
    public Object getItem(int index) {
        if (index >= 0 && index < getItemsCount()) {
            int value = minValue + index;
            return NumFormat(value);
        }
        return "00";
    }

    @Override
    public int getItemsCount() {
        return maxValue - minValue + 1;
    }

    @Override
    public int indexOf(Object o){
        return Integer.parseInt(o.toString()) - minValue;
    }

    private String NumFormat(int num){
        if (num<10&&num>=0){
            return "0"+num;
        }else {
            return num+"";
        }
    }
}
