package com.example.a97159.pickerview.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.View;

import com.example.a97159.pickerview.R;
import com.example.a97159.pickerview.TimePickerView.Type;
import com.example.a97159.pickerview.adapter.TimeNumericWheelAdapter;
import com.example.a97159.pickerview.lib.WheelView;
import com.example.a97159.pickerview.listener.OnItemSelectedListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;


public class WheelTime {
	public static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private View view;
	private WheelView wv_year;
	private WheelView wv_month;
	private WheelView wv_day;
	private WheelView wv_hours;
	private WheelView wv_mins;
	private WheelView wv_secs;

	private Type type;
	public static final int DEFULT_START_YEAR = 1900;
	public static final int DEFULT_END_YEAR = 2050;
	private int startYear = DEFULT_START_YEAR;
	private int endYear = DEFULT_END_YEAR;
	private int textSize=0;
	private boolean[] isShowLabel={true,true,true,true,true,true};
	private String[] labelStrArray=new String[6];

	public WheelTime(View view) {
		super();
		this.view = view;
		type = Type.ALL;
		setView(view);
	}
	public WheelTime(View view, Type type) {
		super();
		this.view = view;
		this.type = type;
		initLabelStr();
		setView(view);
	}
	private void initLabelStr(){
		Context context = view.getContext();
		labelStrArray[0]=context.getString(R.string.picker_view_year);
		labelStrArray[1]=context.getString(R.string.picker_view_month);
		labelStrArray[2]=context.getString(R.string.picker_view_day);
		labelStrArray[3]=context.getString(R.string.picker_view_hours);
		labelStrArray[4]=context.getString(R.string.picker_view_minutes);
		labelStrArray[5]=context.getString(R.string.picker_view_seconds);
	}
	public void setPicker(int year ,int month,int day){
		this.setPicker(year, month, day, 0, 0,0);
	}
	
	public void setPicker(int year ,int month ,int day,int h,int m,int s) {
		// 添加大小月月份并将其转换为list,方便之后的判断
		String[] months_big = { "1", "3", "5", "7", "8", "10", "12" };
		String[] months_little = { "4", "6", "9", "11" };

		final List<String> list_big = Arrays.asList(months_big);
		final List<String> list_little = Arrays.asList(months_little);

		Context context = view.getContext();
		// 年
		wv_year = (WheelView) view.findViewById(R.id.year);
		wv_year.setAdapter(new TimeNumericWheelAdapter(startYear, endYear));// 设置"年"的显示数据
		wv_year.setLabel(isShowLabel[0]?labelStrArray[0]:"");// 添加文字
		wv_year.setCurrentItem(year - startYear);// 初始化时显示的数据

		// 月
		wv_month = (WheelView) view.findViewById(R.id.month);
		wv_month.setAdapter(new TimeNumericWheelAdapter(1, 12));
		wv_month.setLabel(isShowLabel[1]?labelStrArray[1]:"");// 添加文字
		wv_month.setCurrentItem(month);

		// 日
		wv_day = (WheelView) view.findViewById(R.id.day);
		// 判断大小月及是否闰年,用来确定"日"的数据
		if (list_big.contains(String.valueOf(month + 1))) {
			wv_day.setAdapter(new TimeNumericWheelAdapter(1, 31));
		} else if (list_little.contains(String.valueOf(month + 1))) {
			wv_day.setAdapter(new TimeNumericWheelAdapter(1, 30));
		} else {
			// 闰年
			if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0)
				wv_day.setAdapter(new TimeNumericWheelAdapter(1, 29));
			else
				wv_day.setAdapter(new TimeNumericWheelAdapter(1, 28));
		}
		wv_day.setLabel(isShowLabel[2]?labelStrArray[2]:"");// 添加文字
		wv_day.setCurrentItem(day - 1);


        wv_hours = (WheelView)view.findViewById(R.id.hour);
		wv_hours.setAdapter(new TimeNumericWheelAdapter(0, 23));
		wv_hours.setLabel(isShowLabel[3]?labelStrArray[3]:"");// 添加文字
		wv_hours.setCurrentItem(h);

		wv_mins = (WheelView)view.findViewById(R.id.min);
		wv_mins.setAdapter(new TimeNumericWheelAdapter(0, 59));
		wv_mins.setLabel(isShowLabel[4]?labelStrArray[4]:"");// 添加文字
		wv_mins.setCurrentItem(m);

		wv_secs = (WheelView)view.findViewById(R.id.sec);
		wv_secs.setAdapter(new TimeNumericWheelAdapter(0, 59));
		wv_secs.setLabel(isShowLabel[5]?labelStrArray[5]:"");// 添加文字
		wv_secs.setCurrentItem(s);

		// 添加"年"监听
		OnItemSelectedListener wheelListener_year = new OnItemSelectedListener() {
			@Override
			public void onItemSelected(int index) {
				int year_num = index + startYear;
				// 判断大小月及是否闰年,用来确定"日"的数据
				int maxItem = 30;
				if (list_big
						.contains(String.valueOf(wv_month.getCurrentItem() + 1))) {
					wv_day.setAdapter(new TimeNumericWheelAdapter(1, 31));
					maxItem = 31;
				} else if (list_little.contains(String.valueOf(wv_month
						.getCurrentItem() + 1))) {
					wv_day.setAdapter(new TimeNumericWheelAdapter(1, 30));
					maxItem = 30;
				} else {
					if ((year_num % 4 == 0 && year_num % 100 != 0)
							|| year_num % 400 == 0){
						wv_day.setAdapter(new TimeNumericWheelAdapter(1, 29));
						maxItem = 29;
					}
					else{
						wv_day.setAdapter(new TimeNumericWheelAdapter(1, 28));
						maxItem = 28;
					}
				}
				if (wv_day.getCurrentItem() > maxItem - 1){
					wv_day.setCurrentItem(maxItem - 1);
				}
			}
		};
		// 添加"月"监听
		OnItemSelectedListener wheelListener_month = new OnItemSelectedListener() {
			@Override
			public void onItemSelected(int index) {
				int month_num = index + 1;
				int maxItem = 30;
				// 判断大小月及是否闰年,用来确定"日"的数据
				if (list_big.contains(String.valueOf(month_num))) {
					wv_day.setAdapter(new TimeNumericWheelAdapter(1, 31));
					maxItem = 31;
				} else if (list_little.contains(String.valueOf(month_num))) {
					wv_day.setAdapter(new TimeNumericWheelAdapter(1, 30));
					maxItem = 30;
				} else {
					if (((wv_year.getCurrentItem() + startYear) % 4 == 0 && (wv_year
							.getCurrentItem() + startYear) % 100 != 0)
							|| (wv_year.getCurrentItem() + startYear) % 400 == 0){
						wv_day.setAdapter(new TimeNumericWheelAdapter(1, 29));
						maxItem = 29;
					}
					else{
						wv_day.setAdapter(new TimeNumericWheelAdapter(1, 28));
						maxItem = 28;
					}
				}
				if (wv_day.getCurrentItem() > maxItem - 1){
					wv_day.setCurrentItem(maxItem - 1);
				}

			}
		};
		wv_year.setOnItemSelectedListener(wheelListener_year);
		wv_month.setOnItemSelectedListener(wheelListener_month);

		// 根据屏幕密度来指定选择器字体的大小(不同屏幕可能不同)
		int textSize = 2;
		switch(type){
		case ALL:
			textSize = textSize * 5;
			break;
		case YEAR_MONTH_DAY:
			textSize = textSize * 12;
			wv_hours.setVisibility(View.GONE);
			wv_mins.setVisibility(View.GONE);
			wv_secs.setVisibility(View.GONE);
			break;
		case HOURS_MIN:
			textSize = textSize * 12;
			wv_year.setVisibility(View.GONE);
			wv_month.setVisibility(View.GONE);
			wv_day.setVisibility(View.GONE);
			wv_secs.setVisibility(View.GONE);
			break;
		case HOURS_MIN_SEC:
			textSize = textSize * 12;
			wv_year.setVisibility(View.GONE);
			wv_month.setVisibility(View.GONE);
			wv_day.setVisibility(View.GONE);
			break;
		case MIN_SEC:
				textSize = textSize * 12;
				wv_year.setVisibility(View.GONE);
				wv_month.setVisibility(View.GONE);
				wv_day.setVisibility(View.GONE);
				wv_hours.setVisibility(View.GONE);
			break;
		case MONTH_DAY_HOUR_MIN:
			textSize = textSize * 9;
			wv_year.setVisibility(View.GONE);
			wv_secs.setVisibility(View.GONE);
			break;
        case YEAR_MONTH:
            textSize = textSize * 12;
            wv_day.setVisibility(View.GONE);
            wv_hours.setVisibility(View.GONE);
            wv_mins.setVisibility(View.GONE);
			wv_secs.setVisibility(View.GONE);
			break;
			case YEAR_MONTH_DAY_HOURS_MIN:
			textSize = textSize * 6;
			wv_secs.setVisibility(View.GONE);
			break;
		}
		if (this.textSize==0){
			this.textSize=textSize;
		}
		wv_day.setTextSize(this.textSize);
		wv_month.setTextSize(this.textSize);
		wv_year.setTextSize(this.textSize);
		wv_hours.setTextSize(this.textSize);
		wv_mins.setTextSize(this.textSize);
		wv_secs.setTextSize(this.textSize);

	}

	public void setWvTextSize(int textSize){
		this.textSize=textSize;
		wv_day.setTextSize(textSize);
		wv_month.setTextSize(textSize);
		wv_year.setTextSize(textSize);
		wv_hours.setTextSize(textSize);
		wv_mins.setTextSize(textSize);
		wv_secs.setTextSize(textSize);

	}

	public void setWvLabelShow(@NonNull boolean[] isShowLabel){

		if (isShowLabel==null||isShowLabel.length!=6){
			return;
		}else {
			initLabelStr();
			this.isShowLabel=isShowLabel;
			wv_year.setLabel(isShowLabel[0]?labelStrArray[0]:"");
			wv_month.setLabel(isShowLabel[1]?labelStrArray[1]:"");
			wv_day.setLabel(isShowLabel[2]?labelStrArray[2]:"");
			wv_hours.setLabel(isShowLabel[3]?labelStrArray[3]:"");
			wv_mins.setLabel(isShowLabel[4]?labelStrArray[4]:"");
			wv_secs.setLabel(isShowLabel[5]?labelStrArray[5]:"");
		}
	}
	public void setWvLabelShow(@NonNull boolean[] isShowLabel,@NonNull String[] labelStrArray){
		if (isShowLabel==null||isShowLabel.length!=6||labelStrArray==null||labelStrArray.length!=6){
			return;
		}else {
			for (int i=0;i<labelStrArray.length;i++){
				if(labelStrArray[i]==null){
					labelStrArray[i]="";
				}
			}
			this.labelStrArray=labelStrArray;
			this.isShowLabel=isShowLabel;
			wv_year.setLabel(isShowLabel[0]?labelStrArray[0]:"");
			wv_month.setLabel(isShowLabel[1]?labelStrArray[1]:"");
			wv_day.setLabel(isShowLabel[2]?labelStrArray[2]:"");
			wv_hours.setLabel(isShowLabel[3]?labelStrArray[3]:"");
			wv_mins.setLabel(isShowLabel[4]?labelStrArray[4]:"");
			wv_secs.setLabel(isShowLabel[5]?labelStrArray[5]:"");
		}
	}

	/**
	 * 设置是否循环滚动
	 * @param cyclic
	 */
	public void setCyclic(boolean cyclic){
		wv_year.setCyclic(cyclic);
		wv_month.setCyclic(cyclic);
		wv_day.setCyclic(cyclic);
		wv_hours.setCyclic(cyclic);
		wv_mins.setCyclic(cyclic);
		wv_secs.setCyclic(cyclic);
	}
	public String getTime() {
		StringBuffer sb = new StringBuffer();
			sb.append((wv_year.getCurrentItem() + startYear)).append("-")
			.append((wv_month.getCurrentItem() + 1)).append("-")
			.append((wv_day.getCurrentItem() + 1)).append(" ")
			.append(wv_hours.getCurrentItem()).append(":")
			.append(wv_mins.getCurrentItem()).append(":")
			.append(wv_secs.getCurrentItem());
		return sb.toString();
	}
	public int getYear()
	{
		return (wv_year.getCurrentItem() + startYear);
	}
	public int getMonth()
	{
		return (wv_month.getCurrentItem() + 1);
	}
	public int getDay()
	{
		return (wv_day.getCurrentItem() + 1);
	}
	public int getHour()
	{
		return wv_hours.getCurrentItem();
	}
	public int getMins()
	{
		return wv_mins.getCurrentItem();
	}
	public int getSecs()
	{
		return wv_secs.getCurrentItem();
	}
	public String getShortTime() {
		StringBuffer sb = new StringBuffer();

		sb.append(wv_hours.getCurrentItem()).append(":")
				.append(wv_mins.getCurrentItem()).append(":")
				.append(wv_secs.getCurrentItem());
		return sb.toString();
	}
	public View getView() {
		return view;
	}

	public void setView(View view) {
		this.view = view;
	}

	public void setStartYear(int startYear) {
		this.startYear = startYear;
	}

	public void setEndYear(int endYear) {
		this.endYear = endYear;
	}
}
