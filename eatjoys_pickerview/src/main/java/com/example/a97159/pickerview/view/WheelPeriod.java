package com.example.a97159.pickerview.view;

import android.view.View;

import com.example.a97159.pickerview.R;
import com.example.a97159.pickerview.adapter.ArrayWheelAdapter;
import com.example.a97159.pickerview.lib.WheelView;

import java.util.ArrayList;

/**
 * Created by tiger on 2017/4/24.
 */

public class WheelPeriod <T> {
    private View view;
    private WheelView wv_option1;
    private WheelView wv_option2;
    private WheelView wv_option3;
    private WheelView wv_option4;

    private ArrayList<T> mOptions1Items;
    private ArrayList<T> mOptions2Items;
    private ArrayList<T> mOptions3Items;
    private ArrayList<T> mOptions4Items;

    public View getView() {
        return view;
    }

    public void setView(View view) {
        this.view = view;
    }

    public WheelPeriod(View view) {
        super();
        this.view = view;
        setView(view);
    }
    public void setPicker(ArrayList<T> options1Items,
                          ArrayList<T> options2Items,
                          ArrayList<T> options3Items,
                          ArrayList<T> options4Items) {
        this.mOptions1Items = options1Items;
        this.mOptions2Items = options2Items;
        this.mOptions3Items = options3Items;
        this.mOptions4Items = options4Items;
        int len = 3;
        if(this.mOptions4Items == null)
            len=4;
        if (this.mOptions3Items == null)
            len = 8;
        if (this.mOptions2Items == null)
            len = 12;
        // 选项1
        wv_option1 = (WheelView) view.findViewById(R.id.period1);
        wv_option1.setAdapter(new ArrayWheelAdapter(mOptions1Items, len));// 设置显示数据
        wv_option1.setCurrentItem(0);// 初始化时显示的数据
        // 选项2
        wv_option2 = (WheelView) view.findViewById(R.id.period2);
        if (mOptions2Items != null)
            wv_option2.setAdapter(new ArrayWheelAdapter(mOptions2Items,len));// 设置显示数据
        wv_option2.setCurrentItem(0);// 初始化时显示的数据
        // 选项3
        wv_option3 = (WheelView) view.findViewById(R.id.period3);
        if (mOptions3Items != null)
            wv_option3.setAdapter(new ArrayWheelAdapter(mOptions3Items,len));// 设置显示数据
        wv_option3.setCurrentItem(0);// 初始化时显示的数据
        // 选项4
        wv_option4 = (WheelView) view.findViewById(R.id.period4);
        if (mOptions4Items != null)
            wv_option4.setAdapter(new ArrayWheelAdapter(mOptions4Items,len));// 设置显示数据
        wv_option4.setCurrentItem(0);// 初始化时显示的数据
        int textSize = 16;

        wv_option1.setTextSize(textSize);
        wv_option2.setTextSize(textSize);
        wv_option3.setTextSize(textSize);
        wv_option4.setTextSize(textSize);

        if (this.mOptions2Items == null)
            wv_option2.setVisibility(View.GONE);
        if (this.mOptions3Items == null)
            wv_option3.setVisibility(View.GONE);
        if (this.mOptions4Items == null)
            wv_option4.setVisibility(View.GONE);

    }

    /**
     * 设置选项的单位
     * @param label1 单位
     * @param label2 单位
     * @param label3 单位
     */
    public void setLabels(String label1, String label2, String label3, String label4) {
        if (label1 != null)
            wv_option1.setLabel(label1);
        if (label2 != null)
            wv_option2.setLabel(label2);
        if (label3 != null)
            wv_option3.setLabel(label3);
        if (label4 != null)
            wv_option3.setLabel(label4);
    }

    /**
     * 分别设置第一二三级是否循环滚动
     * @param cyclic1,cyclic2,cyclic3 是否循环
     */
    public void setCyclic(boolean cyclic1,boolean cyclic2,boolean cyclic3,boolean cyclic4) {
        wv_option1.setCyclic(cyclic1);
        wv_option2.setCyclic(cyclic2);
        wv_option3.setCyclic(cyclic3);
        wv_option4.setCyclic(cyclic4);
    }
    /**
     * 返回当前选中的结果对应的位置数组 因为支持三级联动效果，分三个级别索引，0，1，2
     * @return 索引数组
     */
    public int[] getCurrentItems() {
        int[] currentItems = new int[4];
        currentItems[0] = wv_option1.getCurrentItem();
        currentItems[1] = wv_option2.getCurrentItem();
        currentItems[2] = wv_option3.getCurrentItem();
        currentItems[3] = wv_option4.getCurrentItem();
        return currentItems;
    }
    public void setCurrentItems(int option1, int option2, int option3, int option4) {
        wv_option1.setCurrentItem(option1);
        wv_option2.setCurrentItem(option2);
        wv_option3.setCurrentItem(option3);
        wv_option4.setCurrentItem(option4);
    }

}
