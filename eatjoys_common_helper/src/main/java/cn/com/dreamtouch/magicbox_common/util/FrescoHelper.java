package cn.com.dreamtouch.magicbox_common.util;

import android.content.Context;
import android.net.Uri;

import com.facebook.drawee.view.SimpleDraweeView;

/**
 * Created by howard on 2016/12/6.
 */

public class FrescoHelper {
    /**
     * 加载本地图片（drawable图片）
     *
     * @param context
     * @param simpleDraweeView
     * @param id
     */
    public static void loadResPic(Context context, SimpleDraweeView simpleDraweeView, int id) {
        if(context == null || simpleDraweeView == null){
            return;
        }
        Uri uri = Uri.parse("res://" +
                context.getPackageName() +
                "/" + id);
        simpleDraweeView.setImageURI(uri);
    }
}

