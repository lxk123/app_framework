package cn.com.dreamtouch.magicbox_common.helper;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.provider.Settings;

/**
 * Created by MuHan on 17/2/20.
 */

public class NetWorkHelper {
    public static boolean checkNetworkState(Context context) {
        boolean flag = false;
        //得到网络连接信息
        ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        //去进行判断网络是否连接
        if (manager.getActiveNetworkInfo() != null) {
            flag = manager.getActiveNetworkInfo().isAvailable();
        }

        return flag;
    }

    /**
     * 获取当前wifi的ssid
     * @param context
     * @return
     */
    public static String getCurrentWIFISSID(Context context)
    {
        WifiManager  mWifiManager = (WifiManager) context.getSystemService (Context.WIFI_SERVICE);
        if(mWifiManager.isWifiEnabled())
        {
            String mConnectedSsid;
            WifiInfo WifiInfo = mWifiManager.getConnectionInfo();
            mConnectedSsid = WifiInfo.getSSID();
            int iLen = mConnectedSsid.length();

            if (iLen == 0)
            {
                return null;
            }

            if (mConnectedSsid.startsWith("\"") && mConnectedSsid.endsWith("\""))
            {
                mConnectedSsid = mConnectedSsid.substring(1, iLen - 1);
            }
            //		mConnectedSsid = mConnectedSsid.replace('\"', ' ');
            //		mConnectedSsid = mConnectedSsid.trim();
            if(mConnectedSsid!=null)
             return mConnectedSsid;
        }
        return null;
    }
    public static void startWifiSetting(Context context)
    {
        Intent intent =  new Intent(Settings.ACTION_WIFI_SETTINGS);
        context.startActivity(intent);
    }
    public static   String getWIFILocalIpAdress(Context mContext) {

        //获取wifi服务
        WifiManager wifiManager = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
        //判断wifi是否开启
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int ipAddress = wifiInfo.getIpAddress();
        String ip = formatIpAddress(ipAddress);
        return ip;
    }

    /**
     * 获取子网掩码
     * @param mContext
     * @return
     */
    public static   String getUdpIp(Context mContext) {

        //获取wifi服务
        WifiManager wifiManager = (WifiManager)mContext.getSystemService(Context.WIFI_SERVICE);
        //判断wifi是否开启
        if (!wifiManager.isWifiEnabled()) {
            wifiManager.setWifiEnabled(true);
        }
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
        return formatBroadcastAddress(wifiInfo.getIpAddress(),dhcpInfo.netmask);
    }
    private static String formatBroadcastAddress(int ipAddress, int netMask) {

        int broadcastAddress= (ipAddress&netMask)|(~netMask);
        return (broadcastAddress & 0xFF ) + "." +
                ((broadcastAddress >> 8 ) & 0xFF) + "." +
                ((broadcastAddress >> 16 ) & 0xFF) + "." +
                ( broadcastAddress >> 24 & 0xFF) ;
    }
    private static String formatIpAddress(int ipAddress) {

        return (ipAddress & 0xFF ) + "." +
                ((ipAddress >> 8 ) & 0xFF) + "." +
                ((ipAddress >> 16 ) & 0xFF) + "." +
                ( ipAddress >> 24 & 0xFF) ;
    }
}
