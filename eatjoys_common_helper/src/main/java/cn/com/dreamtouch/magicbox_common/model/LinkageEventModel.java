package cn.com.dreamtouch.magicbox_common.model;

/**
 * Created by MuHan on 2017/9/5.
 */

public class LinkageEventModel {
    public String key;
    public String value;
    public LinkageEventModel(String key,String value)
    {
        this.key=key;
        this.value=value;
    }
}
