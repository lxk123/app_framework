package cn.com.dreamtouch.magicbox_common.helper;

import com.tbruyelle.rxpermissions2.RxPermissions;

/**
 * Created by MuHan on 17/3/8.
 */

public class CharsetConverter {
    /**
     * 解码
     * @param source
     * @return
     */
    public static String decodeUtf8(String source)
    {

        return source;//现不需要编码解码
//        try {
//         return URLDecoder.decode(source,"UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//            return "";
//        }
    }

    /**
     * 编码
     * @param source
     * @return
     */
    public static String encodeUTF8(String source)
    {
       return source;//现不需要编码解码
//        try {
//            return URLEncoder.encode(source,"UTF-8");
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//            return "";
//        }

    }

}
