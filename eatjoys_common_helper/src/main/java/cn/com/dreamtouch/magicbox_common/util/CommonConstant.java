package cn.com.dreamtouch.magicbox_common.util;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;

/**
 * Created by MuHan on 17/2/9.
 */

public class CommonConstant {

    public static class API {
        public static String url = "http://boss.eatjoys.com/shiqu-weixin/api/";

        public static String getServerHost(Context context) {
            ApplicationInfo appInfo = null;
            try {
                appInfo = context.getPackageManager()
                        .getApplicationInfo(context.getPackageName(),
                                PackageManager.GET_META_DATA);
                String serverhost = appInfo.metaData.getString("com.shiqupad.order.serverhost");

                return serverhost;

            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            return "";
        }


    }

    public static class SPKey {
        public static final String ROLE_TYPE = "roleType";
        public static final String SID = "SID";
        public static final String SHOPID = "SHOPID";
        public static final String EMPLOYID = "EMPLOYID";
        public static final String SHOP_TYPE = "SHOP_TYPE";


    }

    public static class Args {
        public static final String WIFI_SSID = "wifiSsid";


        public static final String ROOM_ID = "ROOM_ID";
        public static final String MESSAGE_TYPE = "MESSAGE_TYPE";
        public static final String TABLE_ID = "TABLE_ID";
        public static final String TABLE_NAME = "TABLE_NAME";

        public static final String DINNER_NUM = "DINNER_NUM";
        public static final String ORDER_ID = "ORDER_ID";
        public static final String MUST_DISHS = "MUST_DISHS";


        /*----------------------------------*/
        public static final String NEW_NOTIFY = "NEW_NOTIFY";
        public static final String CONFIRM_NOTIFY = "CONFIRM_NOTIFY";

        /*------------------------------- EDIT ------------------------------*/
        public static final String EDIT_PREF_NAME                      = "com.qihui.gojob.prefs";

        /*---------------------------- PREFERENCE ---------------------------*/
        public static final String KEY_PREF_LOGIN_STATUS    = "is_login";
        public static final String KEY_PREF_NOTIFY_NUM    = "notifyNum";

    }

    public static class RoleType {
        public static final String SELF = "SELF";
    }

    public static class ImageExtension {
        public static final String JPG = ".jpg";
    }

    /**
     * ActivityRequestCode
     */
    public static class RequestCode {
        public static final int LOGIN = 102;//登录
        public static final int REGISETR = 103;//注册
        public static final int FORGETPASSWORD = 104;//忘记密码

        /*-------------------------- REQUEST CODE --------------------------*/
        public static final String REQ_KEY              = "requestCode";
        public static final String REQ_ORDER            = "request_order";
        public static final String REQ_CLEAR            = "request_clear";
        public static final String REQ_PAY              = "request_pay";
        public static final String REQ_SCAN             = "request_scan";
        public static final String REQ_SEARCH             = "request_SEARCH";

    }

    /*桌台状态信息*/
    /* public static final int DISABLE = 0;禁用
            public static final int FREE = 1;闲置
            public static final int OCCUPY = 2;占用
            public static final int RESERVE = 3;预定
            public static final int INVOICING = 4;清台*/


    public static class TableStatus {
        public static final int DISABLE = 0;
        public static final int FREE = 1;
        public static final int OCCUPY = 2;
        public static final int RESERVE = 3;
        public static final int INVOICING = 4;
        public static final int ALL = -10;

        public static final int TO_BE_CONFIRMED = 9;

    }

    /* 待确认，
    已确认，
    已支付，
    已取消，
    已失效，
    待支付，
    支付失败，
    部分退单，
    待确认*/
    public static class TableOrderStatus {
        public static final int TO_BE_CONFIRMED = 1;
        public static final int HAVE_BEEN_CONFIRMED = 2;
        public static final int HAVE_PAY = 3;
        public static final int HAVE_CANCEL = 4;
        public static final int HAVE_LOST_TIME = 5;
        public static final int TO_BE_PAY = 6;
        public static final int PAY_FAILURE = 7;
        public static final int LIST_SOME_RETURENED = 8;
        public static final int WAIT_CONFIRM_ED = 9;

    }

    public static class OpertionType {
        public static final int CHANGE_TABLE = 1000;//转台
        public static final int BACK_TO_RESUME = 1001;//从后台进入刷新数据
        public static final int CLEAN_TABLE = 1002;//清台
        public static final int UPDATE_DINNER_PEOPLE = 1003;//修改人数
        public static final int CANCEL_ORDER = 1004;//销单
        public static final int QRCODE_TABLE_SUCCESS = 1005;//销单


        public static final int TO_BE_CONFIRMED =1006 ;//确认订单
        public static final int CALL_SUCCESS = 1007;//叫号成功
        public static final int  WAIT_TO_BE_CONFIRMED = 1008;//有新的订单下发
        public static final int  POSTORDER_SUCCESS = 1009;//下单成功
        public static final int  OPEN_TABLE_SUCCESS = 1010;//开台成功
        public static final int PUSH_MESSAGE =1011 ;//有新的消息
        public static final int HAS_READ_MESSAGE = 1012;//消息已读

        public static final int SWITCH_TABLE = 1013;//切换tab
        public static final int PAY_SUCCESS = 1014;
        public static final int WAIT_CONFIRM_ORDER = 1018;//服务员确认订单
    }
    public  static  class PayMethod{
       public final static String PAY_TYPE_01 = "CASHPAY";//现金
       public final static String PAY_TYPE_02 = "ALIPAY";//支付宝
       public final static String PAY_TYPE_03 = "WXPAY";//微信
       public final static String PAY_TYPE_04 = "ALIMEMBERPAY";//支付宝会员卡支付
       public final static String PAY_TYPE_05 = "WXMEMBERPAY";//微信会员卡支付
       public final static String PAY_TYPE_06 = "OTHERPAY";//自定义支付
    }

}