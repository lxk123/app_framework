package cn.com.dreamtouch.magicbox_common.util;

import android.os.CountDownTimer;
import android.text.TextUtils;
import android.widget.Button;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import cn.com.dreamtouch.magicbox_common.R;

/**
 * Created by Ping on 2016/11/11.
 */

public class TimeUtil {
    public static final SimpleDateFormat mMMDddFormat = new SimpleDateFormat("MM月dd日", Locale.getDefault());
    public static final SimpleDateFormat myyyyMMddFormat = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
    public static final SimpleDateFormat myyyy_MM_ddFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    public static final SimpleDateFormat myyyyMMddSlashFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
    public static final SimpleDateFormat mMMddhhmmFormat = new SimpleDateFormat("MM-dd HH:mm", Locale.getDefault());
    public static final SimpleDateFormat mMMddhhmmssFormat = new SimpleDateFormat("MM-dd HH:mm:ss", Locale.getDefault());
    public static final SimpleDateFormat myyyyMMddhhmmssFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    public static final SimpleDateFormat mhhmmssFormat=new SimpleDateFormat("HH:mm:ss",Locale.getDefault());
    public static final SimpleDateFormat myyyyMMddPointFormat = new SimpleDateFormat("yyyy.MM.dd", Locale.getDefault());
    public static final SimpleDateFormat myyyyMMddhhmmSlashFormat = new SimpleDateFormat("yyyy/MM/dd  HH:mm", Locale.getDefault());
    public static final SimpleDateFormat myyyyMMddhhmmssSlashFormat = new SimpleDateFormat("yyyy/MM/dd  HH:mm:ss", Locale.getDefault());
    public static final SimpleDateFormat mhhmmFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    public static final SimpleDateFormat mhhmmssChinesFormat=new SimpleDateFormat("HH小时mm分ss秒",Locale.getDefault());
    public static final SimpleDateFormat mmmssChinesFormat=new SimpleDateFormat("mm分ss秒",Locale.getDefault());
    public static final SimpleDateFormat mssChinesFormat=new SimpleDateFormat("ss秒",Locale.getDefault());


    public TimeUtil() {
    }

    public static int compareDate(String LastDate, String NewDate) {
        if(LastDate.length() == 14) {
            LastDate = LastDate.substring(0, 8);
        }

        if(NewDate.length() == 14) {
            NewDate = NewDate.substring(0, 8);
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd", Locale.getDefault());
        Date date1 = new Date();

        try {
            date1 = simpleDateFormat.parse(LastDate);
        } catch (ParseException var11) {
            var11.printStackTrace();
        }

        Date date2 = new Date();

        try {
            date2 = simpleDateFormat.parse(NewDate);
        } catch (ParseException var10) {
            var10.printStackTrace();
        }

        GregorianCalendar cal1 = new GregorianCalendar();
        GregorianCalendar cal2 = new GregorianCalendar();
        cal1.setTime(date1);
        cal2.setTime(date2);
        double dayCount = (double)((cal2.getTimeInMillis() - cal1.getTimeInMillis()) / 86400000L);
        int dayNumber = (int)dayCount;
        return dayNumber;
    }

    public static int compareTimeStamp(long LastDate, long NewDate) {
        double dayCount = (double)((NewDate - LastDate) / 86400000L);
        return (int)dayCount;
    }

    public static long convertDateToTimeStamp(Date date) {
        return date.getTime() / 1000L;
    }

    public static Date convertTimeStampToDate(long timeStamp) {
        return new Date(timeStamp * 1000L);
    }

    public static String convertDateToString(Date date, SimpleDateFormat simpleDateFormat) {
        return simpleDateFormat.format(date);
    }

    public static Date convertStringToDate(String dateStr, SimpleDateFormat simpleDateFormat) {
        return simpleDateFormat.parse(dateStr, new ParsePosition(0));
    }

    public static String convertTimeStampToString(long timeStamp, SimpleDateFormat simpleDateFormat) {
        return simpleDateFormat.format(convertTimeStampToDate(timeStamp));
    }

    public static Date getShortDateInstance(int year, int month, int day) {
        new Date();
        GregorianCalendar gc = new GregorianCalendar();
        gc.set(1, year);
        gc.set(2, month - 1);
        gc.set(5, day);
        gc.set(11, 0);
        gc.set(12, 0);
        gc.set(13, 0);
        Date date = gc.getTime();
        return date;
    }

    public static Date getLongDateInstance(int year, int month, int day, int hour, int minute) {
        new Date();
        GregorianCalendar gc = new GregorianCalendar();
        gc.set(1, year);
        gc.set(2, month - 1);
        gc.set(5, day);
        gc.set(11, hour);
        gc.set(12, minute);
        gc.set(13, 0);
        Date date = gc.getTime();
        return date;
    }

    public static Date getNowShortDateInstance() {
        Calendar calendar = Calendar.getInstance();
        Date date = (new GregorianCalendar(calendar.get(1), calendar.get(2), calendar.get(5))).getTime();
        return date;
    }

    public static long getNowDateStamp() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime().getTime();
    }

    public static int convertTimeStampToYears(long seconds) {
        int year = compareTimeStamp(seconds * 1000L, convertDateToTimeStamp(new Date()) * 1000L) / 365 + 1;
        return year < 1?1:year;
    }

    public static int getTodayNightTimeStamp() {
        Calendar cal = Calendar.getInstance();
        cal.set(11, 24);
        cal.set(13, 0);
        cal.set(12, 0);
        cal.set(14, 0);
        return (int)(cal.getTimeInMillis() / 1000L);
    }

    public class TimerCount extends CountDownTimer {
        private Button btn;

        public TimerCount(long millisInFuture, long countDownInterval, Button btn) {
            super(millisInFuture, countDownInterval);
            this.btn = btn;
        }

        public void onTick(long l) {
            this.btn.setClickable(false);
            char arg0 = '\uea60';
            this.btn.setText("重新获取" + arg0 / 1000 + "秒");
        }

        public void onFinish() {
            this.btn.setClickable(true);
            this.btn.setText("获取验证码");
        }
    }
}
