package cn.com.dreamtouch.magicbox_common.util;

import java.util.regex.Pattern;

/**
 * Created by tiger on 2017/4/5.
 */

public class TextHelper {
    /**
     * 获取等号之后文字
     *
     * @param text
     */
    public static String getTextAfterEqual(String text) {
        return text.substring(text.indexOf("=")+1);
    }
    public static boolean IsCombineByCh(String text)
    {
        String all = "^[0-9\\u4e00-\\u9fa5]+$";
        Pattern pattern = Pattern.compile(all);
        return pattern.matches(all,text);
    }
    /*
    匹配中英文和数字
     */
    public static boolean IsCombineByChEh(String text)
    {
        String all = "^[A-Za-z0-9\u4e00-\u9fa5]+$";
        Pattern pattern = Pattern.compile(all);
        return pattern.matches(all,text);
    }
}
