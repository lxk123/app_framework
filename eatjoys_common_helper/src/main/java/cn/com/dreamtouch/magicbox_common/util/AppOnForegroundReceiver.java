package cn.com.dreamtouch.magicbox_common.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import cn.com.dreamtouch.common.DTLog;

/**
 * Stomp连接状态监听
 * Created by MuHan on 17/2/20.
 */

public class AppOnForegroundReceiver extends BroadcastReceiver {
    private StateListener connectionStateListener;
    final static String action="cn.com.dreamtouch.AppOnForeground";
    public  void register(Context context, StateListener connectionStateListener)
    {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(action);
        context.registerReceiver(this, intentFilter);
        this.connectionStateListener=connectionStateListener;
    }
    public void unregister ( Context context)
    {
        context.unregisterReceiver(this);
        connectionStateListener=null;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        if(intent.getAction().equals(action)) {
            boolean connected = intent.getBooleanExtra("isForeground",false);
            if(connectionStateListener!=null)
            {
                connectionStateListener.onAppChange(connected);

            }
            else
            {
                DTLog.e("AppOnForegroundReceiver","connectionStateListener is null");
            }

        }

    }
    public static void sendBroadcast(Context context, boolean isForeground)
    {

        Intent intent = new Intent(action);
        intent.putExtra("isForeground",isForeground);
        context.sendBroadcast(intent);
    }
    public  interface  StateListener
    {
        public void onAppChange(boolean isForeground);
    }

}
