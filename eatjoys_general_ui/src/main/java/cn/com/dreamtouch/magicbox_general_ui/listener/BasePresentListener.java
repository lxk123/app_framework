package cn.com.dreamtouch.magicbox_general_ui.listener;

/**
 * Created by MuHan on 16/7/21.
 */
public interface BasePresentListener {
    public void showLoadingProgress();
    public void hideLoadingProgress();
    void basicFailure(String prompt);
}
