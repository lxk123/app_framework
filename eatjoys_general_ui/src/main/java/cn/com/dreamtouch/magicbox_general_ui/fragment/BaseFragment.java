package cn.com.dreamtouch.magicbox_general_ui.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by LuoXueKun
 * on 2018/5/10
 */
public abstract class BaseFragment extends Fragment {
    public BaseFragment() {
    }

    protected abstract void initVariables();

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.initVariables();
    }

    protected abstract View initView(LayoutInflater var1, ViewGroup var2, Bundle var3);

    protected abstract void loadData();

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = this.initView(inflater, container, savedInstanceState);
        this.loadData();
        return view;
    }
}
