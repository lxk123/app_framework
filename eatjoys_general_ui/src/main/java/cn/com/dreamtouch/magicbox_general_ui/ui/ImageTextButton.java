package cn.com.dreamtouch.magicbox_general_ui.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

import java.net.ConnectException;

import cn.com.dreamtouch.magicbox_common.helper.DisplayHelper;
import cn.com.dreamtouch.magicbox_general_ui.R;

/**
 * Created by LuoXueKun
 * on 2018/5/15
 */
public class ImageTextButton extends TextView {

    private Drawable drawableLeft;
    private int scaleWidth; //dp值
    private int scaleHeight;
    private Context mContext;

    public ImageTextButton(Context context) {
        super(context);
    }

    public ImageTextButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context,attrs);
    }

    public ImageTextButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    public void init(Context context, AttributeSet attrs) {
        mContext = context;
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.ImageTextButton);

        drawableLeft = typedArray.getDrawable(R.styleable.ImageTextButton_leftDrawable);
        scaleWidth = typedArray.getDimensionPixelOffset(R.styleable
                .ImageTextButton_drawableWidth, DisplayHelper.dip2px(context,20));
        scaleHeight = typedArray.getDimensionPixelOffset(R.styleable
                .ImageTextButton_drawableHeight,DisplayHelper.dip2px(context,20));
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (drawableLeft != null) {
            drawableLeft.setBounds(0, 0,DisplayHelper.dip2px(mContext,20), DisplayHelper.dip2px(mContext,20));
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.setCompoundDrawables(drawableLeft, null, null, null);
    }

    /**
     * 设置左侧图片并重绘
     * @param drawableLeft
     */
    public void setDrawableLeft(Drawable drawableLeft) {
        this.drawableLeft = drawableLeft;
        invalidate();
    }

    /**
     * 设置左侧图片并重绘
     * @param drawableLeftRes
     */
    public void setDrawableLeft(int drawableLeftRes) {
        this.drawableLeft = DisplayHelper.getContext().getResources().getDrawable(drawableLeftRes);
        invalidate();
    }
}
