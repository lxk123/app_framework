package cn.com.dreamtouch.magicbox_general_ui.ui;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

import cn.com.dreamtouch.magicbox_general_ui.R;

/**
 * Created by MuHan on 2017/10/11.
 */

public class LoadingProgressDialog extends ProgressDialog {
    public LoadingProgressDialog(Context context) {
        super(context);
    }

    public LoadingProgressDialog(Context context, int theme) {
        super(context, theme);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_loading);
        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        getWindow().setAttributes(params);
    }

    ValueAnimator animator;
    /**
     * 当窗口焦点改变时调用
     */
    public void onWindowFocusChanged(boolean hasFocus) {
        final ImageView loadingImage=(ImageView)findViewById(R.id.image);
        final FrameLayout frame=(FrameLayout)findViewById(R.id.frame);
        final Bitmap mRegularBitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.ic_loading);
        FrameLayout.LayoutParams linearParams =(FrameLayout.LayoutParams) frame.getLayoutParams();

        linearParams.width = mRegularBitmap.getWidth();
        linearParams.height = mRegularBitmap.getHeight();
        frame.setLayoutParams(linearParams);

        ValueAnimator animator = ValueAnimator.ofInt(0, mRegularBitmap.getHeight());
        animator.setTarget(loadingImage);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener()
        {
            @Override
            public void onAnimationUpdate(ValueAnimator animation)
            {
                FrameLayout.LayoutParams frameParams =(FrameLayout.LayoutParams) loadingImage.getLayoutParams();

                frameParams.width = mRegularBitmap.getWidth();
                frameParams.height = (int)animation.getAnimatedValue();
                loadingImage.setLayoutParams(frameParams);
            }
        });
        animator.setRepeatCount(ValueAnimator.INFINITE);
        animator.setDuration(4000).start();
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if(animator!=null)
            animator.end();
    }

    /**
     * 弹出自定义ProgressDialog
     *
     * @param context
     *            上下文
     * @param cancelable
     *            是否按返回键取消
     * @param cancelListener
     *            按下返回键监听
     * @return
     */
    public static LoadingProgressDialog show(Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        LoadingProgressDialog dialog = new LoadingProgressDialog(context, R.style.LoadingProgress);
        dialog.setTitle("");


        // 按返回键是否取消
        dialog.setCancelable(cancelable);
        // 监听返回键处理
        if(cancelListener!=null)
        dialog.setOnCancelListener(cancelListener);
        // 设置居中
        dialog.getWindow().getAttributes().gravity = Gravity.CENTER;
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();

        dialog.getWindow().setAttributes(lp);
        dialog.show();

        return dialog;
    }
}
