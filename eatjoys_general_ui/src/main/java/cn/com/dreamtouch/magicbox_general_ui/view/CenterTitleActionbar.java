package cn.com.dreamtouch.magicbox_general_ui.view;

import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.TextView;

import cn.com.dreamtouch.magicbox_general_ui.R;


/**
 * Created by MuHan on 16/3/7.
 */
public class CenterTitleActionbar extends AppBarLayout {
    Toolbar toolbar;
    TextView tv_title;
    AppBarLayout appBarLayout;
    public CenterTitleActionbar(Context context) {
        super(context);
    }

    public CenterTitleActionbar(Context context, AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.view_center_title_actionbar, this, true);
        toolbar=(Toolbar)findViewById(R.id.toolbar_actionbar);
        tv_title=(TextView)findViewById(R.id.tv_title_actionbar);
        appBarLayout=(AppBarLayout)findViewById(R.id.appBarLayout);
        if (isInEditMode()) { return; }
    }
    public Toolbar getToolbar()
    {
        return toolbar;
    }
    public AppBarLayout getAppBarLayout()
    {
        return appBarLayout;
    }
    public void setTitle(String title)
    {
        tv_title.setText(title);
    }
    public TextView getTvTitle()
    {
        return tv_title;
    }
}
