package cn.com.dreamtouch.magicbox_general_ui.fragment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Objects;

import cn.com.dreamtouch.magicbox_general_ui.R;
import cn.com.dreamtouch.magicbox_general_ui.activity.MBBaseActivity;
import cn.com.dreamtouch.magicbox_general_ui.util.MyLogger;


public abstract class MBBaseFragment extends BaseFragment {
    protected MyLogger mLogger;

    @Override
    protected void initVariables() {
        mLogger = MyLogger.kLog();
    }

    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = this.initView(inflater, container, savedInstanceState);

        this.loadData();
        return view;
    }


    /**
     * 视图销毁的时候讲Fragment是否初始化的状态变为false
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();


    }


    @Override
    protected void loadData() {

    }

    public MBBaseActivity getBaseActivity() {
        return (MBBaseActivity) getActivity();
    }

    public void showLoadingProgress()
    {
        if(getBaseActivity() == null){
            return;
        }
        getBaseActivity().showLoadingProgress();
    }

    public void hideLoadingProgress() {
        if(getActivity() == null) {
            return;
        }
        getBaseActivity().hideLoadingProgress();
    }
    public void goToAtivity(Class clazz, Bundle bundle) {
        Intent it = new Intent(getActivity(), clazz);
        if (bundle != null) {
            it.putExtra("bundle", bundle);
        }
        startActivity(it);
        Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.push_left_in, R.anim.push_left_out);
    }
    public void jump2ActivityForResult(Class<? extends Activity> activity, int requestCode, String... extras) {
        getBaseActivity().jump2ActivityForResult(activity, requestCode, extras);
    }
}
