package cn.com.dreamtouch.magicbox_general_ui.presenter;


import rx.subscriptions.CompositeSubscription;

/**
 * Created by MuHan on 16/7/21.
 */
public class BasePresenter {

    protected CompositeSubscription mSubscriptions;//用于取消订阅
    public BasePresenter() {
        mSubscriptions = new CompositeSubscription();
    }
    /*
    取消所有订阅
     */
    public void unSubscribe() {
        if(mSubscriptions!=null)
            mSubscriptions.clear();
    }

}
