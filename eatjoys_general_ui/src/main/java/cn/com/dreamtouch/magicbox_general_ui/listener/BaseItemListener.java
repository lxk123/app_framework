package cn.com.dreamtouch.magicbox_general_ui.listener;

/**
 * Created by MuHan on 17/3/8.
 */

public interface BaseItemListener<T> {
    public void itemClick(T item);

}
