package com.shiqupad.order.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.shiqupad.order.R;
import com.shiqupad.order.util.DisplayIconUtil;

import java.util.ArrayList;
import java.util.List;

import cn.com.dreamtouch.magicbox_general_ui.util.MyLogger;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllPayMethodResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoResponse;


/**
 * Created by LuoXueKun on 2018/4/1.
 */
public class SelectCouponsAdapter extends RecyclerView.Adapter<SelectCouponsAdapter.BaseViewHolder> {
    private ArrayList<GetCustomOrderInfoResponse.DataBean.VoucherVOListBean> dataList = new ArrayList<>();
    private int lastPressIndex = 0;
    private Context mContext;
    OneViewHolder viewHolder;
    public SelectCouponsAdapter(Context context){
        mContext = context;
    }
    public void replaceAll(List<GetCustomOrderInfoResponse.DataBean.VoucherVOListBean> list) {
        dataList.clear();
        if (list != null && list.size() > 0) {
            dataList.addAll(list);
        }
        notifyDataSetChanged();
    }

    @Override
    public SelectCouponsAdapter.BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_couponse, parent, false);
        viewHolder = new OneViewHolder(view);

        return viewHolder;

    }


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        holder.setData(dataList);

    }

    @Override
    public int getItemCount() {
        return dataList != null ? dataList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }




    public class BaseViewHolder extends RecyclerView.ViewHolder {

        private BaseViewHolder(View itemView) {
            super(itemView);
        }

        void setData(Object data) {


        }
    }

    public class OneViewHolder extends BaseViewHolder {
        private RelativeLayout relativeLayout;
        private TextView tvName;
        private ImageView ivChoose;

        private OneViewHolder(View view) {
            super(view);
            relativeLayout = view.findViewById(R.id.item_couponse_rl);

            tvName = view.findViewById(R.id.item_couponse_name_tv);
            ivChoose = view.findViewById(R.id.item_choose_iv);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.e("TAG", "OneViewHolder: ");
                    int position = getAdapterPosition();
                    if (lastPressIndex == position) {
                        lastPressIndex = -1;
                    } else {
                        lastPressIndex = position;
                    }
                    notifyDataSetChanged();
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(position);
                    }

                }

            });

        }

        @Override
        void setData(Object data) {
            if (data != null) {
                if (getAdapterPosition() == lastPressIndex) {
                    relativeLayout.setSelected(true);
                    ivChoose.setVisibility(View.VISIBLE);
                    itemView.setEnabled(false);
                } else {
                    relativeLayout.setSelected(false);
                    ivChoose.setVisibility(View.INVISIBLE);
                    itemView.setEnabled(true);
                }


            }
            tvName.setText(dataList.get(getAdapterPosition()).getVoucherName());
        }
    }

    private onItemClickListener onItemClickListener;

    public interface onItemClickListener {
        void onItemClick(int pos);
    }

    public void setOnItemClickListener(onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
