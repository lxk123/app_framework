package com.shiqupad.order.adapter;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Collection;

import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_general_ui.adapter.BaseRecyclerAdapter;
import cn.com.dreamtouch.magicbox_general_ui.adapter.SmartViewHolder;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import com.shiqupad.order.R;
import com.shiqupad.order.util.MathUtil;

/**
 * Created by LuoXueKun on 2018/5/16.
 */

public class GetAllTableAdapter extends BaseRecyclerAdapter<GetAllTableResponse.Data.ListBean> {
    public GetAllTableAdapter(Context context, Collection<GetAllTableResponse.Data.ListBean> collection,
                              int layoutId, AdapterView.OnItemClickListener listener) {
        super(collection, layoutId, listener);
    }


    @Override
    protected void onBindViewHolder(SmartViewHolder holder, GetAllTableResponse.Data.ListBean data, int position) {
        int status = data.getStatus();
        RelativeLayout rlBgTable = holder.itemView.findViewById(R.id.item_table_rl);
        RelativeLayout rlBgFinish = holder.itemView.findViewById(R.id.item_table_status);
        TextView tvMoney = holder.itemView.findViewById(R.id.item_table_orderMoney_tv);
        switch (status) {
            case CommonConstant.TableStatus.DISABLE:
                rlBgFinish.setVisibility(View.GONE);
                rlBgTable.setBackgroundResource(R.drawable.bg_table_disable_define);
                holder.text(R.id.item_table_peoplePercent_tv, "0/" + data.getPropleNum());
                tvMoney.setVisibility(View.GONE);
                break;
            case CommonConstant.TableStatus.FREE:
                rlBgFinish.setVisibility(View.GONE);
                tvMoney.setVisibility(View.VISIBLE);
                tvMoney.setText("空闲中");
                rlBgTable.setBackgroundResource(R.drawable.bg_table_free_define);
                holder.text(R.id.item_table_peoplePercent_tv, "0/" + data.getPropleNum());

                break;
            case CommonConstant.TableStatus.OCCUPY:
                double money = data.getOrderMoney();
                rlBgFinish.setVisibility(View.GONE);
                tvMoney.setVisibility(View.VISIBLE);
                if (money == 0.0) {
                    tvMoney.setText("点餐中");
                } else {
                    double orderMoney = data.getOrderMoney();
                    double tableFee = data.getTableFee();
                    tvMoney.setText(MathUtil.saveTwoDigit2(orderMoney+tableFee));

                }
                rlBgTable.setBackgroundResource(R.drawable.bg_table_order_define);
                holder.text(R.id.item_table_peoplePercent_tv, data.getDinningPeople() + "/" + data.getPropleNum());
                break;
            case CommonConstant.TableStatus.RESERVE:
                rlBgFinish.setVisibility(View.VISIBLE);
                rlBgTable.setBackgroundResource(R.drawable.bg_table_finish_define);
                tvMoney.setVisibility(View.VISIBLE);
                double orderMoney = data.getOrderMoney();
                double tableFee = data.getTableFee();
                mLogger.e(orderMoney);
                mLogger.e(tableFee);
                tvMoney.setText(String.valueOf(orderMoney+tableFee));
                mLogger.e(orderMoney+tableFee);
                holder.text(R.id.item_table_peoplePercent_tv, data.getDinningPeople() + "/" + data.getPropleNum());

                break;
            case CommonConstant.TableStatus.INVOICING:
                rlBgTable.setBackgroundResource(R.drawable.bg_table_disable_define);
                break;
            case CommonConstant.TableStatus.TO_BE_CONFIRMED:
                rlBgTable.setBackgroundResource(R.drawable.bg_table_disable_define);
                holder.text(R.id.item_table_peoplePercent_tv, data.getDinningPeople() + "/" + data.getPropleNum());
                tvMoney.setText("待确认");
                tvMoney.setVisibility(View.VISIBLE);
                break;

            default:
                break;
        }
        holder.text(R.id.item_table_tableName_tv, data.getTableName());

    }


}
