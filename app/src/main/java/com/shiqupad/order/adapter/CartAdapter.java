package com.shiqupad.order.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.shiqupad.order.listener.MenuChangeListener;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dreamtouch.magicbox_general_ui.ui.AddAndSubView;
import cn.com.dreamtouch.magicbox_general_ui.util.MyLogger;
import cn.com.dreamtouch.magicbox_http_client.network.model.DishSetDetail;
import cn.com.dreamtouch.magicbox_http_client.network.model.DishTasteBean;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;
import com.shiqupad.order.R;

import com.shiqupad.order.util.MathUtil;

/**
 * Created by frank on 2016/4/19.
 */
public class CartAdapter extends BaseAdapter {

    private MenuChangeListener menuChangeListener;

    private Context context;
    private List<GetShopDishResponse.TwoShopDish> list;

    public CartAdapter(List<GetShopDishResponse.TwoShopDish> list, Context context) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.item_cart, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        GetShopDishResponse.TwoShopDish bean = list.get(position);
        if(holder.addAndSubView != null && bean != null){
            holder.addAndSubView.setNum(bean.getCartNum());
        }
        MyLogger.kLog().e(bean.getCartNum()+"bean.getcartNum");
        holder.textDishName.setText(bean.getDishName());
        //double v = Double.parseDouble(bean.getPrice());
        holder.textPrice.setText("￥" + MathUtil.saveTwoDigit2(bean.getPrice()));
        holder.addAndSubView.setOnNumChangeListener(new AddAndSubView.OnNumChangeListener() {
            @Override
            public void onNumChange(View view, int num) {
                if (menuChangeListener != null) {
                    menuChangeListener.onMenuChange(
                            position, num);
                }
            }
        });

        //口味规格时价菜。。。
        List<String> desc = new ArrayList<>();
        TagAdapter<String> adapter = new TagAdapter<String>(desc) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                TextView textView = (TextView) LayoutInflater.from(context).inflate(R.layout.item_tag_dish_desc, parent, false);
                textView.setText(s);
                return textView;
            }
        };
        if (!TextUtils.isEmpty(bean.getIsMarketPriceDish())&& bean.getIsMarketPriceDish().equals("1")) {
            desc.clear();
            desc.add("时价菜");
        } else {
            desc.clear();
            if (!TextUtils.isEmpty(bean.getDishUnit().getUnitName())) {
                desc.add(bean.getDishUnit().getUnitName());
            }
            if (!TextUtils.isEmpty(bean.getDishTaste().getTasteName())) {
                desc.add(bean.getDishTaste().getTasteName());
            }
            if (bean.getFeedList().size() > 0) {
                for (DishTasteBean tasteBean : bean.getFeedList()) {
                    desc.add(context.getString(R.string.format_taste_feed, tasteBean.getTasteName(), tasteBean.getValue()));
                }
            }
            if (bean.getComboList() != null) {
                for (DishSetDetail setDetail : bean.getComboList()) {
                    desc.add(setDetail.getDishName());
                }
            }
        }
        if (desc.size() > 0) {
            holder.flowDesc.setVisibility(View.VISIBLE);
            holder.flowDesc.setAdapter(adapter);
            adapter.notifyDataChanged();
        } else {
            holder.flowDesc.setVisibility(View.GONE);
        }

        return convertView;
    }

    public void setMenuChangeListener(MenuChangeListener menuChangeListener) {
        this.menuChangeListener = menuChangeListener;
    }

    static class ViewHolder {
        @Bind(R.id.item_cart_rootView)
        LinearLayout rootView;
        @Bind(R.id.item_cart_text_dishName)
        TextView textDishName;
        @Bind(R.id.item_cart_flow_desc)
        TagFlowLayout flowDesc;
        @Bind(R.id.item_cart_text_price)
        TextView textPrice;
        @Bind(R.id.item_cart_addAndSubView)
        AddAndSubView addAndSubView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
