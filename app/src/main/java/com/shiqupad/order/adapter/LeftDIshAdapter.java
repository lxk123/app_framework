package com.shiqupad.order.adapter;

import android.content.Context;
import android.widget.AdapterView;

import com.shiqupad.order.listener.MenuChangeListener;

import java.util.Collection;

import cn.com.dreamtouch.magicbox_general_ui.adapter.BaseRecyclerAdapter;
import cn.com.dreamtouch.magicbox_general_ui.adapter.SmartViewHolder;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;

/**
 * Created by LuoXueKun on 2018/5/16.
 */

public class LeftDIshAdapter extends BaseRecyclerAdapter<GetShopDishResponse.TwoShopDish> {
   private MenuChangeListener menuChangeListener;
    private Context mContext;
    public LeftDIshAdapter(Context context, Collection<GetShopDishResponse.TwoShopDish> collection,
                           int layoutId, AdapterView.OnItemClickListener listener) {
        super(collection, layoutId, listener);
    }

    public LeftDIshAdapter(Context context, Collection<GetShopDishResponse.TwoShopDish> collection,
                           int layoutId, MenuChangeListener menuChangeListener) {
        super(collection, layoutId);
        mContext = context;
        this.menuChangeListener = menuChangeListener;
    }


    @Override
    protected void onBindViewHolder(SmartViewHolder holder, GetShopDishResponse.TwoShopDish twoShopDish, int position) {

    }


}
