package com.shiqupad.order.adapter;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.Collection;
import java.util.List;

import cn.com.dreamtouch.magicbox_general_ui.adapter.BaseRecyclerAdapter;
import cn.com.dreamtouch.magicbox_general_ui.adapter.SmartViewHolder;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;
import com.shiqupad.order.R;

/**
 * Created by LuoXueKun on 2018/5/16.
 */

public class DishTypeAdapter extends BaseRecyclerAdapter<GetShopDishResponse.OneShopDish> {
    private int selection = 0;
    private Context mContext;
    private int num;
    private List<GetShopDishResponse.OneShopDish> list;
    public DishTypeAdapter(Context context, Collection<GetShopDishResponse.OneShopDish> collection,
                           int layoutId, AdapterView.OnItemClickListener listener) {
        super(collection, layoutId, listener);
        mContext = context;
    }
    public DishTypeAdapter(Context context, List<GetShopDishResponse.OneShopDish> collection,
                           int layoutId) {
        super(collection, layoutId);
        mContext = context;
        list = collection;
    }


    @Override
    protected void onBindViewHolder(SmartViewHolder holder, GetShopDishResponse.OneShopDish oneShopDish, int position) {
        TextView tvCatogray = holder.itemView.findViewById(R.id.tv_catogray);
        TextView tvCount = holder.itemView.findViewById(R.id.tv_count);
        if (position == selection) {
            tvCatogray.setBackgroundResource(R.drawable.bg_checked_h);
            tvCatogray.setTextColor(mContext.getResources().getColor(R.color.tab_select_h));
        } else {
            tvCatogray.setBackgroundResource(R.color.bg_color_white);
            tvCatogray.setTextColor(mContext.getResources().getColor(R.color.text_color_gray_two));
        }
         tvCatogray.setText(oneShopDish.getTypeName());

        int count = 0;
        for (int i = 0; i < oneShopDish.getDishList().size(); i++) {
            count += oneShopDish.getDishList().get(i).getCartNum();
        }
        list.get(position).setCartNum(count);

        if (count <= 0) {
            tvCount.setVisibility(View.INVISIBLE);
        } else {

            tvCount.setVisibility(View.VISIBLE);
            tvCount.setText(oneShopDish.getCartNum()+"");
        }

    }

    public void setSelection(int selection) {
        this.selection = selection;
        this.notifyDataSetChanged();
    }


}
