package com.shiqupad.order.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.shiqupad.order.R;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;

/**
 * Created by LuoXueKun on 2018/5/16.
 */

public class DishTypeAdapter2 extends BaseAdapter {
    private int selection = 0;
    private int num;

    public void setSelection(int selection) {
        this.selection = selection;
        this.notifyDataSetChanged();
        notifyDataSetChanged();
    }
    private Context context;
    private List<GetShopDishResponse.OneShopDish> list;

    public DishTypeAdapter2(List<GetShopDishResponse.OneShopDish> list, Context context) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.item_card_right, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        TextView tvCatogray = holder.tvCatogray;
        TextView tvCount = holder.tvCount;
        if (position == selection) {
            holder.tvCatogray.setBackgroundResource(R.drawable.bg_checked_h);
            holder.tvCatogray.setTextColor(context.getResources().getColor(R.color.tab_select_h));
        } else {
            holder.tvCatogray.setBackgroundResource(R.color.bg_color_white);
            holder.tvCatogray.setTextColor(context.getResources().getColor(R.color.text_color_gray_two));
        }
        GetShopDishResponse.OneShopDish oneShopDish = list.get(position);
        holder.tvCatogray.setText(oneShopDish.getTypeName());

        int count = 0;
        for (int i = 0; i < oneShopDish.getDishList().size(); i++) {
            count += oneShopDish.getDishList().get(i).getCartNum();
        }
        list.get(position).setCartNum(count);

        if (count <= 0) {
            holder.tvCount.setVisibility(View.INVISIBLE);
        } else {

            holder.tvCount.setVisibility(View.VISIBLE);
            holder.tvCount.setText(oneShopDish.getCartNum()+"");
        }

        return convertView;
    }
    static class ViewHolder {
        @Bind(R.id.tv_catogray)
        TextView tvCatogray;
        @Bind(R.id.tv_count)
        TextView tvCount;
        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
