package com.shiqupad.order.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.shiqupad.order.R;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.List;
import java.util.Set;


import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dreamtouch.magicbox_http_client.network.model.DishSetDetail;
import cn.com.dreamtouch.magicbox_http_client.network.model.DishSetItem;


/**
 * Created by lsy on 2018/4/26.
 */

public class DishSetAdapter extends BaseAdapter {

    private Context ctx;
    private List<DishSetItem> list;

    public DishSetAdapter(Context ctx, List<DishSetItem> list) {
        this.ctx = ctx;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(ctx).inflate(R.layout.item_dish_set, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        final DishSetItem setItem = list.get(position);
        holder.tvTypeName.setText(transText(setItem));
        final TagAdapter<DishSetDetail> adapter = new TagAdapter<DishSetDetail>(setItem.getDishSetDetailList()) {
            @Override
            public View getView(FlowLayout parent, int position, DishSetDetail dishSetDetail) {
                TextView textView;
                if ("3".equals(setItem.getItemType()))
                    textView = (TextView) LayoutInflater.from(ctx).
                            inflate(R.layout.item_dish_spec_tag_selected, parent, false);
                else
                    textView = (TextView) LayoutInflater.from(ctx)
                            .inflate(R.layout.item_dish_spec_tag, parent, false);
                textView.setText(dishSetDetail.getDishName());
                return textView;
            }
        };
        holder.flowDish.setAdapter(adapter);

        if ("1".equals(setItem.getItemType())) {
            holder.flowDish.setMaxSelectCount(1);
            holder.flowDish.setOnSelectListener(new TagFlowLayout.OnSelectListener() {
                @Override
                public void onSelected(Set<Integer> selectPosSet) {
                    for (int i = 0; i < setItem.getDishSetDetailList().size(); i++) {
                        if (selectPosSet.toString().contains(String.valueOf(i)))
                            setItem.getDishSetDetailList().get(i).setIsChecked(true);
                        else
                            setItem.getDishSetDetailList().get(i).setIsChecked(false);
                    }
                }
            });
        } else if ("2".equals(setItem.getItemType())) {
            holder.flowDish.setMaxSelectCount(setItem.getSelectDishCount());
            holder.flowDish.setOnSelectListener(new TagFlowLayout.OnSelectListener() {
                @Override
                public void onSelected(Set<Integer> selectPosSet) {
                    for (int i = 0; i < setItem.getDishSetDetailList().size(); i++) {
                        if (selectPosSet.toString().contains(String.valueOf(i)))
                            setItem.getDishSetDetailList().get(i).setIsChecked(true);
                        else
                            setItem.getDishSetDetailList().get(i).setIsChecked(false);
                    }
                }
            });
        } else if ("3".equals(setItem.getItemType())) {
            holder.flowDish.setMaxSelectCount(-1);
            for (int i = 0; i < setItem.getDishSetDetailList().size(); i++) {
                setItem.getDishSetDetailList().get(i).setIsChecked(true);
            }
        }
        adapter.notifyDataChanged();
        return convertView;
    }

    private String transText(DishSetItem setItem) {
        String s = "";
        switch (setItem.getItemType()) {
            case "1":
                s = setItem.getDishTypeName() + "（单选）";
                break;
            case "2":
                s = ctx.getString(R.string.format_more_select_desc, setItem.getDishTypeName(), setItem.getSelectDishCount(), setItem.getDishSetDetailList().size());
                break;
            case "3":
                s = setItem.getDishTypeName() + "（全选）";
                break;
        }
        return s;
    }

    static class ViewHolder {
        @Bind(R.id.item_dish_set_tv_typeName)
        TextView tvTypeName;
        @Bind(R.id.item_dish_set_flow_dish)
        TagFlowLayout flowDish;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
