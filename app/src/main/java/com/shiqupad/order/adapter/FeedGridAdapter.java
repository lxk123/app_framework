package com.shiqupad.order.adapter;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dreamtouch.magicbox_general_ui.ui.AddAndSubView;
import cn.com.dreamtouch.magicbox_http_client.network.model.DishTasteBean;
import com.shiqupad.order.R;

/**
 * Created by lsy on 2018/4/20.
 */

public class FeedGridAdapter extends BaseAdapter {

    private Context ctx;
    private IFeedGridAdapterCallBack callBack;
    private List<DishTasteBean> list;

    public FeedGridAdapter(Context ctx, List<DishTasteBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public interface IFeedGridAdapterCallBack {
        void onNumChanger(int position, int num);
    }

    public void setIFeedGridAdapterCallBack(IFeedGridAdapterCallBack callBack) {
        this.callBack = callBack;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(ctx).inflate(R.layout.item_feeding, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        DishTasteBean tasteBean = list.get(position);
        holder.tvTasteName.setText(String.format(ctx.getString(R.string.format_dish_spec), tasteBean.getTasteName(), tasteBean.getRisePrice() + ""));
        holder.addAndSubView.setOnNumChangeListener(new AddAndSubView.OnNumChangeListener() {
            @Override
            public void onNumChange(View view, int num) {
                callBack.onNumChanger(position, num);
            }
        });
        holder.rootView.setBackgroundDrawable(tasteBean.getValue() > 0? ContextCompat.getDrawable(ctx,R.drawable.tag_bg_checked):
                ContextCompat.getDrawable(ctx,R.drawable.tag_bg_unchecked));
        return convertView;
    }

    static class ViewHolder {
        @Bind(R.id.item_feeding_rootView)
        ConstraintLayout rootView;
        @Bind(R.id.tv_tasteName)
        TextView tvTasteName;
        @Bind(R.id.addAndSubView)
        AddAndSubView addAndSubView;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
