package com.shiqupad.order.adapter;

import android.content.Context;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.style.StrikethroughSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;


import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dreamtouch.magicbox_http_client.network.model.DetailListBean;
import cn.com.dreamtouch.magicbox_http_client.network.model.DishTasteBean;
import cn.com.dreamtouch.magicbox_http_client.network.model.FeedListBean;
import com.shiqupad.order.R;
import com.shiqupad.order.util.MathUtil;

/**
 * Created by hu on 2016/8/11.
 */
public class OrderDetailActivityAdapter extends BaseAdapter {
    private static final int ITEM_DISH = 0;
    private static final int ITEM_COMBO = 1;
    private static final int ITEM_FEED = 2;
    private static final int ITEM_TASTE = 3;
    private static final int ITEM_REFUND = 4;

    private Context context;
    private List<DetailListBean> list;
    private List<Object> dataSet = new ArrayList<>();

    public OrderDetailActivityAdapter(Context context, List<DetailListBean> list) {
        this.context = context;
        this.list = list;
        makeData(list);

    }

    private void makeData(List<DetailListBean> list) {
        dataSet.clear();
        for (DetailListBean bean : list) {
            dataSet.add(bean);
            if (!TextUtils.isEmpty(bean.getFeedingJson())) {
                List<FeedListBean> feedListBeans = JSON.parseArray(bean.getFeedingJson(), FeedListBean.class);
                dataSet.addAll(feedListBeans);

                for (FeedListBean feedListBean : feedListBeans) {
                    feedListBean.setIsRetire(bean.getIsRetire());
                }
            }
            if (bean.getDetailType() != 2)
                bean.setSumPrice(bean.getPrice() * bean.getDishNumber() + bean.getFeedingPrice());

            if (!TextUtils.isEmpty(bean.getTasteName())) {
                DishTasteBean tasteBean = new DishTasteBean();
                tasteBean.setTasteName(bean.getTasteName());
                tasteBean.setIsRetire(bean.getIsRetire());
                dataSet.add(tasteBean);
            }
        }
    }

    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (dataSet.get(position) instanceof DetailListBean) {
            DetailListBean bean = (DetailListBean) dataSet.get(position);
            if (bean.getDetailType() == 2)
                return ITEM_COMBO;
            else {
                if (bean.getIsRetire() == 0)
                    return ITEM_DISH;
                else
                    return ITEM_REFUND;
            }
        } else if (dataSet.get(position) instanceof FeedListBean) {
            return ITEM_FEED;
        } else {
            return ITEM_TASTE;
        }
    }

    @Override
    public int getViewTypeCount() {
        return 5;
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemViewHolder holder = null;
        ComboViewHolder comboHolder = null;
        RefundViewHolder refundHolder = null;
        if (convertView == null) {
            if (getItemViewType(position) == ITEM_DISH) {
                convertView = LayoutInflater.from(context).inflate(R.layout.item_detail, parent, false);
                holder = new ItemViewHolder(convertView);
                convertView.setTag(holder);
            } else if (getItemViewType(position) == ITEM_REFUND) {
                convertView = LayoutInflater.from(context).inflate(R.layout.item_detail_refund, parent, false);
                refundHolder = new RefundViewHolder(convertView);
                convertView.setTag(refundHolder);
            } else {
                convertView = LayoutInflater.from(context).inflate(R.layout.item_detail_combo, parent, false);
                comboHolder = new ComboViewHolder(convertView);
                convertView.setTag(comboHolder);
            }
        } else {
            if (getItemViewType(position) == ITEM_DISH) {
                holder = (ItemViewHolder) convertView.getTag();
            } else if (getItemViewType(position) == ITEM_REFUND) {
                refundHolder = (RefundViewHolder) convertView.getTag();
            } else {
                comboHolder = (ComboViewHolder) convertView.getTag();
            }
        }
        Object o = dataSet.get(position);
        if (getItemViewType(position) == ITEM_DISH) {
            DetailListBean dishItemBean = (DetailListBean) o;
            if (TextUtils.isEmpty(dishItemBean.getDishUnitName()))
                holder.tvDish.setText(dishItemBean.getDishName());
            else
                holder.tvDish.setText(dishItemBean.getDishName() + "（" + dishItemBean.getDishUnitName() + "）");
            holder.tvNum.setText(dishItemBean.getDishNumber() + "");
            if(dishItemBean.getDishStatus() == 5){
                holder.textSum.setText("已取消");
                holder.textSum.setTextColor(context.getResources().getColor(R.color.text_hint));
            }else {
                holder.textSum.setText(MathUtil.saveTwoDigit2(dishItemBean.getSumPrice()));
                holder.textSum.setTextColor(context.getResources().getColor(R.color.black));
            }
            holder.textPrice.setText(dishItemBean.getPrice() + "");
        } else if (getItemViewType(position) == ITEM_REFUND) {
            DetailListBean dishItemBean = (DetailListBean) o;
            if (TextUtils.isEmpty(dishItemBean.getDishUnitName()))
                refundHolder.tvDish.setText(dishItemBean.getDishName());
            else
                refundHolder.tvDish.setText(dishItemBean.getDishName() + "（" + dishItemBean.getDishUnitName() + "）");

            refundHolder.tvDish.setText(generateSpan(refundHolder.tvDish.getText().toString()));

            refundHolder.tvNum.setText(generateSpan(String.valueOf(dishItemBean.getRetireNum())));
            refundHolder.textPrice.setText(generateSpan(String.valueOf(dishItemBean.getPrice())));
            refundHolder.textSum.setText(generateSpan("已退"));
        } else if (getItemViewType(position) == ITEM_COMBO) {
            DetailListBean dishItemBean = (DetailListBean) o;
            if (dishItemBean.getIsRetire() == 1) {
                comboHolder.tvDish.setText(generateSpan(dishItemBean.getDishName()));
                comboHolder.tvNum.setText(generateSpan(String.valueOf(dishItemBean.getDishNumber())));
            } else {
                comboHolder.tvDish.setText(dishItemBean.getDishName());
                comboHolder.tvNum.setText(dishItemBean.getDishNumber() + "");
            }
        } else if (getItemViewType(position) == ITEM_FEED) {
            FeedListBean feedListBean = (FeedListBean) o;
            if (feedListBean.getIsRetire() == 1) {
                comboHolder.tvDish.setText(generateSpan(feedListBean.getFeedName()));
                comboHolder.tvNum.setText(generateSpan(String.valueOf(feedListBean.getFeedSum())));
                comboHolder.textPrice.setText(generateSpan(String.valueOf(feedListBean.getFeedPrice())));
            } else {
                comboHolder.tvDish.setText(feedListBean.getFeedName());
                comboHolder.tvNum.setText(feedListBean.getFeedSum() + "");
                comboHolder.textPrice.setText(feedListBean.getFeedPrice() + "");
            }
        } else if (getItemViewType(position) == ITEM_TASTE) {
            DishTasteBean tasteBean = (DishTasteBean) o;
            if (tasteBean.getIsRetire() == 1)
                comboHolder.tvDish.setText(generateSpan(String.valueOf(tasteBean.getTasteName())));
            else
                comboHolder.tvDish.setText(tasteBean.getTasteName());
        }
        return convertView;
    }

    private SpannableString generateSpan(String spanString) {
        SpannableString spannableString = new SpannableString(spanString);
        StrikethroughSpan span = new StrikethroughSpan();
        spannableString.setSpan(span, 0, spannableString.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        return spannableString;
    }

    public void setData(List<DetailListBean> data) {
        list.clear();
        list.addAll(data);
        makeData(list);
        notifyDataSetChanged();
    }


    static class ItemViewHolder {
        @Bind(R.id.tv_dish)
        TextView tvDish;//菜品名
        @Bind(R.id.tv_num)
        TextView tvNum;//菜品数量
        @Bind(R.id.text_price)
        TextView textPrice;//菜品单价
        @Bind(R.id.text_sum)
        TextView textSum;//菜品总价

        public ItemViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    static class RefundViewHolder {
        @Bind(R.id.tv_dish)
        TextView tvDish;//菜品名
        @Bind(R.id.tv_num)
        TextView tvNum;//菜品数量
        @Bind(R.id.text_price)
        TextView textPrice;//菜品单价
        @Bind(R.id.text_sum)
        TextView textSum;//菜品总价

        public RefundViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }


    static class ComboViewHolder {
        @Bind(R.id.tv_dish)
        TextView tvDish;
        @Bind(R.id.tv_num)
        TextView tvNum;
        @Bind(R.id.text_price)
        TextView textPrice;
        @Bind(R.id.text_sum)
        TextView textSum;

        public ComboViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
