package com.shiqupad.order.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import com.shiqupad.order.R;


/**
 * Created by WangChang on 2016/4/1.
 */
public class GetNewSwitchTableAdapter extends RecyclerView.Adapter<GetNewSwitchTableAdapter.BaseViewHolder> {
    private List<GetAllTableResponse.Data.ListBean> dataList = new ArrayList<>();
    private int lastPressIndex = 0;

    OneViewHolder viewHolder;

    public void replaceAll(List<GetAllTableResponse.Data.ListBean> list) {
        dataList.clear();
        if (list != null && list.size() > 0) {
            dataList.addAll(list);
        }
        notifyDataSetChanged();
    }

    @Override
    public GetNewSwitchTableAdapter.BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_switch_table, parent, false);
        viewHolder = new OneViewHolder(view);

        return viewHolder;

    }


    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {

        holder.setData(dataList);

    }

    @Override
    public int getItemCount() {
        return dataList != null ? dataList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    public class BaseViewHolder extends RecyclerView.ViewHolder {

        public BaseViewHolder(View itemView) {
            super(itemView);
        }

        void setData(Object data) {


        }
    }

    private class OneViewHolder extends BaseViewHolder {

        private RelativeLayout llBackground;
        private TextView tvTableName;
        private TextView tvNum;

        public OneViewHolder(View view) {
            super(view);

            llBackground = (RelativeLayout) view.findViewById(R.id.item_switch_table_rl);
            tvTableName = (TextView) view.findViewById(R.id.item_switch_table_name_tv);
            tvNum = (TextView) view.findViewById(R.id.item_switch_table_num_tv);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition();
                    if (lastPressIndex == position) {
                        lastPressIndex = -1;
                    } else {
                        lastPressIndex = position;
                    }
                    notifyDataSetChanged();
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(position);
                    }

                }

            });

        }

        @Override
        void setData(Object data) {
            if (data != null) {
                if (getAdapterPosition() == lastPressIndex) {
                    llBackground.setSelected(true);
                    itemView.setEnabled(false);
                } else {
                    llBackground.setSelected(false);
                    itemView.setEnabled(true);
                }

            }
            tvTableName.setText(dataList.get(getAdapterPosition()).getTableName());
            tvNum.setText(dataList.get(getAdapterPosition()).getDinningPeople() + "/" + dataList.get(getAdapterPosition()).getPropleNum());

        }
    }

    public onItemClickListener onItemClickListener;

    public interface onItemClickListener {
        void onItemClick(int pos);
    }

    public void setOnItemClickListener(onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
