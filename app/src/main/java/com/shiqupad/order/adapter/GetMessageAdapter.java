package com.shiqupad.order.adapter;

import android.content.Context;
import android.widget.AdapterView;
import android.widget.ImageView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.Collection;

import cn.com.dreamtouch.magicbox_general_ui.adapter.BaseRecyclerAdapter;
import cn.com.dreamtouch.magicbox_general_ui.adapter.SmartViewHolder;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetMessageResponse;

import com.shiqupad.order.R;
import com.shiqupad.order.util.DisplayIconUtil;

/**
 * Created by LuoXueKun on 2018/5/16.
 */

public class GetMessageAdapter extends BaseRecyclerAdapter<GetMessageResponse.MessageData> {
    private Context mContext;

    public GetMessageAdapter(Context context, Collection<GetMessageResponse.MessageData> collection,
                             int layoutId, AdapterView.OnItemClickListener listener) {
        super(collection, layoutId, listener);
        this.mContext = context;
    }


    @Override
    protected void onBindViewHolder(SmartViewHolder holder, GetMessageResponse.MessageData data, int position) {

        SimpleDraweeView sdv = holder.itemView.findViewById(R.id.item_message_sdv);
        holder.text(R.id.item_message_title_tv, data.getTitle());
        holder.text(R.id.item_message_content_tv, data.getContent());
        holder.text(R.id.item_message_create_tv, data.getCreateDate());
        String title = data.getTitle();
        DisplayIconUtil.showMessageIcon(mContext, title, sdv);
    }


}
