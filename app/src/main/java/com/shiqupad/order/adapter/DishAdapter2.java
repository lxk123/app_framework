package com.shiqupad.order.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shiqupad.order.R;
import com.shiqupad.order.listener.MenuChangeListener;
import com.shiqupad.order.util.MathUtil;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;
import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dreamtouch.magicbox_general_ui.adapter.BaseRecyclerAdapter;
import cn.com.dreamtouch.magicbox_general_ui.adapter.SmartViewHolder;
import cn.com.dreamtouch.magicbox_general_ui.ui.AddAndSubView;
import cn.com.dreamtouch.magicbox_general_ui.util.MyLogger;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;


/**
 * Created by LuoXueKun on 2018/5/16.
 */

public class DishAdapter2 extends BaseAdapter {
    private MenuChangeListener menuChangeListener;


    private Context context;
    private List<GetShopDishResponse.TwoShopDish> list;

    public DishAdapter2(List<GetShopDishResponse.TwoShopDish> list, Context context, MenuChangeListener menuChangeListener) {
        this.context = context;
        this.list = list;
        this.menuChangeListener = menuChangeListener;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(context, R.layout.item_card_center, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        GetShopDishResponse.TwoShopDish twoShopDish = list.get(position);
        MyLogger.kLog().e(twoShopDish.getCartNum());
      /*  TextView tvIsSellNull = holder.itemView.findViewById(R.id.item_card_center_orderMoney_tv);
        TextView tvDishName = holder.itemView.findViewById(R.id.item_card_dishName_tv);
        TextView tvDishPrice = holder.itemView.findViewById(R.id.item_card_center_price_tv);
        AddAndSubView addAndSubView = holder.itemView.findViewById(R.id.item_card_center_awt);
        RelativeLayout rlBg = holder.itemView.findViewById(R.id.item_card_center_rl);
        TextView tvNum = holder.itemView.findViewById(R.id.item_card_center_num_tv);*/
//		addWidget.setData(this, helper.getAdapterPosition(), onAddClick);

        if (twoShopDish != null && !TextUtils.isEmpty(twoShopDish.getDishName())) {
            holder.tvDishName.setText(String.format("%s", twoShopDish.getDishName()));
        }
        holder.tvDishPrice.setText(String.format("￥  %s",
                MathUtil.saveTwoDigit(Objects.requireNonNull(twoShopDish).getPrice())
                )
        );
        if (twoShopDish.getLimitNumber() != null && twoShopDish.getLimitNumber().equals("0")) {
            holder.tvIsSellNull.setVisibility(View.VISIBLE);
            holder.tvNum.setVisibility(View.GONE);
            holder.tvDishName.setTextColor(context.getResources().getColor(R.color.text_color_table_num));
            holder.tvDishPrice.setTextColor(context.getResources().getColor(R.color.text_color_table_num));
            holder.addAndSubView.setVisibility(View.GONE);
        } else if (twoShopDish.getIsHaveTaste().equals("1")
                || twoShopDish.getIsHaveUnit().equals("1")) {//规格、口味
            holder.addAndSubView.setVisibility(View.GONE);
            holder.tvIsSellNull.setVisibility(View.GONE);
            holder.tvDishPrice.setVisibility(View.VISIBLE);
            holder.tvDishName.setTextColor(context.getResources().getColor(R.color.text_color_table_status));
            holder.tvDishPrice.setTextColor(context.getResources().getColor(R.color.tab_select_h));
            if (twoShopDish.getCartNum() > 0) {
                holder.tvNum.setVisibility(View.VISIBLE);
                holder.tvNum.setText(MessageFormat.format("{0}", twoShopDish.getCartNum()));
            } else {
                holder.tvNum.setVisibility(View.GONE);
            }

        } else if (twoShopDish.getIsMarketPriceDish().equals("1")) {//时价菜
            holder.tvDishName.setTextColor(context.getResources().getColor(R.color.text_color_table_status));
            holder.tvDishPrice.setTextColor(context.getResources().getColor(R.color.tab_select_h));
            holder.addAndSubView.setVisibility(View.GONE);
            holder.tvDishPrice.setVisibility(View.GONE);
            if (Integer.valueOf(twoShopDish.getLimitNumber()) > 0) {
                holder.tvIsSellNull.setVisibility(View.GONE);
                holder.tvDishName.setTextColor(context.getResources().getColor(R.color.text_color_table_status));
                if (twoShopDish.getCartNum() > 0) {
                    holder.tvNum.setVisibility(View.VISIBLE);
                    holder.tvNum.setText(MessageFormat.format("{0}", twoShopDish.getCartNum()));

                } else {
                    holder.tvNum.setVisibility(View.GONE);
                }


            } else {
                holder.tvIsSellNull.setVisibility(View.VISIBLE);
                holder.tvNum.setVisibility(View.GONE);

            }

        } else if (twoShopDish.getIsSet().equals("1")) {//套餐
            holder.tvDishName.setTextColor(context.getResources().getColor(R.color.text_color_table_status));
            holder.tvDishPrice.setTextColor(context.getResources().getColor(R.color.tab_select_h));
            holder.addAndSubView.setVisibility(View.GONE);
            holder.tvIsSellNull.setVisibility(View.GONE);
            if (twoShopDish.getCartNum() > 0) {
                holder.tvNum.setText(MessageFormat.format("{0}", twoShopDish.getCartNum()));
                holder.tvNum.setVisibility(View.VISIBLE);
            } else {
                holder.tvNum.setVisibility(View.GONE);
            }
        } else {
            holder.tvIsSellNull.setVisibility(View.GONE);
            holder.tvDishName.setTextColor(context.getResources().getColor(R.color.text_color_table_status));
            holder.tvDishPrice.setTextColor(context.getResources().getColor(R.color.tab_select_h));
            holder.addAndSubView.setVisibility(View.VISIBLE);
            holder.tvNum.setVisibility(View.GONE);
            holder.addAndSubView.setNum(twoShopDish.getCartNum());
            holder.addAndSubView.setOnNumChangeListener(new AddAndSubView.OnNumChangeListener() {
                @Override
                public void onNumChange(View view, int num) {
                    if (menuChangeListener == null) {
                        return;
                    }
                    menuChangeListener.onMenuChange(position, num);
                }
            });
        }
        if (twoShopDish.getCartNum() > 0) {
            holder.rlBg.setBackgroundResource(R.drawable.btn_table_h);
        } else {
            holder.rlBg.setBackgroundResource(R.drawable.btn_table);
        }

        return convertView;
    }

    static class ViewHolder {
        @Bind(R.id.item_card_center_orderMoney_tv)
        TextView tvIsSellNull;
        @Bind(R.id.item_card_dishName_tv)
        TextView tvDishName;
        @Bind(R.id.item_card_center_price_tv)
        TextView tvDishPrice;
        @Bind(R.id.item_card_center_awt)
        AddAndSubView addAndSubView;
        @Bind(R.id.item_card_center_rl)
        RelativeLayout rlBg;
        @Bind(R.id.item_card_center_num_tv)
        TextView tvNum;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
