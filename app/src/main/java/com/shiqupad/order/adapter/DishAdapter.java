package com.shiqupad.order.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shiqupad.order.listener.MenuChangeListener;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Objects;

import cn.com.dreamtouch.magicbox_general_ui.adapter.BaseRecyclerAdapter;
import cn.com.dreamtouch.magicbox_general_ui.adapter.SmartViewHolder;
import cn.com.dreamtouch.magicbox_general_ui.ui.AddAndSubView;
import cn.com.dreamtouch.magicbox_general_ui.util.MyLogger;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;
import com.shiqupad.order.R;
import com.shiqupad.order.util.MathUtil;


/**
 * Created by LuoXueKun on 2018/5/16.
 */

public class DishAdapter extends BaseRecyclerAdapter<GetShopDishResponse.TwoShopDish> {
    private MenuChangeListener menuChangeListener;
    private Context mContext;

    public DishAdapter(Context context, Collection<GetShopDishResponse.TwoShopDish> collection,
                       int layoutId, AdapterView.OnItemClickListener listener) {
        super(collection, layoutId, listener);
    }

    public DishAdapter(Context context, Collection<GetShopDishResponse.TwoShopDish> collection,
                       int layoutId,MenuChangeListener menuChangeListener) {
        super(collection, layoutId);
        this.mContext = context;
        this.menuChangeListener = menuChangeListener;
    }


    @Override
    protected void onBindViewHolder(SmartViewHolder holder, GetShopDishResponse.TwoShopDish twoShopDish, final int position) {

        MyLogger.kLog().e(twoShopDish.getCartNum());
        TextView tvIsSellNull = holder.itemView.findViewById(R.id.item_card_center_orderMoney_tv);
        TextView tvDishName = holder.itemView.findViewById(R.id.item_card_dishName_tv);
        TextView tvDishPrice = holder.itemView.findViewById(R.id.item_card_center_price_tv);
        AddAndSubView addAndSubView = holder.itemView.findViewById(R.id.item_card_center_awt);
        RelativeLayout rlBg = holder.itemView.findViewById(R.id.item_card_center_rl);
        TextView tvNum = holder.itemView.findViewById(R.id.item_card_center_num_tv);
//		addWidget.setData(this, helper.getAdapterPosition(), onAddClick);

        if (twoShopDish != null && !TextUtils.isEmpty(twoShopDish.getDishName())) {
            tvDishName.setText(String.format("%s", twoShopDish.getDishName()));
        }
        tvDishPrice.setText(String.format("￥  %s", MathUtil.saveTwoDigit(Objects.requireNonNull(twoShopDish).getPrice())
              )
        );
        if (twoShopDish.getLimitNumber() != null && twoShopDish.getLimitNumber().equals("0")) {
            tvIsSellNull.setVisibility(View.VISIBLE);
            tvNum.setVisibility(View.GONE);
            tvDishName.setTextColor(mContext.getResources().getColor(R.color.text_color_table_num));
            tvDishPrice.setTextColor(mContext.getResources().getColor(R.color.text_color_table_num));
            addAndSubView.setVisibility(View.GONE);
        } else if (twoShopDish.getIsHaveTaste().equals("1")
                || twoShopDish.getIsHaveUnit().equals("1")) {//规格、口味
            addAndSubView.setVisibility(View.GONE);
            if(twoShopDish.getCartNum()>0){
                tvNum.setVisibility(View.VISIBLE);
                tvNum.setText(MessageFormat.format("{0}", twoShopDish.getCartNum()));
            }else {
                tvNum.setVisibility(View.GONE);
            }

        } else if (twoShopDish.getIsMarketPriceDish().equals("1")) {//时价菜
            addAndSubView.setVisibility(View.GONE);
            tvDishPrice.setVisibility(View.GONE);
            if(Integer.valueOf(twoShopDish.getLimitNumber())>0){
                tvIsSellNull.setVisibility(View.GONE);
                tvDishName.setTextColor(mContext.getResources().getColor(R.color.text_color_table_status));
                if(twoShopDish.getCartNum() >0){
                    tvNum.setVisibility(View.VISIBLE);
                    tvNum.setText(MessageFormat.format("{0}", twoShopDish.getCartNum()));

                }else{
                    tvNum.setVisibility(View.GONE);
                }



            }else {
                tvIsSellNull.setVisibility(View.VISIBLE);
                tvNum.setVisibility(View.GONE);

            }

        } else if (twoShopDish.getIsSet().equals("1")) {//套餐
            addAndSubView.setVisibility(View.GONE);
            if(twoShopDish.getCartNum()>0){
                tvNum.setText(MessageFormat.format("{0}", twoShopDish.getCartNum()));
                tvNum.setVisibility(View.VISIBLE);
            }else {
                tvNum.setVisibility(View.GONE);
            }
        } else {
            tvIsSellNull.setVisibility(View.GONE);
            tvDishName.setTextColor(mContext.getResources().getColor(R.color.text_color_table_status));
            tvDishPrice.setTextColor(mContext.getResources().getColor(R.color.tab_select_h));
            addAndSubView.setVisibility(View.VISIBLE);
            tvNum.setVisibility(View.GONE);
            addAndSubView.setNum(twoShopDish.getCartNum());
            addAndSubView.setOnNumChangeListener(new AddAndSubView.OnNumChangeListener() {
                @Override
                public void onNumChange(View view, int num) {
                    if(menuChangeListener == null){
                        return;
                    }
                    menuChangeListener.onMenuChange(position,num);
                }
            });
        }
        if(twoShopDish.getCartNum()>0){
            rlBg.setBackgroundResource(R.drawable.btn_table_h);
        }else {
            rlBg.setBackgroundResource(R.drawable.btn_table);
        }

    }


}
