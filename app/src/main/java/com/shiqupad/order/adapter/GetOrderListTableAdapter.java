package com.shiqupad.order.adapter;

import android.content.Context;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.Collection;
import java.util.Date;

import cn.com.dreamtouch.magicbox_general_ui.adapter.BaseRecyclerAdapter;
import cn.com.dreamtouch.magicbox_general_ui.adapter.SmartViewHolder;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetOrderListResopnse;
import com.shiqupad.order.R;
import com.shiqupad.order.util.DateTimeUtils;
import com.shiqupad.order.util.MathUtil;

/**
 * Created by LuoXueKun on 2018/5/16.
 */

public class GetOrderListTableAdapter extends BaseRecyclerAdapter<GetOrderListResopnse.DataBean> {
    private Context mContext;
    private int selection = 0;

    public GetOrderListTableAdapter(Context context, Collection<GetOrderListResopnse.DataBean> collection,
                                    int layoutId, AdapterView.OnItemClickListener listener) {
        super(collection, layoutId, listener);
        mContext = context;
    }


    @Override
    protected void onBindViewHolder(SmartViewHolder holder, GetOrderListResopnse.DataBean data, int position) {
        holder.text(R.id.item_order_table_pos_tv, data.getShopSerialNumber() + "");
        holder.text(R.id.item_order_table_name_tv, data.getTableName());
        setPriceFlag(1,holder,data);

        holder.text(R.id.item_order_time_tv,
                DateTimeUtils.parseDate2(
                        DateTimeUtils.getStringDate("yyyy-MM-dd  HH:mm:ss", new Date(data.getCreateDate())),
                        mContext));
        LinearLayout llOrder = holder.itemView.findViewById(R.id.item_order_table_ll);
        if (selection == position) {
            llOrder.setBackgroundColor(mContext.getResources().getColor(R.color.table_select_h));
        } else {
            llOrder.setBackgroundColor(mContext.getResources().getColor(R.color.white));

        }
        TextView tvTableStatus = holder.itemView.findViewById(R.id.item_order_table_status_tv);
        int orderStatus = data.getOrderStatus();
        switch (orderStatus) {
            case 1:
                tvTableStatus.setText("待确认");
                tvTableStatus.setBackground(mContext.getResources().getDrawable(R.drawable.bg_unconfirmed));
                setPriceFlag(1,holder,data);
                break;
            case 2:
                tvTableStatus.setText("已确认");
                tvTableStatus.setBackground(null);
                setPriceFlag(1,holder,data);
                break;
            case 3:
                tvTableStatus.setText("已支付");
                tvTableStatus.setTextColor(mContext.getResources().getColor(R.color.color_clear));
                tvTableStatus.setBackground(mContext.getResources().getDrawable(R.drawable.bg_paid));
                setPriceFlag(1,holder,data);
                break;
            case 4:

                tvTableStatus.setTextColor(mContext.getResources().getColor(R.color.text_color_table_num));
                tvTableStatus.setText("已取消");
                tvTableStatus.setBackground(null);
                setPriceFlag(1,holder,data);
                break;
            case 5:
                setPriceFlag(1,holder,data);
                tvTableStatus.setText("已失效");
                tvTableStatus.setTextColor(mContext.getResources().getColor(R.color.text_color_table_num));
                tvTableStatus.setBackground(null);

                break;
            case 6:
                tvTableStatus.setBackground(null);

                tvTableStatus.setText("待支付");
                tvTableStatus.setTextColor(mContext.getResources().getColor(R.color.tab_select_h));
                setPriceFlag(6,holder,data);
                break;
            case 7:
                tvTableStatus.setTextColor(mContext.getResources().getColor(R.color.text_color_table_num));
                tvTableStatus.setBackground(null);
                setPriceFlag(1,holder,data);
                tvTableStatus.setText("支付失败");
                break;
            case 8:
                setPriceFlag(1,holder,data);
                tvTableStatus.setText("部分退单");
                tvTableStatus.setTextColor(mContext.getResources().getColor(R.color.text_color_table_num));
                tvTableStatus.setBackground(null);
                break;
            case 9:
                setPriceFlag(1,holder,data);
                tvTableStatus.setText("待确认");
                tvTableStatus.setBackground(mContext.getResources().getDrawable(R.drawable.bg_unconfirmed));
                break;
        }
    }

    public void setSelection(int selection) {
        this.selection = selection;
        notifyDataSetChanged();
    }
    private   void setPriceFlag(int flag,SmartViewHolder holder,GetOrderListResopnse.DataBean data){
        if(flag == 6){
            holder.text(R.id.item_order_table_price_tv, "￥  " +

                    MathUtil.decimalConvert(data.getShouldPayAmount() +data.getTableFee())
                    + "");
        }else {
            holder.text(R.id.item_order_table_price_tv, "￥  " +

                    MathUtil.decimalConvert(data.getShouldPayAmount()));
        }
    }
}
