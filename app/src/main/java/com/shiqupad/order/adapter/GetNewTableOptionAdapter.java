package com.shiqupad.order.adapter;

import android.content.Context;
import android.widget.AdapterView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.Collection;

import cn.com.dreamtouch.magicbox_general_ui.adapter.BaseRecyclerAdapter;
import cn.com.dreamtouch.magicbox_general_ui.adapter.SmartViewHolder;
import com.shiqupad.order.R;
import com.shiqupad.order.bean.TableOptionBean;
import com.shiqupad.order.util.DisplayIconUtil;

/**
 * Created by LuoXueKun on 2018/5/16.
 */

public class GetNewTableOptionAdapter extends BaseRecyclerAdapter<TableOptionBean> {
    public Context mContext;

    public GetNewTableOptionAdapter(Context context, Collection<TableOptionBean> collection,
                                    int layoutId, AdapterView.OnItemClickListener listener) {
        super(collection, layoutId, listener);
        this.mContext = context;
    }


    @Override
    protected void onBindViewHolder(SmartViewHolder holder, TableOptionBean data, int position) {

        holder.text(R.id.item_new_option_name_tv, data.getName());
        SimpleDraweeView iv = holder.itemView.findViewById(R.id.itm_new_option_iv);
        DisplayIconUtil.showIcon(mContext, position, iv);
    }


}
