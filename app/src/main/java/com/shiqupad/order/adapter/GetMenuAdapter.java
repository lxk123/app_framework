package com.shiqupad.order.adapter;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shiqupad.order.R;
import com.shiqupad.order.listener.ExitOrderListener;

import java.util.Collection;

import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_general_ui.adapter.BaseRecyclerAdapter;
import cn.com.dreamtouch.magicbox_general_ui.adapter.SmartViewHolder;
import cn.com.dreamtouch.magicbox_http_client.network.model.DetailListBean;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoResponse;

/**
 * Created by LuoXueKun on 2018/5/16.
 */

public class GetMenuAdapter extends BaseRecyclerAdapter<DetailListBean> {
    private ExitOrderListener exitOrderListener;
    public GetMenuAdapter(Context context, Collection<DetailListBean> collection,
                          int layoutId, AdapterView.OnItemClickListener listener, ExitOrderListener exitOrderListener) {
        super(collection, layoutId, listener);
        this.exitOrderListener = exitOrderListener;
    }


    @Override
    protected void onBindViewHolder(SmartViewHolder holder, final DetailListBean data, final int position) {
          holder.text(R.id.title_item_position_tv,position+1+"");
          holder.text(R.id.title_item_name_tv,data.getDishName());
          holder.text(R.id.title_item_count_tv,data.getDishNumber()+"份");
          holder.text(R.id.title_item_price_tv,"￥  "+data.getPrice()+"");
          holder.text(R.id.title_item_total_price_tv,data.getDishNumber()*data.getPrice()+"");
          TextView tvExit = holder.itemView.findViewById(R.id.title_item_exit_dish_tv);
          tvExit.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View v) {
                  if(exitOrderListener != null) {
                      exitOrderListener.exitOrder(position);
                  }
              }
          });
    }


}
