package com.shiqupad.order.adapter;

import android.content.Context;
import android.widget.AdapterView;
import android.widget.RelativeLayout;

import java.util.Collection;

import cn.com.dreamtouch.magicbox_general_ui.adapter.BaseRecyclerAdapter;
import cn.com.dreamtouch.magicbox_general_ui.adapter.SmartViewHolder;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import com.shiqupad.order.R;

/**
 * Created by LuoXueKun on 2018/5/16.
 */

public class GetSwitchTableAdapter extends BaseRecyclerAdapter<GetAllTableResponse.Data.ListBean> {
    private  int lastPressIndex = 0;
    public GetSwitchTableAdapter(Context context, Collection<GetAllTableResponse.Data.ListBean> collection,
                                 int layoutId, AdapterView.OnItemClickListener listener) {
        super(collection, layoutId, listener);
    }


    @Override
    protected void onBindViewHolder(SmartViewHolder holder, GetAllTableResponse.Data.ListBean data, int position) {
        RelativeLayout rlBg = holder.itemView.findViewById(R.id.item_switch_table_rl);
        holder.text(R.id.item_switch_table_name_tv, data.getTableName());
        holder.text(R.id.item_switch_table_num_tv, data.getDinningPeople() + "/" + data.getPropleNum());
        if(position == lastPressIndex){
            rlBg.setSelected(true);
        }else {
            rlBg.setSelected(false);
        }
    }


}
