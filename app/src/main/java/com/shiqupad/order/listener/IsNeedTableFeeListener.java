package com.shiqupad.order.listener;

import cn.com.dreamtouch.magicbox_general_ui.listener.BasePresentListener;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangePasswordResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.IsNeedTableFeeResponse;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public interface IsNeedTableFeeListener extends BasePresentListener {
    void isNeedTableFeeSuccess(IsNeedTableFeeResponse isNeedTableFeeResponse);
}
