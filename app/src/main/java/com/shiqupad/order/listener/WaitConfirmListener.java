package com.shiqupad.order.listener;

import cn.com.dreamtouch.magicbox_general_ui.listener.BasePresentListener;
import cn.com.dreamtouch.magicbox_http_client.network.model.CancelOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.WaitConfirmResponse;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public interface WaitConfirmListener extends BasePresentListener {
    void waitConfirmSuccess(WaitConfirmResponse waitConfirmResponse);
}
