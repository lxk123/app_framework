package com.shiqupad.order.listener;

import cn.com.dreamtouch.magicbox_general_ui.listener.BasePresentListener;
import cn.com.dreamtouch.magicbox_http_client.network.model.RetreatOrderResponse;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public interface RetreatOrderListener extends BasePresentListener {
    void retreatOrderSuccess(RetreatOrderResponse retreatOrderResopnse);
}
