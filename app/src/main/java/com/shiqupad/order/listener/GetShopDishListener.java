package com.shiqupad.order.listener;

import cn.com.dreamtouch.magicbox_general_ui.listener.BasePresentListener;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangeTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public interface GetShopDishListener extends BasePresentListener {
    void getShopDishSuccess(GetShopDishResponse getShopDishResponse);
}
