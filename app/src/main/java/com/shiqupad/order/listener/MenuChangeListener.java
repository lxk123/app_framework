package com.shiqupad.order.listener;

import android.view.ViewGroup;

public interface MenuChangeListener {
    void onMenuChange( int position, int num);

}
