package com.shiqupad.order.listener;

import cn.com.dreamtouch.magicbox_general_ui.listener.BasePresentListener;
import cn.com.dreamtouch.magicbox_http_client.network.model.CancelOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetMessageResponse;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public interface GetMessageListener extends BasePresentListener {
    void getMessageSuccess(GetMessageResponse  getMessageResponse);
}
