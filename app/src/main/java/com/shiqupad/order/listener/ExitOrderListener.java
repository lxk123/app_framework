package com.shiqupad.order.listener;

/**
 * Created by LuoXueKun
 * on 2018/6/4
 */
public interface ExitOrderListener {
    public void exitOrder(int position);
}
