package com.shiqupad.order.listener;

import cn.com.dreamtouch.magicbox_general_ui.listener.BasePresentListener;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.LoginResponse;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public interface GetAllTableListener extends BasePresentListener {
    void getAllTableSuccess(GetAllTableResponse getAllTableResponse);
}
