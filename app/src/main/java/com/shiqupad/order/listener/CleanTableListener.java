package com.shiqupad.order.listener;

import cn.com.dreamtouch.magicbox_general_ui.listener.BasePresentListener;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangeTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChearTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ClearTableRequest;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public interface CleanTableListener extends BasePresentListener {
    void clearTableSuccess(ChearTableResponse chearTableResponse);
}
