package com.shiqupad.order.activity;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.shiqupad.order.R;
import com.shiqupad.order.listener.GetMessageListener;
import com.shiqupad.order.presenter.GetMessagePresenter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_general_ui.activity.MBBaseActivity;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetMessageResponse;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.UserRemoteData;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import com.shiqupad.order.adapter.GetMessageAdapter;

/**
 * Created by LuoXueKun
 * on 2018/5/24
 */
public class GetMessageActivity extends MBBaseActivity implements GetMessageListener, AdapterView.OnItemClickListener {
    private GetMessagePresenter mGetMessagePresenter;
    private List<GetMessageResponse.MessageData> mMessageList;
    private List<GetMessageResponse.MessageData> mNewMessageList;
    private GetMessageAdapter mGetMessageAdapter;
    @Bind(R.id.refreshLayout)
    SmartRefreshLayout mSmartRefreshLayout;
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.cont_call_head_title_tv)
    TextView tvTitle;
    @Bind(R.id.iv_close)
    ImageView ivClose;
    int page = 1;
    //判断是下拉刷新还是上拉加载
    int refreshOrLoad = 0;
    private String mShopId;

    @Override
    protected void initVariables() {
        super.initVariables();
        mShopId = UserLocalData.getInstance(this).getShopID();
        mNewMessageList = new ArrayList<>();
        mMessageList = new ArrayList<>();
        mGetMessagePresenter = new GetMessagePresenter(UserRepository.getInstance(UserLocalData.getInstance(this)
                , UserRemoteData.getInstance(this)
        ), this);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        setContentView(R.layout.act_get_message);
        ButterKnife.bind(this);
        tvTitle.setText("消息");
        initRecyclerViewAndAdapter();
    }

    private void initRecyclerViewAndAdapter() {
        mGetMessageAdapter = new GetMessageAdapter(this, mMessageList, R.layout.item_message, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mGetMessageAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mSmartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshlayout) {
                refreshOrLoad = 1;
                mGetMessagePresenter.getMessage(mShopId, String.valueOf(page));
            }
        });
        mSmartRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(final RefreshLayout refreshlayout) {
                refreshOrLoad = 2;
                page++;
                mGetMessagePresenter.getMessage(mShopId, String.valueOf(page));
            }
        });
        mSmartRefreshLayout.setEnableRefresh(true);
        mSmartRefreshLayout.autoRefresh();
    }

    @Override
    protected void loadData() {
        super.loadData();
    }

    @Override
    public void getMessageSuccess(GetMessageResponse getMessageResponse) {
        mNewMessageList.clear();
        if (getMessageResponse.data != null && getMessageResponse.data.size() > 0) {
            mNewMessageList.addAll(getMessageResponse.data);
        }
        switch (refreshOrLoad) {
            case 1:
                page = 1;
                mMessageList.clear();
                mMessageList.addAll(mNewMessageList);
                mGetMessageAdapter.refresh(mNewMessageList);
                mSmartRefreshLayout.finishRefresh();
                break;
            case 2:
                mMessageList.addAll(mNewMessageList);
                mGetMessageAdapter.loadMore(mNewMessageList);
                mSmartRefreshLayout.finishLoadMore();
                break;
        }
    }

    @Override
    public void basicFailure(String prompt) {
        DTLog.showMessageShort(this, prompt);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
    @OnClick(R.id.iv_close)
    public void onClick(View view){
        switch (view.getId()){
            case R.id.iv_close:
                finish();
                overridePendingTransition(R.anim.activity_open,R.anim.activity_close);
                break;
        }
    }
}
