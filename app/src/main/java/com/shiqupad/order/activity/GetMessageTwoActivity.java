package com.shiqupad.order.activity;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.shiqupad.order.R;
import com.shiqupad.order.fragment.MessageFragment;
import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_general_ui.activity.MBBaseActivity;
import cn.com.dreamtouch.magicbox_general_ui.util.RxBusUtil;
import cn.com.dreamtouch.magicbox_http_client.network.model.RxEvent;

/**
 * Created by LuoXueKun
 * on 2018/5/24
 */
public class GetMessageTwoActivity extends MBBaseActivity implements OnTabSelectListener {
    @Bind(R.id.back)
    ImageView ivBack;
    @Bind(R.id.cont_call_head_title_tv)
    TextView tvTitle;
    @Bind(R.id.iv_close)
    ImageView ivClose;
    @Bind(R.id.cont_stl)
    SlidingTabLayout mSlidingTabLayout;
    @Bind(R.id.cont_viewpager)
    ViewPager mViewPager;


    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private List<String> mTitleList ;
    private MyPagerAdapter mAdapter;
    @Override
    protected void initVariables() {
        super.initVariables();
        mTitleList = new ArrayList<>();
        mTitleList.add("服务员消息");
        mTitleList.add("自助点餐");
        mTitleList.add("打印机提醒");
        mFragments.add(MessageFragment.newInstance(0));
        mFragments.add(MessageFragment.newInstance(1));
        mFragments.add(MessageFragment.newInstance(2));
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        setContentView(R.layout.act_get_two_message);
        ButterKnife.bind(this);
        tvTitle.setText("消息");
        initTabAndView();
    }

    @Override
    protected void loadData() {
        super.loadData();
        RxBusUtil.getDefault().post(new RxEvent(CommonConstant.OpertionType.HAS_READ_MESSAGE,
                CommonConstant.OpertionType.HAS_READ_MESSAGE));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    private void initTabAndView() {
        mAdapter = new MyPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setOnTabSelectListener(this);
        mViewPager.setCurrentItem(0);
        mViewPager.setOffscreenPageLimit(mTitleList.size());
    }

    @Override
    public void onTabSelect(int position) {

    }

    @Override
    public void onTabReselect(int position) {

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        private MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);
        }

        @Override
        public Fragment getItem(int position) {


            return mFragments.get(position);

        }
    }
   @OnClick(R.id.back)
    public void onClick(View view){
        switch (view.getId()){
            case R.id.back:
                finish();
                //overridePendingTransition(R.anim.activity_open,R.anim.activity_close);
                break;
        }
   }
}
