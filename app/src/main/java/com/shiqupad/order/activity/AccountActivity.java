package com.shiqupad.order.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.shiqupad.order.R;
import com.shiqupad.order.adapter.OrderDetailActivityAdapter;
import com.shiqupad.order.adapter.RechargeAdapter;
import com.shiqupad.order.listener.GetAllPayMethodListener;
import com.shiqupad.order.listener.GetCustomOrderInfoListener;
import com.shiqupad.order.listener.GetShopDishListener;
import com.shiqupad.order.listener.PayOrderListener;
import com.shiqupad.order.listener.PrintOrderListener;
import com.shiqupad.order.listener.PushIsOnOffListener;
import com.shiqupad.order.presenter.GetAllPayMethodPresenter;
import com.shiqupad.order.presenter.GetCustomOrderInfoPresenter;
import com.shiqupad.order.presenter.GetShopDishPresenter;
import com.shiqupad.order.presenter.PayOrderPresenter;
import com.shiqupad.order.presenter.PrintOrderPresenter;
import com.shiqupad.order.presenter.PushIsOnOffPresenter;
import com.shiqupad.order.ui.ModifyPriceDialog;
import com.shiqupad.order.ui.SelectCouponsDialog;
import com.shiqupad.order.util.DateTimeUtils;
import com.shiqupad.order.util.MathUtil;
import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_general_ui.activity.MBBaseActivity;
import cn.com.dreamtouch.magicbox_general_ui.activity.ScanCodeActivity;
import cn.com.dreamtouch.magicbox_general_ui.ui.MyListView;
import cn.com.dreamtouch.magicbox_general_ui.util.RxBusUtil;
import cn.com.dreamtouch.magicbox_http_client.network.model.DetailListBean;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllPayMethodResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayParam;
import cn.com.dreamtouch.magicbox_http_client.network.model.PrintOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushIsOnOffResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.RxEvent;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.UserRemoteData;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import io.reactivex.functions.Consumer;

import static cn.com.dreamtouch.magicbox_common.util.CommonConstant.PayMethod.PAY_TYPE_01;
import static cn.com.dreamtouch.magicbox_common.util.CommonConstant.PayMethod.PAY_TYPE_02;
import static cn.com.dreamtouch.magicbox_common.util.CommonConstant.PayMethod.PAY_TYPE_03;
import static cn.com.dreamtouch.magicbox_common.util.CommonConstant.PayMethod.PAY_TYPE_05;
import static cn.com.dreamtouch.magicbox_common.util.CommonConstant.PayMethod.PAY_TYPE_06;

/**
 * Created by LuoXueKun
 * on 2018/6/4
 */
public class AccountActivity extends MBBaseActivity implements
        GetAllPayMethodListener, PushIsOnOffListener,
        GetCustomOrderInfoListener, SelectCouponsDialog.onSelectCouponseListerner, PayOrderListener, PrintOrderListener, ModifyPriceDialog.OnModifyFinalPriceListener, GetShopDishListener {
    @Bind(R.id.fr_order_shopcart_tv)
    TextView tvShopCart;
    @Bind(R.id.fr_order_tab_fee_tv)
    TextView tvTabFee;
    @Bind(R.id.fr_order_total_price_tv)
    TextView tvTotalPrice;
    @Bind(R.id.cont_call_head_title_tv)
    TextView tvTitle;
    @Bind(R.id.iv_close)
    ImageView ivClose;

    @Bind(R.id.cont_fr_order_detail_tableName_tv)
    TextView tvTableName;
    @Bind(R.id.cont_fr_order_detail_num_tv)
    TextView tvPeopleNum;
    @Bind(R.id.cont_fr_order_detail_serialNum_tv)
    TextView tvOrderNum;
    @Bind(R.id.cont_fr_order_detail_waiter_tv)
    TextView tvWaiter;
    @Bind(R.id.cont_fr_order_detail_createTime_tv)
    TextView tvCreateTime;
    @Bind(R.id.cont_fr_order_detail_tableStatus_tv)
    TextView tvTableStatus;
    @Bind(R.id.cont_account_wait_price_tv)
    TextView tvWaitPrice;
    @Bind(R.id.cont_account_right_use_tv)
    TextView tvUseConpous;
    @Bind(R.id.pay_sure_tv2)
    TextView tvPaySure;
    @Bind(R.id.pay_print_tv2)
    TextView tvPayPrint;
    @Bind(R.id.cont_account_right_rl)
    RelativeLayout rlSelectCouns;
    RechargeAdapter rechargeAdapter;
    @Bind(R.id.cont_account_right_rv)
    RecyclerView recyclerView;

    @Bind(R.id.lv_order_detail)
    MyListView myListView;
    @Bind(R.id.cont_fr_order_detail_orderNum_tv)
    TextView tvOrderPeople;
    @Bind(R.id.modify)
    TextView tvModify;
    GetAllPayMethodPresenter mGetAllPayMethodPresenter;
    private GetCustomOrderInfoPresenter mGetCustomOrderInfoPresenter;
    private PrintOrderPresenter mPrintOrderPresenter;
    private PayOrderPresenter mPayOrderPresenter;

    private PushIsOnOffPresenter mPushIsOnOffPresenter;
    private String fwyXJSK;
    private String mShopId;
    private String orderId;
    private List<GetCustomOrderInfoResponse.DataBean.VoucherVOListBean> mVoucherVOList;
    private SelectCouponsDialog mSelectCouponsDialog;
    private ModifyPriceDialog mModifyPriceDialog;
    private String peopleNum;
    private String tableID;
    private String voucherCode;
    private String voucherMoney;
    private double tableFee;
    private String paymentType;
    private String paymentMethodCode;
    private double totalPrice;
    private double finalTotalPrice;
    private double mustPrice;
    List<GetAllPayMethodResponse.DataBean> data;
    private ArrayList<PayParam.PayDetailParam> payDetailParams = new ArrayList<>();
    List<DetailListBean> list;
    OrderDetailActivityAdapter adapter;
    private String jsonMustOrders;
    private List<GetShopDishResponse.TwoShopDish> mustOrderDishs = new ArrayList<>();
    private GetShopDishPresenter mGetShopDishPresenter;

    @Override
    protected void initVariables() {
        super.initVariables();
        data = new ArrayList<>();
        mVoucherVOList = new ArrayList<>();
        peopleNum = getIntent().getStringExtra(CommonConstant.Args.DINNER_NUM);
        orderId = getIntent().getStringExtra(CommonConstant.Args.ORDER_ID);
        //jsonMustOrders = getIntent().getStringExtra(CommonConstant.Args.MUST_DISHS);


        mShopId = UserLocalData.getInstance(this).getShopID();
        mPushIsOnOffPresenter = new PushIsOnOffPresenter(UserRepository.getInstance(UserLocalData.
                        getInstance(this)
                , UserRemoteData.getInstance(this)), this);
        mGetAllPayMethodPresenter = new GetAllPayMethodPresenter(UserRepository.getInstance(UserLocalData.getInstance(this)
                , UserRemoteData.getInstance(this)), this);
        mGetCustomOrderInfoPresenter = new GetCustomOrderInfoPresenter(UserRepository.getInstance(UserLocalData.getInstance(this)
                , UserRemoteData.getInstance(this)), this);
        mPayOrderPresenter = new PayOrderPresenter(UserRepository.getInstance(UserLocalData.getInstance(this)
                , UserRemoteData.getInstance(this)), this);
        mPrintOrderPresenter = new PrintOrderPresenter(UserRepository.getInstance(UserLocalData.getInstance(this)
                , UserRemoteData.getInstance(this)), this);
        mGetShopDishPresenter = new GetShopDishPresenter(UserRepository.getInstance(UserLocalData.getInstance(this)
                , UserRemoteData.getInstance(this)
        ), this);

    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        setContentView(R.layout.activity_account);
        ButterKnife.bind(this);
        initRecyclerView();
        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishCurrentActivity();
            }
        });
    }

    @Override
    protected void loadData() {
        mGetShopDishPresenter.getShopDish();
        mPushIsOnOffPresenter.pullIsOnOff(UserLocalData.getInstance(this).getEmployeeID());

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGetAllPayMethodPresenter.getAllPayMethod();
        tvTitle.setText("结账");
    }

    @OnClick({R.id.cont_account_right_rl, R.id.iv_close, R.id.pay_sure_tv2,
            R.id.pay_print_tv2, R.id.modify})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cont_account_right_rl:
                selectCouponusDialog();
                break;
           /* case R.id.iv_close:
                finishCurrentActivity();

                break;*/
            case R.id.pay_sure_tv2:
                mLogger.e("paysuretv");
                generatePayParam();
                break;
            case R.id.pay_print_tv2:
                printOrder();
                break;
            case R.id.modify:

                if (!PAY_TYPE_01.equals(paymentType) &
                        !PAY_TYPE_06.equals(paymentType)) {
                    DTLog.showMessageShort(this,"该付款方式暂不可修改金额");
                    return;
                }

                modifyFinalMoneyDialog();
                break;
        }
    }

    private void finishCurrentActivity() {
        tvPayPrint.setEnabled(true);
        tvPayPrint.getBackground().setAlpha(255);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                finish();
            }
        }, 500);
    }

    private void generatePayParam() {
        payDetailParams.clear();
        if (PAY_TYPE_01.equals(paymentType) ||
                PAY_TYPE_06.equals(paymentType)
                ) {
            PayParam.PayDetailParam detailParam = new PayParam.PayDetailParam(paymentType,
                    Double.parseDouble(tvTotalPrice.getText().toString()),
                    null, null, null, null, 1,
                    paymentMethodCode);
            payDetailParams.add(detailParam);
            confirmPay();
        } else {
            openCameraWithPermission();
        }

    }

    private void confirmPay() {
        if(mShopId == null || orderId == null || tableID == null ){
            return;
        }
        PayParam payParam = new PayParam(Integer.valueOf(mShopId), Integer.valueOf(orderId), Integer.valueOf(tableID),
                Double.parseDouble(tvTotalPrice.getText().toString()),
                voucherCode,
                (voucherCode == null) ? 0 : Double.parseDouble(voucherMoney), String.valueOf(tableFee));
        payParam.setPaymentList(payDetailParams);
        mPayOrderPresenter.payOrder(payParam);
    }

    private void selectCouponusDialog() {
        if (mSelectCouponsDialog == null) {
            mSelectCouponsDialog = new SelectCouponsDialog(this, mVoucherVOList, totalPrice);
            mSelectCouponsDialog.setOnSelectCounponseListener(this);
        }
        mSelectCouponsDialog.show();
    }

    private void modifyFinalMoneyDialog() {
        if (mModifyPriceDialog == null) {
            mModifyPriceDialog = new ModifyPriceDialog(this, mVoucherVOList, totalPrice);
            mModifyPriceDialog.setOnModifyFinalPrice(this);
        }
        mModifyPriceDialog.show();
    }

    @Override
    public void getAllPayMethodSuccess(GetAllPayMethodResponse getAllPayMethodResponse) {
        List<GetAllPayMethodResponse.DataBean> newDataList = getAllPayMethodResponse.getData();


        for (int i = 0; i < newDataList.size(); i++) {
            if ("N".equals(newDataList.get(i).getIsEnable())) {
                newDataList.remove(i);
            } else {
                getAllPayMethodResponse.getData().get(i).setPaymentType(PAY_TYPE_06);
            }
        }
        if (!TextUtils.isEmpty(fwyXJSK) && "1".equals(fwyXJSK)) {

        }
        newDataList.add(new GetAllPayMethodResponse.DataBean("现金支付", PAY_TYPE_01));
        newDataList.add(new GetAllPayMethodResponse.DataBean("微信会员支付", PAY_TYPE_05));
        newDataList.add(new GetAllPayMethodResponse.DataBean("微信", PAY_TYPE_03));
        newDataList.add(new GetAllPayMethodResponse.DataBean("支付宝", PAY_TYPE_02));
        data.clear();
        data.addAll(newDataList);


        rechargeAdapter.replaceAll(data);
        paymentType = getAllPayMethodResponse.getData().get(0).getPaymentType();
        paymentMethodCode = getAllPayMethodResponse.getData().get(0).getPaymentMethodCode();
    }

    @Override
    public void basicFailure(String prompt) {
        DTLog.showMessageShort(this, prompt);
    }

    private void initRecyclerView() {
        recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
        rechargeAdapter = new RechargeAdapter(this);
        recyclerView.setAdapter(rechargeAdapter);
        rechargeAdapter.setOnItemClickListener(new RechargeAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int pos) {

                paymentType = data.get(pos).getPaymentType();
                paymentMethodCode = data.get(pos).getPaymentMethodCode();
                if (!PAY_TYPE_01.equals(paymentType) &
                        !PAY_TYPE_06.equals(paymentType)) {
                    tvWaitPrice.setText(String.format("%s", voucherMoney ==null?totalPrice:
                            MathUtil.decimalConvert(totalPrice-Double.parseDouble(voucherMoney))));
                    tvTotalPrice.setText(String.format("%s",voucherMoney==null?totalPrice:
                            MathUtil.decimalConvert(totalPrice-Double.parseDouble(voucherMoney))));

                }
            }
        });
    }

    @Override
    public void pushIsOnOffSuccess(PushIsOnOffResponse pushIsOnOffResponse) {
        fwyXJSK = pushIsOnOffResponse.getData().getFwyXJSK();
    }

    @Override
    public void getCustomOrderInfoSuccess(GetCustomOrderInfoResponse getCustomOrderInfoResponse) {

        tableID = getCustomOrderInfoResponse.getData().getTableID();
        tableFee = getCustomOrderInfoResponse.getData().getTableFee();
        mVoucherVOList.clear();
        List<GetCustomOrderInfoResponse.DataBean.VoucherVOListBean> voucherVOList = getCustomOrderInfoResponse.getData().getVoucherVOList();
        mVoucherVOList.addAll(voucherVOList);

        initStatus(getCustomOrderInfoResponse.getData());
        initRightListView(getCustomOrderInfoResponse.getData());
        initTextView(getCustomOrderInfoResponse);


    }

    private void initRightListView(GetCustomOrderInfoResponse.DataBean data) {
        list = new ArrayList<>();
        list.clear();
        list.addAll(data.getCustomorderdetailDOList());

        //switchToDetailListBean(mustOrderDishs);
        // list.addAll(mustOrderDishs);
        if (adapter == null) {
            adapter = new OrderDetailActivityAdapter(this,
                    list);
        } else {
            adapter.setData(list);
        }
        myListView.setAdapter(adapter);
    }

    private void switchToDetailListBean(List<GetShopDishResponse.TwoShopDish> mustOrderDishs) {
        for (int i = 0; i < mustOrderDishs.size(); i++) {
            GetShopDishResponse.TwoShopDish twoShopDish = mustOrderDishs.get(i);
            DetailListBean detailListBean = new DetailListBean();
            detailListBean.setDishName(twoShopDish.getDishName());
            detailListBean.setPrice(twoShopDish.getPrice());
            detailListBean.setFeedingJson(JSON.toJSONString(twoShopDish.getFeedList()));
            detailListBean.setDishNumber(Integer.valueOf(peopleNum));
            mustPrice += twoShopDish.getPrice() * Integer.valueOf(peopleNum);
            detailListBean.setSumPrice(twoShopDish.getPrice() * Integer.valueOf(peopleNum));
            list.add(detailListBean);
        }
    }

    private void initTextView(GetCustomOrderInfoResponse getCustomOrderInfoResponse) {
        GetCustomOrderInfoResponse.DataBean data = getCustomOrderInfoResponse.getData();
        tvTableName.setText(data.getTableName());
        tvPeopleNum.setText(peopleNum);
        tvWaiter.setText("");
        tvOrderNum.setText(data.getSerialNumber());
        tvCreateTime.setText(DateTimeUtils.parseDate2(
                DateTimeUtils.getStringDate("yyyy-MM-dd  HH:mm:ss", new Date(data.getCreateDate())),
                this));
        totalPrice = MathUtil.decimalConvert(data.getShouldPayAmount() +
                data.getTableFee() + mustPrice);
        finalTotalPrice = totalPrice;
        //菜品合计
        tvShopCart.setText(String.format("菜品合计：  %s", MathUtil.decimalConvert(data.getShouldPayAmount()
                + mustPrice)));
        //开台费
        tvTabFee.setText(String.format("开台费：  %s", data.getTableFee()));
        //总计
        tvTotalPrice.setText(String.format("%s", totalPrice));
        tvWaitPrice.setText(String.format("%s", totalPrice));
        tvOrderPeople.setText(data.getCreateUserName());
    }

    @Override
    public void onSelectCouponse(String newNum, int selectPos) {
        voucherCode = mVoucherVOList.get(selectPos).getVoucherCode();

        GetCustomOrderInfoResponse.DataBean.VoucherVOListBean voucherVOListBean = mVoucherVOList.get(selectPos);
        tvUseConpous.setText(String.format("%s    金额   %s", voucherVOListBean.getVoucherName(), newNum));
        voucherMoney = newNum;
        if (TextUtils.isEmpty(newNum)) {
            return;
        }
        finalTotalPrice = MathUtil.decimalConvert(totalPrice - Double.parseDouble(newNum));
        tvWaitPrice.setText(String.format("%s", finalTotalPrice));
        tvTotalPrice.setText(String.format("%s", finalTotalPrice));
    }

    @Override
    public void payOrderSuccess(PayOrderResponse payorderResponse) {
        DTLog.showMessageShort(this, "支付成功");
        // showPaySuccessDialog("支付成功");
       // tvPayPrint.setVisibility(View.VISIBLE);
        tvPaySure.setVisibility(View.GONE);

        RxBusUtil.getDefault().post(new RxEvent(CommonConstant.OpertionType.PAY_SUCCESS,
                CommonConstant.OpertionType.PAY_SUCCESS));
        finishCurrentActivity();
    }

    @SuppressLint("CheckResult")
    @TargetApi(23)
    private void openCameraWithPermission() {
        RxPermissions rxPermission = new RxPermissions(this);
        rxPermission.requestEach(
                Manifest.permission.CAMERA
        ).subscribe(new Consumer<Permission>() {
            @Override
            public void accept(Permission permission) throws Exception {
                if (permission.granted) {
                    // 用户已经同意该权限
                    mLogger.e(permission.granted);
                    goToScanActivity();
                } else if (permission.shouldShowRequestPermissionRationale) {
                    mLogger.e(permission.shouldShowRequestPermissionRationale);
                    // 用户拒绝了该权限，没有选中『不再询问』（Never ask again）,那么下次再次启动时，还会提示请求权限的对话框
                } else {
                    mLogger.e("用户拒绝了该权限，并且选中『不再询问』");
                    DTLog.showMessageShort(AccountActivity.this,"请到应用设置中打开相机权限");

                    // 用户拒绝了该权限，并且选中『不再询问』
                }
            }
        });
    }

    private void goToScanActivity() {
        //假如你要用的是fragment进行界面的跳转
        /*IntentIntegrator intentIntegrator = IntentIntegrator.forSupportFragment(TableScanFragment.this).
                setCaptureActivity(ScanCodeActivity.class);*/
        IntentIntegrator intentIntegrator = new IntentIntegrator(this);
        intentIntegrator
                .setDesiredBarcodeFormats(IntentIntegrator.ALL_CODE_TYPES)
                .setPrompt("将二维码/条码放入框内，即可自动扫描")//写那句提示的话
                .setOrientationLocked(false)//扫描方向固定
                .setCaptureActivity(ScanCodeActivity.class) // 设置自定义的activity是CustomActivity
                .initiateScan(); // 初始化扫描
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mLogger.e("onActivityResult");
        IntentResult intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (intentResult != null) {
            if (intentResult.getContents() == null) {

            } else {
                // ScanResult 为获取到的字符串
                String scanResult = intentResult.getContents();
                PayParam.PayDetailParam detailParam = new PayParam.PayDetailParam(paymentType,
                        Double.parseDouble(tvTotalPrice.getText().toString()),
                        "BARCODE", null, null, null, 1,
                        paymentMethodCode);
                detailParam.setAuthCode(scanResult);
                payDetailParams.add(detailParam);
                confirmPay();

                // code = scanResult.substring(scanResult.length() - 6, scanResult.length());
                // requestGetTableByID(code);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void showPaySuccessDialog(String title) {
        if (!Objects.requireNonNull(this).isFinishing()) {
            //DTLog.showMessage(this,"支付成功");

            new cn.com.dreamtouch.magicbox_general_ui.ui.AlertDialog(this).builder()
                    .setTitle(title)
                    .setMsg("")
                    .setCancelable(true)
                    .setPositiveButton("确认", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //printOrder();
                        }
                    })
                    .setNegativeButton("取消", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }).show();
        }
    }

    private void printOrder() {
        mPrintOrderPresenter.reprintOrder(orderId, mShopId);

    }

    @Override
    public void printOrderSuccess(PrintOrderResponse printOrderResponse) {
        tvPayPrint.setEnabled(false);
        tvPayPrint.getBackground().setAlpha(50);
        DTLog.showMessageShort(this, "小票打印成功");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishCurrentActivity();
    }

    private void initStatus(GetCustomOrderInfoResponse.DataBean dataBean) {


        int orderStatus = dataBean.getOrderStatus();
        switch (orderStatus) {
            case 1:
                tvTableStatus.setText("待确认");
                break;
            case 2:
                tvTableStatus.setText("已确认");
                break;
            case 3:
                tvTableStatus.setText("已支付");

                break;
            case 4:
                tvTableStatus.setText("已取消");
                break;
            case 5:
                tvTableStatus.setText("已失效");

                break;
            case 6:
                tvTableStatus.setText("待支付");
                break;
            case 7:
                tvTableStatus.setText("支付失败");
                break;
            case 8:
                tvTableStatus.setText("部分退单");
                break;
            case 9:
                tvTableStatus.setText("待确认");
                break;
        }

    }

    @Override
    public void onModifyFinalPrice(String newNum) {
        if (mModifyPriceDialog != null) {
            mModifyPriceDialog.dismiss();
            mModifyPriceDialog = null;
        }
        finalTotalPrice = Double.parseDouble(newNum);
        tvTotalPrice.setText(finalTotalPrice + "");
        tvWaitPrice.setText(finalTotalPrice + "");
    }

    @Override
    public void getShopDishSuccess(GetShopDishResponse getShopDishResponse) {
        List<GetShopDishResponse.OneShopDish> data = getShopDishResponse.getData();
        for (int i = 0; i < data.size(); i++) {
            GetShopDishResponse.OneShopDish oneShopDish = data.get(i);
            List<GetShopDishResponse.TwoShopDish> dishDishList = oneShopDish.getDishList();
            for (int j = 0; j < dishDishList.size(); j++) {
                GetShopDishResponse.TwoShopDish twoShopDish = dishDishList.get(j);
                if (twoShopDish.getIsMustOrder().equals("1")) {
                    mustOrderDishs.add(twoShopDish);
                }
            }

        }
        mGetCustomOrderInfoPresenter.getCustomOrderInfo(mShopId, orderId);
    }
}
