package com.shiqupad.order.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.alibaba.fastjson.JSON;
import com.shiqupad.order.R;
import com.shiqupad.order.adapter.GetMenuAdapter;
import com.shiqupad.order.listener.CleanTableListener;
import com.shiqupad.order.listener.ExitOrderListener;
import com.shiqupad.order.listener.GetCustomOrderInfoListener;
import com.shiqupad.order.listener.PushIsOnOffListener;
import com.shiqupad.order.listener.RetreatOrderListener;
import com.shiqupad.order.presenter.CleanTablePresenter;
import com.shiqupad.order.presenter.GetCustomOrderInfoPresenter;
import com.shiqupad.order.presenter.PushIsOnOffPresenter;
import com.shiqupad.order.presenter.RetreatOrderPresenter;
import com.shiqupad.order.ui.RefundDishDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_general_ui.activity.MBBaseActivity;
import cn.com.dreamtouch.magicbox_general_ui.util.MyLogger;
import cn.com.dreamtouch.magicbox_general_ui.util.RxBusUtil;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChearTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.DetailListBean;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushIsOnOffResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.RefundDishes;
import cn.com.dreamtouch.magicbox_http_client.network.model.RetreatOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.RxEvent;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.UserRemoteData;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;

/**
 * Created by LuoXueKun
 * on 2018/5/28
 */
public class MenuActivity extends MBBaseActivity implements GetCustomOrderInfoListener, AdapterView.OnItemClickListener, ExitOrderListener, RetreatOrderListener, RefundDishDialog.IRefundDishDialogListener, PushIsOnOffListener, CleanTableListener {

    @Bind(R.id.recyclerview)
    RecyclerView mRecyclerView;
    private CleanTablePresenter mCleanTablePresenter;

    GetCustomOrderInfoPresenter mGetCustomOrderInfoPresenter;
    RetreatOrderPresenter mRetreatOrderPresenter;
    RefundDishDialog mRefundDishDialog;
    private PushIsOnOffPresenter mPushIsOnOffPresenter;
    @Bind(R.id.cont_iv)
    ImageView contIv;
    @Bind(R.id.cont_call_head_title_tv)
    TextView contTv;
    @Bind(R.id.iv_close)
    ImageView contCloseIv;
    private String mShopId;
    private String orderID;
    private String customOrderId;
    private String mTableId;
    private List<DetailListBean> list;
    private GetMenuAdapter mGtMenuAdapter;
    private DetailListBean detailListBean;

    @Override
    protected void initVariables() {
        super.initVariables();
        orderID = getIntent().getStringExtra(CommonConstant.Args.ORDER_ID);
        mTableId = getIntent().getStringExtra(CommonConstant.Args.TABLE_ID);
        mShopId = UserLocalData.getInstance(this).getShopID();
        mGetCustomOrderInfoPresenter = new GetCustomOrderInfoPresenter(UserRepository.getInstance
                (UserLocalData.getInstance(this), UserRemoteData.getInstance(this)), this);
        mRetreatOrderPresenter = new RetreatOrderPresenter(UserRepository.getInstance
                (UserLocalData.getInstance(this), UserRemoteData.getInstance(this)), this);

        mPushIsOnOffPresenter = new PushIsOnOffPresenter(UserRepository.getInstance(UserLocalData.getInstance(this)
                , UserRemoteData.getInstance(this)), this);
        mCleanTablePresenter = new CleanTablePresenter(UserRepository.getInstance(UserLocalData.getInstance(this)
                , UserRemoteData.getInstance(this)), this);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        setContentView(R.layout.act_menu);
        ButterKnife.bind(this);
        initHeadView();
        initRecyclerViewAndAdapter();

    }

    private void initHeadView() {
        contTv.setText("菜单");
    }

    @Override
    protected void loadData() {
        super.loadData();
        mGetCustomOrderInfoPresenter.getCustomOrderInfo(mShopId, orderID);

    }


    private void initRecyclerViewAndAdapter() {

        list = new ArrayList<>();
        mGtMenuAdapter = new GetMenuAdapter(this, list, R.layout.title_item_layout2, this, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(mGtMenuAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
    }


    @Override
    public void getCustomOrderInfoSuccess(GetCustomOrderInfoResponse getCustomOrderInfoResponse) {
        GetCustomOrderInfoResponse.DataBean data = getCustomOrderInfoResponse.getData();
        customOrderId = String.valueOf(data.getCustomOrderID());
        List<DetailListBean> customorderdetailDOList = data.getCustomorderdetailDOList();
        List<DetailListBean> fitterList = new ArrayList<>();
        for (int i = 0; i < customorderdetailDOList.size(); i++) {
            DetailListBean detailListBean = customorderdetailDOList.get(i);
            if (detailListBean.getDetailType() != 2 && detailListBean.getDishStatus() != 5) {
                fitterList.add(detailListBean);
            }
        }
        list.clear();
        list.addAll(fitterList);
        mGtMenuAdapter.refresh(list);

        if(list != null &&list.size() <= 0) {
            mCleanTablePresenter.cleanTable(mTableId,mShopId);
        }
    }

    @Override
    public void basicFailure(String prompt) {
        DTLog.showMessageShort(this, prompt);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @OnClick({R.id.iv_close})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_close:
                finish();
                break;
        }
    }

    @Override
    public void exitOrder(int position) {
        MyLogger.kLog().e(position);
        detailListBean = list.get(position);
        mPushIsOnOffPresenter.pullIsOnOff(UserLocalData.getInstance(this).getEmployeeID());

    }

    public void checkIsPushDialog(PushIsOnOffResponse pushIsOnOffResponse) {
        PushIsOnOffResponse.DataBean data = pushIsOnOffResponse.getData();

        if (TextUtils.isEmpty(orderID) || orderID.equals("0")) {
            DTLog.showMessageShort(this, "暂无订单");
            return;
        }
        if (!"1".equals(data.getFwyXD())) {
            DTLog.showMessageShort(this, "您没有退菜权限");
            return;
        }
        if (mRefundDishDialog == null) {
            mRefundDishDialog = new RefundDishDialog(this, detailListBean);
        }
        mRefundDishDialog.setIRefundDishDialogListener(this);
        mRefundDishDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mRefundDishDialog = null;
            }
        });
        mRefundDishDialog.show();
    }

    @Override
    public void retreatOrderSuccess(RetreatOrderResponse retreatOrderResopnse) {
        loadData();


    }

    //生成退菜单参数
    private RefundDishes generateParam(DetailListBean dish, String num, String cause) {
        RefundDishes.DetailListBean detailListBean = new RefundDishes.DetailListBean();
        detailListBean.setCustomOrderDetailID(dish.getCustomOrderDetailID());
        detailListBean.setDishID(dish.getDishID());
        detailListBean.setDishNumber(Integer.valueOf(num));
        detailListBean.setRemark(cause);

        List<RefundDishes.DetailListBean> detailList = new ArrayList<>();
        detailList.add(detailListBean);

        RefundDishes refundDishes = new RefundDishes();
        refundDishes.setOrderID(customOrderId+ "");
        refundDishes.setShopID(mShopId + "");
        refundDishes.setTableID(mTableId + "");
        refundDishes.setDetailList(detailList);

        Log.d("aaa", "tuicai" + JSON.toJSONString(refundDishes));
        return refundDishes;

    }

    @Override
    public void onBtnConfirmClick(DetailListBean dish, String num, String cause) {
        if (mRefundDishDialog != null) {
            mRefundDishDialog.dismiss();

        }
        RefundDishes refundDishes = generateParam(dish, num, cause);

        mRetreatOrderPresenter.retreatOrder(refundDishes);
    }

    @Override
    public void pushIsOnOffSuccess(PushIsOnOffResponse pushIsOnOffResponse) {
        checkIsPushDialog(pushIsOnOffResponse);
    }

    @Override
    public void clearTableSuccess(ChearTableResponse chearTableResponse) {

    }
}
