package com.shiqupad.order.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import java.util.concurrent.TimeUnit;

import cn.com.dreamtouch.magicbox_general_ui.activity.MBBaseActivity;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class SplashActivity extends MBBaseActivity  {
    private static final String TAG = "SplashActivity";
    private  String mSid;
    @Override
    protected void initVariables() {
        super.initVariables();
        mSid = UserLocalData.getInstance(this).getSid();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(TextUtils.isEmpty(mSid)){
            enterActivity(LoginActivity.class);

        }else {
            enterActivity(MainActivity.class);
        }
    }
   public void enterActivity(final Class clazz){
       Observable.timer(1, TimeUnit.SECONDS).subscribe(new Observer<Long>() {
           @Override
           public void onSubscribe(Disposable d) {

           }

           @Override
           public void onNext(Long aLong) {
               goToAtivity(clazz,new Bundle());
               finish();
           }

           @Override
           public void onError(Throwable e) {

           }

           @Override
           public void onComplete() {

           }
       });

   }
}
