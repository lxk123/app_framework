package com.shiqupad.order.activity;

import android.app.Dialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.shiqupad.order.R;
import com.shiqupad.order.listener.LoginListener;
import com.shiqupad.order.presenter.LoginPresenter;
import com.shiqupad.order.util.XunfeiSoundPlay;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_common.util.EncryptUtil;
import cn.com.dreamtouch.magicbox_general_ui.activity.MBBaseActivity;
import cn.com.dreamtouch.magicbox_general_ui.ui.LoadingDialog;
import cn.com.dreamtouch.magicbox_http_client.network.model.LoginResponse;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.UserRemoteData;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;

/**
 * Created by LuoXueKun
 * on 2018/5/30
 */
public class LoginActivity extends MBBaseActivity implements LoginListener {
    @Bind(R.id.act_login_et)
    EditText mEtLogin;
    @Bind(R.id.act_login_pwd)
    EditText mEtPwd;
    @Bind(R.id.act_login_tv)
    TextView mTvLogin;
    LoginPresenter mLoginPresenter;
    LoginResponse.Data data;
    Dialog dialog;
    private String mShopId;
    @Override
    protected void initVariables() {
        super.initVariables();
        mLoginPresenter = new LoginPresenter(UserRepository.getInstance(UserLocalData.getInstance(this)
                , UserRemoteData.getInstance(this)), this);
    }

    @Override
    protected void initView(Bundle savedInstanceState) {
        super.initView(savedInstanceState);
        setContentView(R.layout.act_login);
        ButterKnife.bind(this);
    }

    @Override
    protected void loadData() {
        super.loadData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @OnClick({R.id.act_login_tv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.act_login_tv:
                login();
                break;
        }
    }

    private void login() {

       String userName = mEtLogin.getText().toString();
        String pwd = mEtPwd.getText().toString();
        //String userName ="18627911512";
        //String pwd = "123456";
        if (TextUtils.isEmpty(userName) || TextUtils.isEmpty(pwd)) {
            DTLog.showMessageShort(this, "请输入用户名或密码");
            return;
        }
        LoadingDialog.Builder builder1 = new LoadingDialog.Builder(LoginActivity.this)
                .setMessage("登录中...")
                .setCancelable(false);
        dialog = builder1.create();
        dialog .show();
        mLoginPresenter.login(userName,  EncryptUtil.MD5(pwd + "_EatFunny"));
    }

    @Override
    public void loginSuccess(LoginResponse loginResponse) {
        if(dialog != null) {
            dialog.dismiss();
        }
        data = loginResponse.getData();
        mShopId = String.valueOf(data.getShopID());


        setTagAndAlias();
        UserLocalData.getInstance(this).setSid(data.getSid());
        UserLocalData.getInstance(this).setEmployeeID(data.getEmployeeID());
        UserLocalData.getInstance(this).setShopID(data.getShopID() + "");
        UserLocalData.getInstance(this).setShopType(data.getShopType()+"");
        goToAtivity(MainActivity.class, new Bundle());
        finish();
    }

    @Override
    public void basicFailure(String prompt) {
        DTLog.showMessageShort(this,prompt);
        if(dialog != null) {
            dialog.dismiss();
        }
    }
    /**
     * 设置标签与别名
     */
    private void setTagAndAlias() {
        /**
         *这里设置了别名，在这里获取的用户登录的信息
         *并且此时已经获取了用户的userId,然后就可以用用户的userId来设置别名了
         **/
        //false状态为未设置标签与别名成功
        //if (UserUtils.getTagAlias(getHoldingActivity()) == false) {
        Set<String> tags = new HashSet<String>();
        //这里可以设置你要推送的人，一般是用户uid 不为空在设置进去 可同时添加多个
        if (!TextUtils.isEmpty(mShopId)){
            tags.add("T_DISH_"+mShopId);//设置tag
        }
        //上下文、别名【Sting行】、标签【Set型】、回调
        JPushInterface.setAliasAndTags(this, mShopId, tags,
                mAliasCallback);

        // }
    }
    /**
     * /**
     * TagAliasCallback类是JPush开发包jar中的类，用于
     * 设置别名和标签的回调接口，成功与否都会回调该方法
     * 同时给定回调的代码。如果code=0,说明别名设置成功。
     * /**
     * 6001   无效的设置，tag/alias 不应参数都为 null
     * 6002   设置超时    建议重试
     * 6003   alias 字符串不合法    有效的别名、标签组成：字母（区分大小写）、数字、下划线、汉字。
     * 6004   alias超长。最多 40个字节    中文 UTF-8 是 3 个字节
     * 6005   某一个 tag 字符串不合法  有效的别名、标签组成：字母（区分大小写）、数字、下划线、汉字。
     * 6006   某一个 tag 超长。一个 tag 最多 40个字节  中文 UTF-8 是 3 个字节
     * 6007   tags 数量超出限制。最多 100个 这是一台设备的限制。一个应用全局的标签数量无限制。
     * 6008   tag/alias 超出总长度限制。总长度最多 1K 字节
     * 6011   10s内设置tag或alias大于3次 短时间内操作过于频繁
     **/
    private final TagAliasCallback mAliasCallback = new TagAliasCallback() {
        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs;
            switch (code) {
                case 0:
                    //这里可以往 SharePreference 里写一个成功设置的状态。成功设置一次后，以后不必再次设置了。
                    //UserUtils.saveTagAlias(getHoldingActivity(), true);
                    logs = "Set tag and alias success极光推送别名设置成功";
                    Iterator<String> iterator = tags.iterator();
                    while (iterator.hasNext()){
                        String next = iterator.next();
                        mLogger.e(next);
                    }
                    Log.e("TAG", logs);
                    break;
                case 6002:
                    //极低的可能设置失败 我设置过几百回 出现3次失败 不放心的话可以失败后继续调用上面那个方面 重连3次即可 记得return 不要进入死循环了...
                    logs = "Failed to set alias and tags due to timeout. Try again after 60s.极光推送别名设置失败，60秒后重试";
                    Log.e("TAG", logs);
                    break;
                default:
                    logs = "极光推送设置失败，Failed with errorCode = " + code;
                    Log.e("TAG", logs);
                    break;
            }
        }
    };
}
