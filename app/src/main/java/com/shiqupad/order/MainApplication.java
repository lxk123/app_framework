package com.shiqupad.order;

import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.facebook.stetho.Stetho;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechUtility;
import com.pgyersdk.crash.PgyCrashManager;
import com.squareup.leakcanary.LeakCanary;
import com.tencent.bugly.Bugly;
import com.tencent.bugly.crashreport.CrashReport;
import com.youngfeng.snake.Snake;

import java.util.List;

import cn.com.dreamtouch.magicbox_common.util.AppOnForegroundReceiver;
import cn.com.dreamtouch.magicbox_common.util.NetworkChangeReceiver;
import cn.com.dreamtouch.magicbox_general_ui.util.FrescoUtil;
import cn.jpush.android.api.JPushInterface;


/**
 * Created by MuHan on 17/2/9.
 */

public class MainApplication extends Application {

    NetworkChangeReceiver networkChangeReceiver;
    @Override
    public void onCreate() {
        super.onCreate();



        ActivityManager am = ((ActivityManager) getSystemService(Context.ACTIVITY_SERVICE));
        List<ActivityManager.RunningAppProcessInfo> processInfoList = null;
        if (am != null) {
            processInfoList = am.getRunningAppProcesses();
        }
        if (processInfoList == null) {
            return;
        }
        String mainProcessName = getPackageName();
        int myPid = android.os.Process.myPid();
        for (ActivityManager.RunningAppProcessInfo info : processInfoList) {
            if (info.pid == myPid && mainProcessName.equals(info.processName)) {
                //来自主进程
                JPushInterface.setDebugMode(true);
                JPushInterface.init(this);
                Snake.init(this);
                networkChangeReceiver = new NetworkChangeReceiver();//监测网络
                networkChangeReceiver.mListener = new NetworkChangeReceiver.OnNetworkChangeListener() {
                    @Override
                    public void onNetworkChanged(Context context, boolean hasWifi, boolean isAvailable) {


                    }
                };
                networkChangeReceiver.register(getApplicationContext());
                initStompProtocolClient();
                //LeakCanary.install(this);
                Stetho.initializeWithDefaults(this);
                FrescoUtil.initFresco(this.getApplicationContext());//在Application 初始化时，进行初始化Fresco
                PgyCrashManager.register(this);//蒲公英
                Bugly.init(getApplicationContext(), "bcafb7dd83", true);
                CrashReport.initCrashReport(getApplicationContext(), "bcafb7dd83", false);
                SpeechUtility.createUtility(getApplicationContext(), SpeechConstant.APPID + "=5b10f827");

            }
        }


//        StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
//                        .detectLeakedSqlLiteObjects()
//                        .detectLeakedClosableObjects()
//                        .penaltyLog()
//                        .penaltyDeath()
//                        .build());//检查泄漏的 SQLite 对象以及 Closeable 对象

    }

    private void initStompProtocolClient() {

        AppOnForegroundReceiver appOnForegroundReceiver = new AppOnForegroundReceiver();
        appOnForegroundReceiver.register(this, new AppOnForegroundReceiver.StateListener() {
            @Override
            public void onAppChange(boolean isForeground) {


            }
        });
    }

    /**
     * 方法数超过64k限制
     *
     */
    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);

        MultiDex.install(this);
    }

}
