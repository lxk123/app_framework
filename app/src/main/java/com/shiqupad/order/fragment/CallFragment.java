package com.shiqupad.order.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.shiqupad.order.listener.PushMessageListener;
import com.shiqupad.order.presenter.PushMessagePresenter;
import com.jakewharton.rxbinding.view.RxView;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_general_ui.fragment.MBBaseFragment;
import cn.com.dreamtouch.magicbox_general_ui.ui.AlertDialog;
import cn.com.dreamtouch.magicbox_general_ui.ui.XNumberKeyboardView;
import cn.com.dreamtouch.magicbox_general_ui.util.RxBusUtil;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushMessageResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.RxEvent;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.UserRemoteData;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import com.shiqupad.order.R;
import rx.Observer;

/**
 * Created by LuoXueKun
 * on 2018/5/14
 */
public class CallFragment extends MBBaseFragment implements XNumberKeyboardView.IOnKeyboardListener, PushMessageListener {
    @Bind(R.id.fr_call_tv)
    TextView mFrCallTv;
    @Bind(R.id.fr_call_et)
    EditText mFrCallEt;
    @Bind(R.id.fr_call_keyboard_kbv)
    XNumberKeyboardView mFrCallKeyboardKbv;
    @Bind(R.id.fr_call_tv_sure)
    TextView mCallTv;
    @Bind(R.id.cont_call_head_title_tv)
    TextView tvTitle;
    @Bind(R.id.iv_close)
    ImageView ivClose;
    private String mShopId;
    private PushMessagePresenter mPushMessagePresenter;

    public CallFragment() {
    }

    public static CallFragment newInstance() {
        CallFragment fragment = new CallFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initVariables() {
        super.initVariables();
        mShopId = UserLocalData.getInstance(getActivity()).getShopID();
        mPushMessagePresenter = new PushMessagePresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
    }

    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_call, container, false);
        ButterKnife.bind(this, view);
        tvTitle.setText("叫号取餐");
        mFrCallKeyboardKbv.setIOnKeyboardListener(this);
        ivClose.setVisibility(View.GONE);
        initClick();

        return view;
    }

    private void initClick() {
        RxView.clicks(mCallTv).throttleFirst(3, TimeUnit.SECONDS)
                .subscribe(new Observer<Void>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Void aVoid) {
                        String phoneNum = mFrCallEt.getText().toString();
                        if (TextUtils.isEmpty(phoneNum)) {
                            return;
                        }
                        mPushMessagePresenter.pushMessage(phoneNum, mShopId);
                    }
                });
    }


    public void refreshTabData() {

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onInsertKeyEvent(String text) {
        mFrCallEt.append(text);
    }

    @Override
    public void onDeleteKeyEvent() {
        int start = mFrCallEt.length() - 1;
        if (start >= 0) {
            mFrCallEt.getText().delete(start, start + 1);
        }
    }

    @Override
    public void pushMessageSuccess(PushMessageResponse pushMessageResponse) {
        mFrCallEt.setText("");
        if (!Objects.requireNonNull(getActivity()).isFinishing()) {

            new AlertDialog(getActivity()).builder()
                    .setTitle("叫号成功")
                    .setMsg("去点菜！")
                    .setCancelable(true)
                    .setPositiveButton("确认", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            RxBusUtil.getDefault().post(new RxEvent(CommonConstant.OpertionType.CALL_SUCCESS,
                                    CommonConstant.OpertionType.CALL_SUCCESS ));
                        }
                    })
                    .setNegativeButton("取消", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }).show();
        }
    }

    @Override
    public void basicFailure(String prompt) {
        if(getActivity()!=null) {
            DTLog.showMessageShort(getActivity(), prompt);
        }
    }

    @OnClick(R.id.fr_call_tv_sure)
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.fr_call_tv_sure:


                break;
        }
    }
}
