package com.shiqupad.order.fragment;

import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.shiqupad.order.listener.GetMessageListener;
import com.shiqupad.order.presenter.GetMessagePresenter;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.OnLoadmoreListener;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_general_ui.fragment.MBBaseFragment;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetMessageResponse;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.UserRemoteData;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import com.shiqupad.order.R;
import com.shiqupad.order.adapter.GetMessageAdapter;

/**
 * Created by LuoXueKun
 * on 2018/5/24
 */
public class MessageFragment  extends MBBaseFragment  implements GetMessageListener,AdapterView.OnItemClickListener{
    private GetMessagePresenter mGetMessagePresenter;
    private List<GetMessageResponse.MessageData> mMessageList;
    private List<GetMessageResponse.MessageData> mNewMessageList;
    private GetMessageAdapter mGetMessageAdapter;
    @Bind(R.id.refreshLayout)
    SmartRefreshLayout mSmartRefreshLayout;
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.cont_call_head_title_tv)
    TextView tvTitle;
    @Bind(R.id.iv_close)
    ImageView ivClose;
    int page = 1;
    //判断是下拉刷新还是上拉加载
    int refreshOrLoad = 0;
    private String mShopId;
    private int messageType;
    public static MessageFragment newInstance(int messageType) {
        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        args.putInt(CommonConstant.Args.MESSAGE_TYPE, messageType);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.act_get_message,container,false);
        ButterKnife.bind(this,view);
        initRecyclerViewAndAdapter();
        return view;
    }

    @Override
    protected void initVariables() {
        super.initVariables();
        if (getArguments() != null) {
            messageType = getArguments().getInt(CommonConstant.Args.MESSAGE_TYPE);
        }
        mShopId = UserLocalData.getInstance(getActivity()).getShopID();
        mNewMessageList = new ArrayList<>();
        mMessageList = new ArrayList<>();
        mGetMessagePresenter = new GetMessagePresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())
        ), this);
    }


    private void initRecyclerViewAndAdapter() {
        mGetMessageAdapter = new GetMessageAdapter(getActivity(), mMessageList, R.layout.item_message, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mGetMessageAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mSmartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshlayout) {
                refreshOrLoad = 1;
                mGetMessagePresenter.getMessage(mShopId, String.valueOf(page));
            }
        });
        mSmartRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(final RefreshLayout refreshlayout) {
                refreshOrLoad = 2;
                page++;
                mGetMessagePresenter.getMessage(mShopId, String.valueOf(page));
            }
        });
        mSmartRefreshLayout.setEnableRefresh(true);
        mSmartRefreshLayout.autoRefresh();
    }

    @Override
    protected void loadData() {
        super.loadData();
    }

    @Override
    public void getMessageSuccess(GetMessageResponse getMessageResponse) {
        mNewMessageList.clear();
        if (getMessageResponse.data != null && getMessageResponse.data.size() > 0) {
            List<GetMessageResponse.MessageData> messageData = fitterMessageList(messageType, getMessageResponse.data);
            mNewMessageList.addAll(messageData);
        }
        switch (refreshOrLoad) {
            case 1:
                page = 1;
                mMessageList.clear();
                mMessageList.addAll(mNewMessageList);
                mGetMessageAdapter.refresh(mNewMessageList);
                mSmartRefreshLayout.finishRefresh();
                break;
            case 2:
                mMessageList.addAll(mNewMessageList);
                mGetMessageAdapter.loadMore(mNewMessageList);
                mSmartRefreshLayout.finishLoadMore();
                break;
        }
    }

    private List<GetMessageResponse.MessageData> fitterMessageList(int messageType, List<GetMessageResponse.MessageData> list) {
         List<GetMessageResponse.MessageData> fitterList = new ArrayList<>();
         switch (messageType){
             case 0:
                 for (int i = 0; i < list.size(); i++) {
                     if(list.get(i).getTitle().equals("呼叫服务员")){
                          fitterList.add(list.get(i));
                     }
                 }
                 break;
             case 1:
                 for (int i = 0; i < list.size(); i++) {
                     if(list.get(i).getTitle().equals("待接单提醒")){
                         fitterList.add(list.get(i));
                     }
                 }
                 break;
             case 2:
                 for (int i = 0; i < list.size(); i++) {
                     if(list.get(i).getTitle().equals("打印机提醒")){
                         fitterList.add(list.get(i));
                     }
                 }
                 break;
         }
         return fitterList;
    }

    @Override
    public void basicFailure(String prompt) {
        DTLog.showMessageShort(getActivity(), prompt);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

}
