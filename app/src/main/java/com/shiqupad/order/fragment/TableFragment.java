package com.shiqupad.order.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.alibaba.fastjson.JSON;
import com.shiqupad.order.activity.AccountActivity;
import com.shiqupad.order.activity.NewShopCartActivity2;
import com.shiqupad.order.cache.CartCacheManager;
import com.shiqupad.order.listener.CancelOrderListener;
import com.shiqupad.order.listener.ChangeTableListener;
import com.shiqupad.order.listener.CleanTableListener;
import com.shiqupad.order.listener.GetAllTableListener;
import com.shiqupad.order.listener.OpenTableListener;
import com.shiqupad.order.listener.PushIsOnOffListener;
import com.shiqupad.order.listener.UpdateDinnerPeopleListener;
import com.shiqupad.order.presenter.CancelOrderPresenter;
import com.shiqupad.order.presenter.ChangeTablePresenter;
import com.shiqupad.order.presenter.CleanTablePresenter;
import com.shiqupad.order.presenter.GetAllTablePresenter;
import com.shiqupad.order.presenter.OpenTablePresenter;
import com.shiqupad.order.presenter.PushIsOnOffPresenter;
import com.shiqupad.order.presenter.UpdateDinnerPeoplePresenter;
import com.shiqupad.order.ui.ChangePeopleNumTableDialog;
import com.shiqupad.order.ui.NewTableOptionDialog;
import com.shiqupad.order.ui.OpenTableDialog;
import com.shiqupad.order.ui.OpenTableForQrcodeDialog;
import com.shiqupad.order.ui.SwitchTableDialog;
import com.shiqupad.order.ui.TableOptionDialog;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_general_ui.util.MyLogger;
import cn.com.dreamtouch.magicbox_general_ui.util.RxBusUtil;
import cn.com.dreamtouch.magicbox_http_client.network.model.CancelOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangeTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChearTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.OpenTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushIsOnOffResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.RxEvent;
import cn.com.dreamtouch.magicbox_http_client.network.model.UpdateDinnerPeopleResponse;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.UserRemoteData;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;

import com.shiqupad.order.R;
import com.shiqupad.order.activity.MenuActivity;
import com.shiqupad.order.activity.NewShopCartActivity;
import com.shiqupad.order.adapter.GetAllTableAdapter;

import rx.functions.Action1;

/**
 * Created by LuoXueKun
 * on 2018/5/14
 */
public class TableFragment extends LazyFragment implements
        SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener,
        GetAllTableListener, OpenTableDialog.OnStartTableDialogListener, OpenTableListener, ChangePeopleNumTableDialog.OnChangePeopleNumTableDialogListener, UpdateDinnerPeopleListener,
        SwitchTableDialog.OnSwitchTableDialogListener, ChangeTableListener, NewTableOptionDialog.OnNewTableOptionClickListener, CleanTableListener, PushIsOnOffListener, CancelOrderListener, OpenTableForQrcodeDialog.OnStartTableForQrCodeDialogListener {
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.refreshLayout)
    SwipeRefreshLayout mRefreshLayout;
    private GetAllTableAdapter mGetAllTableAdapter;
    private GetAllTablePresenter mGetAllTablePresenter;
    private OpenTablePresenter mOpenTablePresenter;
    private UpdateDinnerPeoplePresenter mUpdateDinnerPeoplePresenter;
    private ChangeTablePresenter mChangeTablePresenter;
    private CleanTablePresenter mCleanTablePresenter;
    private PushIsOnOffPresenter mPushIsOnOffPresenter;
    private CancelOrderPresenter mCancelOrderPresenter;
    private ArrayList<GetAllTableResponse.Data> mAllTableDataList;
    private List<GetAllTableResponse.Data.ListBean> mAllTableList;
    private List<GetAllTableResponse.Data.ListBean> mFilterTableList;
    private String mSid;
    private String mShopID;
    private int mRoomId = -1;
    private int mFilterTableStatus;
    private OpenTableDialog mOpenTableDialog;
    private OpenTableForQrcodeDialog mOpenTableForQrcodeDialog;
    private ChangePeopleNumTableDialog mChangePeopleNumTableDialog;
    private TableOptionDialog mTableOptionDialog;
    private NewTableOptionDialog mNewTableOptionDialog;
    private SwitchTableDialog mSwitchTableDialog;
    private String mOldTableID;
    private String mOrderID;
    private String mEmployId;
    private int mTablePosition;
    private boolean mIsOrderNow;
    private int mPeopleNum;
    private BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(CommonConstant.Args.NEW_NOTIFY)) {
                //refreshNotifyStatus();
            } else if (intent.getAction().equals(CommonConstant.Args.CONFIRM_NOTIFY)) {
                loadData();
                RxBusUtil.getDefault().post(new RxEvent(CommonConstant.OpertionType.WAIT_TO_BE_CONFIRMED,
                        CommonConstant.OpertionType.WAIT_TO_BE_CONFIRMED));
            }
        }
    };

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        LocalBroadcastManager broadcastManager = LocalBroadcastManager.getInstance(getActivity());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CommonConstant.Args.NEW_NOTIFY);
        intentFilter.addAction(CommonConstant.Args.CONFIRM_NOTIFY);
        broadcastManager.registerReceiver(receiver, intentFilter);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onResume() {
        super.onResume();

        if (isVisible && this.isAdded()) {

            RxBusUtil.getDefault().post(new RxEvent(CommonConstant.OpertionType.BACK_TO_RESUME,
                    CommonConstant.OpertionType.BACK_TO_RESUME));

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    public TableFragment() {
    }

    public static TableFragment newInstance(int roomId) {
        Bundle bundle = new Bundle();
        bundle.putInt(CommonConstant.Args.ROOM_ID, roomId);
        TableFragment mTableFragment = new TableFragment();
        mTableFragment.setArguments(bundle);

        return mTableFragment;
    }


    private void initRecyclerViewAndAdapter() {
        mFilterTableList = new ArrayList<>();
        mAllTableList = new ArrayList<>();
        mRefreshLayout.setColorSchemeColors(Color.RED, Color.BLUE, Color.GREEN);
        mRefreshLayout.setOnRefreshListener(this);
        mGetAllTableAdapter = new GetAllTableAdapter(getActivity(), mAllTableList, R.layout.item_table, this);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 6));
        mRecyclerView.setAdapter(mGetAllTableAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
    }

    @Override
    protected void initVariables() {
        super.initVariables();
        mAllTableDataList = new ArrayList<>();
        mGetAllTablePresenter = new GetAllTablePresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
        mOpenTablePresenter = new OpenTablePresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
        mUpdateDinnerPeoplePresenter = new UpdateDinnerPeoplePresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
        mChangeTablePresenter = new ChangeTablePresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
        mCleanTablePresenter = new CleanTablePresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
        mPushIsOnOffPresenter = new PushIsOnOffPresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
        mCancelOrderPresenter = new CancelOrderPresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
        if (getArguments() != null) {
            mRoomId = getArguments().getInt(CommonConstant.Args.ROOM_ID);
        }
        mSid = UserLocalData.getInstance(getActivity()).getSid();
        mShopID = UserLocalData.getInstance(getActivity()).getShopID();
        mEmployId = UserLocalData.getInstance(getActivity()).getEmployeeID();
        mFilterTableStatus = -10;//默认是该房间全部桌台
        getEvent();
    }

    private void getEvent() {
        RxBusUtil.getDefault().toObservable(RxEvent.class).subscribe(new Action1<RxEvent>() {
            @Override
            public void call(RxEvent event) {

                switch (event.getFlag()) {
                    case CommonConstant.TableStatus.DISABLE:
                        mFilterTableStatus = event.getData();
                        filterAllTable(mFilterTableStatus);
                        break;
                    case CommonConstant.TableStatus.FREE:
                        mFilterTableStatus = event.getData();
                        filterAllTable(mFilterTableStatus);
                        break;
                    case CommonConstant.TableStatus.OCCUPY:
                        mFilterTableStatus = event.getData();
                        filterAllTable(mFilterTableStatus);
                        break;
                    case CommonConstant.TableStatus.RESERVE:
                        mFilterTableStatus = event.getData();
                        filterAllTable(mFilterTableStatus);
                        break;
                    case CommonConstant.TableStatus.INVOICING:
                        mFilterTableStatus = event.getData();
                        filterAllTable(mFilterTableStatus);
                        break;
                    case CommonConstant.TableStatus.ALL:
                        mFilterTableStatus = event.getData();
                        mGetAllTableAdapter.refresh(mAllTableList);
                        break;
                    case CommonConstant.OpertionType.CHANGE_TABLE:
                        loadData();
                        break;
                    case CommonConstant.OpertionType.BACK_TO_RESUME:
                        loadData();
                        break;
                    case CommonConstant.OpertionType.CLEAN_TABLE:
                        loadData();
                        mLogger.e("清台");
                        CartCacheManager.getInstance(getActivity()).clearCart(Integer.valueOf(mOldTableID));
                        break;

                    case CommonConstant.OpertionType.UPDATE_DINNER_PEOPLE:
                        loadData();
                        break;
                    case CommonConstant.OpertionType.CANCEL_ORDER:
                        loadData();
                        break;
                    case CommonConstant.OpertionType.CALL_SUCCESS:
                        loadData();
                        break;

                    case CommonConstant.OpertionType.QRCODE_TABLE_SUCCESS:
                        int tableId = event.getData();
                        openTableForQrcode(tableId);
                        break;
                    case CommonConstant.OpertionType.OPEN_TABLE_SUCCESS:
                        mLogger.e("开台成功");
                        loadData();
                        break;

                    case CommonConstant.OpertionType.POSTORDER_SUCCESS:
                        loadData();
                        break;
                    case CommonConstant.OpertionType.SWITCH_TABLE:
                        loadData();
                        break;


                }


            }
        }, new Action1<Throwable>() {
            @Override
            public void call(Throwable throwable) {
                throwable.printStackTrace();

            }
        });
    }

    private void filterAllTable(int tableStatus) {
        mFilterTableList.clear();
        for (int i = 0; i < mAllTableList.size(); i++) {
            if (tableStatus == mAllTableList.get(i).getStatus()) {
                GetAllTableResponse.Data.ListBean listBean = mAllTableList.get(i);
                mFilterTableList.add(listBean);
            }
        }
        mGetAllTableAdapter.refresh(mFilterTableList);
    }

    @Override
    protected void loadData() {
        super.loadData();
        mFilterTableStatus = -10;
        mGetAllTablePresenter.getAllTable(mSid, mShopID, "1");
    }

    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_table, container, false);
        ButterKnife.bind(this, view);
        initRecyclerViewAndAdapter();
        return view;
    }

    @Override
    protected void lazyLoad() {

    }

    @Override
    protected void onInvisible() {
        super.onInvisible();
    }

    @Override
    public void onRefresh() {

        loadData();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        int tableStatus;
        if (mFilterTableStatus == -10) {
            tableStatus = mAllTableList.get(position).getStatus();
            mOldTableID = String.valueOf(mAllTableList.get(position).getTableID());
            mOrderID = String.valueOf(mAllTableList.get(position).getOrderID());
        } else {
            tableStatus = mFilterTableList.get(position).getStatus();
            mOldTableID = String.valueOf(mFilterTableList.get(position).getTableID());
            mOrderID = String.valueOf(mFilterTableList.get(position).getOrderID());
        }
        mTablePosition = position;
        switch (tableStatus) {
            case CommonConstant.TableStatus.DISABLE:

                break;
            case CommonConstant.TableStatus.FREE:
                openTable(position);
                break;
            case CommonConstant.TableStatus.OCCUPY:

                openNewOrderTable(position);

                break;
            case CommonConstant.TableStatus.RESERVE:

                break;
            case CommonConstant.TableStatus.INVOICING:

                break;
            case CommonConstant.TableStatus.TO_BE_CONFIRMED:

                RxBusUtil.getDefault().post(new RxEvent(CommonConstant.OpertionType.WAIT_CONFIRM_ORDER,
                        Integer.valueOf(mOldTableID)));
                break;
        }
    }

    private void openNewOrderTable(int position) {
        if (mNewTableOptionDialog == null) {
            mNewTableOptionDialog = new NewTableOptionDialog(getActivity(),
                    mFilterTableStatus == -10 ?
                            mAllTableList.get(position) : mFilterTableList.get(position), position);
            mNewTableOptionDialog.setOnNewTableOptionClickListener(this);
        }
        mNewTableOptionDialog.setCanceledOnTouchOutside(true);
        mNewTableOptionDialog.show();
        mNewTableOptionDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mNewTableOptionDialog = null;
            }
        });
    }

    private void openTable(int position) {
        if (mOpenTableDialog == null) {
            mOpenTableDialog = new OpenTableDialog(getActivity(),
                    mFilterTableStatus == -10 ?
                            mAllTableList.get(position) : mFilterTableList.get(position), position);
            mOpenTableDialog.setOnStartTableDialogListener(this);
        }
        mOpenTableDialog.setCanceledOnTouchOutside(true);
        mOpenTableDialog.show();
        mOpenTableDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mOpenTableDialog = null;
            }
        });
    }

    private void openTableForQrcode(int tableId) {
        if (mOpenTableForQrcodeDialog == null) {
            mOpenTableForQrcodeDialog = new OpenTableForQrcodeDialog(getActivity(), tableId);
            mOpenTableForQrcodeDialog.setStartForQrcodeListener(this);
        }
        mOpenTableForQrcodeDialog.setCanceledOnTouchOutside(true);
        mOpenTableForQrcodeDialog.show();
        mOpenTableForQrcodeDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mOpenTableForQrcodeDialog = null;
            }
        });
    }

    @Override
    public void getAllTableSuccess(GetAllTableResponse getAllTableResponse) {
        mLogger.e(mRoomId + "roomid");
        mAllTableList.clear();
        mAllTableDataList.clear();
        mAllTableDataList.addAll(getAllTableResponse.getData());
        for (int i = 0; i < mAllTableDataList.size(); i++) {
            if (mAllTableDataList.get(i).getAreaID() == mRoomId) {
                List<GetAllTableResponse.Data.ListBean> newListBeanList = mAllTableDataList.get(i).getList();
                mAllTableList.addAll(newListBeanList);
            } else if (mRoomId == -1) {
                mAllTableList.addAll(mAllTableDataList.get(i).getList());
            }
        }
        mGetAllTableAdapter.refresh(mAllTableList);
        setRefreshing(false);
    }

    @Override
    public void showLoadingProgress() {

    }

    @Override
    public void hideLoadingProgress() {

    }

    @Override
    public void basicFailure(String prompt) {
        if(getActivity()!=null) {
            DTLog.showMessageShort(getActivity(), prompt);
        }
    }

    public void setRefreshing(final boolean refreshing) {
        if (mRefreshLayout == null) {
            return;
        }
        mRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                if(mRefreshLayout != null) {
                    mRefreshLayout.setRefreshing(refreshing);
                }
            }
        });
    }


    @Override
    public void onBtnConfirmClick(int peopleNum, String remark, boolean isOrder, String tableId) {
        mIsOrderNow = isOrder;
        mOpenTableDialog = null;
        mLogger.e("onBtnConfirmClick");
        mPeopleNum = peopleNum;
        mOpenTablePresenter.openTable(tableId, mShopID, String.valueOf(peopleNum), remark);
    }

    @Override
    public void openTableSuccess(OpenTableResponse openTableResponse) {
        if (mOpenTableDialog != null) {
            mOpenTableDialog.dismiss();
            mOpenTableDialog = null;
        }
        mGetAllTablePresenter.getAllTable(mSid, mShopID, "1");
        RxBusUtil.getDefault().post(new RxEvent(CommonConstant.OpertionType.OPEN_TABLE_SUCCESS,
                CommonConstant.OpertionType.OPEN_TABLE_SUCCESS));

        if (mIsOrderNow) {
            goToShopCartActivity();
        }
    }


    private void changePeopleNumTable() {
        if (mChangePeopleNumTableDialog == null) {
            mChangePeopleNumTableDialog = new ChangePeopleNumTableDialog(getActivity(),
                    mFilterTableStatus == -10 ? mAllTableList.get(mTablePosition) :
                            mFilterTableList.get(mTablePosition));
            mChangePeopleNumTableDialog.setOnChangePeopleNumTableDialogListener(this);
        }
        mChangePeopleNumTableDialog.setCanceledOnTouchOutside(true);
        mChangePeopleNumTableDialog.show();
        mChangePeopleNumTableDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mChangePeopleNumTableDialog = null;
            }
        });
    }

    @Override
    public void onChangePeopleNum(String newPeopleNum) {

        mUpdateDinnerPeoplePresenter.updateDinnerPeople(mShopID, mOldTableID,
                mOrderID, newPeopleNum);
    }

    @Override
    public void updateDinnerPeopleSuccess(UpdateDinnerPeopleResponse updateDinnerPeopleResponse) {
        RxBusUtil.getDefault().post(new RxEvent(CommonConstant.OpertionType.UPDATE_DINNER_PEOPLE
                , CommonConstant.OpertionType.UPDATE_DINNER_PEOPLE
        ));
    }

    private void switchTable() {

        if (mSwitchTableDialog == null) {
            mSwitchTableDialog = new SwitchTableDialog(getActivity(),
                    mAllTableDataList);
            mSwitchTableDialog.setOnSwitchTableDialogListener(this);
        }
        mSwitchTableDialog.setCanceledOnTouchOutside(true);
        mSwitchTableDialog.show();
        mSwitchTableDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mSwitchTableDialog = null;
            }
        });
    }

    @Override
    public void onSwitchTable(String tableID) {
        mChangeTablePresenter.changeTable(mOldTableID, tableID, mShopID, mOrderID);
    }

    @Override
    public void changeTableSuccess(ChangeTableResponse changeTableResponse) {
        if (mSwitchTableDialog != null) {
            mSwitchTableDialog.dismiss();
        }
        RxBusUtil.getDefault().post(new RxEvent(CommonConstant.OpertionType.CHANGE_TABLE,
                CommonConstant.OpertionType.CHANGE_TABLE));
        loadData();
    }

    @Override
    public void onNewTableOptionClick(int position) {
        if (mNewTableOptionDialog != null) {
            mNewTableOptionDialog.dismiss();
        }
        mNewTableOptionDialog = null;
        switch (position) {
            case NewTableOptionDialog.ITEM_ADD:
                goToShopCartActivity();
                break;
            case NewTableOptionDialog.ITEM_PAY:
                MyLogger.kLog().e(mOrderID);
                if (mOrderID.equals("0")) {
                    showNoOrderDialog("暂无订单");
                    return;
                }
                Intent intent1 = new Intent(getActivity(), MenuActivity.class);
                intent1.putExtra(CommonConstant.Args.ORDER_ID, mOrderID);
                intent1.putExtra(CommonConstant.Args.TABLE_ID, mOldTableID);
                startActivity(intent1);
                break;
            case NewTableOptionDialog.ITEM_CHANGE:

                switchTable();

                break;
            case NewTableOptionDialog.ITEM_ORDER:
                if (mOrderID.equals("0")) {
                    showNoOrderDialog("暂无订单");
                    return;
                }
                goToAccountActivity();
                break;
            case NewTableOptionDialog.ITEM_PRINT:
                if (mOrderID.equals("0")) {
                    showNoOrderDialog("暂无订单，暂不支持修改人数，快去下单吧");
                    return;
                }
                changePeopleNumTable();
                break;
            case NewTableOptionDialog.ITEM_CLEAN:
                if (!mOrderID.equals("0")) {
                    showAccountDialog("请结账后进行清台");
                    return;
                }
                mCleanTablePresenter.cleanTable(mOldTableID, mShopID);
                break;
            case NewTableOptionDialog.ITEM_CANCEL:
                mPushIsOnOffPresenter.pullIsOnOff(mEmployId);
                break;
            case NewTableOptionDialog.ITEM_MERGE:
                DTLog.showMessageShort(getActivity(), "暂未开放");
                break;
            case NewTableOptionDialog.ITEM_SIGN:

                break;
        }
    }

    private void goToMenuActivity() {

        Intent intent = new Intent(getActivity(), NewShopCartActivity2.class);
        intent.putExtra(CommonConstant.Args.TABLE_ID, mFilterTableStatus == -10 ? mAllTableList.get(mTablePosition).getTableID()
                : mFilterTableList.get(mTablePosition).getTableID()
        );
        intent.putExtra(CommonConstant.Args.TABLE_NAME, mFilterTableStatus == -10 ? mAllTableList.get(mTablePosition).getTableName()
                : mFilterTableList.get(mTablePosition).getTableName()
        );
        intent.putExtra(CommonConstant.Args.DINNER_NUM, mFilterTableStatus == -10 ? mAllTableList.get(mTablePosition).getDinningPeople()
                : mFilterTableList.get(mTablePosition).getDinningPeople()
        );
        startActivity(intent);
        //Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.activity_open, R.anim.activity_close);
    }

    private void goToShopCartActivity() {

        Intent intent = new Intent(getActivity(), NewShopCartActivity2.class);
        intent.putExtra(CommonConstant.Args.TABLE_ID, mFilterTableStatus == -10 ? mAllTableList.get(mTablePosition).getTableID()
                : mFilterTableList.get(mTablePosition).getTableID()
        );
        intent.putExtra(CommonConstant.Args.TABLE_NAME, mFilterTableStatus == -10 ? mAllTableList.get(mTablePosition).getTableName()
                : mFilterTableList.get(mTablePosition).getTableName()
        );
        intent.putExtra(CommonConstant.Args.DINNER_NUM, mPeopleNum
        );
        startActivity(intent);
        // Objects.requireNonNull(getActivity()).overridePendingTransition(R.anim.activity_open, R.anim.activity_close);
    }

    private void goToAccountActivity() {
        Intent intent = new Intent(getActivity(), AccountActivity.class);
        intent.putExtra(CommonConstant.Args.ORDER_ID, mOrderID);
        intent.putExtra(CommonConstant.Args.DINNER_NUM,
                mFilterTableStatus == -10 ? mAllTableList.get(mTablePosition).getDinningPeople() + ""
                        : mFilterTableList.get(mTablePosition).getDinningPeople() + "");
        startActivity(intent);
    }

    private void showNoOrderDialog(String title) {
        if (!Objects.requireNonNull(getActivity()).isFinishing()) {

            new cn.com.dreamtouch.magicbox_general_ui.ui.AlertDialog(getActivity()).builder()
                    .setTitle(title)
                    .setMsg("快去点餐下单吧！")
                    .setCancelable(true)
                    .setPositiveButton("确认", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            goToMenuActivity();
                        }
                    })
                    .setNegativeButton("取消", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }).show();
        }
    }

    private void showAccountDialog(String title) {
        if (!Objects.requireNonNull(getActivity()).isFinishing()) {

            new cn.com.dreamtouch.magicbox_general_ui.ui.AlertDialog(getActivity()).builder()
                    .setTitle(title)
                    .setMsg("快去结账吧！")
                    .setCancelable(true)
                    .setPositiveButton("确认", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            goToAccountActivity();
                        }
                    })
                    .setNegativeButton("取消", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                        }
                    }).show();
        }
    }


    @Override
    public void clearTableSuccess(ChearTableResponse chearTableResponse) {
        RxBusUtil.getDefault().post(new RxEvent(CommonConstant.OpertionType.CLEAN_TABLE,
                CommonConstant.OpertionType.CLEAN_TABLE));
        loadData();
    }

    @Override
    public void pushIsOnOffSuccess(PushIsOnOffResponse pushIsOnOffResponse) {
        PushIsOnOffResponse.DataBean data = pushIsOnOffResponse.getData();

        if (TextUtils.isEmpty(mOrderID) || mOrderID.equals("0")) {
            DTLog.showMessageShort(getActivity(), "暂无订单");
            return;
        }
        if (!"1".equals(data.getFwyXD())) {
            DTLog.showMessageShort(getActivity(), "您没有销单权限");
            return;
        }
        new AlertDialog.Builder(Objects.requireNonNull(getActivity()))
                .setTitle("提示")
                .setMessage("是否销单！")
                .setNegativeButton(
                        "确定",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                mCancelOrderPresenter.cancelOrder(mShopID, mOrderID);
                            }
                        }).show();
    }

    @Override
    public void cancelOrderSuccess(CancelOrderResponse cancelOrderResponse) {
        mCleanTablePresenter.cleanTable(mOldTableID, mShopID);
    }

    @Override
    public void onStartTableForQrcode(int peopleNum, String remark, boolean isOrder, String tableID) {
        mIsOrderNow = isOrder;
        mOpenTableForQrcodeDialog = null;
        mPeopleNum = peopleNum;
        mOpenTablePresenter.openTable(tableID, mShopID, String.valueOf(peopleNum), remark);

    }
}
