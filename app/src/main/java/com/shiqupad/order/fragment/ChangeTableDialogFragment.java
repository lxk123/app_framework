package com.shiqupad.order.fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.shiqupad.order.listener.GetAllTableListener;
import com.shiqupad.order.presenter.GetAllTablePresenter;
import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.UserRemoteData;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import com.shiqupad.order.R;
/**
 * Created by LuoXueKun
 * on 2018/5/22
 */
public class ChangeTableDialogFragment extends DialogFragment implements GetAllTableListener, OnTabSelectListener {
    private Dialog dialog;

    @Bind(R.id.ct_main_stl)
    SlidingTabLayout mSlidingTabLayout;
    @Bind(R.id.ct_main_scan)
    LinearLayout ctMainScan;
    @Bind(R.id.fr_table_scan_vp)
    ViewPager mViewPager;
    @Bind(R.id.content_foot_free_ll)
    LinearLayout mFreeLl;
    @Bind(R.id.content_foot_order_ll)
    LinearLayout mOrderLl;
    @Bind(R.id.content_foot_finish_ll)
    LinearLayout mFinishLl;
    @Bind(R.id.content_foot_disable_ll)
    LinearLayout mDisableLl;
    @Bind(R.id.content_foot_message_iv)
    ImageView mMessageIv;

    private View view;

    private String mSid;
    private String mShopID;
    private ArrayList<Fragment> mFragments = new ArrayList<>();

    private List<String> mTitleList;
    public ArrayList<GetAllTableResponse.Data> mAllTableDataList;
    private MyPagerAdapter mAdapter;
    private GetAllTablePresenter getAllTablePresenter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initVariables();
    }

    protected void initVariables() {
        mSid = UserLocalData.getInstance(getActivity()).getSid();
        mShopID = UserLocalData.getInstance(getActivity()).getShopID();
        mTitleList = new ArrayList<>();
        mAllTableDataList = new ArrayList<>();
        getAllTablePresenter = new GetAllTablePresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
    }
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.fr_table_scan, null, false);
       initDialogStyle(rootView);
        loadData();
        ButterKnife.bind(this, rootView);
        return dialog;

    }
    protected void loadData() {
        getAllTablePresenter.getAllTable(mSid, mShopID,"1");
    }
    private void initDialogStyle(View view) {
        dialog = new Dialog(getContext(), R.style.CustomGiftDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // 设置Content前设定
        dialog.setContentView(view);
        dialog.setCanceledOnTouchOutside(true); // 外部点击取消
        // 设置宽度为屏宽, 靠近屏幕底部。
        Window window = dialog.getWindow();
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.gravity = Gravity.CENTER; // 紧贴底部
        lp.width = WindowManager.LayoutParams.MATCH_PARENT; // 宽度持平
        window.setAttributes(lp);
    }

    @Override
    public void getAllTableSuccess(GetAllTableResponse getAllTableResponse) {
        List<GetAllTableResponse.Data> data = getAllTableResponse.getData();
        mAllTableDataList.clear();
        mAllTableDataList.addAll(data);
        mTitleList.add("全部");
        mFragments.add(TableFragment.newInstance(0));
        for (int i = 0; i < data.size(); i++) {
            String areaName = data.get(i).getAreaName();
            mTitleList.add(areaName);
            TableFragment tableFragment = TableFragment.newInstance(data.get(i).getAreaID());
            mFragments.add(tableFragment);
        }
        initTabAndView();
    }
    private void initTabAndView() {
        mAdapter = new MyPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setOnTabSelectListener(this);
        mViewPager.setCurrentItem(0);
        mViewPager.setOffscreenPageLimit(mAllTableDataList.size());
    }

    @Override
    public void showLoadingProgress() {

    }

    @Override
    public void hideLoadingProgress() {

    }

    @Override
    public void basicFailure(String prompt) {

    }

    @Override
    public void onTabSelect(int position) {

    }

    @Override
    public void onTabReselect(int position) {

    }

    private class MyPagerAdapter extends FragmentPagerAdapter {
        private MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);
        }

        @Override
        public Fragment getItem(int position) {


            return mFragments.get(position);

        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getAllTablePresenter.unSubscribe();
    }

}
