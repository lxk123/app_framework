package com.shiqupad.order.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_general_ui.fragment.MBBaseFragment;
import com.shiqupad.order.R;
/**
 * Created by LuoXueKun
 * on 2018/5/30
 */
public class BookingOrderFragment extends MBBaseFragment {

    public static BookingOrderFragment newInstance(int roomId) {
        Bundle bundle = new Bundle();
        bundle.putInt(CommonConstant.Args.ROOM_ID, roomId);
        BookingOrderFragment bookingOrderFragment = new BookingOrderFragment();
        bookingOrderFragment.setArguments(bundle);
        return bookingOrderFragment;
    }
    @Override
    protected void initVariables() {
        super.initVariables();
    }

    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fr_delivery,container,false);
        return view;
    }

    @Override
    protected void loadData() {
        super.loadData();
    }
}
