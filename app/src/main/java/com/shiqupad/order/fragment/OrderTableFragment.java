package com.shiqupad.order.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.shiqupad.order.activity.AccountActivity;
import com.shiqupad.order.activity.NewShopCartActivity;
import com.shiqupad.order.activity.NewShopCartActivity2;
import com.shiqupad.order.adapter.GetOrderListTableAdapter;
import com.shiqupad.order.adapter.OrderDetailActivityAdapter;
import com.shiqupad.order.listener.GetCustomOrderInfoListener;
import com.shiqupad.order.listener.GetOrderListListener;
import com.shiqupad.order.listener.WaitConfirmListener;
import com.shiqupad.order.presenter.GetCustomOrderInfoPresenter;
import com.shiqupad.order.presenter.GetOrderListPresenter;
import com.shiqupad.order.presenter.WaitConfirmPresenter;
import com.shiqupad.order.util.DateTimeUtils;
import com.shiqupad.order.util.MathUtil;
import com.flyco.tablayout.SegmentTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_general_ui.fragment.MBBaseFragment;
import cn.com.dreamtouch.magicbox_general_ui.ui.ActionSheetDialog;
import cn.com.dreamtouch.magicbox_general_ui.ui.AlertDialog;
import cn.com.dreamtouch.magicbox_general_ui.ui.MyListView;
import cn.com.dreamtouch.magicbox_general_ui.util.MyLogger;
import cn.com.dreamtouch.magicbox_general_ui.util.RxBusUtil;
import cn.com.dreamtouch.magicbox_http_client.network.model.DetailListBean;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetOrderListResopnse;
import cn.com.dreamtouch.magicbox_http_client.network.model.RxEvent;
import cn.com.dreamtouch.magicbox_http_client.network.model.WaitConfirmResponse;
import cn.com.dreamtouch.magicbox_repository.datasource.local.UserLocalData;
import cn.com.dreamtouch.magicbox_repository.datasource.remote.UserRemoteData;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import rx.functions.Action1;

import com.shiqupad.order.R;

/**
 * Created by LuoXueKun
 * on 2018/5/17
 */
public class OrderTableFragment extends MBBaseFragment implements OnTabSelectListener, AdapterView.OnItemClickListener, GetOrderListListener, SimpleCalendarDialogFragment.OnItemTabClickListener, GetCustomOrderInfoListener, WaitConfirmListener {
    @Bind(R.id.refreshLayout)
    SmartRefreshLayout mSmartRefreshLayout;
    @Bind(R.id.recyclerView)
    RecyclerView mRecyclerView;
    @Bind(R.id.cont_fr_table_search_tv)
    TextView mTvSearch;
    @Bind(R.id.tab_stl)
    SegmentTabLayout mStlTab;
    @Bind(R.id.tab_stl2)
    SegmentTabLayout mStlTab2;

    @Bind(R.id.cont_order_detail_rl)
    RelativeLayout llOrderDetail;
    @Bind(R.id.cont_order_account_detail_rl)
    RelativeLayout llOrderAccount;
    @Bind(R.id.lv_order_detail)
    MyListView myListView;
    @Bind(R.id.cont_order_detail_sv)
    ScrollView scrollView;
    @Bind(R.id.cont_fr_order_status_tobe_confirm_ll)
    LinearLayout llToBeConfirm;
    @Bind(R.id.cont_fr_order_status_tobe_confirm_refuse_tv)
    TextView tvRefuse;
    @Bind(R.id.cont_fr_order_status_tobe_confirm_sure_tv)
    TextView tvSure;

    @Bind(R.id.cont_fr_platform_subsidies_tv)
    TextView tvPlatformSubsidies;
    @Bind(R.id.cont_fr_member_discount_tv)
    TextView tvMemerDiscount;
    @Bind(R.id.cont_fr_voucher_tv)
    TextView tvVoucher;


    @Bind(R.id.cont_fr_find_zero_tv)
    TextView tvFindZero;
    @Bind(R.id.fr_order_shopcart_tv)
    TextView tvShopCart;
    @Bind(R.id.fr_order_tab_fee_tv)
    TextView tvTabFee;
    @Bind(R.id.fr_order_total_price_tv)
    TextView tvTotalPrice;
    /*-------*/
    @Bind(R.id.cont_fr_order_detail_tableName_tv)
    TextView tvTableName;
    @Bind(R.id.cont_fr_order_detail_num_tv)
    TextView tvDinnerNum;
    @Bind(R.id.cont_fr_order_detail_serialNum_tv)
    TextView tvSericalNum;
    @Bind(R.id.cont_fr_order_detail_waiter_tv)
    TextView tvWaiter;
    @Bind(R.id.cont_fr_order_detail_createTime_tv)
    TextView tvCreateName;
    @Bind(R.id.cont_fr_order_detail_tableStatus_tv)
    TextView tvTableStatus;
    /*-----------------------*/
    @Bind(R.id.cont_fr_total_consumption_right_tv)
    TextView tvTotalRightConsumption;
    @Bind(R.id.cont_fr_account_right_amount_tv)
    TextView tvTotalRightAmount;
    @Bind(R.id.cont_fr_ma_ling_tv)
    TextView tvMaLing;
    @Bind(R.id.cont_fr_cash_payments_tv)
    TextView tvCashPayments;
    @Bind(R.id.cont_fr_order_detail_orderNum_tv)
    TextView tvOrderPeople;
    @Bind(R.id.cont_fr_order_detail_refuse_ll)
    LinearLayout llRefuseCause;
    @Bind(R.id.cont_fr_order_detail_refuse_tv)
    TextView tvRefuseCause;
    @Bind(R.id.cont_fr_pay_time_tv)
    TextView tvPayTime;
    @Bind(R.id.cont_fr_order_detail_note_tv)
    TextView tvNote;

    @Bind(R.id.cont_fr_order_detail_total_ll)
    LinearLayout llTotalPrice;
    private List<GetOrderListResopnse.DataBean> dataBeanList;
    private List<GetOrderListResopnse.DataBean> newDataBeanList;

    OrderDetailActivityAdapter adapter;
    List<DetailListBean> list;
    private String mShopId;
    private String[] mTitles = {"今日未结订单", "历史已结订单"};
    private String[] mTitles2 = {"订单详情", "结算明细"};
    private GetOrderListTableAdapter mGetOrderListTableAdapter;
    private GetOrderListPresenter mGetOrderListPresenter;
    private GetCustomOrderInfoPresenter mGetCustomOrderInfoPresenter;
    private WaitConfirmPresenter mWaitConfirmPresenter;
    private String dateStr;
    private int mTabPosition = 0;
    private int mOrderStatus;
    int page = 1;
    //判断是下拉刷新还是上拉加载
    int refreshOrLoad = 0;
    private String remark;
    private String orderId;
    private boolean isConfirm;
    private String mTableId;
    private String mTableName;
    private String mDinnerPeople;
    private  int waitToConfirmTableId;
    private  int orderPosition;
    private  String paymentTypeName;
    public static OrderTableFragment newInstance(int roomId) {
        Bundle bundle = new Bundle();
        bundle.putInt(CommonConstant.Args.ROOM_ID, roomId);
        OrderTableFragment orderTableFragment = new OrderTableFragment();
        orderTableFragment.setArguments(bundle);
        return orderTableFragment;
    }

    @Override
    protected void initVariables() {
        super.initVariables();

        newDataBeanList = new ArrayList<>();
        dateStr = DateTimeUtils.currentDateTime(DateTimeUtils.yyyyMMdd);
        mShopId = UserLocalData.getInstance(getActivity()).getShopID();
        mGetOrderListPresenter = new GetOrderListPresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
        mGetCustomOrderInfoPresenter = new GetCustomOrderInfoPresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
        mWaitConfirmPresenter = new WaitConfirmPresenter(UserRepository.getInstance(UserLocalData.getInstance(getActivity())
                , UserRemoteData.getInstance(getActivity())), this);
    }

    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_order_table, container, false);
        ButterKnife.bind(this, view);
        mTvSearch.setText(dateStr);
        initRecyclerViewAndAdapter();
        initTab();
        getRxEvent();
        return view;
    }

    private void getRxEvent() {
        RxBusUtil.getDefault().toObservable(RxEvent.class).subscribe(new Action1<RxEvent>() {
            @Override
            public void call(RxEvent rxEvent) {
                switch (rxEvent.getFlag()) {
                    case CommonConstant.OpertionType.WAIT_TO_BE_CONFIRMED:
                        //initData();
                        break;
                    case CommonConstant.OpertionType.POSTORDER_SUCCESS:
                        initData();
                        break;
                    case CommonConstant.OpertionType.SWITCH_TABLE:
                        initData();
                        break;
                    case CommonConstant.OpertionType.WAIT_CONFIRM_ORDER:
                        waitToConfirmTableId = rxEvent.getData();
                        mLogger.e(waitToConfirmTableId+"收到的信息");
                        for (int i = 0; i < newDataBeanList.size(); i++) {
                            GetOrderListResopnse.DataBean dataBean = newDataBeanList.get(i);
                            if(dataBean.getOrderStatus() == 9 &&
                                    dataBean.getTableID().equals(String.valueOf(waitToConfirmTableId))){

                                mGetOrderListTableAdapter.setSelection(i);
                                mLogger.e(i+"定位");
                                orderPosition = i;
                                mGetCustomOrderInfoPresenter.getCustomOrderInfo(mShopId,
                                        String.valueOf(dataBean.getCustomOrderID()));

                            }
                        }

                        break;
                }
            }
        });
    }

    private void initTab() {
        mStlTab.setTabData(mTitles);
        mStlTab.setOnTabSelectListener(this);
        mStlTab2.setTabData(mTitles2);
        mStlTab2.setCurrentTab(0);
        mStlTab2.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelect(int position) {
                if (position == 0) {
                    llOrderAccount.setVisibility(View.GONE);
                    llOrderDetail.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.VISIBLE);
                } else if (position == 1) {
                    llOrderAccount.setVisibility(View.VISIBLE);
                    llOrderDetail.setVisibility(View.GONE);
                    scrollView.setVisibility(View.GONE);
                }
            }

            @Override
            public void onTabReselect(int position) {

            }
        });
    }

    @Override
    protected void loadData() {
        super.loadData();

    }

    public void initData() {
        mGetOrderListPresenter.getOrderList(dateStr, mShopId, "300", "1");
    }

    private void initRecyclerViewAndAdapter() {
        dataBeanList = new ArrayList<>();
        mGetOrderListTableAdapter = new GetOrderListTableAdapter(getActivity(), dataBeanList, R.layout.item_order_table, this);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setAdapter(mGetOrderListTableAdapter);
        mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        mSmartRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(final RefreshLayout refreshlayout) {
                refreshOrLoad = 1;
                initData();
            }
        });
       /* mSmartRefreshLayout.setOnLoadmoreListener(new OnLoadmoreListener() {
            @Override
            public void onLoadmore(final RefreshLayout refreshlayout) {
                refreshOrLoad = 2;
                page++;
               // loadData();
            }
        });*/
        mSmartRefreshLayout.setEnableRefresh(true);
        mSmartRefreshLayout.autoRefresh();
        mStlTab.setOnTabSelectListener(this);
        // mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        myListView.setAdapter(adapter);

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onTabSelect(int position) {
        mTabPosition = position;
        fitterTableOrder(mTabPosition);
    }

    private void fitterTableOrder(int position) {
        if (position == 1) {
            newDataBeanList.clear();
            for (GetOrderListResopnse.DataBean dataBean : dataBeanList) {
                if (dataBean.getOrderStatus() == CommonConstant.TableOrderStatus.HAVE_PAY
                        || dataBean.getOrderStatus() == CommonConstant.TableOrderStatus.HAVE_CANCEL
                        || dataBean.getOrderStatus() == CommonConstant.TableOrderStatus.LIST_SOME_RETURENED) {
                    newDataBeanList.add(dataBean);
                }
            }
        } else if (position == 0) {
            newDataBeanList.clear();

            for (GetOrderListResopnse.DataBean dataBean : dataBeanList) {
                if (dataBean.getOrderStatus() != CommonConstant.TableOrderStatus.HAVE_PAY
                        && dataBean.getOrderStatus() != CommonConstant.TableOrderStatus.HAVE_CANCEL
                        && dataBean.getOrderStatus() != CommonConstant.TableOrderStatus.LIST_SOME_RETURENED) {
                    newDataBeanList.add(dataBean);
                }
            }

        }
        mGetOrderListTableAdapter.refresh(newDataBeanList);
        if (newDataBeanList.size() <= 0) {
            return;
        }
        mLogger.e(orderPosition);
        if(orderPosition <newDataBeanList.size()){
            orderId = String.valueOf(newDataBeanList.get(orderPosition).getCustomOrderID());
            mTableId = newDataBeanList.get(orderPosition).getTableID();
            mTableName = newDataBeanList.get(orderPosition).getTableName();
            mDinnerPeople = String.valueOf(newDataBeanList.get(orderPosition).getPeopleNumber());
        }else {
            orderId = String.valueOf(newDataBeanList.get(0).getCustomOrderID());
            mTableId = newDataBeanList.get(0).getTableID();
            mTableName = newDataBeanList.get(0).getTableName();
            mDinnerPeople = String.valueOf(newDataBeanList.get(0).getPeopleNumber());
            mGetOrderListTableAdapter.setSelection(0);
        }
        mGetCustomOrderInfoPresenter.getCustomOrderInfo(mShopId, orderId);
        if (newDataBeanList == null && newDataBeanList.size() <= 0) {
            return;
        }


    }

    @Override
    public void onTabReselect(int position) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        orderPosition = position;
        mGetOrderListTableAdapter.setSelection(position);
        orderId = String.valueOf(newDataBeanList.get(position).getCustomOrderID());
        mTableId = newDataBeanList.get(position).getTableID();
        mTableName = newDataBeanList.get(position).getTableName();
        mDinnerPeople = String.valueOf(newDataBeanList.get(position).getPeopleNumber());
        mGetCustomOrderInfoPresenter.getCustomOrderInfo(mShopId, String.valueOf(orderId));
    }

    @Override
    public void getOrderListSuccess(GetOrderListResopnse getOrderListResopnse) {
        List<GetOrderListResopnse.DataBean> data = getOrderListResopnse.getData();
        if (data != null && data.size() > 0) {
            newDataBeanList.clear();
            newDataBeanList.addAll(data);
        } else {
            newDataBeanList.clear();
        }
        dataBeanList.clear();
        dataBeanList.addAll(newDataBeanList);
        //mGetOrderListTableAdapter.refresh(dataBeanList);

        switch (refreshOrLoad) {
            case 1:
                page = 1;
                mGetOrderListTableAdapter.refresh(dataBeanList);
                if (mSmartRefreshLayout == null) {
                    return;
                }
                mSmartRefreshLayout.finishRefresh();
                break;
            case 2:
                mSmartRefreshLayout.finishLoadMore();
                break;
        }

       fitterTableOrder(mTabPosition);
    }

    @Override
    public void basicFailure(String prompt) {
        if (getActivity() == null) {
            return;
        }
        DTLog.showMessageShort(getActivity(), prompt);
    }

    private void chooseDate() {
        SimpleCalendarDialogFragment simpleCalendarDialogFragment = new SimpleCalendarDialogFragment();
        simpleCalendarDialogFragment.setOnItemTabClickListener(this);
        simpleCalendarDialogFragment.show(getChildFragmentManager(),
                "test-simple-calendar");
    }

    @Override
    public void onItemTabClick(String dateStr) {
        mLogger.e(dateStr);
        if(dateStr == null) {
           DTLog.showMessageShort(getActivity(),"请选择查询时间");
           return;
        }
        this.dateStr = dateStr;
        mTvSearch.setText(dateStr);
        initData();
    }

    @Override
    public void getCustomOrderInfoSuccess(GetCustomOrderInfoResponse getCustomOrderInfoResponse) {
        GetCustomOrderInfoResponse.DataBean data = getCustomOrderInfoResponse.getData();
        initRightData(data);
    }

    private void initRightData(GetCustomOrderInfoResponse.DataBean data) {
        String tableName = data.getTableName();
        int peopleNumber = data.getPeopleNumber();
        String serialNumber = data.getSerialNumber();
        mOrderStatus = data.getOrderStatus();
        long createDate = data.getCreateDate();

        switch (mOrderStatus) {
            //待确认
            case CommonConstant.TableOrderStatus.WAIT_CONFIRM_ED:
                llTotalPrice.setVisibility(View.VISIBLE);
                tvShopCart.setVisibility(View.VISIBLE);
                tvRefuse.setVisibility(View.VISIBLE);
                tvRefuse.setText("拒绝");
                tvSure.setText("确认下单");
                llToBeConfirm.setVisibility(View.VISIBLE);
                tvTotalPrice.setText(String.format("%s", MathUtil.decimalConvert(data.getShouldPayAmount() +
                        data.getTableFee())));
                break;
            //待支付
            case CommonConstant.TableOrderStatus.TO_BE_PAY:
                llTotalPrice.setVisibility(View.VISIBLE);
                tvShopCart.setVisibility(View.VISIBLE);
                tvRefuse.setText("加菜");
                tvRefuse.setVisibility(View.VISIBLE);
                tvSure.setText("结账");
                llToBeConfirm.setVisibility(View.VISIBLE);
                tvTotalPrice.setText(String.format("%s", MathUtil.decimalConvert(data.getShouldPayAmount() +
                        data.getTableFee())));
                break;
            //已支付:
            case CommonConstant.TableOrderStatus.HAVE_PAY:
                llTotalPrice.setVisibility(View.GONE);
                tvRefuse.setVisibility(View.GONE);
                tvSure.setText("开票");
                llToBeConfirm.setVisibility(View.GONE);
                tvShopCart.setVisibility(View.GONE);
                tvTotalPrice.setText(String.format("%s", MathUtil.decimalConvert(data.getShouldPayAmount())));
                break;
                //部分退单
            case CommonConstant.TableOrderStatus.LIST_SOME_RETURENED:
                llTotalPrice.setVisibility(View.GONE);
                tvShopCart.setVisibility(View.GONE);
                llToBeConfirm.setVisibility(View.GONE);
                tvTotalPrice.setText(String.format("%s", MathUtil.decimalConvert(data.getShouldPayAmount())));
                break;
            default:
                llTotalPrice.setVisibility(View.GONE);
                tvShopCart.setVisibility(View.GONE);
                llToBeConfirm.setVisibility(View.GONE);
                tvTotalPrice.setText(String.format("%s", MathUtil.decimalConvert(data.getTotalPrice())));

                break;
        }
        list = new ArrayList<>();
        list.clear();
        list.addAll(data.getCustomorderdetailDOList());
        if (adapter == null) {
            adapter = new OrderDetailActivityAdapter(getContext(),
                    list);
        } else {
            adapter.setData(list);
        }
        myListView.setAdapter(adapter);
        initConsumDetail(data);
        initTextView(data);
    }

    private void initConsumDetail(GetCustomOrderInfoResponse.DataBean data) {
        tvPlatformSubsidies.setText(String.format("%s", data.getPlatfomDiscountAmount()));
        if (data.getVoucherName() == null) {
            tvVoucher.setText("-0.00");
        } else {
            tvVoucher.setText(String.format("-%s", data.getVoucherAmount()));
        }

        tvTabFee.setText(String.format("开台费：  %s", data.getTableFee()));
        tvShopCart.setText(String.format("菜品合计：  %s", data.getShouldPayAmount()));
        tvTotalRightConsumption.setText(data.getShouldPayAmount() + "");


    }

    @OnClick({R.id.cont_fr_table_search_tv,
            R.id.cont_fr_order_status_tobe_confirm_sure_tv,
            R.id.cont_fr_order_status_tobe_confirm_refuse_tv})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.cont_fr_order_status_tobe_confirm_sure_tv:
                switch (mOrderStatus) {
                    case CommonConstant.TableOrderStatus.WAIT_CONFIRM_ED:
                        isConfirm = true;
                        mWaitConfirmPresenter.waiterConfirmOrder(mShopId, orderId, "true", remark);
                        break;
                    case CommonConstant.TableOrderStatus.TO_BE_PAY:
                        goToAccountActivity();
                        break;
                }
                break;
            case R.id.cont_fr_order_status_tobe_confirm_refuse_tv:
                switch (mOrderStatus) {
                    case CommonConstant.TableOrderStatus.WAIT_CONFIRM_ED:
                        isConfirm = false;
                        rejectOrder();
                        break;
                    case CommonConstant.TableOrderStatus.TO_BE_PAY:
                        //goToNewShopAtivity();
                        DTLog.showMessageShort(getActivity(),"暂不支持该功能");
                        break;
                }
                break;
            case R.id.cont_fr_table_search_tv:
                chooseDate();
                break;
        }
    }

    private void goToNewShopAtivity() {
        Intent intent = new Intent(getActivity(), NewShopCartActivity2.class);
        intent.putExtra(CommonConstant.Args.TABLE_ID, mTableId
        );
        intent.putExtra(CommonConstant.Args.TABLE_NAME, mTableName
        );
        intent.putExtra(CommonConstant.Args.DINNER_NUM, mDinnerPeople
        );
        startActivity(intent);
    }

    private void rejectOrder() {
        new ActionSheetDialog(Objects.requireNonNull(getActivity())).builder()
                .setTitle("请选择拒绝原因？")
                .addSheetItem("厨师下班", ActionSheetDialog.SheetItemColor.Red, new ActionSheetDialog.OnSheetItemClickListener() {
                    @Override
                    public void onClick(int which) {
                        mWaitConfirmPresenter.waiterConfirmOrder(mShopId, orderId, "false", "厨师下班");
                    }
                }).addSheetItem("桌台无人", ActionSheetDialog.SheetItemColor.Red,
                new ActionSheetDialog.OnSheetItemClickListener() {
                    @Override
                    public void onClick(int which) {
                        mWaitConfirmPresenter.waiterConfirmOrder(mShopId, orderId, "false", "桌台无人");
                    }
                }).addSheetItem("全部售罄", ActionSheetDialog.SheetItemColor.Red, new ActionSheetDialog.OnSheetItemClickListener() {
            @Override
            public void onClick(int which) {
                mWaitConfirmPresenter.waiterConfirmOrder(mShopId, orderId, "false", "全部售罄");
            }
        }).addSheetItem("其他原因", ActionSheetDialog.SheetItemColor.Red, new ActionSheetDialog.OnSheetItemClickListener() {
            @Override
            public void onClick(int which) {
                mWaitConfirmPresenter.waiterConfirmOrder(mShopId, orderId, "false", "其他原因");
            }
        })
                .setCancelable(true)
                .show();
    }

    @Override
    public void waitConfirmSuccess(WaitConfirmResponse waitConfirmResponse) {
        if (isConfirm) {
            showSureOrRefuseDialog("下单成功");
        } else {
            showSureOrRefuseDialog("已拒单");
        }

        initData();
    }

    private void showSureOrRefuseDialog(String title) {
        new AlertDialog(Objects.requireNonNull(getActivity())).builder()
                .setTitle(title)
                .setMsg("是否回到大厅继续点餐！")
                .setCancelable(true)
                .setPositiveButton("确认", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        RxBusUtil.getDefault().post(new RxEvent(CommonConstant.OpertionType.CALL_SUCCESS,
                                CommonConstant.OpertionType.CALL_SUCCESS));
                    }
                })
                .setNegativeButton("取消", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }).show();
    }

    private void initTextView(GetCustomOrderInfoResponse.DataBean dataBean) {
        double discountAmount = dataBean.getDiscountAmount();
        if (dataBean.getVoucherAmount() == null) {
            tvMaLing.setText("未选择代金券");
        } else {
            tvMaLing.setText(dataBean.getVoucherAmount() + "");
        }
        if(dataBean.getRemark() == null){
            tvNote.setText("");
        }else {
            tvNote.setText(dataBean.getRemark());
        }
        tvTotalRightAmount.setText(discountAmount + "");
        /*------*/
        tvTableName.setText(dataBean.getTableName());
        tvOrderPeople.setText(dataBean.getCreateUserName());
        tvDinnerNum.setText(dataBean.getPeopleNumber() + "");
        tvWaiter.setText("");
        tvSericalNum.setText(dataBean.getSerialNumber());

        int paymentTypeCode = dataBean.getPaymentTypeCode();
        paymentTypeName = (String) dataBean.getPaymentTypeName();
        initPayCode(paymentTypeCode);


        tvCreateName.setText(DateTimeUtils.parseDate2(
                DateTimeUtils.getStringDate("yyyy-MM-dd  HH:mm:ss", new Date(dataBean.getCreateDate())),
                getActivity()));
        tvPayTime.setText(dataBean.getPayTime());

        int orderStatus = dataBean.getOrderStatus();
        switch (orderStatus) {
            case 1:
                tvTableStatus.setText("待确认");
                llRefuseCause.setVisibility(View.GONE);

                break;
            case 2:
                tvTableStatus.setText("已确认");
                llRefuseCause.setVisibility(View.GONE);

                break;
            case 3:
                tvTableStatus.setText("已支付");
                llRefuseCause.setVisibility(View.GONE);

                break;
            case 4:
                tvTableStatus.setText("已取消");
                llRefuseCause.setVisibility(View.VISIBLE);
                if (dataBean.getRejectReason() == null) {
                    tvRefuseCause.setText("无");
                } else {
                    tvRefuseCause.setText(dataBean.getRejectReason() + "");
                }
                break;
            case 5:
                tvTableStatus.setText("已失效");
                llRefuseCause.setVisibility(View.GONE);

                break;
            case 6:
                tvTableStatus.setText("待支付");
                llRefuseCause.setVisibility(View.GONE);




                break;
            case 7:
                tvTableStatus.setText("支付失败");
                llRefuseCause.setVisibility(View.GONE);

                break;
            case 8:
                tvTableStatus.setText("部分退单");
                llRefuseCause.setVisibility(View.GONE);

                break;
            case 9:
                tvTableStatus.setText("待确认");
                llRefuseCause.setVisibility(View.GONE);

                break;
        }

    }

    private void initPayCode(int paymentTypeCode) {
        String payTypeStr = "";
        switch (paymentTypeCode) {
            case 1:
                payTypeStr = "支付宝";
                break;
            case 2:
                payTypeStr = "微信支付";

                break;
            case 3:
                payTypeStr = "银联";

                break;
            case 4:
                payTypeStr = "会员";

                break;
            case 5:
                payTypeStr = "现金";

                break;
            case 6:
                payTypeStr = "恩牛";

                break;
            case 7:
                payTypeStr = "美团";

                break;
            case 8:
                payTypeStr = "小程序";

                break;
            case 9:
                payTypeStr = "其它";
                if(paymentTypeName.equals("null")){
                    tvFindZero.setText("无");
                }else {
                    tvFindZero.setText(paymentTypeName);
                }
                return;
        }
        tvFindZero.setText(payTypeStr);
    }

    private void goToAccountActivity() {
        Intent intent = new Intent(getActivity(), AccountActivity.class);
        intent.putExtra(CommonConstant.Args.ORDER_ID, orderId);
        intent.putExtra(CommonConstant.Args.DINNER_NUM, mDinnerPeople);
        startActivity(intent);
    }
}
