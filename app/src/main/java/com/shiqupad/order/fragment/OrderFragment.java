package com.shiqupad.order.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.flyco.tablayout.SlidingTabLayout;
import com.flyco.tablayout.listener.OnTabSelectListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cn.com.dreamtouch.magicbox_general_ui.fragment.MBBaseFragment;
import com.shiqupad.order.R;
/**
 * Created by LuoXueKun
 * on 2018/5/14
 */
public class OrderFragment extends MBBaseFragment implements OnTabSelectListener, SimpleCalendarDialogFragment.OnItemTabClickListener {
    private static final DateFormat FORMATTER = SimpleDateFormat.getDateInstance();

    @Bind(R.id.ct_main_stl)
    SlidingTabLayout mSlidingTabLayout;
    @Bind(R.id.fr_table_scan_vp)
    ViewPager mViewPager;
    @Bind(R.id.content_foot_message_iv)
    ImageView ivMessage;
    @Bind(R.id.cont_order_search_tv)
    TextView tvSearch;
    private ArrayList<Fragment> mFragments = new ArrayList<>();
    private List<String> mTitleList;
    private MyPagerAdapter mAdapter;
    public OrderFragment() {
    }

    public static OrderFragment newInstance() {
        OrderFragment fragment = new OrderFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void initVariables() {
        super.initVariables();
        mTitleList = new ArrayList<>();
        mTitleList.add("堂食订单");
        //mTitleList.add("预约订单");
       // mTitleList.add("外卖订单");
    }

    @Override
    protected View initView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fr_order, container, false);
        ButterKnife.bind(this, view);
        initTabAndView();
        return view;
    }


    public void refreshTabData() {

    }
    private void initTabAndView() {
        OrderTableFragment orderTableFragment = OrderTableFragment.newInstance(1);
        mFragments.add(orderTableFragment);
        //mFragments.add(BookingOrderFragment.newInstance(1));
        //mFragments.add(DeliveryOrdersFragment.newInstance(1));
        mAdapter = new MyPagerAdapter(getChildFragmentManager());
        mViewPager.setAdapter(mAdapter);
        mSlidingTabLayout.setViewPager(mViewPager);
        mSlidingTabLayout.setOnTabSelectListener(this);
        mViewPager.setCurrentItem(0);
        mViewPager.setOffscreenPageLimit(mTitleList.size());
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onTabSelect(int position) {

    }

    @Override
    public void onTabReselect(int position) {

    }

    @Override
    public void onItemTabClick(String dateStr) {
        tvSearch.setText(dateStr);
    }


    private class MyPagerAdapter extends FragmentPagerAdapter {
        private MyPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mTitleList.get(position);
        }

        @Override
        public Fragment getItem(int position) {


            return mFragments.get(position);

        }
    }
    @OnClick(R.id.cont_order_search_tv)
    public void onClick(View view){
        switch (view.getId()){
            case R.id.cont_order_search_tv:

                break;
        }
    }

}
