package com.shiqupad.order.fragment;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.shiqupad.order.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import cn.com.dreamtouch.magicbox_common.util.TimeUtil;

/**
 * Created by LuoXueKun
 * on 2018/5/24
 */
public  class SimpleCalendarDialogFragment extends AppCompatDialogFragment
        implements OnDateSelectedListener {
    private static final DateFormat FORMATTER = SimpleDateFormat.getDateInstance();

    private TextView textView;
    private  String dateStr;
    public void setOnItemTabClickListener(OnItemTabClickListener onItemTabClickListener){
        this.onItemTabClickListener = onItemTabClickListener;
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_basic, null);
        textView = (TextView) view.findViewById(R.id.textView);
        MaterialCalendarView widget = (MaterialCalendarView) view.findViewById(R.id.calendarView);
        widget.setShowOtherDates(MaterialCalendarView.SHOW_ALL);
        Calendar instance = Calendar.getInstance();
        widget.setSelectedDate(instance.getTime());
        Calendar instance1 = Calendar.getInstance();
        instance1.set(instance1.get(Calendar.YEAR), Calendar.JANUARY, 1);
        Calendar instance2 = Calendar.getInstance();
        instance2.set(instance2.get(Calendar.YEAR), instance2.get(Calendar.MONTH), instance2.get(Calendar.DAY_OF_MONTH));
        widget.state().edit()
               // .setMinimumDate(instance1.getTime())
                .setMaximumDate(instance2.getTime())
                .commit();
        widget.setOnDateChangedListener(this);

        return new AlertDialog.Builder(getActivity())
                .setView(view)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(onItemTabClickListener != null) {
                            onItemTabClickListener.onItemTabClick(dateStr);
                        }
                    }
                })
                .create();
    }

    @Override
    public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
        //dateStr = FORMATTER.format(date.getDate());
        dateStr = TimeUtil.convertDateToString(date.getDate(), TimeUtil.myyyy_MM_ddFormat);
        textView.setText(dateStr);

    }
    private  OnItemTabClickListener  onItemTabClickListener;
    interface  OnItemTabClickListener  {

        void onItemTabClick(String dateStr);
    }
}