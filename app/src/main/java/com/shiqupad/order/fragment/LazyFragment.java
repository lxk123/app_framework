package com.shiqupad.order.fragment;

/**
 * Created by LuoXueKun
 * on 2018/5/15
 */

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import cn.com.dreamtouch.magicbox_general_ui.fragment.MBBaseFragment;
import cn.com.dreamtouch.magicbox_general_ui.util.MyLogger;

/**
 * fragment 基类
 */
public abstract class LazyFragment extends MBBaseFragment {

    /**
     * Fragment当前状态是否可见
     */
    protected boolean isVisible;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * 在这里实现Fragment数据的缓加载(该方法搭配ViewPager才能生效).
     * @param isVisibleToUser
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getUserVisibleHint()) {
            isVisible = true;
            onVisible();
        } else {
            isVisible = false;
            onInvisible();
        }
    }

    /**
     * 可见
     */
    protected void onVisible() {
        lazyLoad();
    }

    protected abstract void lazyLoad();

    /**
     * 不可见
     */
    protected void onInvisible() {
    }
}
