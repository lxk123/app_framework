package com.shiqupad.order.util;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * description: 时间操作工具类
 * <p/>
 * Created by FANGFEI on 2016/2/3 13:54.
 */
public class DateTimeUtils {

    private final static String TAG = DateTimeUtils.class.getSimpleName();

    public final static String yyyyMMdd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
    public final static String yyyyMMdd = "yyyy-MM-dd";
    public final static String yyyyMMddHHmmss = "yyyyMMddHHmmss";

    /**
     * 获取当前时间的指定格式字符串
     * @param format String ; 时间格式
     * @return String
     */
    public static String currentDateTime(String format) {
       /* if (StringUtils.isBlank(format)) {
            return "";
        }*/
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(format, Locale.CHINA);
            return sdf.format(new Date());
        } catch (Exception e) {
            Log.e(TAG, "get format current datetime fail!", e);
        }
        return "";
    }


    /** * 获取指定日期是星期几
     * 参数为null时表示获取当前日期是星期几
     * @param date
     * @return
     */
    public static String getWeekOfDate(Date date) {
        String[] weekOfDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar calendar = Calendar.getInstance();
        if(date != null){
            calendar.setTime(date);
        }
        int w = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0){
            w = 0;
        }
        return weekOfDays[w];
    }

    public static String getStringDate(String format, Date Date) {
        if (TextUtils.isEmpty(format)) return null;

        SimpleDateFormat sdf = new SimpleDateFormat(format);

        return sdf.format(Date);
    }

    public static String getYear(String date) {
        return date.substring(0, 4);
    }

    public static String getMonth(String date) {
        String month = date.substring(5,7);

        if(month.startsWith("0")){
            return month.substring(1);
        }

        return month;
    }

    public static String getDay(String date) {
        String day = date.substring(8,10);

        if(day.startsWith("0")){
            return day.substring(1);
        }

        return day;
    }

    public static Date getDate(String Time) {
        if (TextUtils.isEmpty(Time)) return null;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = sdf.parse(Time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * 时间截取
     *
     * @param datetime
     * @param splite
     * @return
     */
    public static int[] getYMDArray(String datetime, String splite) {
        int date[] = { 0, 0, 0, 0, 0 };
        if (datetime != null && datetime.length() > 0) {
            String[] dates = datetime.split(splite);
            int position = 0;
            for (String temp : dates) {
                date[position] = Integer.valueOf(temp);
                position++;
            }
        }
        return date;
    }

    public static String nextMonthFirstDate(String dateStr) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(dateStr);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.MONTH, 1);
        return sdf.format(calendar.getTime());
    }

    public static String thisMonthFirstDate(String dateStr) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date date = sdf.parse(dateStr);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return sdf.format(calendar.getTime());
    }

    public static String getEarlyDate(int early) {

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_MONTH, -early);
        return sdf.format(calendar.getTime());
    }

    public static String parseDate(String createTime) {
        try {
            String ret = "";
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            long create = sdf.parse(createTime).getTime();
            Calendar now = Calendar.getInstance();
            long ms = 1000 * (now.get(Calendar.HOUR_OF_DAY) * 3600 + now.get(Calendar.MINUTE) * 60 + now.get(Calendar.SECOND));//毫秒数
            long ms_now = now.getTimeInMillis();
            if (ms_now - create < 24 * 3600 * 1000) {
                ret = "今天";
            } else if (ms_now - create < 24 * 3600 * 1000 * 2) {
                ret = "昨天";
            } else if (ms_now - create < 24 * 3600 * 1000 * 3) {
                ret = "前天";
            } else {
                ret = DateTimeUtils.getWeekOfDate(sdf.parse(createTime));
            }
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String parseDate2(String createTime, Context context) {
        String ret = "";
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            long create = sdf.parse(createTime).getTime();
            String now = getStringDate("yyyy-MM-dd HH:mm:ss", new Date());
            long ms_now = sdf.parse(now).getTime();
            if (ms_now - create < 60 * 1000) {
                ret = "刚刚";
            } else if (now.substring(0, 10).contains(createTime.substring(0, 10))) {
                ret = createTime.substring(12, 17);
            } else {
                ret = createTime.replace("-", "/");
            }
            return ret;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    public static boolean beforeCurrentDay(String date){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date creatDate = sdf.parse(date);
            return creatDate.getTime() <= new Date().getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    public static boolean beforeCurrentMonth(String date){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM");
        try {
            Date creatDate = sdf.parse(date);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.DAY_OF_MONTH,1);
            return creatDate.getTime() <= calendar.getTime().getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * 对比时间，返回时间差
     *
     * @param date1 传入的时间描述 如: "2015-11-28"
     * @param date2 传入的时间描述 如: "2015-11-28"
     * @return 两天之间的时间差 (单位：天)
     */
    public static int compareTime(String date1, String date2) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date d1 = format.parse(date1);
            Date d2 = format.parse(date2);
            long compareTime = d2.getTime() - d1.getTime();

            return (int) (compareTime / 3600000 / 24);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return -1;
    }

}
