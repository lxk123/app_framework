package com.shiqupad.order.util;

import java.math.BigDecimal;
import java.text.NumberFormat;

/**
 * Created by Administrator on 2016/3/23.
 */
public class MathUtil {

    public static String getIntStr(double x){
        return String.valueOf((int)x);
    }

    public static String getTwoDigitStr(double x){
        double d = x-(int)x;
        java.text.DecimalFormat df = new java.text.DecimalFormat("#.##");
        return df.format(d).substring(1);
    }

    public static String saveOneDigit(double x){
        java.text.DecimalFormat df = new java.text.DecimalFormat("#.#");
        return df.format(x);
    }

    public static String saveTwoDigit(double x){
        java.text.DecimalFormat df = new java.text.DecimalFormat("#.##");
        return df.format(x);
    }

    public static String saveTwoDigit2(double x){
        BigDecimal b = new BigDecimal(Double.toString(x));
        double f1 = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
        java.text.DecimalFormat df = new java.text.DecimalFormat("#.##");
        return df.format(f1);
    }

    public static String getPercentInstance(double x){
        NumberFormat nf = NumberFormat.getPercentInstance();
        nf.setMaximumFractionDigits(1);
        return nf.format(x);
    }

    //小数转化
    public static double decimalConvert(double value) {
        BigDecimal bd = new BigDecimal(value);
        return bd.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

}
