package com.shiqupad.order.util;

import android.content.Context;

import com.facebook.drawee.view.SimpleDraweeView;
import com.shiqupad.order.R;

import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_common.util.FrescoHelper;


/**
 * Created by LuoXueKun
 * on 2018/5/23
 */
public class DisplayIconUtil {
    public static void showIcon(Context context, int position, SimpleDraweeView iv) {
        switch (position) {
            case 0:
                FrescoHelper.loadResPic(context, iv, R.drawable.btn_add);
                break;
            case 1:
                FrescoHelper.loadResPic(context, iv, R.drawable.btn_order);
                break;
            case 2:
                FrescoHelper.loadResPic(context, iv, R.drawable.btn_transfer);
                break;
            case 3:
                FrescoHelper.loadResPic(context, iv, R.drawable.btn_checkout);
                break;
            case 4:
                FrescoHelper.loadResPic(context, iv, R.drawable.btn_modify);
                break;
            case 5:
                FrescoHelper.loadResPic(context, iv, R.drawable.btn_clean);
                break;
            case 6:
                FrescoHelper.loadResPic(context, iv, R.drawable.btn_pin);
                break;
            case 7:
                FrescoHelper.loadResPic(context, iv, R.drawable.btn_merge);
                break;

        }
    }

    public static void showMessageIcon(Context context, String messageType, SimpleDraweeView iv) {
        /* //"呼叫服务员  待接单提醒   打印机提醒"*/
        switch (messageType) {
            case "呼叫服务员":
                FrescoHelper.loadResPic(context, iv, R.drawable.ic_message);
                break;
            case "待接单提醒":
                FrescoHelper.loadResPic(context, iv, R.drawable.ic_dingdan);
                break;
            case "打印机提醒":
                FrescoHelper.loadResPic(context, iv, R.drawable.ic_printer);
                break;
            default:
                break;
        }
    }

    public static void showPayMethodIcon(Context context, String paymentType, SimpleDraweeView iv) {
        /*
        *   public final static String PAY_TYPE_01 = "CASHPAY";//现金
       public final static String PAY_TYPE_02 = "ALIPAY";//支付宝
       public final static String PAY_TYPE_03 = "WXPAY";//微信
       public final static String PAY_TYPE_04 = "ALIMEMBERPAY";//支付宝会员卡支付
       public final static String PAY_TYPE_05 = "WXMEMBERPAY";//微信会员卡支付
       public final static String PAY_TYPE_06 = "OTHERPAY";//自定义支付
        *
        * */
        switch (paymentType) {
            case CommonConstant.PayMethod.PAY_TYPE_01:
                FrescoHelper.loadResPic(context, iv, R.drawable.btn_checkout);
                break;
            case CommonConstant.PayMethod.PAY_TYPE_02:
                FrescoHelper.loadResPic(context, iv, R.drawable.ic_alipay_method);
                break;
            case CommonConstant.PayMethod.PAY_TYPE_03:
                FrescoHelper.loadResPic(context, iv, R.drawable.ic_weixinpay);
                break;
            case CommonConstant.PayMethod.PAY_TYPE_04:
                FrescoHelper.loadResPic(context, iv, R.drawable.ic_cash);
                break;
            case CommonConstant.PayMethod.PAY_TYPE_05:
                FrescoHelper.loadResPic(context, iv, R.drawable.ic_cash);
                break;
            case CommonConstant.PayMethod.PAY_TYPE_06:
                FrescoHelper.loadResPic(context, iv, R.drawable.ic_other);
                break;
            default:
                break;
        }
    }

}
