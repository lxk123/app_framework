package com.shiqupad.order.util;

import android.content.Context;
import android.mtp.MtpStorageInfo;
import android.os.Bundle;

import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechSynthesizer;

import cn.com.dreamtouch.magicbox_general_ui.util.MyLogger;

/**
 * Created by wanxinyang on 4/9/2017.
 */

public class XunfeiSoundPlay {
    /***********语音合成*************/
    public static void initSpeechSynthesizer(Context context, String printContent) {
        //1.创建SpeechSynthesizer对象, 第二个参数：本地合成时传InitListener
        SpeechSynthesizer mTts = SpeechSynthesizer.createSynthesizer(context, null);
        MyLogger.kLog().e(mTts ==null);
        //2.合成参数设置，详见《科大讯飞MSC API手册(Android)》SpeechSynthesizer 类
        mTts.setParameter(SpeechConstant.VOICE_NAME, "xiaoqi");//设置发音人
        mTts.setParameter(SpeechConstant.SPEED, "50");//设置语速
        mTts.setParameter(SpeechConstant.VOLUME, "80");//设置音量，范围0~100
        mTts.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD); //设置云端
        //设置合成音频保存位置（可自定义保存位置），保存在“./sdcard/iflytek.pcm”
        //保存在SD卡需要在AndroidManifest.xml添加写SD卡权限
        //如果不需要保存合成音频，注释该行代码
        //mTts.setParameter(SpeechConstant.TTS_AUDIO_PATH, "./sdcard/iflytek.pcm");
        //3.开始合成
//        String speechText = (printContent.getChannel() == 1 ? "微信" : "支付宝") + "成功收款" + printContent.getMoney() + "元";
        mTts.startSpeaking(printContent, new com.iflytek.cloud.SynthesizerListener() {

            //开始播放
            @Override
            public void onSpeakBegin() {
                    MyLogger.kLog().e("onSpeakBegin");
            }

            //缓冲进度回调
            //percent为缓冲进度0~100，beginPos为缓冲音频在文本中开始位置，endPos表示缓冲音频在文本中结束位置，info为附加信息。
            @Override
            public void onBufferProgress(int i, int i1, int i2, String s) {
                MyLogger.kLog().e("onBufferProgress");
            }

            //暂停播放
            @Override
            public void onSpeakPaused() {
                MyLogger.kLog().e("onSpeakPaused");
            }

            //恢复播放回调接口
            @Override
            public void onSpeakResumed() {
                MyLogger.kLog().e("onSpeakResumed");
            }

            //播放进度回调
            //percent为播放进度0~100,beginPos为播放音频在文本中开始位置，endPos表示播放音频在文本中结束位置.
            @Override
            public void onSpeakProgress(int i, int i1, int i2) {
                MyLogger.kLog().e("onSpeakProgress");
            }

            //会话结束回调接口，没有错误时，error为null
            @Override
            public void onCompleted(SpeechError speechError) {
                MyLogger.kLog().e("onCompleted");
            }

            //会话事件回调接口
            @Override
            public void onEvent(int i, int i1, int i2, Bundle bundle) {
                MyLogger.kLog().e("onEvent");
            }
        });
    }
}
