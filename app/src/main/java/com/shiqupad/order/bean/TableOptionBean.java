package com.shiqupad.order.bean;

/**
 * Created by LuoXueKun
 * on 2018/5/23
 */
public class TableOptionBean {
    private String name;

    public TableOptionBean(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
