package com.shiqupad.order.cache;

import android.content.Context;
import android.content.SharedPreferences;
import cn.com.dreamtouch.magicbox_common.util.CommonConstant;

/**
 * Manage SharedPreferences data
 * Created by zj on 5/7/15.
 */
public class ZJPrefer {


    private static ZJPrefer sInstance;
    private static SharedPreferences mPref;


    private ZJPrefer(Context context) {
        mPref = context.getSharedPreferences(CommonConstant.Args.EDIT_PREF_NAME, Context.MODE_PRIVATE);
    }


    public static ZJPrefer getInstance(Context context) {
        if (sInstance == null) {
            synchronized (ZJPrefer.class) {
                sInstance = new ZJPrefer(context);
            }
        }
        return sInstance;
    }


    public SharedPreferences getPrefs() {
        return mPref;
    }


    public static void remove(String key) {
        mPref.edit().remove(key).apply();
    }

    public static boolean clear() {
        return mPref.edit().clear().commit();
    }


}
