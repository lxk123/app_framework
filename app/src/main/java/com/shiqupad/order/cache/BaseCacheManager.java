package com.shiqupad.order.cache;

import android.content.Context;



import com.shiqupad.order.MainApplication;

/**
 * 内存缓存管理基类
 * Created by lsy on 16/8/18.
 */
public class BaseCacheManager {
    protected MainApplication mApp;
    protected String TAG = getClass().getSimpleName();

    protected BaseCacheManager(Context context) {
        mApp = (MainApplication) context.getApplicationContext();
    }
}
