package com.shiqupad.order.cache;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;

/**
 * 购物车缓存管理
 * Created by lsy on 16/8/18.
 */
public class CartCacheManager extends BaseCacheManager {

    private static CartCacheManager instance = null;
    private ACache cache;
    private HashMap<Integer, ArrayList<GetShopDishResponse.TwoShopDish>> cartCache = new HashMap<>();

    public static CartCacheManager getInstance(Context context) {
        if (instance == null) {
            synchronized (CartCacheManager.class) {
                if (instance == null) {
                    instance = new CartCacheManager(context);
                }
            }
        }
        return instance;
    }
    private CartCacheManager(Context context) {
        super(context);
        cache = ACache.get(context, 1024 * 1024 * 50, Integer.MAX_VALUE);
    }

    /**
     * 获取当前桌台购物车
     *
     * @param tableId
     * @return ArrayList<DishInfo>
     */
    public List<GetShopDishResponse.TwoShopDish> getCartList(int tableId) {
        List<GetShopDishResponse.TwoShopDish> infos = new Gson().fromJson(
                cache.getAsString(String.valueOf(tableId)),
                new TypeToken<List<GetShopDishResponse.TwoShopDish>>(){}.getType());
        if (infos != null) {
        }
        return infos;
    }

    /**
     * 更新当前桌台购物车
     *
     * @param tableId int
     * @param list
     *
     */
    public void updateCart(int tableId, List<GetShopDishResponse.TwoShopDish> list) {

        if (cache.file(String.valueOf(tableId)) != null) {
            cache.remove(String.valueOf(tableId));
        }
        cache.put(String.valueOf(tableId), new Gson().toJson(list), 60 * 30);

    }


    /**
     * 移除购物车
     *
     * @param tableId
     * @return 是否移除成功
     */
    public boolean clearCart(int tableId) {
        if (cache.file(String.valueOf(tableId)) != null) {
            return cache.remove(String.valueOf(tableId));
        }
        return true;
    }
}
