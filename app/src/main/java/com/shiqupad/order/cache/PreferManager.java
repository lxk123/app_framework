package com.shiqupad.order.cache;

import android.content.Context;
import android.content.SharedPreferences;

import cn.com.dreamtouch.magicbox_common.util.CommonConstant;

/**
 * Preference manager
 * Created by zj on 15/11/10.
 */
public class PreferManager {


    private ZJPrefer mZJPrefer;
    private static PreferManager sInstance;


    public static PreferManager getInstance(Context context) {
        if (sInstance == null) {
            synchronized (ZJPrefer.class) {
                sInstance = new PreferManager(context);
            }
        }
        return sInstance;
    }

    private PreferManager(Context context) {
        mZJPrefer = ZJPrefer.getInstance(context.getApplicationContext());
    }


    /**
     * 是否已登录
     *
     * @return boolean
     */
    public boolean isLogin() {
        SharedPreferences sp = mZJPrefer.getPrefs();
        return sp.getBoolean(CommonConstant.Args.KEY_PREF_LOGIN_STATUS, false);
    }


    /**
     * 修改登录状态
     */
    public void changeLoginStatus(boolean isLogin) {
        SharedPreferences.Editor editor = mZJPrefer.getPrefs().edit();
        editor.putBoolean(CommonConstant.Args.KEY_PREF_LOGIN_STATUS, isLogin);
        editor.apply();
    }

    /**
     * 消息数增加
     *
     * @return boolean
     */
    public void addNotifyNm() {
        SharedPreferences sp = mZJPrefer.getPrefs();
        SharedPreferences.Editor editor = mZJPrefer.getPrefs().edit();
        editor.putInt(CommonConstant.Args.KEY_PREF_NOTIFY_NUM, sp.getInt(CommonConstant.Args.KEY_PREF_NOTIFY_NUM, 0) + 1);
        editor.apply();
    }

    /**
     * 消息数清空
     *
     * @return boolean
     */
    public void clearNotifyNm() {
        SharedPreferences.Editor editor = mZJPrefer.getPrefs().edit();
        editor.putInt(CommonConstant.Args.KEY_PREF_NOTIFY_NUM, 0);
        editor.apply();
    }

    /**
     * 消息数清空
     *
     * @return boolean
     */
    public int getNotifyNm() {
        SharedPreferences sp = mZJPrefer.getPrefs();
        return sp.getInt(CommonConstant.Args.KEY_PREF_NOTIFY_NUM, 0);
    }


}
