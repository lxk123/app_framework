package com.shiqupad.order.presenter;

import android.support.annotation.NonNull;


import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_general_ui.presenter.BasePresenter;
import cn.com.dreamtouch.magicbox_http_client.network.model.LoginResponse;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import com.shiqupad.order.listener.LoginListener;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */

public class LoginPresenter extends BasePresenter {
    private LoginListener listener;
    private UserRepository userRepository;

    public LoginPresenter(@NonNull UserRepository userRepository, @NonNull LoginListener listener) {
        super();//完成基类的初始化工作
        this.listener = listener;
        this.userRepository=userRepository;
    }

    //登录
    public void login(String username,String password) {
       /* if (listener != null)
            listener.showLoadingProgress();*/
        rx.Observable<LoginResponse> observable = userRepository.login(username, password);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<LoginResponse>() {
                    @Override
                    public void onCompleted() {
                        if (listener != null)
                            listener.hideLoadingProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        MagicBoxResponseException exception = MagicBoxResponseException.convertApiThrowable(e);
                        if (listener != null) {
                            listener.basicFailure(exception.getResultMessage().prompt);
                            listener.hideLoadingProgress();
                        }
                    }

                    @Override
                    public void onNext(LoginResponse response) {
                        if (response.getCode() == 1 ) {
                            listener.loginSuccess(response);
                         }else if(response.getCode() == 503){
                            listener.basicFailure(response.getMessage());
                        } else {
                            listener.basicFailure(response.getMessage());
                        }
                    }
                });
        mSubscriptions.add(subscription);
    }

}
