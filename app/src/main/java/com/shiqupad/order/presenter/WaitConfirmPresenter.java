package com.shiqupad.order.presenter;

import android.support.annotation.NonNull;

import com.shiqupad.order.listener.ChangePasswordListener;
import com.shiqupad.order.listener.WaitConfirmListener;

import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_general_ui.presenter.BasePresenter;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangePasswordResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.WaitConfirmResponse;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */
public class WaitConfirmPresenter extends BasePresenter {
    private WaitConfirmListener listener;
    private UserRepository userRepository;

    public WaitConfirmPresenter(@NonNull UserRepository userRepository,
                                @NonNull WaitConfirmListener listener) {
        super();//完成基类的初始化工作
        this.listener = listener;
        this.userRepository=userRepository;
    }


    public void waiterConfirmOrder(String shopId, String orderID,String confirm,
                                   String remark) {
        if (listener != null)
            listener.showLoadingProgress();
        rx.Observable<WaitConfirmResponse> observable = userRepository.waiterConfirmOrder(shopId,
                orderID,confirm,remark);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<WaitConfirmResponse>() {
                    @Override
                    public void onCompleted() {
                        if (listener != null)
                            listener.hideLoadingProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        MagicBoxResponseException exception = MagicBoxResponseException.convertApiThrowable(e);
                        if (listener != null) {
                            listener.basicFailure(exception.getResultMessage().prompt);
                            listener.hideLoadingProgress();
                        }
                    }

                    @Override
                    public void onNext(WaitConfirmResponse response) {
                        if (response.getCode() == 1) {
                            listener.waitConfirmSuccess(response);
                        } else {
                            listener.basicFailure(response.getMessage());
                        }
                    }
                });
        mSubscriptions.add(subscription);
    }

}
