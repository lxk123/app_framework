package com.shiqupad.order.presenter;

import android.support.annotation.NonNull;

import com.shiqupad.order.listener.CancelOrderListener;
import com.shiqupad.order.listener.PrintOrderListener;

import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_general_ui.presenter.BasePresenter;
import cn.com.dreamtouch.magicbox_http_client.network.model.CancelOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PrintOrderResponse;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */
public class PrintOrderPresenter extends BasePresenter {
    private PrintOrderListener listener;
    private UserRepository userRepository;

    public PrintOrderPresenter(@NonNull UserRepository userRepository,
                               @NonNull PrintOrderListener listener) {
        super();//完成基类的初始化工作
        this.listener = listener;
        this.userRepository=userRepository;
    }


    public void reprintOrder(String orderID, String shopID) {
        if (listener != null)
            listener.showLoadingProgress();
        rx.Observable<PrintOrderResponse> observable = userRepository.reprintOrder(orderID, shopID);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PrintOrderResponse>() {
                    @Override
                    public void onCompleted() {
                        if (listener != null)
                            listener.hideLoadingProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        MagicBoxResponseException exception = MagicBoxResponseException.convertApiThrowable(e);
                        if (listener != null) {
                            listener.basicFailure(exception.getResultMessage().prompt);
                            listener.hideLoadingProgress();
                        }
                    }

                    @Override
                    public void onNext(PrintOrderResponse response) {
                        if (response.getCode() == 1) {
                            listener.printOrderSuccess(response);
                        } else {
                            listener.basicFailure(response.getMessage());
                        }
                    }
                });
        mSubscriptions.add(subscription);
    }

}
