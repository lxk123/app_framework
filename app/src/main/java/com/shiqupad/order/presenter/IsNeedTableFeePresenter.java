package com.shiqupad.order.presenter;

import android.support.annotation.NonNull;

import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_general_ui.presenter.BasePresenter;
import cn.com.dreamtouch.magicbox_http_client.network.model.IsNeedTableFeeResponse;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;

import com.shiqupad.order.listener.IsNeedTableFeeListener;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */
public class IsNeedTableFeePresenter extends BasePresenter {
    private IsNeedTableFeeListener listener;
    private UserRepository userRepository;

    public IsNeedTableFeePresenter(@NonNull UserRepository userRepository,
                                   @NonNull IsNeedTableFeeListener listener) {
        super();//完成基类的初始化工作
        this.listener = listener;
        this.userRepository=userRepository;
    }


    public void isNeedTableFee(String tableID, String shopID) {
        if (listener != null)
            listener.showLoadingProgress();
        rx.Observable<IsNeedTableFeeResponse> observable = userRepository.isNeedTableFee(tableID,
                shopID);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<IsNeedTableFeeResponse>() {
                    @Override
                    public void onCompleted() {
                        if (listener != null)
                            listener.hideLoadingProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        MagicBoxResponseException exception = MagicBoxResponseException.convertApiThrowable(e);
                        if (listener != null) {
                            listener.basicFailure(exception.getResultMessage().prompt);
                            listener.hideLoadingProgress();
                        }
                    }

                    @Override
                    public void onNext(IsNeedTableFeeResponse response) {
                        if (response.getCode() == 1) {
                            listener.isNeedTableFeeSuccess(response);
                        } else {
                            listener.basicFailure(response.getMessage());
                        }
                    }
                });
        mSubscriptions.add(subscription);
    }

}
