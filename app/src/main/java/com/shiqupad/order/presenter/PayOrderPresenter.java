package com.shiqupad.order.presenter;

import android.support.annotation.NonNull;

import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_general_ui.presenter.BasePresenter;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayOrderStrBody;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayParam;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import com.shiqupad.order.listener.PayOrderListener;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */

public class PayOrderPresenter extends BasePresenter {
    private PayOrderListener listener;
    private UserRepository userRepository;

    public PayOrderPresenter(@NonNull UserRepository userRepository,
                             @NonNull PayOrderListener listener) {
        super();//完成基类的初始化工作
        this.listener = listener;
        this.userRepository=userRepository;
    }


    public void payOrder( PayParam payOrderStrBody) {
        if (listener != null)
            listener.showLoadingProgress();
        rx.Observable<PayOrderResponse> observable = userRepository.payOrder(
               payOrderStrBody);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<PayOrderResponse>() {
                    @Override
                    public void onCompleted() {
                        if (listener != null)
                            listener.hideLoadingProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        MagicBoxResponseException exception = MagicBoxResponseException.convertApiThrowable(e);
                        if (listener != null) {
                            listener.basicFailure(exception.getResultMessage().prompt);
                            listener.hideLoadingProgress();
                        }
                    }

                    @Override
                    public void onNext(PayOrderResponse response) {
                        PayOrderResponse.DataBean data = response.getData();
                        if (response.getCode() == 1 && data.getResultCode().equals("1") ) {
                            listener.payOrderSuccess(response);
                        } else {
                            listener.basicFailure(data.getResultMessage());
                        }
                    }
                });
        mSubscriptions.add(subscription);
    }

}
