package com.shiqupad.order.presenter;

import android.support.annotation.NonNull;

import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_general_ui.presenter.BasePresenter;
import cn.com.dreamtouch.magicbox_http_client.network.model.OrderStrBody;
import cn.com.dreamtouch.magicbox_http_client.network.model.RefundDishes;
import cn.com.dreamtouch.magicbox_http_client.network.model.RetreatOrderResponse;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;

import com.shiqupad.order.listener.RetreatOrderListener;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */

public class RetreatOrderPresenter extends BasePresenter {
    private RetreatOrderListener listener;
    private UserRepository userRepository;

    public RetreatOrderPresenter(@NonNull UserRepository userRepository,
                                 @NonNull RetreatOrderListener listener) {
        super();//完成基类的初始化工作
        this.listener = listener;
        this.userRepository=userRepository;
    }


    public void retreatOrder( RefundDishes orderStrBody) {
        if (listener != null)
            listener.showLoadingProgress();
        rx.Observable<RetreatOrderResponse> observable = userRepository.retreatOrder(
               orderStrBody);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<RetreatOrderResponse>() {
                    @Override
                    public void onCompleted() {
                        if (listener != null)
                            listener.hideLoadingProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        MagicBoxResponseException exception = MagicBoxResponseException.convertApiThrowable(e);
                        if (listener != null) {
                            listener.basicFailure(exception.getResultMessage().prompt);
                            listener.hideLoadingProgress();
                        }
                    }

                    @Override
                    public void onNext(RetreatOrderResponse response) {
                        if (response.getCode() == 1) {
                            listener.retreatOrderSuccess(response);
                        } else {
                            listener.basicFailure(response.getMessage());
                        }
                    }
                });
        mSubscriptions.add(subscription);
    }

}
