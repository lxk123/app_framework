package com.shiqupad.order.presenter;

import android.support.annotation.NonNull;

import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_general_ui.presenter.BasePresenter;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetMessageResponse;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import com.shiqupad.order.listener.GetMessageListener;

import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */
public class GetMessagePresenter extends BasePresenter {
    private GetMessageListener listener;
    private UserRepository userRepository;

    public GetMessagePresenter(@NonNull UserRepository userRepository,
                               @NonNull GetMessageListener listener) {
        super();//完成基类的初始化工作
        this.listener = listener;
        this.userRepository=userRepository;
    }


    public void getMessage( String shopID,String page) {

        rx.Observable<GetMessageResponse> observable = userRepository.getMessage(shopID,
                page);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GetMessageResponse>() {
                    @Override
                    public void onCompleted() {
                        if (listener != null)
                            listener.hideLoadingProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        MagicBoxResponseException exception = MagicBoxResponseException.convertApiThrowable(e);
                        if (listener != null) {
                            listener.basicFailure(exception.getResultMessage().prompt);
                            listener.hideLoadingProgress();
                        }
                    }

                    @Override
                    public void onNext(GetMessageResponse response) {
                        if (response.getCode() == 1) {
                            listener.getMessageSuccess(response);
                        } else {
                            listener.basicFailure(response.getMessage());
                        }
                    }
                });
        mSubscriptions.add(subscription);
    }

}
