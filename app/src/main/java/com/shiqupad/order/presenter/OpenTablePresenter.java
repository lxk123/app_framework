package com.shiqupad.order.presenter;

import android.support.annotation.NonNull;

import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_general_ui.presenter.BasePresenter;
import cn.com.dreamtouch.magicbox_http_client.network.model.OpenTableResponse;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;

import com.shiqupad.order.listener.OpenTableListener;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */

public class OpenTablePresenter extends BasePresenter {
    private OpenTableListener listener;
    private UserRepository userRepository;

    public OpenTablePresenter(@NonNull UserRepository userRepository,
                              @NonNull OpenTableListener listener) {
        super();//完成基类的初始化工作
        this.listener = listener;
        this.userRepository=userRepository;
    }


    public void openTable(String tableID , String shopID,
                            String peopleNumber, String remark) {
        if (listener != null)
            listener.showLoadingProgress();
        rx.Observable<OpenTableResponse> observable = userRepository.openTable(tableID, shopID,peopleNumber,remark);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<OpenTableResponse>() {
                    @Override
                    public void onCompleted() {
                        if (listener != null)
                            listener.hideLoadingProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        MagicBoxResponseException exception = MagicBoxResponseException.convertApiThrowable(e);
                        if (listener != null) {
                            listener.basicFailure(exception.getResultMessage().prompt);
                            listener.hideLoadingProgress();
                        }
                    }

                    @Override
                    public void onNext(OpenTableResponse response) {
                        if (response.getCode() == 1) {
                            listener.openTableSuccess(response);
                        } else {
                            listener.basicFailure(response.getMessage());
                        }
                    }
                });
        mSubscriptions.add(subscription);
    }

}
