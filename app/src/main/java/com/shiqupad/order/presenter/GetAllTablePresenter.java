package com.shiqupad.order.presenter;

import android.support.annotation.NonNull;

import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_general_ui.presenter.BasePresenter;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import com.shiqupad.order.listener.GetAllTableListener;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */

public class GetAllTablePresenter extends BasePresenter {
    private GetAllTableListener listener;
    private UserRepository userRepository;

    public GetAllTablePresenter(@NonNull UserRepository userRepository,
                                @NonNull GetAllTableListener listener) {
        super();//完成基类的初始化工作
        this.listener = listener;
        this.userRepository=userRepository;
    }


    public void getAllTable(String sid,String shopID,String updateFlag) {
        rx.Observable<GetAllTableResponse> observable = userRepository.getAllTable(sid, shopID,updateFlag);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<GetAllTableResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        MagicBoxResponseException exception = MagicBoxResponseException.convertApiThrowable(e);
                        if (listener != null) {
                            listener.basicFailure(exception.getResultMessage().prompt);
                            listener.hideLoadingProgress();
                        }
                    }

                    @Override
                    public void onNext(GetAllTableResponse response) {
                        if (response.getCode() == 1) {
                            listener.getAllTableSuccess(response);
                        } else {
                            listener.basicFailure(response.getMessage());
                        }
                    }
                });
        mSubscriptions.add(subscription);
    }

}
