package com.shiqupad.order.presenter;

import android.support.annotation.NonNull;

import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_general_ui.presenter.BasePresenter;
import cn.com.dreamtouch.magicbox_http_client.network.model.UpdateDinnerPeopleResponse;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;

import com.shiqupad.order.listener.UpdateDinnerPeopleListener;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */

public class UpdateDinnerPeoplePresenter extends BasePresenter {
    private UpdateDinnerPeopleListener listener;
    private UserRepository userRepository;

    public UpdateDinnerPeoplePresenter(@NonNull UserRepository userRepository,
                                       @NonNull UpdateDinnerPeopleListener listener) {
        super();//完成基类的初始化工作
        this.listener = listener;
        this.userRepository=userRepository;
    }


    public void updateDinnerPeople(String shopID, String tableID,
                            String orderID, String peopleNum) {
        if (listener != null)
            listener.showLoadingProgress();
        rx.Observable<UpdateDinnerPeopleResponse> observable = userRepository.updateDinnerPeople(shopID,
                tableID,orderID,peopleNum);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<UpdateDinnerPeopleResponse>() {
                    @Override
                    public void onCompleted() {
                        if (listener != null)
                            listener.hideLoadingProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        MagicBoxResponseException exception = MagicBoxResponseException.convertApiThrowable(e);
                        if (listener != null) {
                            listener.basicFailure(exception.getResultMessage().prompt);
                            listener.hideLoadingProgress();
                        }
                    }

                    @Override
                    public void onNext(UpdateDinnerPeopleResponse response) {
                        if (response.getCode() == 1) {
                            listener.updateDinnerPeopleSuccess(response);
                        } else {
                            listener.basicFailure(response.getMessage());
                        }
                    }
                });
        mSubscriptions.add(subscription);
    }

}
