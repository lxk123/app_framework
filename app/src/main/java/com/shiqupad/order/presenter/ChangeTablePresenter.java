package com.shiqupad.order.presenter;

import android.support.annotation.NonNull;

import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_general_ui.presenter.BasePresenter;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangeTableResponse;
import cn.com.dreamtouch.magicbox_repository.repository.UserRepository;
import com.shiqupad.order.listener.ChangeTableListener;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */
public class ChangeTablePresenter extends BasePresenter {
    private ChangeTableListener listener;
    private UserRepository userRepository;

    public ChangeTablePresenter(@NonNull UserRepository userRepository,
                                @NonNull ChangeTableListener listener) {
        super();//完成基类的初始化工作
        this.listener = listener;
        this.userRepository=userRepository;
    }


    public void changeTable( String oldTableID,
                            String tableID, String shopID, String orderID) {
        if (listener != null)
            listener.showLoadingProgress();
        rx.Observable<ChangeTableResponse> observable = userRepository.changeTable(
                oldTableID,tableID,shopID,orderID);
        Subscription subscription = observable.observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<ChangeTableResponse>() {
                    @Override
                    public void onCompleted() {
                        if (listener != null)
                            listener.hideLoadingProgress();
                    }

                    @Override
                    public void onError(Throwable e) {
                        MagicBoxResponseException exception = MagicBoxResponseException.convertApiThrowable(e);
                        if (listener != null) {
                            listener.basicFailure(exception.getResultMessage().prompt);
                            listener.hideLoadingProgress();
                        }
                    }

                    @Override
                    public void onNext(ChangeTableResponse response) {
                        if (response.getCode() == 1) {
                            listener.changeTableSuccess(response);
                        } else {
                            listener.basicFailure(response.getMessage());
                        }
                    }
                });
        mSubscriptions.add(subscription);
    }

}
