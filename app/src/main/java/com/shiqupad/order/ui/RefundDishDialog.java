package com.shiqupad.order.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.shiqupad.order.R;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_http_client.network.model.DetailListBean;

/**
 * Created by hu on 2016/8/15.
 */
public class RefundDishDialog extends Dialog implements
        View.OnClickListener {

    public interface IRefundDishDialogListener {
        void onBtnConfirmClick(DetailListBean detailListBean,String num, String cause);
    }

    /**
     * set Listener
     */
    public void setIRefundDishDialogListener(IRefundDishDialogListener listener) {
        this.mListener = listener;
    }

    private Context ctx;
    private TextView mTextDishName;
    private TextView mTextRefundNum;
    private EditText mEdtNum;
    private String[] dataSet;
    private TagFlowLayout flowLayout;
    private TagAdapter<String> tAdapter;
    private IRefundDishDialogListener mListener;

    private DetailListBean dish;

    public RefundDishDialog(Context context, DetailListBean dish) {
        super(context, R.style.myDialog);
        this.dish = dish;
        this.ctx = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_refund);
        initView();
    }


    private void initView() {
        findViewById(R.id.dialog_refund_btn_cancel).setOnClickListener(this);
        findViewById(R.id.dialog_refund_btn_confirm).setOnClickListener(this);

        mTextDishName = (TextView) findViewById(R.id.dialog_refund_text_dishName);
        mTextRefundNum = (TextView) findViewById(R.id.dialog_refund_text_refundNum);
        mEdtNum = (EditText) findViewById(R.id.dialog_refund_edt_num);
        flowLayout = (TagFlowLayout) findViewById(R.id.flow_case);

        mTextDishName.setText(dish.getDishName());
        mTextRefundNum.setText(String.format("可退数量: %d", dish.getDishNumber()));
        mEdtNum.setText(String.valueOf(dish.getDishNumber()));
        mEdtNum.setHint(String.format("可退数量: %d", dish.getDishNumber()));

        dataSet = ctx.getResources().getStringArray(R.array.refund_causes);

        tAdapter = new TagAdapter<String>(dataSet) {
            @Override
            public View getView(FlowLayout parent, int position, String s) {
                TextView textView = (TextView) LayoutInflater.from(ctx).inflate(R.layout.item_dish_spec_tag, flowLayout, false);
                textView.setText(s);
                return textView;
            }
        };
        flowLayout.setAdapter(tAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_refund_btn_cancel:
                dismiss();
                break;
            case R.id.dialog_refund_btn_confirm:
                handleBtnConfirmClick();
                break;
        }
    }

    private void handleBtnConfirmClick() {
        if (TextUtils.isEmpty(mEdtNum.getText().toString()) || Integer.valueOf(mEdtNum.getText().toString()) == 0) {
            DTLog.showMessageShort(ctx,"请填写退菜个数");
            return;
        } else {
            if (Integer.valueOf(mEdtNum.getText().toString()) > dish.getDishNumber()) {
                DTLog.showMessageShort(ctx,"退菜个数有误");
                return;
            }
            mListener.onBtnConfirmClick(dish,mEdtNum.getText().toString(),
                    dataSet[flowLayout.getSelectedList()
                            .toArray(new Integer[flowLayout.getSelectedList().size()])[0]]);
            ;
            dismiss();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        tAdapter.setSelectedList(0);
        tAdapter.notifyDataChanged();
    }
}
