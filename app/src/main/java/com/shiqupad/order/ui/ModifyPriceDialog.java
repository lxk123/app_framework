package com.shiqupad.order.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.shiqupad.order.R;
import com.shiqupad.order.adapter.SelectCouponsAdapter;

import java.lang.reflect.Method;
import java.util.List;
import java.util.Objects;

import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_general_ui.ui.NumberKeyboardView;
import cn.com.dreamtouch.magicbox_general_ui.ui.XNumberKeyboardView;
import cn.com.dreamtouch.magicbox_general_ui.util.MyLogger;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoResponse;


/**
 * Created by LuoXueKun on 2016/4/15.
 */
public class ModifyPriceDialog extends Dialog implements View.OnClickListener,
        XNumberKeyboardView.IOnKeyboardListener, NumberKeyboardView.OnNumberClickListener {
    private InputMethodManager mInputMethodManager; //定义一个输入法管理对象,用于管理关闭软键盘
    private Context mContext;
    private EditText  edtNewNum;
    private List<GetCustomOrderInfoResponse.DataBean.VoucherVOListBean> mVoucherVOListBeans;
    private OnModifyFinalPriceListener mListener;

    private  double mTotalPrice;
    @Override
    public void onInsertKeyEvent(String text) {

        edtNewNum.append(text);
        String newNumStr = edtNewNum.getText().toString();

    }

    @Override
    public void onDeleteKeyEvent() {
        int start = edtNewNum.length() - 1;
        if (start >= 0) {
            edtNewNum.getText().delete(start, start + 1);
        }
    }

    @Override
    public void onNumberReturn(String number) {
        edtNewNum.append(number);
        String newNumStr = edtNewNum.getText().toString();
    }

    @Override
    public void onNumberDelete() {
        int start = edtNewNum.length() - 1;
        if (start >= 0) {
            edtNewNum.getText().delete(start, start + 1);
        }
    }


    public interface OnModifyFinalPriceListener {
        void onModifyFinalPrice(String newNum);
    }

    public void setOnModifyFinalPrice(OnModifyFinalPriceListener onModifyFinalPriceListener) {
        mListener = onModifyFinalPriceListener;
    }

    public ModifyPriceDialog(Context context,
                             List<GetCustomOrderInfoResponse.DataBean.VoucherVOListBean> voucherVOListBeans
     , double totalPrice) {
        super(context, R.style.myDialog);
        this.mContext = context;
        this.mVoucherVOListBeans = voucherVOListBeans;
        mTotalPrice = totalPrice;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.modify_price_dialog);

        initView();
    }

    private void initView() {
        findViewById(R.id.dialog_tv_cancel).setOnClickListener(this);
        findViewById(R.id.dialog_tv_sure).setOnClickListener(this);
        findViewById(R.id.dialog_iv_close).setOnClickListener(this);
        findViewById(R.id.dialog_keyboard_kbv).setOnClickListener(this);

        mInputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);

        edtNewNum = findViewById(R.id.modify_dialog_price_et);
        NumberKeyboardView xNumberKeyboardView = findViewById(R.id.dialog_keyboard_kbv);
        xNumberKeyboardView.setOnNumberClickListener(this);

        disableShowSoftInput();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_tv_sure:

                handleBtnConfirmClick();
                break;
            case R.id.dialog_tv_cancel:

                //mInputMethodManager.hideSoftInputFromWindow( Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                dismiss();
                break;
            case R.id.dialog_keyboard_kbv:

                break;
            case R.id.dialog_iv_close:
                if(mInputMethodManager!=null){
                  //  mInputMethodManager.hideSoftInputFromWindow( Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                }
                dismiss();
                break;
        }
    }

    private void handleBtnConfirmClick() {
        String newNum = edtNewNum.getText().toString();
        if(TextUtils.isEmpty(newNum) || newNum.startsWith(".")){
            DTLog.showMessage(mContext,"请输入金额");
            return;
        }
        int count = 0;
        char[] chars = newNum.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            if(chars[i] == '.'){
                count++;
            }
        }
        MyLogger.kLog().e(count);
        if(count >= 2){
            DTLog.showMessage(mContext,"输入格式不合法");
            return;
        }
        if(mListener != null){
            mListener.onModifyFinalPrice(newNum);
        }
    }

    @Override
    public void show() {
        super.show();

        if (!TextUtils.isEmpty(edtNewNum.getText().toString())) {
            edtNewNum.setText("");
        }
    }

    private void disableShowSoftInput() {
        try {
            Class<EditText> cls = EditText.class;
            Method setShowSoftInputOnFocus;
            setShowSoftInputOnFocus = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
            setShowSoftInputOnFocus.setAccessible(true);
            setShowSoftInputOnFocus.invoke(edtNewNum, false);
        } catch (Exception e) {
            edtNewNum.setInputType(InputType.TYPE_NULL);
            e.printStackTrace();
        }
    }
}
