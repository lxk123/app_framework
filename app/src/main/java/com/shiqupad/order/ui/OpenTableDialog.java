package com.shiqupad.order.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.lang.reflect.Method;

import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_general_ui.ui.XNumberKeyboardView;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import com.shiqupad.order.R;


/**
 * Created by LuoXueKun on 2016/4/15.
 */
public class OpenTableDialog extends Dialog implements View.OnClickListener,
        XNumberKeyboardView.IOnKeyboardListener {
    private InputMethodManager mInputMethodManager; //定义一个输入法管理对象,用于管理关闭软键盘
    private Context mContext;
    private EditText edtPeopleNum, edtRemark;
    private ImageView ivClose;
    private XNumberKeyboardView xNumberKeyboardView;
    private GetAllTableResponse.Data.ListBean mTableBean;
    private OnStartTableDialogListener mListener;
    private  int mTableId;
    @Override
    public void onInsertKeyEvent(String text) {
        edtPeopleNum.append(text);
        String newNumStr = edtPeopleNum.getText().toString();
       /* if(Integer.valueOf(newNumStr) > mTableBean.getPropleNum()){
            DTLog.showMessage(mContext,"超过桌台容纳人数");
            edtPeopleNum.setText("");
        }*/
    }

    @Override
    public void onDeleteKeyEvent() {
        int start = edtPeopleNum.length() - 1;
        if (start >= 0) {
            edtPeopleNum.getText().delete(start, start + 1);
        }
    }


    public interface OnStartTableDialogListener {
        void onBtnConfirmClick(int peopleNum, String remark, boolean isOrder, String tableID);
    }

    public void setOnStartTableDialogListener(OnStartTableDialogListener listener) {
        mListener = listener;
    }

    public OpenTableDialog(Context context, GetAllTableResponse.Data.ListBean tableListBean, int tablePos) {
        super(context, R.style.myDialog);
        this.mContext = context;
        this.mTableBean = tableListBean;

    }
    public OpenTableDialog(Context context,int tableId){
         super(context,R.style.myDialog);
         mContext = context;
         mTableId = tableId;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_start_table);

        initView();
    }

    private void initView() {
        findViewById(R.id.dialog_tv_cancel).setOnClickListener(this);
        findViewById(R.id.dialog_tv_sure).setOnClickListener(this);
        findViewById(R.id.dialog_iv_close).setOnClickListener(this);
        findViewById(R.id.dialog_keyboard_kbv).setOnClickListener(this);

        mInputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);

        edtPeopleNum = findViewById(R.id.dialog_table_people_et);
        edtRemark = findViewById(R.id.dialog_table_note_et);

        xNumberKeyboardView = findViewById(R.id.dialog_keyboard_kbv);
        xNumberKeyboardView.setIOnKeyboardListener(this);

        disableShowSoftInput();
        edtPeopleNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                edtPeopleNum.setSelection(s.length());
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_tv_sure:

                handleBtnConfirmClick();
                break;
            case R.id.dialog_tv_cancel:
                mInputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                dismiss();
                break;
            case R.id.dialog_keyboard_kbv:

                break;
            case R.id.dialog_iv_close:
                mInputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                dismiss();
                break;
        }
    }

    private void handleBtnConfirmClick() {

        CheckBox checkBox = (CheckBox) findViewById(R.id.cb_order);

        String peopleNum = edtPeopleNum.getText().toString();
        String remark = edtRemark.getText().toString();

        if (TextUtils.isEmpty(peopleNum)) {
            Toast.makeText(mContext, "请填写就餐人数", Toast.LENGTH_SHORT).show();
            return;
        } else if (Integer.valueOf(peopleNum) < 0) {
            Toast.makeText(mContext, "就餐人数不能为0", Toast.LENGTH_SHORT).show();
            return;
        } else {
            mListener.onBtnConfirmClick(Integer.valueOf(peopleNum), remark,
                    checkBox.isChecked(), mTableBean.getTableID() + "");
            dismiss();
        }
    }

    @Override
    public void show() {
        super.show();
        if (!TextUtils.isEmpty(edtPeopleNum.getText().toString())) {
            edtPeopleNum.setText("");
        }
        if (!TextUtils.isEmpty(edtRemark.getText().toString())) {
            edtRemark.setText("");
        }
    }

    private void disableShowSoftInput() {
        try {
            Class<EditText> cls = EditText.class;
            Method setShowSoftInputOnFocus;
            setShowSoftInputOnFocus = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
            setShowSoftInputOnFocus.setAccessible(true);
            setShowSoftInputOnFocus.invoke(edtPeopleNum, false);
        } catch (Exception e) {
            edtPeopleNum.setInputType(InputType.TYPE_NULL);
            e.printStackTrace();
        }
    }

}
