package com.shiqupad.order.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Objects;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.shiqupad.order.R;

/**
 * Created by Administrator on 2016/3/25.
 */
public class InputMessageDialog extends Dialog implements
        View.OnClickListener {


    @Bind(R.id.input_dialog_title_tv)
    TextView mTitleTv;
    @Bind(R.id.input_dialog_cont_et)
    EditText mContEt;
    @Bind(R.id.input_dialog_iv)
    ImageView mIvClose;
    @Bind(R.id.input_cont_ll)
    RelativeLayout mContLl;
    @Bind(R.id.dialog_tv_cancel)
    TextView mCancelTv;
    @Bind(R.id.dialog_tv_sure)
    TextView mSureTv;
    private Context context;
    private String title;
    private String hint;
    private IInputMessageDialogCallback callback;

    public interface IInputMessageDialogCallback {
        void getInputMessage(String message);
    }

    public InputMessageDialog(Context context, String title, String hint, IInputMessageDialogCallback callback) {
        super(context, R.style.myDialog);
        this.context = context;
        this.title = title;
        this.hint = hint;
        this.callback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_input_layout);
        ButterKnife.bind(this);
        init();
    }

    private void init() {

        findViewById(R.id.dialog_tv_cancel).setOnClickListener(this);
        findViewById(R.id.dialog_tv_sure).setOnClickListener(this);
        findViewById(R.id.input_dialog_iv).setOnClickListener(this);

        mTitleTv.setText(title);
        mContEt.setHint(hint);


        Window dialogWindow = getWindow();
        WindowManager.LayoutParams lp = Objects.requireNonNull(dialogWindow).getAttributes();
        DisplayMetrics d = context.getResources().getDisplayMetrics(); // 获取屏幕宽、高用
        lp.width = (int) (d.widthPixels * 0.75); // 高度设置为屏幕的0.6
        dialogWindow.setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_tv_sure:
                if (TextUtils.isEmpty(mContEt.getText().toString()))
                    return;

                callback.getInputMessage(mContEt.getText().toString());
                break;
            case R.id.input_dialog_iv:
                mContEt.setText("");
                break;
        }
        dismiss();
    }

}
