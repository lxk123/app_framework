package com.shiqupad.order.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import java.lang.reflect.Method;
import java.util.Objects;

import cn.com.dreamtouch.common.DTLog;
import cn.com.dreamtouch.magicbox_general_ui.ui.XNumberKeyboardView;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import com.shiqupad.order.R;


/**
 * Created by LuoXueKun on 2016/4/15.
 */
public class ChangePeopleNumTableDialog extends Dialog implements View.OnClickListener,
        XNumberKeyboardView.IOnKeyboardListener {
    private InputMethodManager mInputMethodManager; //定义一个输入法管理对象,用于管理关闭软键盘
    private Context mContext;
    private EditText edtPeopleNum, edtNewNum;
    private GetAllTableResponse.Data.ListBean mTableBean;
    private OnChangePeopleNumTableDialogListener mListener;


    @Override
    public void onInsertKeyEvent(String text) {

        edtNewNum.append(text);
        String newNumStr = edtNewNum.getText().toString();
       /* if(Integer.valueOf(newNumStr) > mTableBean.getPropleNum()){
            DTLog.showMessage(mContext,"超过桌台容纳人数");
            edtNewNum.setText("");
        }*/
    }

    @Override
    public void onDeleteKeyEvent() {
        int start = edtNewNum.length() - 1;
        if (start >= 0) {
            edtNewNum.getText().delete(start, start + 1);
        }
    }


    public interface OnChangePeopleNumTableDialogListener {
        void onChangePeopleNum(String newNum);
    }

    public void setOnChangePeopleNumTableDialogListener(OnChangePeopleNumTableDialogListener onChangePeopleNumTableDialogListener) {
        mListener = onChangePeopleNumTableDialogListener;
    }

    public ChangePeopleNumTableDialog(Context context, GetAllTableResponse.Data.ListBean tableListBean) {
        super(context, R.style.myDialog);
        this.mContext = context;
        this.mTableBean = tableListBean;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_modify_people_num_table);

        initView();
    }

    private void initView() {
        findViewById(R.id.dialog_tv_cancel).setOnClickListener(this);
        findViewById(R.id.dialog_tv_sure).setOnClickListener(this);
        findViewById(R.id.dialog_iv_close).setOnClickListener(this);
        findViewById(R.id.dialog_keyboard_kbv).setOnClickListener(this);

        mInputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);

        edtPeopleNum = findViewById(R.id.dialog_table_people_et);
        edtNewNum = findViewById(R.id.dialog_table_note_et);
        XNumberKeyboardView xNumberKeyboardView = findViewById(R.id.dialog_keyboard_kbv);
        xNumberKeyboardView.setIOnKeyboardListener(this);

        disableShowSoftInput();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_tv_sure:

                handleBtnConfirmClick();
                break;
            case R.id.dialog_tv_cancel:

                mInputMethodManager.hideSoftInputFromWindow( Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                dismiss();
                break;
            case R.id.dialog_keyboard_kbv:

                break;
            case R.id.dialog_iv_close:
                mInputMethodManager.hideSoftInputFromWindow( Objects.requireNonNull(getCurrentFocus()).getWindowToken(), 0);
                dismiss();
                break;
        }
    }

    private void handleBtnConfirmClick() {
        String newNum = edtNewNum.getText().toString();
        if (!TextUtils.isEmpty(newNum) &&Integer.valueOf(newNum) < 0) {
            DTLog.showMessage(mContext, "就餐人数不能为0");
        } else {
            if(mListener != null){
                mListener.onChangePeopleNum(newNum);
                dismiss();
            }

        }
    }

    @Override
    public void show() {
        super.show();
        if (mTableBean != null) {
            edtPeopleNum.setText(String.valueOf(mTableBean.getDinningPeople()));
        }
        if (!TextUtils.isEmpty(edtNewNum.getText().toString())) {
            edtNewNum.setText("");
        }
    }

    private void disableShowSoftInput() {
        try {
            Class<EditText> cls = EditText.class;
            Method setShowSoftInputOnFocus;
            setShowSoftInputOnFocus = cls.getMethod("setShowSoftInputOnFocus", boolean.class);
            setShowSoftInputOnFocus.setAccessible(true);
            setShowSoftInputOnFocus.invoke(edtNewNum, false);
        } catch (Exception e) {
            edtNewNum.setInputType(InputType.TYPE_NULL);
            e.printStackTrace();
        }
    }
}
