package com.shiqupad.order.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;

import com.flyco.tablayout.CommonTabLayout;
import com.flyco.tablayout.listener.CustomTabEntity;
import com.flyco.tablayout.listener.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

import cn.com.dreamtouch.magicbox_common.util.CommonConstant;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import com.shiqupad.order.R;
import com.shiqupad.order.bean.TabEntity;
import com.shiqupad.order.adapter.GetNewSwitchTableAdapter;


/**
 * Created by LuoXueKun on 2016/4/15.
 */
public class SwitchTableDialog extends Dialog implements View.OnClickListener,
        OnTabSelectListener, AdapterView.OnItemClickListener {
    private CommonTabLayout mCommonTabLayout;
    private RecyclerView mRv;
    private ArrayList<CustomTabEntity> mTabEntities = new ArrayList<>();
    private Context mContext;
    private OnSwitchTableDialogListener mListener;
    private List<String> mTitleList;
    private List<GetAllTableResponse.Data> mAllTableDataList;
    private List<GetAllTableResponse.Data.ListBean> mSwitchTableList;
    // private GetSwitchTableAdapter mGetSwitchTableAdapter;
    private GetNewSwitchTableAdapter mGetNewSwitchTableAdapter;

    private int mNewTablePos;


    @Override
    public void onTabSelect(int position) {
        mSwitchTableList.clear();
        if (position == 0) {
            for (int i = 0; i < mAllTableDataList.size(); i++) {
                List<GetAllTableResponse.Data.ListBean> list = mAllTableDataList.get(i).getList();
                for (int j = 0; j < list.size(); j++) {
                    if (list.get(j).getStatus() == CommonConstant.TableStatus.FREE) {
                        mSwitchTableList.add(list.get(j));
                    }
                }
            }
        } else {
            for (int i = 0; i < mAllTableDataList.size(); i++) {
                if (i == position) {
                    List<GetAllTableResponse.Data.ListBean> list = mAllTableDataList.get(i - 1).getList();
                    for (int j = 0; j < list.size(); j++) {
                        if (list.get(j).getStatus() == CommonConstant.TableStatus.FREE) {
                            mSwitchTableList.add(list.get(j));
                        }
                    }
                }
            }
        }
        //mGetSwitchTableAdapter.refresh(mSwitchTableList);
        mGetNewSwitchTableAdapter.replaceAll(mSwitchTableList);
    }

    @Override
    public void onTabReselect(int position) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }


    public interface OnSwitchTableDialogListener {
        void onSwitchTable(String tableID);
    }

    public void setOnSwitchTableDialogListener(OnSwitchTableDialogListener listener) {
        mListener = listener;
    }

    public SwitchTableDialog(Context context, List<GetAllTableResponse.Data> dataList) {
        super(context, R.style.myDialog);
        this.mContext = context;
        this.mAllTableDataList = new ArrayList<>();
        this.mAllTableDataList.addAll(dataList);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_switch_table);

        initView();
    }

    private void initView() {

        findViewById(R.id.dialog_tv_cancel).setOnClickListener(this);
        findViewById(R.id.dialog_tv_sure).setOnClickListener(this);
        findViewById(R.id.dialog_iv_close).setOnClickListener(this);

        initRecyclerViewAndAdapter();
        initTabAndView();

    }

    private void initRecyclerViewAndAdapter() {
        mSwitchTableList = new ArrayList<>();
        mCommonTabLayout = findViewById(R.id.dialog_switch_ctl);
        mRv = findViewById(R.id.dialog_switch_table_rv);
        mCommonTabLayout.setOnTabSelectListener(this);
        for (int i = 0; i < mAllTableDataList.size(); i++) {
            List<GetAllTableResponse.Data.ListBean> list = mAllTableDataList.get(i).getList();
            for (int j = 0; j < list.size(); j++) {
                if (list.get(j).getStatus() == CommonConstant.TableStatus.FREE) {
                    mSwitchTableList.add(list.get(j));
                }
            }
        }
        //mGetSwitchTableAdapter = new GetSwitchTableAdapter(mContext, mSwitchTableList, R.layout.item_switch_table, this);
        mGetNewSwitchTableAdapter = new GetNewSwitchTableAdapter();
        mRv.setLayoutManager(new GridLayoutManager(mContext, 4));
        //  mRv.setAdapter(mGetSwitchTableAdapter);
        mRv.setAdapter(mGetNewSwitchTableAdapter);
        mRv.setItemAnimator(new DefaultItemAnimator());

        mGetNewSwitchTableAdapter.replaceAll(mSwitchTableList);
        mGetNewSwitchTableAdapter.setOnItemClickListener(new GetNewSwitchTableAdapter.onItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                mNewTablePos = pos;
            }
        });
    }


    private void initTabAndView() {
        mTitleList = new ArrayList<>();
        mTitleList.add("全部");
        for (int i = 0; i < mAllTableDataList.size(); i++) {
            String areaName = mAllTableDataList.get(i).getAreaName();
            mTitleList.add(areaName);
        }
        for (int i = 0; i < mTitleList.size(); i++) {
            mTabEntities.add(new TabEntity(mTitleList.get(i)));
        }
        mCommonTabLayout.setTabData(mTabEntities);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_tv_sure:
                String tableID = "";
                if(mSwitchTableList != null && mSwitchTableList.size()>0){
                    tableID = String.valueOf(mSwitchTableList.get(mNewTablePos).getTableID());
                }
                handleBtnConfirmClick(tableID);
                break;
            case R.id.dialog_tv_cancel:
                dismiss();
                break;

            case R.id.dialog_iv_close:
                dismiss();
                break;
        }
    }

    private void handleBtnConfirmClick(String tableID) {
        if (mListener != null) {
            mListener.onSwitchTable(tableID);
        }

    }

    @Override
    public void show() {
        super.show();

    }


}
