package com.shiqupad.order.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import com.shiqupad.order.R;
import com.shiqupad.order.adapter.GetNewTableOptionAdapter;
import com.shiqupad.order.bean.TableOptionBean;


/**
 * Created by LuoXueKun on 2016/4/15.
 */
public class NewTableOptionDialog extends Dialog implements View.OnClickListener,
        AdapterView.OnItemClickListener {
    private RecyclerView mRv;
    private Context mContext;
    private TextView tvTitle;
    private GetNewTableOptionAdapter mGetNewTableOptionAdapter;
    private List<TableOptionBean> tableOptionBeanList = new ArrayList<>();
    private OnNewTableOptionClickListener onNewTableOptionClickListener;
    private GetAllTableResponse.Data.ListBean mListBean;
    public final static int ITEM_ADD = 0;       //加菜
    public final static int ITEM_PAY = 1;       //结账
    public final static int ITEM_CHANGE = 2;    //转台
    public final static int ITEM_ORDER = 3;     //点菜
    public final static int ITEM_PRINT = 4;     //修改人数
    public final static int ITEM_CLEAN = 5;     //清台
    public final static int ITEM_CANCEL = 6;     //销单
    public final static int ITEM_MERGE = 7;     //并台
    public final static int ITEM_SIGN = 8;     //签单
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        handleBtnConfirmClick(position);
    }


    public interface OnNewTableOptionClickListener {
        void onNewTableOptionClick(int position);
    }

    public void setOnNewTableOptionClickListener(OnNewTableOptionClickListener listener) {
        this.onNewTableOptionClickListener = listener;
    }

    public NewTableOptionDialog(Context context,GetAllTableResponse.Data.ListBean listBean,int tablePos) {
        super(context, R.style.myDialog);
        this.mContext = context;
        tableOptionBeanList.clear();
        this.mListBean = listBean;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_new_table_option);
        initView();
    }

    private void initView() {
        findViewById(R.id.dialog_iv_close).setOnClickListener(this);
        initRecyclerViewAndAdapter();
    }

    private void initRecyclerViewAndAdapter() {

        mRv = findViewById(R.id.dialog_new_table_option_rv);
        tableOptionBeanList = new ArrayList<>();
        tableOptionBeanList.add(new TableOptionBean("加菜"));
        tableOptionBeanList.add(new TableOptionBean("点菜单"));
        tableOptionBeanList.add(new TableOptionBean("转台"));
        tableOptionBeanList.add(new TableOptionBean("结账"));
        tableOptionBeanList.add(new TableOptionBean("修改人数"));
        tableOptionBeanList.add(new TableOptionBean("清台"));
        tableOptionBeanList.add(new TableOptionBean("销单"));
        tableOptionBeanList.add(new TableOptionBean("并台"));

        mGetNewTableOptionAdapter = new GetNewTableOptionAdapter(mContext, tableOptionBeanList, R.layout.item_new_option_table, this);
        mRv.setLayoutManager(new GridLayoutManager(mContext, 3));
        mRv.setAdapter(mGetNewTableOptionAdapter);
        mRv.setItemAnimator(new DefaultItemAnimator());

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_iv_close:
                dismiss();
                break;
        }
    }

    private void handleBtnConfirmClick(int position) {
        onNewTableOptionClickListener.onNewTableOptionClick(position);
    }

    @Override
    public void show() {
        super.show();
        tvTitle = (TextView) findViewById(R.id.tv_table_type);
        tvTitle.setText(String.format("桌台类型：%d人桌", mListBean.getPropleNum()));
    }


}
