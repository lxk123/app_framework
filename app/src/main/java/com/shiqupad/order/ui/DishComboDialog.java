package com.shiqupad.order.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;



import java.util.ArrayList;
import java.util.List;

import cn.com.dreamtouch.magicbox_http_client.network.model.DishSetDetail;
import cn.com.dreamtouch.magicbox_http_client.network.model.DishSetItem;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetDishDetailResponse;
import com.shiqupad.order.R;
import com.shiqupad.order.adapter.DishSetAdapter;


/**
 * 菜品规格、口味   Dialog
 * Created by lsy on 2017/11/22.
 */

public class DishComboDialog extends Dialog implements
        View.OnClickListener {

    private Context ctx;
    private TextView tvDishName;
    private ListView listView;
    private DishSetAdapter adapter;
    private List<DishSetDetail> selectCombo = new ArrayList<>();
    private List<DishSetItem> dataSet = new ArrayList<>();
    private  GetDishDetailResponse.DataBean  dishBean;
    private int selection;
    private int position;

    public DishComboDialog(Context context, GetDishDetailResponse.DataBean bean) {
        super(context, R.style.myDialog);
        ctx = context;
        this.dishBean = bean;

        for (int i = 0; i < this.dishBean.getSetItemList().size(); i++) {
            DishSetItem setItem = this.dishBean.getSetItemList().get(i);
            List<DishSetDetail> dishSetDetailList = new ArrayList<>();
            for (int j = 0; j < this.dishBean.getSetDetailList().size(); j++) {
                DishSetDetail setDetail = this.dishBean.getSetDetailList().get(j);
                if (setItem.getDishTypeID().equals(setDetail.getDishTypeID()))
                    dishSetDetailList.add(setDetail);
            }
            setItem.setDishSetDetailList(dishSetDetailList);
            if ("1".equals(setItem.getItemType()))
                setItem.setSelectDishCount(1);
            if ("3".equals(setItem.getItemType()))
                setItem.setSelectDishCount(setItem.getDishSetDetailList().size());

            dataSet.add(setItem);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_dish_combo);

        init();
    }

    private void init() {
        findViewById(R.id.dialog_combo_btn_cancel).setOnClickListener(this);
        findViewById(R.id.dialog_combo_btn_confirm).setOnClickListener(this);
        tvDishName = (TextView) findViewById(R.id.dialog_combo_tv_dishName);
        listView = (ListView) findViewById(R.id.dialog_combo_listView);
        adapter = new DishSetAdapter(ctx, dataSet);
        listView.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_combo_btn_confirm:
                if (checkDish()) {
                    callBack.onDishComboDialogCallBack(selectCombo,selection,position);
                    dismiss();
                } else
                    Toast.makeText(ctx, "请选择菜品", Toast.LENGTH_SHORT).show();
                break;
            case R.id.dialog_combo_btn_cancel:
                dismiss();
                break;
        }
    }

    private boolean checkDish() {
        selectCombo.clear();
        for (int i = 0; i < dataSet.size(); i++) {
            DishSetItem setItem = dataSet.get(i);
            int selectCount = 0;
            for (int j = 0; j < setItem.getDishSetDetailList().size(); j++) {
                if (setItem.getDishSetDetailList().get(j).getIsChecked()) {
                    selectCount++;
                    selectCombo.add(setItem.getDishSetDetailList().get(j));
                }
            }
            if (selectCount != setItem.getSelectDishCount())
                return false;
        }
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();


        tvDishName.setText(ctx.getString(R.string.format_dish_name, dishBean.getDishName()));

    }

    private IDishComboDialogCallBack callBack;

    public void setCallBack(IDishComboDialogCallBack callBack,int selection,int position) {
        this.callBack = callBack;
        this.selection = selection;
        this.position = position;
    }

    public interface IDishComboDialogCallBack {
        void onDishComboDialogCallBack(List<DishSetDetail> comboList, int selection, int position);
    }
}
