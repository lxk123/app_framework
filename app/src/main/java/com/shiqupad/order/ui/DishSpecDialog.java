package com.shiqupad.order.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.List;

import cn.com.dreamtouch.magicbox_http_client.network.model.DishTasteBean;
import cn.com.dreamtouch.magicbox_http_client.network.model.DishUnitBean;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetDishDetailResponse;
import com.shiqupad.order.R;
import com.shiqupad.order.adapter.FeedGridAdapter;


/**
 * 菜品规格、口味   Dialog
 * Created by lsy on 2017/11/22.
 */

public class DishSpecDialog extends Dialog implements
        View.OnClickListener,
        FeedGridAdapter.IFeedGridAdapterCallBack {

    private Context ctx;
    private final LayoutInflater inflater;
    private TextView tvEmpty1, tvEmpty2, tvEmpty3;
    private TagAdapter<DishUnitBean> tAdapter;
    private FeedGridAdapter mAdapter;
    private TagAdapter<DishTasteBean> bAdapter;
    private TagFlowLayout tasteFlow;
    private TagFlowLayout specFlow;
    private MyGridView gvFeeding;
    private List<DishTasteBean> bDataSet;//口味即忌口
    private List<DishTasteBean> mDataSet;//加料
    private List<DishUnitBean> tDataSet;//规格
    private TextView tvDishName, tvConfirm, tvMustFeeding;
  //  private DishItemBean dishBean;
   private GetDishDetailResponse.DataBean dishBean;
    public DishSpecDialog(Context context) {
        super(context, R.style.myDialog);
        ctx = context;
        inflater = LayoutInflater.from(ctx);
    }

    public DishSpecDialog(Context context, GetDishDetailResponse.DataBean bean) {
        super(context, R.style.myDialog);
        ctx = context;
        inflater = LayoutInflater.from(ctx);
        this.dishBean = bean;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_dish_spec);
        init();
    }

    public void setDataSet(List<DishTasteBean> bList, List<DishUnitBean> tList) {
        tDataSet = tList;
        bDataSet = bList;
    }

    public void setDataSet(List<DishTasteBean> bList, List<DishTasteBean> mList, List<DishUnitBean> tList) {
        tDataSet = tList;
        bDataSet = bList;
        mDataSet = mList;
    }

    private void init() {
        findViewById(R.id.dialog_spec_btn_cancel).setOnClickListener(this);
        tvConfirm = (TextView) findViewById(R.id.dialog_spec_btn_confirm);
        tvMustFeeding = (TextView) findViewById(R.id.dialog_spec_tv_mustFeeding);
        specFlow = (TagFlowLayout) findViewById(R.id.flow_spec);
        tasteFlow = (TagFlowLayout) findViewById(R.id.flow_taste);
        gvFeeding = (MyGridView) findViewById(R.id.gv_feeding);
        tvEmpty1 = (TextView) findViewById(R.id.tv_empty_1);
        tvEmpty2 = (TextView) findViewById(R.id.tv_empty_2);
        tvEmpty3 = (TextView) findViewById(R.id.tv_empty_3);
        tvDishName = (TextView) findViewById(R.id.tv_dishName);

        updateBtnText();

        tAdapter = new TagAdapter<DishUnitBean>(tDataSet) {
            @Override
            public View getView(FlowLayout parent, int position, DishUnitBean unitBean) {
                TextView textView = (TextView) inflater.inflate(R.layout.item_dish_spec_tag, specFlow, false);
                textView.setText(String.format(ctx.getString(R.string.format_dish_spec), unitBean.getUnitName(), unitBean.getPrice() + ""));
                return textView;
            }
        };
        /*mAdapter = new TagAdapter<DishTasteBean>(mDataSet) {
            @Override
            public View getView(FlowLayout parent, int position, DishTasteBean DishTasteBean) {
                TextView textView = (TextView) inflater.inflate(R.layout.item_dish_spec_tag, feedingFlow, false);
                textView.setText(String.format(ctx.getString(R.string.format_dish_spec), DishTasteBean.getTasteName(), DishTasteBean.getRisePrice() + ""));
                return textView;
            }
        };*/
        mAdapter = new FeedGridAdapter(ctx, mDataSet);
        mAdapter.setIFeedGridAdapterCallBack(this);
        bAdapter = new TagAdapter<DishTasteBean>(bDataSet) {
            @Override
            public View getView(FlowLayout parent, int position, DishTasteBean tasteBean) {
                TextView textView = (TextView) inflater.inflate(R.layout.item_dish_spec_tag, tasteFlow, false);
                textView.setText(tasteBean.getTasteName());
                return textView;
            }
        };
        specFlow.setAdapter(tAdapter);
        tasteFlow.setAdapter(bAdapter);
        gvFeeding.setAdapter(mAdapter);

        specFlow.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
            @Override
            public boolean onTagClick(View view, int position, FlowLayout parent) {
                tAdapter.setSelectedList(position);
                return false;
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.dialog_spec_btn_confirm:
                DishTasteBean selectTaste = null;
                DishUnitBean selectUnit = null;
                List<DishTasteBean> dishFeedings = new ArrayList<>();
                if (tDataSet.size() != 0)
                    selectUnit = (!specFlow.getSelectedList().isEmpty()) ? tDataSet.get((int) specFlow.getSelectedList().toArray()[0]) : null;
                if (bDataSet.size() != 0)
                    selectTaste = (!tasteFlow.getSelectedList().isEmpty()) ? bDataSet.get((int) tasteFlow.getSelectedList().toArray()[0]) : null;

                for (int i = 0; i < mDataSet.size(); i++) {
                    if (mDataSet.get(i).getValue() > 0) {
                        dishFeedings.add(mDataSet.get(i));
                    }
                }

                callBack.onSpecDialogCallBack(selectUnit, selectTaste, dishFeedings);
                dismiss();
                break;
            case R.id.dialog_spec_btn_cancel:
                dismiss();
                break;
        }
    }

    @Override
    public void onNumChanger(int position, int num) {
        mDataSet.get(position).setValue(num);
        mAdapter.notifyDataSetChanged();
        updateBtnText();
    }

    private void updateBtnText() {
        int feedNum = 0;
        for (int i = 0; i < mDataSet.size(); i++) {
            feedNum += mDataSet.get(i).getValue();
        }
        if (dishBean.getIsMustFeeding() - feedNum <= 0) {
            tvConfirm.setOnClickListener(this);
            tvConfirm.setText(ctx.getString(R.string.button_sure));
            tvConfirm.setTextColor(ContextCompat.getColor(ctx, R.color.tab_select_h));
        } else {
            tvConfirm.setOnClickListener(null);
            tvConfirm.setText(ctx.getString(R.string.format_feed_select_num, dishBean.getIsMustFeeding() - feedNum));
            tvConfirm.setTextColor(ContextCompat.getColor(ctx, R.color.text_grey));
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (tDataSet.size() > 0) {
            tAdapter.setSelectedList(0);
            tAdapter.notifyDataChanged();
            tvEmpty1.setVisibility(View.GONE);
            specFlow.setVisibility(View.VISIBLE);
        } else {
            tvEmpty1.setVisibility(View.VISIBLE);
            specFlow.setVisibility(View.GONE);
        }
        if (bDataSet.size() > 0) {
            bAdapter.setSelectedList(0);
            bAdapter.notifyDataChanged();
            tvEmpty2.setVisibility(View.GONE);
            tasteFlow.setVisibility(View.VISIBLE);
        } else {
            tvEmpty2.setVisibility(View.VISIBLE);
            tasteFlow.setVisibility(View.GONE);
        }
        if (mDataSet.size() > 0) {
            mAdapter.notifyDataSetChanged();
            tvEmpty3.setVisibility(View.GONE);
            gvFeeding.setVisibility(View.VISIBLE);
        } else {
            tvEmpty3.setVisibility(View.VISIBLE);
            gvFeeding.setVisibility(View.GONE);
        }
        tvDishName.setText(ctx.getString(R.string.format_dish_name, dishBean.getDishName()));
        if (dishBean.getIsMustFeeding() > 0)
            tvMustFeeding.setText(ctx.getString(R.string.format_must_feeding, dishBean.getIsMustFeeding()));
        tAdapter.notifyDataChanged();
        bAdapter.notifyDataChanged();
        mAdapter.notifyDataSetChanged();
    }

    private IDishSpecDialogCallBack callBack;

    public void setCallBack(IDishSpecDialogCallBack callBack) {
        this.callBack = callBack;
    }

    public interface IDishSpecDialogCallBack {
        void onSpecDialogCallBack(DishUnitBean selectUnit, DishTasteBean selectTaste, List<DishTasteBean> feedingList);
    }
}
