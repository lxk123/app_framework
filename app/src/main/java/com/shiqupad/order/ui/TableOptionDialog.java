package com.shiqupad.order.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import com.shiqupad.order.R;
/**
 * Created by frank on 2016/4/15.
 */
public class TableOptionDialog extends Dialog implements View.OnClickListener {

    public final static int ITEM_ADD = 0;       //加菜
    public final static int ITEM_PAY = 1;       //结账
    public final static int ITEM_CHANGE = 2;    //转台
    public final static int ITEM_ORDER = 3;     //点菜
    public final static int ITEM_PRINT = 4;     //修改人数
    public final static int ITEM_CLEAN = 5;     //清台
    public final static int ITEM_CANCEL = 6;     //销单
    public final static int ITEM_MERGE = 7;     //并台
    public final static int ITEM_SIGN = 8;     //签单
    private  int mTablePos;
    public interface OnTableOptionListener {
        void onOptionItemClick(int item,int tablePos);
    }

    private TextView tvTitle;
    private Context context;
    private OnTableOptionListener mListener;
    private GetAllTableResponse.Data.ListBean mListBean;

    public TableOptionDialog(Context context,GetAllTableResponse.Data.ListBean listBean,int tablePos) {
        super(context, R.style.myDialog);
        this.context = context;
        mListBean = listBean;
        this.mTablePos = tablePos;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_table_option);

        initView();
    }

    public void setOnTableOptionListener(OnTableOptionListener listener) {
        this.mListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (mListener == null) {
            return;
        }
        switch (v.getId()) {
            case R.id.ll_add_dish:
                mListener.onOptionItemClick(ITEM_ADD,mTablePos);
                break;
            case R.id.ll_payment:
                mListener.onOptionItemClick(ITEM_PAY,mTablePos);
                break;
            case R.id.ll_change_table:
                mListener.onOptionItemClick(ITEM_CHANGE,mTablePos);
                break;
            case R.id.ll_order_dish:
                mListener.onOptionItemClick(ITEM_ORDER,mTablePos);
                break;
            case R.id.ll_print:
                mListener.onOptionItemClick(ITEM_PRINT,mTablePos);
                break;
            case R.id.ll_empty_table:
                mListener.onOptionItemClick(ITEM_CLEAN,mTablePos);
                break;
            case R.id.ll_cancelOrder:
                mListener.onOptionItemClick(ITEM_CANCEL,mTablePos);
                break;
            case R.id.ll_merge:
                mListener.onOptionItemClick(ITEM_MERGE,mTablePos);
                break;
            case R.id.ll_sign:
                mListener.onOptionItemClick(ITEM_SIGN,mTablePos);
                break;
        }
    }

    private void initView() {
        findViewById(R.id.ll_add_dish).setOnClickListener(this);
        findViewById(R.id.ll_payment).setOnClickListener(this);
        findViewById(R.id.ll_change_table).setOnClickListener(this);
        findViewById(R.id.ll_order_dish).setOnClickListener(this);
        findViewById(R.id.ll_print).setOnClickListener(this);
        findViewById(R.id.ll_empty_table).setOnClickListener(this);
        findViewById(R.id.ll_cancelOrder).setOnClickListener(this);
        findViewById(R.id.ll_merge).setOnClickListener(this);
        findViewById(R.id.ll_sign).setOnClickListener(this);

        tvTitle = (TextView) findViewById(R.id.tv_table_type);
        tvTitle.setText("桌台类型：" + mListBean.getPropleNum() + "人桌");

    }


}
