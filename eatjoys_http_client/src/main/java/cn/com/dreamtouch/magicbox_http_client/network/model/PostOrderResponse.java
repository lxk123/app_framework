package cn.com.dreamtouch.magicbox_http_client.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public class PostOrderResponse extends BaseResponse{

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    private String data;

    @Override
    public String toString() {
        return "LoginResponse{" +
                "data=" + data +
                '}';
    }





}
