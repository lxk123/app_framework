package cn.com.dreamtouch.magicbox_http_client.network.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by LuoXueKun
 * on 2018/5/9
 */
public class GetShopDishResponse extends BaseResponse {
    private List<OneShopDish> data;

    public List<OneShopDish> getData() {
        return data;
    }

    public void setData(List<OneShopDish> data) {
        this.data = data;
    }

    public static class OneShopDish {
        private List<TwoShopDish> dishList;
        private String dishTypeID;
        private String mealtimeID;
        private String mealtimeName;
        private String shopID;
        private String sortNumber;
        private String typeName;
        private int cartNum;

        public int getCartNum() {
            return cartNum;
        }

        public void setCartNum(int cartNum) {
            this.cartNum = cartNum;
        }

        public List<TwoShopDish> getDishList() {
            return dishList;
        }

        public void setDishList(List<TwoShopDish> dishList) {
            this.dishList = dishList;
        }

        public String getDishTypeID() {
            return dishTypeID;
        }

        public void setDishTypeID(String dishTypeID) {
            this.dishTypeID = dishTypeID;
        }

        public String getMealtimeID() {
            return mealtimeID;
        }

        public void setMealtimeID(String mealtimeID) {
            this.mealtimeID = mealtimeID;
        }

        public String getMealtimeName() {
            return mealtimeName;
        }

        public void setMealtimeName(String mealtimeName) {
            this.mealtimeName = mealtimeName;
        }

        public String getShopID() {
            return shopID;
        }

        public void setShopID(String shopID) {
            this.shopID = shopID;
        }

        public String getSortNumber() {
            return sortNumber;
        }

        public void setSortNumber(String sortNumber) {
            this.sortNumber = sortNumber;
        }

        public String getTypeName() {
            return typeName;
        }

        public void setTypeName(String typeName) {
            this.typeName = typeName;
        }


    }

    public static class TwoShopDish implements Cloneable{
        @Override
        public String toString() {
            return "TwoShopDish{" +
                    "boxFee='" + boxFee + '\'' +
                    ", code='" + code + '\'' +
                    ", defaultImageUrl='" + defaultImageUrl + '\'' +
                    ", description='" + description + '\'' +
                    ", dishEnglishName='" + dishEnglishName + '\'' +
                    ", dishID='" + dishID + '\'' +
                    ", dishName='" + dishName + '\'' +
                    ", dishOtherName='" + dishOtherName + '\'' +
                    ", dishTasteList='" + dishTasteList + '\'' +
                    ", dishTypeID='" + dishTypeID + '\'' +
                    ", dishTypeName='" + dishTypeName + '\'' +
                    ", dishTypeNamedishUnitList='" + dishTypeNamedishUnitList + '\'' +
                    ", isHaveTaste='" + isHaveTaste + '\'' +
                    ", isHaveUnit='" + isHaveUnit + '\'' +
                    ", isMarketPriceDish='" + isMarketPriceDish + '\'' +
                    ", isMustFeeding='" + isMustFeeding + '\'' +
                    ", isMustOrder='" + isMustOrder + '\'' +
                    ", isNeedUpdate='" + isNeedUpdate + '\'' +
                    ", isRebate='" + isRebate + '\'' +
                    ", isRecommend='" + isRecommend + '\'' +
                    ", isSet='" + isSet + '\'' +
                    ", isUse='" + isUse + '\'' +
                    ", isWeight='" + isWeight + '\'' +
                    ", limitNumber='" + limitNumber + '\'' +
                    ", makeTime='" + makeTime + '\'' +
                    ", measureUnitName='" + measureUnitName + '\'' +
                    ", orderUnitName='" + orderUnitName + '\'' +
                    ", originCost='" + originCost + '\'' +
                    ", price=" + price +
                    ", saleNumber='" + saleNumber + '\'' +
                    ", setDetailList='" + setDetailList + '\'' +
                    ", setItemList='" + setItemList + '\'' +
                    ", shopDishID='" + shopDishID + '\'' +
                    ", shopID='" + shopID + '\'' +
                    ", shopName='" + shopName + '\'' +
                    ", sortNumber='" + sortNumber + '\'' +
                    ", totalSaleNumber='" + totalSaleNumber + '\'' +
                    ", selectCount=" + selectCount +
                    ", cartNum=" + cartNum +
                    ", dishUnit=" + dishUnit +
                    ", dishTaste=" + dishTaste +
                    ", feedList=" + feedList +
                    ", comboList=" + comboList +
                    ", dishUnitName='" + dishUnitName + '\'' +
                    ", dishTsateName='" + dishTsateName + '\'' +
                    ", dishUnitID='" + dishUnitID + '\'' +
                    ", feedDesc='" + feedDesc + '\'' +
                    ", dishTasteID='" + dishTasteID + '\'' +
                    ", comboDesc='" + comboDesc + '\'' +
                    '}';
        }

        private String boxFee;
        private String code;
        private String defaultImageUrl;
        private String description;
        private String dishEnglishName;
        private String dishID;
        private String dishName;
        private String dishOtherName;
        private String dishTasteList;
        private String dishTypeID;
        private String dishTypeName;
        private String dishTypeNamedishUnitList;
        //rivate String imageList;
        private String isHaveTaste;
        private String isHaveUnit;
        private String isMarketPriceDish;
        private String isMustFeeding;
        private String isMustOrder;
        private String isNeedUpdate;
        private String isRebate;
        private String isRecommend;
        private String isSet;
        private String isUse;
        private String isWeight;
        private String limitNumber;
        private String makeTime;
        private String measureUnitName;
        private String orderUnitName;
        private String originCost;
        private double price;
        private String saleNumber;
        private String setDetailList;
        private String setItemList;
        private String shopDishID;
        private String shopID;
        private String shopName;
        private String sortNumber;
        private String totalSaleNumber;
        private long selectCount;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getDefaultImageUrl() {
            return defaultImageUrl;
        }

        public void setDefaultImageUrl(String defaultImageUrl) {
            this.defaultImageUrl = defaultImageUrl;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getDishEnglishName() {
            return dishEnglishName;
        }

        public void setDishEnglishName(String dishEnglishName) {
            this.dishEnglishName = dishEnglishName;
        }

        public String getDishID() {
            return dishID;
        }

        public void setDishID(String dishID) {
            this.dishID = dishID;
        }

        public String getDishName() {
            return dishName;
        }

        public void setDishName(String dishName) {
            this.dishName = dishName;
        }

        public String getDishOtherName() {
            return dishOtherName;
        }

        public void setDishOtherName(String dishOtherName) {
            this.dishOtherName = dishOtherName;
        }

        public String getDishTasteList() {
            return dishTasteList;
        }

        public void setDishTasteList(String dishTasteList) {
            this.dishTasteList = dishTasteList;
        }

        public String getDishTypeID() {
            return dishTypeID;
        }

        public void setDishTypeID(String dishTypeID) {
            this.dishTypeID = dishTypeID;
        }

        public String getDishTypeName() {
            return dishTypeName;
        }

        public void setDishTypeName(String dishTypeName) {
            this.dishTypeName = dishTypeName;
        }

        public String getDishTypeNamedishUnitList() {
            return dishTypeNamedishUnitList;
        }

        public void setDishTypeNamedishUnitList(String dishTypeNamedishUnitList) {
            this.dishTypeNamedishUnitList = dishTypeNamedishUnitList;
        }


        public String getIsHaveTaste() {
            return isHaveTaste;
        }

        public void setIsHaveTaste(String isHaveTaste) {
            this.isHaveTaste = isHaveTaste;
        }

        public String getIsHaveUnit() {
            return isHaveUnit;
        }

        public void setIsHaveUnit(String isHaveUnit) {
            this.isHaveUnit = isHaveUnit;
        }

        public String getIsMarketPriceDish() {
            return isMarketPriceDish;
        }

        public void setIsMarketPriceDish(String isMarketPriceDish) {
            this.isMarketPriceDish = isMarketPriceDish;
        }

        public String getIsMustFeeding() {
            return isMustFeeding;
        }

        public void setIsMustFeeding(String isMustFeeding) {
            this.isMustFeeding = isMustFeeding;
        }

        public String getIsMustOrder() {
            return isMustOrder;
        }

        public void setIsMustOrder(String isMustOrder) {
            this.isMustOrder = isMustOrder;
        }

        public String getIsNeedUpdate() {
            return isNeedUpdate;
        }

        public void setIsNeedUpdate(String isNeedUpdate) {
            this.isNeedUpdate = isNeedUpdate;
        }

        public String getIsRebate() {
            return isRebate;
        }

        public void setIsRebate(String isRebate) {
            this.isRebate = isRebate;
        }

        public String getIsRecommend() {
            return isRecommend;
        }

        public void setIsRecommend(String isRecommend) {
            this.isRecommend = isRecommend;
        }

        public String getIsSet() {
            return isSet;
        }

        public void setIsSet(String isSet) {
            this.isSet = isSet;
        }

        public String getIsUse() {
            return isUse;
        }

        public void setIsUse(String isUse) {
            this.isUse = isUse;
        }

        public String getIsWeight() {
            return isWeight;
        }

        public void setIsWeight(String isWeight) {
            this.isWeight = isWeight;
        }

        public String getLimitNumber() {
            return limitNumber;
        }

        public void setLimitNumber(String limitNumber) {
            this.limitNumber = limitNumber;
        }

        public String getMakeTime() {
            return makeTime;
        }

        public void setMakeTime(String makeTime) {
            this.makeTime = makeTime;
        }

        public String getMeasureUnitName() {
            return measureUnitName;
        }

        public void setMeasureUnitName(String measureUnitName) {
            this.measureUnitName = measureUnitName;
        }

        public String getOrderUnitName() {
            return orderUnitName;
        }

        public void setOrderUnitName(String orderUnitName) {
            this.orderUnitName = orderUnitName;
        }

        public String getOriginCost() {
            return originCost;
        }

        public void setOriginCost(String originCost) {
            this.originCost = originCost;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public String getSaleNumber() {
            return saleNumber;
        }

        public void setSaleNumber(String saleNumber) {
            this.saleNumber = saleNumber;
        }

        public String getSetDetailList() {
            return setDetailList;
        }

        public void setSetDetailList(String setDetailList) {
            this.setDetailList = setDetailList;
        }

        public String getSetItemList() {
            return setItemList;
        }

        public void setSetItemList(String setItemList) {
            this.setItemList = setItemList;
        }

        public String getShopDishID() {
            return shopDishID;
        }

        public void setShopDishID(String shopDishID) {
            this.shopDishID = shopDishID;
        }

        public String getShopID() {
            return shopID;
        }

        public void setShopID(String shopID) {
            this.shopID = shopID;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public String getSortNumber() {
            return sortNumber;
        }

        public void setSortNumber(String sortNumber) {
            this.sortNumber = sortNumber;
        }

        public String getTotalSaleNumber() {
            return totalSaleNumber;
        }

        public void setTotalSaleNumber(String totalSaleNumber) {
            this.totalSaleNumber = totalSaleNumber;
        }

        public String getBoxFee() {
            return boxFee;
        }

        public void setBoxFee(String boxFee) {
            this.boxFee = boxFee;
        }

        public long getSelectCount() {
            return selectCount;
        }

        public void setSelectCount(long selectCount) {
            this.selectCount = selectCount;
        }

        private int cartNum;

        public int getCartNum() {
            return cartNum;
        }

        public void setCartNum(int cartNum) {
            this.cartNum = cartNum;
        }


        /*==============================*/
        private DishUnitBean dishUnit = new DishUnitBean();
        private DishTasteBean dishTaste = new DishTasteBean();
        private List<DishTasteBean> feedList = new ArrayList<>();
        private List<DishSetDetail> comboList = new ArrayList<>();
        private String dishUnitName;
        private String dishTsateName;
        private String dishUnitID;
        private String feedDesc;//加料描述
        private String dishTasteID;
        private String comboDesc;//套餐描述

        public DishUnitBean getDishUnit() {
            return dishUnit;
        }

        public void setDishUnit(DishUnitBean dishUnit) {
            this.dishUnit = dishUnit;
        }

        public List<DishTasteBean> getFeedList() {
            return feedList;
        }

        public void setFeedList(List<DishTasteBean> feedList) {
            this.feedList = feedList;
        }
        public DishTasteBean getDishTaste() {
            return dishTaste;
        }

        public void setDishTaste(DishTasteBean dishTaste) {
            this.dishTaste = dishTaste;
        }

        public List<DishSetDetail> getComboList() {
            return comboList;
        }

        public void setComboList(List<DishSetDetail> comboList) {
            this.comboList = comboList;
        }

        public void setDishUnitName(String dishUnitName) {
            this.dishUnitName = dishUnitName;
        }

        public String getDishTsateName() {
            return dishTsateName;
        }
        public String getDishUnitID() {
            return dishUnitID;
        }

        public void setDishUnitID(String dishUnitID) {
            this.dishUnitID = dishUnitID;
        }

        public String getFeedDesc() {
            return feedDesc;
        }

        public void setFeedDesc(String feedDesc) {
            this.feedDesc = feedDesc;
        }


        public String getDishTasteID() {
            return dishTasteID;
        }

        public void setDishTasteID(String dishTasteID) {
            this.dishTasteID = dishTasteID;
        }


        public void setDishTsateName(String dishTsateName) {
            this.dishTsateName = dishTsateName;
        }

        public String getComboDesc() {
            return comboDesc;
        }

        public void setComboDesc(String comboDesc) {
            this.comboDesc = comboDesc;
        }
        @Override
        public Object clone() {
            TwoShopDish bean = null;
            try {
                bean = (TwoShopDish) super.clone();
            } catch (CloneNotSupportedException e) {
                e.printStackTrace();
            }
            return bean;
        }
    }

}
