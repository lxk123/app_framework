package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by hu on 2016/8/11.
 */
public class DetailListBean {
    private int customOrderDetailID;
    private int waitressOrderDetailID;
    private int shopID;
    private int waitressOrderID;
    private int sortNumber;
    private int dishID;
    private String dishName;
    private int waiterID;
    private Object waiterName;
    private double tastePrice;
    private int dishStatus;
    private Integer serailNumber;
    private Object orginalPrice;
    private double price;
    private int dishNumber;
    private Object backNumber;
    private Object rebate;
    private Object isPresent;
    private Object remark;
    private Object salerID;
    private Object confirmTime;
    private Object kitchenTime;
    private Object rowTime;
    private int isDelete;
    private String createUserID;
    private String createUserName;
    private String createDate;
    private String modifyUserID;
    private String modifyUserName;
    private String modifyDate;
    private String dishUnitName;
    private String tasteName;
    private String isMustOrder;
    private String feedingJson;
    private int detailType;//1：菜--2：套餐内菜品
    private double feedingPrice;//加料总价
    private double sumPrice;//总价
    private int isRetire;//0正常1退菜
    private int retireNum;//退菜数量

    public int getIsRetire() {
        return isRetire;
    }

    public void setIsRetire(int isRetire) {
        this.isRetire = isRetire;
    }

    public int getRetireNum() {
        return retireNum;
    }

    public void setRetireNum(int retireNum) {
        this.retireNum = retireNum;
    }

    public double getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(double sumPrice) {
        this.sumPrice = sumPrice;
    }

    public double getFeedingPrice() {
        return feedingPrice;
    }

    public void setFeedingPrice(double feedingPrice) {
        this.feedingPrice = feedingPrice;
    }

    public int getDetailType() {
        return detailType;
    }

    public void setDetailType(int detailType) {
        this.detailType = detailType;
    }

    public String getTasteName() {
        return tasteName;
    }

    public void setTasteName(String tasteName) {
        this.tasteName = tasteName;
    }

    public int getCustomOrderDetailID() {
        return customOrderDetailID;
    }

    public void setCustomOrderDetailID(int customOrderDetailID) {
        this.customOrderDetailID = customOrderDetailID;
    }



    public String getFeedingJson() {
        return feedingJson;
    }

    public void setFeedingJson(String feedingJson) {
        this.feedingJson = feedingJson;
    }
    public String getDishUnitName() {
        return dishUnitName;
    }

    public void setDishUnitName(String dishUnitName) {
        this.dishUnitName = dishUnitName;
    }

    public String getIsMustOrder() {
        return isMustOrder;
    }

    public void setIsMustOrder(String isMustOrder) {
        this.isMustOrder = isMustOrder;
    }

    public int getWaitressOrderDetailID() {
        return waitressOrderDetailID;
    }

    public void setWaitressOrderDetailID(int waitressOrderDetailID) {
        this.waitressOrderDetailID = waitressOrderDetailID;
    }

    public int getShopID() {
        return shopID;
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }

    public int getWaitressOrderID() {
        return waitressOrderID;
    }

    public void setWaitressOrderID(int waitressOrderID) {
        this.waitressOrderID = waitressOrderID;
    }

    public int getSortNumber() {
        return sortNumber;
    }

    public void setSortNumber(int sortNumber) {
        this.sortNumber = sortNumber;
    }

    public int getDishID() {
        return dishID;
    }

    public void setDishID(int dishID) {
        this.dishID = dishID;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public int getWaiterID() {
        return waiterID;
    }

    public void setWaiterID(int waiterID) {
        this.waiterID = waiterID;
    }

    public Object getWaiterName() {
        return waiterName;
    }

    public void setWaiterName(Object waiterName) {
        this.waiterName = waiterName;
    }

    public double getTastePrice() {
        return tastePrice;
    }

    public void setTastePrice(int tastePrice) {
        this.tastePrice = tastePrice;
    }

    public int getDishStatus() {
        return dishStatus;
    }

    public void setDishStatus(int dishStatus) {
        this.dishStatus = dishStatus;
    }

    public Integer getSerailNumber() {
        return serailNumber;
    }

    public void setSerailNumber(Integer serailNumber) {
        this.serailNumber = serailNumber;
    }

    public Object getOrginalPrice() {
        return orginalPrice;
    }

    public void setOrginalPrice(Object orginalPrice) {
        this.orginalPrice = orginalPrice;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDishNumber() {
        return dishNumber;
    }

    public void setDishNumber(int dishNumber) {
        this.dishNumber = dishNumber;
    }

    public Object getBackNumber() {
        return backNumber;
    }

    public void setBackNumber(Object backNumber) {
        this.backNumber = backNumber;
    }

    public Object getRebate() {
        return rebate;
    }

    public void setRebate(Object rebate) {
        this.rebate = rebate;
    }

    public Object getIsPresent() {
        return isPresent;
    }

    public void setIsPresent(Object isPresent) {
        this.isPresent = isPresent;
    }

    public Object getRemark() {
        return remark;
    }

    public void setRemark(Object remark) {
        this.remark = remark;
    }

    public Object getSalerID() {
        return salerID;
    }

    public void setSalerID(Object salerID) {
        this.salerID = salerID;
    }

    public Object getConfirmTime() {
        return confirmTime;
    }

    public void setConfirmTime(Object confirmTime) {
        this.confirmTime = confirmTime;
    }

    public Object getKitchenTime() {
        return kitchenTime;
    }

    public void setKitchenTime(Object kitchenTime) {
        this.kitchenTime = kitchenTime;
    }

    public Object getRowTime() {
        return rowTime;
    }

    public void setRowTime(Object rowTime) {
        this.rowTime = rowTime;
    }

    public int getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(int isDelete) {
        this.isDelete = isDelete;
    }

    public String getCreateUserID() {
        return createUserID;
    }

    public void setCreateUserID(String createUserID) {
        this.createUserID = createUserID;
    }

    public String getCreateUserName() {
        return createUserName;
    }

    public void setCreateUserName(String createUserName) {
        this.createUserName = createUserName;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getModifyUserID() {
        return modifyUserID;
    }

    public void setModifyUserID(String modifyUserID) {
        this.modifyUserID = modifyUserID;
    }

    public String getModifyUserName() {
        return modifyUserName;
    }

    public void setModifyUserName(String modifyUserName) {
        this.modifyUserName = modifyUserName;
    }

    public String getModifyDate() {
        return modifyDate;
    }

    public void setModifyDate(String modifyDate) {
        this.modifyDate = modifyDate;
    }
}
