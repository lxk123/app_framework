package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by lsy on 2018/4/19.
 */

public class DishTasteBean {
    private String dishTasteID;
    private int shopID;
    private int dishID;
    private String tasteName;
    private double risePrice;
    private int type;
    private int value;
    private boolean selected;
    private int isRetire;//0正常1退菜

    @Override
    public String toString() {
        return "DishTasteBean{" +
                "dishTasteID='" + dishTasteID + '\'' +
                ", tasteName='" + tasteName + '\'' +
                ", risePrice=" + risePrice +
                '}';
    }

    public int getIsRetire() {
        return isRetire;
    }

    public void setIsRetire(int isRetire) {
        this.isRetire = isRetire;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getDishTasteID() {
        return dishTasteID;
    }

    public void setDishTasteID(String dishTasteID) {
        this.dishTasteID = dishTasteID;
    }

    public int getShopID() {
        return shopID;
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }

    public int getDishID() {
        return dishID;
    }

    public void setDishID(int dishID) {
        this.dishID = dishID;
    }

    public String getTasteName() {
        return tasteName;
    }

    public void setTasteName(String tasteName) {
        this.tasteName = tasteName;
    }

    public double getRisePrice() {
        return risePrice;
    }

    public void setRisePrice(double risePrice) {
        this.risePrice = risePrice;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
