package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by LuoXueKun
 * on 2018/5/10
 */
public class GetMessageRequest extends  BaseRequest{
    private String shopID;

    public String getShopID() {
        return shopID;
    }

    public void setShopID(String shopID) {
        this.shopID = shopID;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    private String page;


}
