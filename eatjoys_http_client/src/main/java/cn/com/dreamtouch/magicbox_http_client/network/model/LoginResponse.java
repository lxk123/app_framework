package cn.com.dreamtouch.magicbox_http_client.network.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public class LoginResponse extends BaseResponse{

    @SerializedName("data")
    private Data data;

    @Override
    public String toString() {
        return "LoginResponse{" +
                "data=" + data +
                '}';
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public static class Data{
        /**
         * loginChannel : APP_B
         * channel : null
         * customID : -2
         * userName : 18957987331
         * shopID : 4540
         * shopName : null
         * shopType : 1
         * accountType : 5
         * sid : zsze97od7rq0hog4zlq7gba6
         * mobile : 18957987331
         * address : 浙江杭州湖滨
         * employeeID : 1064
         * deliveryMode : 0
         */

        private String loginChannel;
        private Object channel;
        private int customID;
        private String userName;
        private int shopID;
        private String shopName;
        private int shopType;
        private int accountType;
        private String sid;
        private String mobile;
        private String address;
        private String employeeID;
        private int deliveryMode;

        public String getLoginChannel() {
            return loginChannel;
        }

        public void setLoginChannel(String loginChannel) {
            this.loginChannel = loginChannel;
        }

        public Object getChannel() {
            return channel;
        }

        public void setChannel(Object channel) {
            this.channel = channel;
        }

        public int getCustomID() {
            return customID;
        }

        public void setCustomID(int customID) {
            this.customID = customID;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public int getShopID() {
            return shopID;
        }

        public void setShopID(int shopID) {
            this.shopID = shopID;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public int getShopType() {
            return shopType;
        }

        public void setShopType(int shopType) {
            this.shopType = shopType;
        }

        public int getAccountType() {
            return accountType;
        }

        public void setAccountType(int accountType) {
            this.accountType = accountType;
        }

        public String getSid() {
            return sid;
        }

        public void setSid(String sid) {
            this.sid = sid;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getEmployeeID() {
            return employeeID;
        }

        public void setEmployeeID(String employeeID) {
            this.employeeID = employeeID;
        }

        public int getDeliveryMode() {
            return deliveryMode;
        }

        public void setDeliveryMode(int deliveryMode) {
            this.deliveryMode = deliveryMode;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "loginChannel='" + loginChannel + '\'' +
                    ", channel=" + channel +
                    ", customID=" + customID +
                    ", userName='" + userName + '\'' +
                    ", shopID=" + shopID +
                    ", shopName='" + shopName + '\'' +
                    ", shopType=" + shopType +
                    ", accountType=" + accountType +
                    ", sid='" + sid + '\'' +
                    ", mobile='" + mobile + '\'' +
                    ", address='" + address + '\'' +
                    ", employeeID='" + employeeID + '\'' +
                    ", deliveryMode=" + deliveryMode +
                    '}';
        }
    }

}
