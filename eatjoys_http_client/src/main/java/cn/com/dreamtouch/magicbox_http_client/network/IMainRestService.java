package cn.com.dreamtouch.magicbox_http_client.network;

import cn.com.dreamtouch.magicbox_http_client.network.model.CancelOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangePasswordResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangeTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChearTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllPayMethodResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetDishDetailResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetMessageResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetOrderListResopnse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.IsNeedTableFeeResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.LoginResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.OpenTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PostOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PrintOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushIsOnOffResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushMessageResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.QrCodeTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.RetreatOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.SendVaildateCodeResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.UpdateDinnerPeopleResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.WaitConfirmResponse;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by MuHan on 17/2/20.
 */

public interface IMainRestService {


    @GET("menu/login")
    Observable<LoginResponse> login(@Query("username") String username,
                                    @Query("password") String password);

    @GET("table/getAllTable")
    Observable<GetAllTableResponse> getAllTable(@Query("sid") String sid,
                                                @Query("shopID") String shopID ,
                                                @Query("updateFlag") String updateFlag
                                                );

    @GET("table/openTable")
    Observable<OpenTableResponse> openTable(@Query("tableID") String tableID,
                                            @Query("shopID") String shopID,
                                            @Query("peopleNumber") String peopleNumber,
                                            @Query("remark") String remark);

    @GET("table/changeTable")
    Observable<ChangeTableResponse> changeTable(@Query("sid") String sid,
                                                @Query("oldTableID") String oldTableID,
                                                @Query("tableID") String tableID,
                                                @Query("shopID") String shopID,
                                                @Query("orderID") String orderID);

    @GET("table/cleanTable")
    Observable<ChearTableResponse> cleanTable(@Query("tableID") String tableID,
                                              @Query("shopID") String shopID);

    @GET("dish/getDishDetail")
    Observable<GetDishDetailResponse> getDishDetail(@Query("shopDishID") String shopDishID,
                                                    @Query("shopID") String shopID);

    @GET("order/getCustomOrderInfo")
    Observable<GetCustomOrderInfoResponse> getCustomOrderInfo(@Query("shopID") String shopID,
                                                              @Query("orderID") String orderID ,
                                                              @Query("isUpdate") String isUpdate

    );

    @GET("order/cancelOrder")
    Observable<CancelOrderResponse> cancelOrder(@Query("shopID") String shopID,
                                                @Query("orderID") String orderID);

    @GET("table/qrcodeTable")
    Observable<QrCodeTableResponse> qrCodeTable(@Query("shopID") String shopID,
                                                @Query("qrcode") String qrcode);

    @GET("order/getOrderList")
    Observable<GetOrderListResopnse> getOrderList(@Query("date") String date,
                                                  @Query("shopID") String shopID,
                                                  @Query("pageSize") String pageSize,
                                                  @Query("pageIndex") String pageIndex);

    @GET("order/updateDinnerPeople")
    Observable<UpdateDinnerPeopleResponse> updateDinnerPeople(@Query("shopID") String shopID,
                                                              @Query("tableID") String tableID,
                                                              @Query("orderID") String orderID,
                                                              @Query("peopleNum") String peopleNum);

    @POST("menu/changePassword")
    Observable<ChangePasswordResponse> changePassword(@Query("mobile") String mobile,
                                                      @Query("pwd") String pwd,
                                                      @Query("code") String code);
    @POST("order/waiterConfirmOrder")
    Observable<WaitConfirmResponse> waiterConfirmOrder(@Query("shopID") String shopID ,
                                                       @Query("orderID") String orderID,
                                                       @Query("confirm") String confirm,
                                                       @Query("remark") String remark
    );

    @GET("menu/sendValidateCode")
    Observable<SendVaildateCodeResponse> sendValidateCode(@Query("mobile") String mobile);

    @GET("boss/pushMessage")
    Observable<PushMessageResponse> pushMessage(@Query("shopSerialNumber") String shopSerialNumber,
                                                @Query("shopID") String shopID);

    @GET("employee/pullIsOnOff")
        Observable<PushIsOnOffResponse> pullIsOnOff(@Query("employeeID") String employeeID);

    @GET("order/isNeedTableFee")
    Observable<IsNeedTableFeeResponse> isNeedTableFee(@Query("tableID") String tableID,
                                                      @Query("shopID") String shopID);
    @GET("boss/reprintOrder")
    Observable<PrintOrderResponse> reprintOrder(@Query("orderID") String orderID,
                                                @Query("shopID") String shopID);

    @GET("paymethod/getAllPayMethod")
    Observable<GetAllPayMethodResponse> getAllPayMethod();

    @GET("menu/getShopDish")
    Observable<GetShopDishResponse> getShopDish();
    @GET("pushMessage/getMessage")
    Observable<GetMessageResponse> getMessage(@Query("shopId") String shopId, @Query("page") String page);

    @POST("order/postOrder")
    Observable<PostOrderResponse> postOrder(@Query("orderStr") String orderStr);


    @POST("order/payOrder")
    Observable<PayOrderResponse> payOrder(@Query("orderStr") String orderStr);

    @POST("order/retreatOrder")
    Observable<RetreatOrderResponse> retreatOrder(@Query("orderStr") String orderStr);

}
