package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by lsy on 2018/4/26.
 */

public class DishSetDetail {
    private int dishSetDetailID;
    private String dishSetID;
    private String dishDetailID;
    private String dishDetailNumber;
    private String dishTypeID;
    private String dishTypeName;
    private String dishName;
    private boolean isChecked;

    public boolean getIsChecked() {
        return isChecked;
    }

    public void setIsChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }

    public int getDishSetDetailID() {
        return dishSetDetailID;
    }

    public void setDishSetDetailID(int dishSetDetailID) {
        this.dishSetDetailID = dishSetDetailID;
    }

    public String getDishSetID() {
        return dishSetID;
    }

    public void setDishSetID(String dishSetID) {
        this.dishSetID = dishSetID;
    }

    public String getDishDetailID() {
        return dishDetailID;
    }

    public void setDishDetailID(String dishDetailID) {
        this.dishDetailID = dishDetailID;
    }

    public String getDishDetailNumber() {
        return dishDetailNumber;
    }

    public void setDishDetailNumber(String dishDetailNumber) {
        this.dishDetailNumber = dishDetailNumber;
    }

    public String getDishTypeID() {
        return dishTypeID;
    }

    public void setDishTypeID(String dishTypeID) {
        this.dishTypeID = dishTypeID;
    }

    public String getDishTypeName() {
        return dishTypeName;
    }

    public void setDishTypeName(String dishTypeName) {
        this.dishTypeName = dishTypeName;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    @Override
    public String toString() {
        return "DishSetDetail{" +
                "dishSetDetailID=" + dishSetDetailID +
                ", dishSetID='" + dishSetID + '\'' +
                ", dishDetailID='" + dishDetailID + '\'' +
                ", dishDetailNumber='" + dishDetailNumber + '\'' +
                ", dishTypeID='" + dishTypeID + '\'' +
                ", dishTypeName='" + dishTypeName + '\'' +
                ", dishName='" + dishName + '\'' +
                ", isChecked=" + isChecked +
                '}';
    }
}
