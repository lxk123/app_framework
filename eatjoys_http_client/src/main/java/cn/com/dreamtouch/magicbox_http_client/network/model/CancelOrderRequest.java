package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by LuoXueKun
 * on 2018/5/10
 */
public class CancelOrderRequest  extends  BaseRequest{
    private String shopID;
    private String orderID;

    public String getShopID() {
        return shopID;
    }

    public void setShopID(String shopID) {
        this.shopID = shopID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }
}
