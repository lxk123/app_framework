package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by LuoXueKun
 * on 2018/5/9
 */
public class GetCustomOrderInfoRequest  extends  BaseRequest{
    private String shopID;

    public String getShopID() {
        return shopID;
    }

    public void setShopID(String shopID) {
        this.shopID = shopID;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    private String orderID;
}
