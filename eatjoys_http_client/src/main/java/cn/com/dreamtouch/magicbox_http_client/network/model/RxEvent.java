package cn.com.dreamtouch.magicbox_http_client.network.model;

import retrofit2.http.PUT;

/**
 * Created by LuoXueKun
 * on 2018/5/16
 */
public class RxEvent {
    private int flag;
    private int data;

    public RxEvent(int flag, int data) {
        this.flag = flag;
        this.data = data;
    }



    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
