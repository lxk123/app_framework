package cn.com.dreamtouch.magicbox_http_client.network.model;

import android.annotation.SuppressLint;
import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */
public class GetAllTableResponse extends BaseResponse{

    private List<Data> data;

    public List<Data> getData() {
        return data;
    }

    public void setData(List<Data> data) {
        this.data = data;
    }

    public static class Data implements Parcelable{
        /**
         * areaID : 4348
         * areaName : 大堂
         * list : [{"propleNum":1,"tableID":39319,"tableName":"新1号桌","status":2},{"propleNum":6,"orderMoney":202,"orderID":3101188,"dinningPeople":1,"tableID":44162,"tableName":"新6号桌","status":2},{"propleNum":8,"tableID":44164,"tableName":"新8号桌","status":1},{"propleNum":9,"orderMoney":47,"orderID":3102125,"dinningPeople":1,"tableID":45525,"tableName":"新9号桌","status":2},{"propleNum":10,"orderMoney":1,"orderID":3101995,"dinningPeople":1,"tableID":46004,"tableName":"新10号桌","status":2}]
         */

        private int areaID;
        private String areaName;
        private List<ListBean> list;

        protected Data(Parcel in) {
            areaID = in.readInt();
            areaName = in.readString();
        }

        public static final Creator<Data> CREATOR = new Creator<Data>() {
            @Override
            public Data createFromParcel(Parcel in) {
                return new Data(in);
            }

            @Override
            public Data[] newArray(int size) {
                return new Data[size];
            }
        };

        public int getAreaID() {
            return areaID;
        }

        public void setAreaID(int areaID) {
            this.areaID = areaID;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public List<ListBean> getList() {
            return list;
        }

        public void setList(List<ListBean> list) {
            this.list = list;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(areaID);
            dest.writeString(areaName);
        }

        public static class ListBean {
            @Override
            public String toString() {
                return "ListBean{" +
                        "propleNum=" + propleNum +
                        ", tableID=" + tableID +
                        ", tableName='" + tableName + '\'' +
                        ", status=" + status +
                        ", orderMoney=" + orderMoney +
                        ", orderID=" + orderID +
                        ", dinningPeople=" + dinningPeople +
                        '}';
            }

            /**
             * propleNum : 1
             * tableID : 39319
             * tableName : 新1号桌
             * status : 2
             * orderMoney : 202.0
             * orderID : 3101188
             * dinningPeople : 1
             */

            private int propleNum;
            private int tableID;
            private String tableName;
            private int status;
            private double orderMoney;
            private double tableFee;

            public double getTableFee() {
                return tableFee;
            }

            public void setTableFee(double tableFee) {
                this.tableFee = tableFee;
            }

            private int orderID;
            private int dinningPeople;


            public int getPropleNum() {
                return propleNum;
            }

            public void setPropleNum(int propleNum) {
                this.propleNum = propleNum;
            }

            public int getTableID() {
                return tableID;
            }

            public void setTableID(int tableID) {
                this.tableID = tableID;
            }

            public String getTableName() {
                return tableName;
            }

            public void setTableName(String tableName) {
                this.tableName = tableName;
            }

            public int getStatus() {
                return status;
            }

            public void setStatus(int status) {
                this.status = status;
            }

            public double getOrderMoney() {
                return orderMoney;
            }

            public void setOrderMoney(double orderMoney) {
                this.orderMoney = orderMoney;
            }

            public int getOrderID() {
                return orderID;
            }

            public void setOrderID(int orderID) {
                this.orderID = orderID;
            }

            public int getDinningPeople() {
                return dinningPeople;
            }

            public void setDinningPeople(int dinningPeople) {
                this.dinningPeople = dinningPeople;
            }
        }
    }
}
