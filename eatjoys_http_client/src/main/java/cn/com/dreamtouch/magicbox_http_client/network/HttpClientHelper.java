package cn.com.dreamtouch.magicbox_http_client.network;


import android.app.WallpaperInfo;
import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import cn.com.dreamtouch.magicbox_http_client.network.model.CancelOrderRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.CancelOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangePasswordRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangePasswordResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangeTableRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChangeTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ChearTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ClearTableRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllPayMethodResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetAllTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetCustomOrderInfoResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetDishDetailRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetDishDetailResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetMessageRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetMessageResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetOrderListResopnse;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetOrderListResquest;
import cn.com.dreamtouch.magicbox_http_client.network.model.GetShopDishResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.IsNeedTableFeeRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.IsNeedTableFeeResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.LoginRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.LoginResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.OpenTableRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.OpenTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayOrderRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.PayOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PostOrderRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.PostOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PrintOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushIsOnOffRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushIsOnOffResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushMessageRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.PushMessageResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.QrCodeTableRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.QrCodeTableResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.ReprintOrderRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.RetreatOrderRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.RetreatOrderResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.SendVaildateCodeRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.SendVaildateCodeResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.UpdateDinnerPeopleRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.UpdateDinnerPeopleResponse;
import cn.com.dreamtouch.magicbox_http_client.network.model.WaitConfirmRequest;
import cn.com.dreamtouch.magicbox_http_client.network.model.WaitConfirmResponse;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

/**
 * Created by LuoXueKun on 17/2/20.
 */

public class HttpClientHelper {
    private IMainRestService service;
    private static String mSid;
    private static HttpClientHelper httpClientHelper;

    public HttpClientHelper(String apiUrl) {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient mOkHttpClient = (new OkHttpClient.Builder().addNetworkInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                HttpUrl url;
                HttpUrl.Builder builder = chain.request().url().newBuilder();
                if (mSid != null) {
                    url = builder
                            .addQueryParameter("sid", mSid)
                            .addQueryParameter("t", System.currentTimeMillis()+"")
                            .build();
                } else {
                    url = builder.build();
                }

                Request request = chain.request().newBuilder()

                        .addHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8")
                        .addHeader("Connection", "keep-alive")
                        .addHeader("Accept-Language", Locale.getDefault().getLanguage())//获取系统语言
                        .method(chain.request().method(), chain.request().body())
                        .url(url)
                        .build();
                return chain.proceed(request);
            }
        })).writeTimeout(10L, TimeUnit.SECONDS).readTimeout(10L, TimeUnit.SECONDS)

                .addInterceptor(logging).build();
        if(TextUtils.isEmpty(apiUrl)){
            return;
        }
        Retrofit retrofit = (new retrofit2.Retrofit.Builder()).baseUrl(apiUrl)

                .addConverterFactory(GsonConverterFactory.create()).
                        addCallAdapterFactory(RxJavaCallAdapterFactory.create()).
                        client(mOkHttpClient).build();
        this.service = retrofit.create(IMainRestService.class);
    }

    public static HttpClientHelper getInstance(String apiUrl) {
        if (httpClientHelper != null)

            return httpClientHelper;
        else {
            httpClientHelper = new HttpClientHelper(apiUrl);
            return httpClientHelper;
        }
    }

    public static HttpClientHelper getInstance(String apiUrl, String sid) {
        if (httpClientHelper != null) {
            mSid = sid;
            return httpClientHelper;
        } else {
            httpClientHelper = new HttpClientHelper(apiUrl);
            mSid = sid;
            return httpClientHelper;
        }
    }


    public Observable<LoginResponse> login(LoginRequest request) {

        Observable<LoginResponse> call1 = service.login(request.getUsername(), request.getPassword());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*获取区域和桌台信息*/
    public Observable<GetAllTableResponse> getAllTable(GetAllTableRequest request) {

        Observable<GetAllTableResponse> call1 = service.getAllTable(request.getSid(),
                request.getShopID(),request.getUpdateFlag());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*开台*/
    public Observable<OpenTableResponse> openTable(OpenTableRequest request) {

        Observable<OpenTableResponse> call1 = service.openTable(request.getTableID(),request.getShopID(), request.getPeopleNumber(), request.getRemark());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*转台*/
    public Observable<ChangeTableResponse> changeTable(ChangeTableRequest request) {

        Observable<ChangeTableResponse> call1 = service.changeTable(request.getSid(), request.getOldTableID(),
                request.getTableID(), request.getShopID(), request.getOrderID());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*清台*/
    public Observable<ChearTableResponse> cleanTable(ClearTableRequest request) {

        Observable<ChearTableResponse> call1 = service.cleanTable(request.getTableID(), request.getShopID());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*获取商品规格*/
    public Observable<GetDishDetailResponse> getDishDetail(GetDishDetailRequest request) {

        Observable<GetDishDetailResponse> call1 = service.getDishDetail(request.getShopDishID(), request.getShopID());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*点菜单*/
    public Observable<GetCustomOrderInfoResponse> getCustomOrderInfo(GetCustomOrderInfoRequest request) {

        Observable<GetCustomOrderInfoResponse> call1 = service.getCustomOrderInfo(request.getShopID(), request.getOrderID(),"1");
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }
    /*取消订单*/
    public Observable<CancelOrderResponse> cancelOrder(CancelOrderRequest request) {

        Observable<CancelOrderResponse> call1 = service.cancelOrder(request.getShopID(), request.getOrderID());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*通过二维码获取桌台信息*/
    public Observable<QrCodeTableResponse> qrCodeTable(QrCodeTableRequest request) {

        Observable<QrCodeTableResponse> call1 = service.qrCodeTable(request.getShopID(), request.getQrcode());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*获取订单信息*/
    public Observable<GetOrderListResopnse> getOrderList(GetOrderListResquest request) {

        Observable<GetOrderListResopnse> call1 = service.getOrderList(request.getDate(), request.getShopID()
                , request.getPageSize(), request.getPageIndex());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*更新用餐人数*/
    public Observable<UpdateDinnerPeopleResponse> updateDinnerPeople(UpdateDinnerPeopleRequest request) {

        Observable<UpdateDinnerPeopleResponse> call1 = service.updateDinnerPeople(request.getShopID(),request.getTableID(),
                request.getOrderID(),request.getPeopleNum());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }
    /*更改密码*/
    public Observable<ChangePasswordResponse> changePassword(ChangePasswordRequest request) {

        Observable<ChangePasswordResponse> call1 = service.changePassword(request.getMobile(),request.getPwd(),request.getCode());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }
    /*服务员确认拒绝*/
    public Observable<WaitConfirmResponse> waiterConfirmOrder(WaitConfirmRequest request) {

        Observable<WaitConfirmResponse> call1 = service.waiterConfirmOrder(request.getShopID(),request.getOrderID()
                  ,request.getConfirm(),request.getRemark());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }
    /*获取验证码*/
    public Observable<SendVaildateCodeResponse> sendValidateCode(SendVaildateCodeRequest request) {

        Observable<SendVaildateCodeResponse> call1 = service.sendValidateCode(request.getMobile());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }
    /*叫号取餐*/
    public Observable<PushMessageResponse> pushMessage(PushMessageRequest request) {

        Observable<PushMessageResponse> call1 = service.pushMessage(request.getShopSerialNumber(),request.getShopID());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }
    /*检查服务员权限*/
    public Observable<PushIsOnOffResponse> pullIsOnOff(PushIsOnOffRequest request) {

        Observable<PushIsOnOffResponse> call1 = service.pullIsOnOff(request.getEmployeeID());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }
    /*获取桌台费用*/
    public Observable<IsNeedTableFeeResponse> isNeedTableFee(IsNeedTableFeeRequest request) {

        Observable<IsNeedTableFeeResponse> call1 = service.isNeedTableFee(request.getTableID(),request.getShopID());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }
    /*打印小票*/
    public Observable<PrintOrderResponse> reprintOrder(ReprintOrderRequest request) {

        Observable<PrintOrderResponse> call1 = service.reprintOrder(request.getOrderID(),request.getShopID());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*获取商品列表*/
    public Observable<GetShopDishResponse> getShopDish() {

        Observable<GetShopDishResponse> call1 = service.getShopDish();
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }
    /*获取消息列表*/
    public Observable<GetMessageResponse> getMessage(GetMessageRequest getMessageRequest) {

        Observable<GetMessageResponse> call1 = service.getMessage(getMessageRequest.getShopID(),getMessageRequest.getPage());
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }
    /*获取支付方式*/
    public Observable<GetAllPayMethodResponse> getAllPayMethod() {

        Observable<GetAllPayMethodResponse> call1 = service.getAllPayMethod();
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*下单*/
    public Observable<PostOrderResponse> postOrder(PostOrderRequest request) {

        String toJson = new Gson().toJson(request.getOrderParam());
        Observable<PostOrderResponse> call1 = service.postOrder(toJson);
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*结账支付*/
    public Observable<PayOrderResponse> payOrder(PayOrderRequest request) {

        String toJson = new Gson().toJson(request.getPayOrderStrBody());
        Observable<PayOrderResponse> call1 = service.payOrder(toJson);
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

    /*退菜*/
    public Observable<RetreatOrderResponse> retreatOrder(RetreatOrderRequest request) {

        String toJson = new Gson().toJson(request.getOrderStrBody());
        Observable<RetreatOrderResponse> call1 = service.retreatOrder(toJson);
        MagicBoxObservableCallHelper call2 = new MagicBoxObservableCallHelper();
        return call2.map(call1);
    }

}
