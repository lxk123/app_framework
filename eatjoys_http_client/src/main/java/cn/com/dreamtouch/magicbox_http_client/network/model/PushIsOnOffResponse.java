package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by LuoXueKun
 * on 2018/5/10
 */
public class PushIsOnOffResponse extends BaseResponse {

    /**
     * data : {"fwyXD":"1","fwyTC":"1","fwyXJSK":"1"}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * fwyXD : 1
         * fwyTC : 1
         * fwyXJSK : 1
         */

        private String fwyXD;
        private String fwyTC;
        private String fwyXJSK;

        public String getFwyXD() {
            return fwyXD;
        }

        public void setFwyXD(String fwyXD) {
            this.fwyXD = fwyXD;
        }

        public String getFwyTC() {
            return fwyTC;
        }

        public void setFwyTC(String fwyTC) {
            this.fwyTC = fwyTC;
        }

        public String getFwyXJSK() {
            return fwyXJSK;
        }

        public void setFwyXJSK(String fwyXJSK) {
            this.fwyXJSK = fwyXJSK;
        }
    }
}
