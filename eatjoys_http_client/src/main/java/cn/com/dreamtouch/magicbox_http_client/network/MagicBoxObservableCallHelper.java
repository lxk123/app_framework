package cn.com.dreamtouch.magicbox_http_client.network;

import android.text.TextUtils;

import cn.com.dreamtouch.magicbox_common.model.MagicBoxResponseException;
import cn.com.dreamtouch.magicbox_http_client.network.model.BaseResponse;
import rx.Observable;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by MuHan on 17/2/20.
 */

public class MagicBoxObservableCallHelper<T extends BaseResponse> {


    public Observable<T > map(Observable<T> call) {
        return call.subscribeOn(Schedulers.newThread()).observeOn(Schedulers.io()).flatMap(new Func1<T, Observable<T>>() {
            @Override
            public Observable<T> call(T baseResponse) {
                if(baseResponse.getCode() == 0
                       /* || TextUtils.equals(baseResponse.getStatus(),"-1")*/ )//先判断 success
                {
                    MagicBoxResponseException.MsgModel result = new MagicBoxResponseException.MsgModel();
                    result.error = baseResponse.getMessage();
                    result.prompt = baseResponse.getMessage();
                    return Observable.error(new MagicBoxResponseException(baseResponse.getCode()+"",result));
                }
                else return Observable.just(baseResponse);
            }
        });
    }
}
