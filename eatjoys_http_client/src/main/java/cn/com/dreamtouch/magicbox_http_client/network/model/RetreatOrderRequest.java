package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by LuoXueKun
 * on 2018/5/9
 */
public class RetreatOrderRequest extends  BaseRequest {
    private RefundDishes orderStrBody;

    public RefundDishes getOrderStrBody() {
        return orderStrBody;
    }

    public void setOrderStrBody(RefundDishes orderStrBody) {
        this.orderStrBody = orderStrBody;
    }
}
