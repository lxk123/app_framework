package cn.com.dreamtouch.magicbox_http_client.network.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsy on 2018/4/19.
 */

public class DishItemBean implements Cloneable {
    private int cartNum;
    private String dishTypeID;
    private String dishUnitName;
    private String dishTsateName;
    private String dishUnitID;
    private String dishTasteID;
    private String dishID;
    private String dishName;
    private int sortNumber;
    private double basePrice;
    private double currentPrice;
    private int limitNumber;
    private int totalSaleNumber;
    private String code;
    private boolean IsSet;
    private boolean IsHaveUnit;
    private boolean IsHaveTaste;
    private boolean IsMarketPriceDish;
    private double price;
    private String isMustOrder;//必点菜 1 必点
    private double boxFee;//餐盒费
    private String originCost;//原价
    private String measureUnitName;//原价
    private DishUnitBean dishUnit = new DishUnitBean();
    private DishTasteBean dishTaste = new DishTasteBean();
    private String feedDesc;//加料描述
    private String comboDesc;//套餐描述
    private int isMustFeeding;//必选加料数量
    private List<DishTasteBean> feedList = new ArrayList<>();//加料
    private List<DishSetDetail> comboList;

    @Override
    public Object clone() {
        DishItemBean bean = null;
        try {
            bean = (DishItemBean) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return bean;
    }

    public String getComboDesc() {
        return comboDesc;
    }

    public void setComboDesc(String comboDesc) {
        this.comboDesc = comboDesc;
    }

    public List<DishSetDetail> getComboList() {
        return comboList;
    }

    public void setComboList(List<DishSetDetail> comboList) {
        this.comboList = comboList;
    }

    public String getFeedDesc() {
        return feedDesc;
    }

    public void setFeedDesc(String feedDesc) {
        this.feedDesc = feedDesc;
    }

    public List<DishTasteBean> getFeedList() {
        return feedList;
    }

    public void setFeedList(List<DishTasteBean> feedList) {
        this.feedList = feedList;
    }

    public DishUnitBean getDishUnit() {
        return dishUnit;
    }

    public void setDishUnit(DishUnitBean dishUnit) {
        this.dishUnit = dishUnit;
    }

    public DishTasteBean getDishTaste() {
        return dishTaste;
    }

    public void setDishTaste(DishTasteBean dishTaste) {
        this.dishTaste = dishTaste;
    }

    public String getMeasureUnitName() {
        return measureUnitName;
    }

    public void setMeasureUnitName(String measureUnitName) {
        this.measureUnitName = measureUnitName;
    }

    public String getOriginCost() {
        return originCost;
    }

    public void setOriginCost(String originCost) {
        this.originCost = originCost;
    }

    public double getBoxFee() {
        return boxFee;
    }

    public void setBoxFee(double boxFee) {
        this.boxFee = boxFee;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getIsMustOrder() {
        return isMustOrder;
    }

    public void setIsMustOrder(String isMustOrder) {
        this.isMustOrder = isMustOrder;
    }

    public boolean getIsMarketPriceDish() {
        return IsMarketPriceDish;
    }

    public void setIsMarketPriceDish(boolean marketPriceDish) {
        IsMarketPriceDish = marketPriceDish;
    }

    public DishItemBean() {
    }

    @Override
    public String toString() {
        return "DishItemBean{" +
                "cartNum=" + cartNum +
                ", dishUnitName='" + dishUnitName + '\'' +
                ", dishTsateName='" + dishTsateName + '\'' +
                ", dishUnitID='" + dishUnitID + '\'' +
                ", dishTasteID='" + dishTasteID + '\'' +
                ", dishID='" + dishID + '\'' +
                ", dishName='" + dishName + '\'' +
                ", IsHaveUnit=" + IsHaveUnit +
                ", IsHaveTaste=" + IsHaveTaste +
                ", IsMarketPriceDish=" + IsMarketPriceDish +
                ", price=" + price +
                ", isMustOrder='" + isMustOrder + '\'' +
                ", boxFee=" + boxFee +
                ", originCost='" + originCost + '\'' +
                ", feedList=" + feedList +
                '}';
    }

    /**
     * @param o Object
     * @return true:dishId一致即相同
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DishItemBean)) return false;
        DishItemBean dishInfo = (DishItemBean) o;
        return dishID.equals(dishInfo.dishID);
    }

    public String getDishTypeID() {
        return dishTypeID;
    }

    public void setDishTypeID(String dishTypeID) {
        this.dishTypeID = dishTypeID;
    }

    public String getDishUnitName() {
        return dishUnitName;
    }

    public void setDishUnitName(String dishUnitName) {
        this.dishUnitName = dishUnitName;
    }

    public String getDishTsateName() {
        return dishTsateName;
    }

    public void setDishTsateName(String dishTsateName) {
        this.dishTsateName = dishTsateName;
    }

    public String getDishUnitID() {
        return dishUnitID;
    }

    public void setDishUnitID(String dishUnitID) {
        this.dishUnitID = dishUnitID;
    }

    public String getDishTasteID() {
        return dishTasteID;
    }

    public void setDishTasteID(String dishTasteID) {
        this.dishTasteID = dishTasteID;
    }

    public boolean isSet() {
        return IsSet;
    }

    public void setSet(boolean set) {
        IsSet = set;
    }

    public boolean isHaveUnit() {
        return IsHaveUnit;
    }

    public void setHaveUnit(boolean haveUnit) {
        IsHaveUnit = haveUnit;
    }

    public boolean isHaveTaste() {
        return IsHaveTaste;
    }

    public void setHaveTaste(boolean haveTaste) {
        IsHaveTaste = haveTaste;
    }

    public int getCartNum() {
        return cartNum;
    }

    public void setCartNum(int cartNum) {
        this.cartNum = cartNum;
    }


    public boolean getIsHaveTaste() {
        return IsHaveTaste;
    }

    public void setIsHaveTaste(boolean haveTaste) {
        IsHaveTaste = haveTaste;
    }


    public boolean getIsSet() {
        return IsSet;
    }

    public void setIsSet(boolean set) {
        IsSet = set;
    }

    public boolean getIsHaveUnit() {
        return IsHaveUnit;
    }

    public void setIsHaveUnit(boolean haveUnit) {
        IsHaveUnit = haveUnit;
    }

    public String getDishID() {
        return dishID;
    }

    public void setDishID(String dishID) {
        this.dishID = dishID;
    }

    public String getDishName() {
        return dishName;
    }

    public void setDishName(String dishName) {
        this.dishName = dishName;
    }

    public int getSortNumber() {
        return sortNumber;
    }

    public void setSortNumber(int sortNumber) {
        this.sortNumber = sortNumber;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public int getLimitNumber() {
        return limitNumber;
    }

    public void setLimitNumber(int limitNumber) {
        this.limitNumber = limitNumber;
    }

    public int getTotalSaleNumber() {
        return totalSaleNumber;
    }

    public void setTotalSaleNumber(int totalSaleNumber) {
        this.totalSaleNumber = totalSaleNumber;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getIsMustFeeding() {
        return isMustFeeding;
    }

    public void setIsMustFeeding(int isMustFeeding) {
        this.isMustFeeding = isMustFeeding;
    }
}
