package cn.com.dreamtouch.magicbox_http_client.network.model;

import java.util.List;

/**
 * Created by LuoXueKun
 * on 2018/5/9
 */
public class GetMessageResponse extends BaseResponse{
    public List<MessageData> data;

    public class  MessageData{
        public String getMessageID() {
            return messageID;
        }

        public void setMessageID(String messageID) {
            this.messageID = messageID;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getShopID() {
            return shopID;
        }

        public void setShopID(String shopID) {
            this.shopID = shopID;
        }

        public String getMessageType() {
            return messageType;
        }

        public void setMessageType(String messageType) {
            this.messageType = messageType;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getPublishDate() {
            return publishDate;
        }

        public void setPublishDate(String publishDate) {
            this.publishDate = publishDate;
        }

        public String getIsDelete() {
            return isDelete;
        }

        public void setIsDelete(String isDelete) {
            this.isDelete = isDelete;
        }

        public String getCreateUserID() {
            return createUserID;
        }

        public void setCreateUserID(String createUserID) {
            this.createUserID = createUserID;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public String getCreateDate() {
            return createDate;
        }

        public void setCreateDate(String createDate) {
            this.createDate = createDate;
        }

        public String getModifyUserID() {
            return modifyUserID;
        }

        public void setModifyUserID(String modifyUserID) {
            this.modifyUserID = modifyUserID;
        }

        public String getModifyUserName() {
            return modifyUserName;
        }

        public void setModifyUserName(String modifyUserName) {
            this.modifyUserName = modifyUserName;
        }

        public String getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(String modifyDate) {
            this.modifyDate = modifyDate;
        }

        private  String  messageID;
        private  String  title;
        private  String  shopID;
        private  String  messageType;
        private  String  content;
        private  String  publishDate;
        private  String  isDelete;
        private  String  createUserID;
        private  String  createUserName;
        private  String  createDate;
        private  String  modifyUserID;
        private  String  modifyUserName;
        private  String  modifyDate;
    }
}
