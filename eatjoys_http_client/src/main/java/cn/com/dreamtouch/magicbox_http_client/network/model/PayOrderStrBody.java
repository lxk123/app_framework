package cn.com.dreamtouch.magicbox_http_client.network.model;

import java.util.List;

/**
 * Created by LuoXueKun
 * on 2018/5/9
 */
public class PayOrderStrBody {

    /**
     * money : 194.1
     * orderID : 3107678
     * paymentList : [{"money":194.1,"paymentType":"CASHPAY","rebate":1}]
     * shopID : 4540
     * tableFee : 0
     * tableID : 44164
     * voucherAmount : 0
     * voucherCode : NONE
     */

    private double money;
    private int orderID;
    private int shopID;
    private String tableFee;
    private int tableID;
    private int voucherAmount;
    private String voucherCode;
    private List<PaymentListBean> paymentList;

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getShopID() {
        return shopID;
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }

    public String getTableFee() {
        return tableFee;
    }

    public void setTableFee(String tableFee) {
        this.tableFee = tableFee;
    }

    public int getTableID() {
        return tableID;
    }

    public void setTableID(int tableID) {
        this.tableID = tableID;
    }

    public int getVoucherAmount() {
        return voucherAmount;
    }

    public void setVoucherAmount(int voucherAmount) {
        this.voucherAmount = voucherAmount;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public List<PaymentListBean> getPaymentList() {
        return paymentList;
    }

    public void setPaymentList(List<PaymentListBean> paymentList) {
        this.paymentList = paymentList;
    }

    public static class PaymentListBean {
        /**
         * money : 194.1
         * paymentType : CASHPAY
         * rebate : 1
         */

        private double money;
        private String paymentType;
        private int rebate;

        public double getMoney() {
            return money;
        }

        public void setMoney(double money) {
            this.money = money;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public int getRebate() {
            return rebate;
        }

        public void setRebate(int rebate) {
            this.rebate = rebate;
        }
    }
}
