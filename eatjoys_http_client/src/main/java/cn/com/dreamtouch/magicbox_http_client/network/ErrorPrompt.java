package cn.com.dreamtouch.magicbox_http_client.network;

/**
 * 错误友好提示语管理
 * Created by MuHan on 17/2/20.
 */

public class ErrorPrompt {
    private static final String UNKNOWN_FAIL = "未知错误";

    public static String getPrompt(String error) {
        switch (error) {
            case "invalid_request":
                return UNKNOWN_FAIL;
        }
        return error;
    }
}
