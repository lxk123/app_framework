package cn.com.dreamtouch.magicbox_http_client.network.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */
public class GetDishDetailResponse extends BaseResponse {
    /**
     * data : {"dishID":"292885","shopDishID":"292885","shopID":4540,"shopName":"28金钱豹","dishTypeID":"26912","dishTypeName":"周五热销菜","sortNumber":0,"dishName":"新服务测试菜","measureUnitName":"份","orderUnitName":"份","price":0.02,"makeTime":0,"isRebate":1,"isSet":0,"isWeight":0,"isUse":1,"isHaveUnit":1,"isHaveTaste":1,"isRecommend":0,"limitNumber":999967,"saleNumber":11,"totalSaleNumber":0,"isNeedUpdate":1,"isMustOrder":0,"isMarketPriceDish":0,"code":"XFWCSC","description":"","originCost":20,"relationPersonNumber":false,"imageList":[],"dishUnitList":[],"dishTasteList":[{"dishTasteID":"206277","shopID":4540,"dishID":292885,"tasteName":"糖","risePrice":1,"type":4},{"dishTasteID":"206278","shopID":4540,"dishID":292885,"tasteName":"糖","risePrice":1,"type":4},{"dishTasteID":"207554","shopID":4540,"dishID":292885,"tasteName":"l","risePrice":1,"type":4},{"dishTasteID":"207557","shopID":4540,"dishID":292885,"tasteName":"阳光","risePrice":1,"type":4},{"dishTasteID":"207558","shopID":4540,"dishID":292885,"tasteName":"阳光","risePrice":1,"type":4},{"dishTasteID":"207559","shopID":4540,"dishID":292885,"tasteName":"阳光","risePrice":1,"type":4},{"dishTasteID":"207766","shopID":4540,"dishID":292885,"tasteName":"微辣","risePrice":0,"type":1},{"dishTasteID":"207767","shopID":4540,"dishID":292885,"tasteName":"中辣","risePrice":0,"type":1},{"dishTasteID":"207768","shopID":4540,"dishID":292885,"tasteName":"重辣","risePrice":0,"type":1}],"mustNumber":0,"isInnerDish":0,"isMustFeeding":0,"recommendOrder":9999,"boxFee":2,"need":false,"mustNeed":false}
     * success : true
     */

    private DataBean data;
    private boolean success;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class DataBean {
        /**
         * dishID : 292885
         * shopDishID : 292885
         * shopID : 4540
         * shopName : 28金钱豹
         * dishTypeID : 26912
         * dishTypeName : 周五热销菜
         * sortNumber : 0
         * dishName : 新服务测试菜
         * measureUnitName : 份
         * orderUnitName : 份
         * price : 0.02
         * makeTime : 0
         * isRebate : 1
         * isSet : 0
         * isWeight : 0
         * isUse : 1
         * isHaveUnit : 1
         * isHaveTaste : 1
         * isRecommend : 0
         * limitNumber : 999967
         * saleNumber : 11.0
         * totalSaleNumber : 0.0
         * isNeedUpdate : 1
         * isMustOrder : 0
         * isMarketPriceDish : 0
         * code : XFWCSC
         * description :
         * originCost : 20.0
         * relationPersonNumber : false
         * imageList : []
         * dishUnitList : []
         * dishTasteList : [{"dishTasteID":"206277","shopID":4540,"dishID":292885,"tasteName":"糖","risePrice":1,"type":4},{"dishTasteID":"206278","shopID":4540,"dishID":292885,"tasteName":"糖","risePrice":1,"type":4},{"dishTasteID":"207554","shopID":4540,"dishID":292885,"tasteName":"l","risePrice":1,"type":4},{"dishTasteID":"207557","shopID":4540,"dishID":292885,"tasteName":"阳光","risePrice":1,"type":4},{"dishTasteID":"207558","shopID":4540,"dishID":292885,"tasteName":"阳光","risePrice":1,"type":4},{"dishTasteID":"207559","shopID":4540,"dishID":292885,"tasteName":"阳光","risePrice":1,"type":4},{"dishTasteID":"207766","shopID":4540,"dishID":292885,"tasteName":"微辣","risePrice":0,"type":1},{"dishTasteID":"207767","shopID":4540,"dishID":292885,"tasteName":"中辣","risePrice":0,"type":1},{"dishTasteID":"207768","shopID":4540,"dishID":292885,"tasteName":"重辣","risePrice":0,"type":1}]
         * mustNumber : 0
         * isInnerDish : 0
         * isMustFeeding : 0
         * recommendOrder : 9999
         * boxFee : 2.0
         * need : false
         * mustNeed : false
         */

        private String dishID;
        private String shopDishID;
        private int shopID;
        private String shopName;
        private String dishTypeID;
        private String dishTypeName;
        private int sortNumber;
        private String dishName;
        private String measureUnitName;
        private String orderUnitName;
        private double price;
        private int makeTime;
        private int isRebate;
        private int isSet;
        private int isWeight;
        private int isUse;
        private int isHaveUnit;
        private int isHaveTaste;
        private int isRecommend;
        private int limitNumber;
        private double saleNumber;
        private double totalSaleNumber;
        private int isNeedUpdate;
        private int isMustOrder;
        private int isMarketPriceDish;
        @SerializedName("code")
        private String codeX;
        private String description;
        private double originCost;
        private boolean relationPersonNumber;
        private int mustNumber;
        private int isInnerDish;
        private int isMustFeeding;
        private int recommendOrder;
        private double boxFee;
        private boolean need;
        private boolean mustNeed;
        private List<?> imageList;
        private List<DishUnitBean> dishUnitList;
        private List<DishTasteBean> dishTasteList;

        public String getDishID() {
            return dishID;
        }

        public void setDishID(String dishID) {
            this.dishID = dishID;
        }

        public String getShopDishID() {
            return shopDishID;
        }

        public void setShopDishID(String shopDishID) {
            this.shopDishID = shopDishID;
        }

        public int getShopID() {
            return shopID;
        }

        public void setShopID(int shopID) {
            this.shopID = shopID;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public String getDishTypeID() {
            return dishTypeID;
        }

        public void setDishTypeID(String dishTypeID) {
            this.dishTypeID = dishTypeID;
        }

        public String getDishTypeName() {
            return dishTypeName;
        }

        public void setDishTypeName(String dishTypeName) {
            this.dishTypeName = dishTypeName;
        }

        public int getSortNumber() {
            return sortNumber;
        }

        public void setSortNumber(int sortNumber) {
            this.sortNumber = sortNumber;
        }

        public String getDishName() {
            return dishName;
        }

        public void setDishName(String dishName) {
            this.dishName = dishName;
        }

        public String getMeasureUnitName() {
            return measureUnitName;
        }

        public void setMeasureUnitName(String measureUnitName) {
            this.measureUnitName = measureUnitName;
        }

        public String getOrderUnitName() {
            return orderUnitName;
        }

        public void setOrderUnitName(String orderUnitName) {
            this.orderUnitName = orderUnitName;
        }

        public double getPrice() {
            return price;
        }

        public void setPrice(double price) {
            this.price = price;
        }

        public int getMakeTime() {
            return makeTime;
        }

        public void setMakeTime(int makeTime) {
            this.makeTime = makeTime;
        }

        public int getIsRebate() {
            return isRebate;
        }

        public void setIsRebate(int isRebate) {
            this.isRebate = isRebate;
        }

        public int getIsSet() {
            return isSet;
        }

        public void setIsSet(int isSet) {
            this.isSet = isSet;
        }

        public int getIsWeight() {
            return isWeight;
        }

        public void setIsWeight(int isWeight) {
            this.isWeight = isWeight;
        }

        public int getIsUse() {
            return isUse;
        }

        public void setIsUse(int isUse) {
            this.isUse = isUse;
        }

        public int getIsHaveUnit() {
            return isHaveUnit;
        }

        public void setIsHaveUnit(int isHaveUnit) {
            this.isHaveUnit = isHaveUnit;
        }

        public int getIsHaveTaste() {
            return isHaveTaste;
        }

        public void setIsHaveTaste(int isHaveTaste) {
            this.isHaveTaste = isHaveTaste;
        }

        public int getIsRecommend() {
            return isRecommend;
        }

        public void setIsRecommend(int isRecommend) {
            this.isRecommend = isRecommend;
        }

        public int getLimitNumber() {
            return limitNumber;
        }

        public void setLimitNumber(int limitNumber) {
            this.limitNumber = limitNumber;
        }

        public double getSaleNumber() {
            return saleNumber;
        }

        public void setSaleNumber(double saleNumber) {
            this.saleNumber = saleNumber;
        }

        public double getTotalSaleNumber() {
            return totalSaleNumber;
        }

        public void setTotalSaleNumber(double totalSaleNumber) {
            this.totalSaleNumber = totalSaleNumber;
        }

        public int getIsNeedUpdate() {
            return isNeedUpdate;
        }

        public void setIsNeedUpdate(int isNeedUpdate) {
            this.isNeedUpdate = isNeedUpdate;
        }

        public int getIsMustOrder() {
            return isMustOrder;
        }

        public void setIsMustOrder(int isMustOrder) {
            this.isMustOrder = isMustOrder;
        }

        public int getIsMarketPriceDish() {
            return isMarketPriceDish;
        }

        public void setIsMarketPriceDish(int isMarketPriceDish) {
            this.isMarketPriceDish = isMarketPriceDish;
        }

        public String getCodeX() {
            return codeX;
        }

        public void setCodeX(String codeX) {
            this.codeX = codeX;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public double getOriginCost() {
            return originCost;
        }

        public void setOriginCost(double originCost) {
            this.originCost = originCost;
        }

        public boolean isRelationPersonNumber() {
            return relationPersonNumber;
        }

        public void setRelationPersonNumber(boolean relationPersonNumber) {
            this.relationPersonNumber = relationPersonNumber;
        }

        public int getMustNumber() {
            return mustNumber;
        }

        public void setMustNumber(int mustNumber) {
            this.mustNumber = mustNumber;
        }

        public int getIsInnerDish() {
            return isInnerDish;
        }

        public void setIsInnerDish(int isInnerDish) {
            this.isInnerDish = isInnerDish;
        }

        public int getIsMustFeeding() {
            return isMustFeeding;
        }

        public void setIsMustFeeding(int isMustFeeding) {
            this.isMustFeeding = isMustFeeding;
        }

        public int getRecommendOrder() {
            return recommendOrder;
        }

        public void setRecommendOrder(int recommendOrder) {
            this.recommendOrder = recommendOrder;
        }

        public double getBoxFee() {
            return boxFee;
        }

        public void setBoxFee(double boxFee) {
            this.boxFee = boxFee;
        }

        public boolean isNeed() {
            return need;
        }

        public void setNeed(boolean need) {
            this.need = need;
        }

        public boolean isMustNeed() {
            return mustNeed;
        }

        public void setMustNeed(boolean mustNeed) {
            this.mustNeed = mustNeed;
        }

        public List<?> getImageList() {
            return imageList;
        }

        public void setImageList(List<?> imageList) {
            this.imageList = imageList;
        }

        public List<DishUnitBean> getDishUnitList() {
            return dishUnitList;
        }

        public void setDishUnitList(List<DishUnitBean> dishUnitList) {
            this.dishUnitList = dishUnitList;
        }

        public List<DishTasteBean> getDishTasteList() {
            return dishTasteList;
        }

        public void setDishTasteList(List<DishTasteBean> dishTasteList) {
            this.dishTasteList = dishTasteList;
        }

        private List<DishSetDetail> setDetailList;
        private List<DishSetItem> setItemList;

        public List<DishSetDetail> getSetDetailList() {
            return setDetailList;
        }

        public void setSetDetailList(List<DishSetDetail> setDetailList) {
            this.setDetailList = setDetailList;
        }
        public List<DishSetItem> getSetItemList() {
            return setItemList;
        }

        public void setSetItemList(List<DishSetItem> setItemList) {
            this.setItemList = setItemList;
        }

        public static class DishTasteListBean {
            /**
             * dishTasteID : 206277
             * shopID : 4540
             * dishID : 292885
             * tasteName : 糖
             * risePrice : 1.0
             * type : 4
             */

            private String dishTasteID;
            private int shopID;
            private int dishID;
            private String tasteName;
            private double risePrice;
            private int type;

            public String getDishTasteID() {
                return dishTasteID;
            }

            public void setDishTasteID(String dishTasteID) {
                this.dishTasteID = dishTasteID;
            }

            public int getShopID() {
                return shopID;
            }

            public void setShopID(int shopID) {
                this.shopID = shopID;
            }

            public int getDishID() {
                return dishID;
            }

            public void setDishID(int dishID) {
                this.dishID = dishID;
            }

            public String getTasteName() {
                return tasteName;
            }

            public void setTasteName(String tasteName) {
                this.tasteName = tasteName;
            }

            public double getRisePrice() {
                return risePrice;
            }

            public void setRisePrice(double risePrice) {
                this.risePrice = risePrice;
            }

            public int getType() {
                return type;
            }

            public void setType(int type) {
                this.type = type;
            }
        }
    }


    /**
     * data : {"dishID":"308333","shopDishID":"308333","shopID":4540,"shopName":"28金钱豹","dishTypeID":"29206","dishTypeName":"飞鹅打印","sortNumber":0,"dishName":"豆芽","measureUnitName":"份","price":100,"makeTime":0,"isRebate":1,"isSet":0,"isWeight":0,"isUse":1,"isHaveUnit":0,"isHaveTaste":1,"isRecommend":0,"limitNumber":999851,"saleNumber":148,"totalSaleNumber":0,"defaultImageUrl":"/Attachment/4540/2018-05-03/8cceaeddf73a4c10b477671790b79d30.jpg","isNeedUpdate":1,"isMustOrder":0,"isMarketPriceDish":0,"description":"","relationPersonNumber":false,"imageList":["/Attachment/4540/2018-05-03/8cceaeddf73a4c10b477671790b79d30.jpg"],"dishTasteList":[{"dishTasteID":"210497","shopID":4540,"dishID":308333,"tasteName":"葱","risePrice":1,"type":4},{"dishTasteID":"210498","shopID":4540,"dishID":308333,"tasteName":"姜","risePrice":2,"type":4},{"dishTasteID":"210499","shopID":4540,"dishID":308333,"tasteName":"蒜","risePrice":3,"type":4}],"mustNumber":0,"isInnerDish":0,"isMustFeeding":0,"recommendOrder":9999,"boxFee":0,"need":false,"mustNeed":false}
     * success : true
     */


}
