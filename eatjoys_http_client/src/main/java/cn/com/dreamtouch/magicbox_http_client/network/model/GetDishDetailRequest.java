package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public class GetDishDetailRequest extends BaseRequest{

      private String shopID;
      private String shopDishID;

    public String getShopID() {
        return shopID;
    }

    public void setShopID(String shopID) {
        this.shopID = shopID;
    }

    public String getShopDishID() {
        return shopDishID;
    }

    public void setShopDishID(String shopDishID) {
        this.shopDishID = shopDishID;
    }
}
