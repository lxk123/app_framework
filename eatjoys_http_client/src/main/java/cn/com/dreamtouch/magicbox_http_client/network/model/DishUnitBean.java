package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by lsy on 2018/4/19.
 */

public class DishUnitBean {
    private String dishUnitID;
    private int shopID;
    private int dishID;
    private String unitName;
    private double price;
    private boolean selected;

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getDishUnitID() {
        return dishUnitID;
    }

    public void setDishUnitID(String dishUnitID) {
        this.dishUnitID = dishUnitID;
    }

    public int getShopID() {
        return shopID;
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }

    public int getDishID() {
        return dishID;
    }

    public void setDishID(int dishID) {
        this.dishID = dishID;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
