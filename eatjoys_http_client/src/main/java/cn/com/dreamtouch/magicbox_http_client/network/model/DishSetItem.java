package cn.com.dreamtouch.magicbox_http_client.network.model;

import java.util.List;

/**
 * Created by lsy on 2018/4/26.
 */

public class DishSetItem {
    private String dishSetItemID;
    private String dishSetID;
    private String itemType;
    private String dishTypeID;
    private String dishTypeName;
    private int selectDishCount;
    private List<DishSetDetail> dishSetDetailList;

    public List<DishSetDetail> getDishSetDetailList() {
        return dishSetDetailList;
    }

    public void setDishSetDetailList(List<DishSetDetail> dishSetDetailList) {
        this.dishSetDetailList = dishSetDetailList;
    }

    public int getSelectDishCount() {
        return selectDishCount;
    }

    public void setSelectDishCount(int selectDishCount) {
        this.selectDishCount = selectDishCount;
    }

    public String getDishSetItemID() {
        return dishSetItemID;
    }

    public void setDishSetItemID(String dishSetItemID) {
        this.dishSetItemID = dishSetItemID;
    }

    public String getDishSetID() {
        return dishSetID;
    }

    public void setDishSetID(String dishSetID) {
        this.dishSetID = dishSetID;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getDishTypeID() {
        return dishTypeID;
    }

    public void setDishTypeID(String dishTypeID) {
        this.dishTypeID = dishTypeID;
    }

    public String getDishTypeName() {
        return dishTypeName;
    }

    public void setDishTypeName(String dishTypeName) {
        this.dishTypeName = dishTypeName;
    }
}
