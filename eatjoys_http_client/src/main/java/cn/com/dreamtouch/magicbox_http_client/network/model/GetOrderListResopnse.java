package cn.com.dreamtouch.magicbox_http_client.network.model;

import java.util.List;

/**
 * Created by LuoXueKun
 * on 2018/5/9
 */
public class GetOrderListResopnse extends BaseResponse{


    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * bitmask : null
         * jsonbox : null
         * customOrderID : 3210132
         * customID : -2
         * shopID : 4540
         * shopName : 28金钱豹
         * tableID : 39319
         * tableName : 新1号桌
         * activityID : 3689
         * linkTel : null
         * receiveName : null
         * receiveAddress : null
         * receiveTime : null
         * orderType : 4
         * orderSource : 7
         * orderStatus : 4
         * peopleNumber : 8
         * money : 0.0
         * totalPrice : 27.02
         * tableFee : 0.01
         * ticketAmount : 0.0
         * packageFee : 0.0
         * deliverFee : null
         * discountAmount : 0.0
         * platfomDiscountAmount : 0.0
         * coinType : 0
         * shouldPayAmount : 27.02
         * actualPayAmount : 0.0
         * remark : null
         * isDelete : 0
         * isPackage : 0
         * sendCoupon : null
         * couponID : null
         * createUserID : 18015914821
         * createUserName : 18015914821
         * createDate : 1527517620000
         * modifyUserID : null
         * modifyUserName : null
         * modifyDate : null
         * isExchange : 0
         * exchangeTime : null
         * shopSerialNumber : 26
         * serialNumber : 20180528004540662060
         * voucherCode : null
         * voucherAmount : null
         * rejectReason : null
         */

        private Object bitmask;
        private Object jsonbox;
        private int customOrderID;
        private int customID;
        private int shopID;
        private String shopName;
        private String tableID;
        private String tableName;
        private int activityID;
        private Object linkTel;
        private Object receiveName;
        private Object receiveAddress;
        private Object receiveTime;
        private int orderType;
        private int orderSource;
        private int orderStatus;
        private int peopleNumber;
        private double money;
        private double totalPrice;
        private double tableFee;
        private double ticketAmount;
        private double packageFee;
        private Object deliverFee;
        private double discountAmount;
        private double platfomDiscountAmount;
        private int coinType;
        private double shouldPayAmount;
        private double actualPayAmount;
        private Object remark;
        private int isDelete;
        private int isPackage;
        private Object sendCoupon;
        private Object couponID;
        private String createUserID;
        private String createUserName;
        private long createDate;
        private Object modifyUserID;
        private Object modifyUserName;
        private Object modifyDate;
        private int isExchange;
        private Object exchangeTime;
        private int shopSerialNumber;
        private String serialNumber;
        private Object voucherCode;
        private Object voucherAmount;
        private Object rejectReason;

        public Object getBitmask() {
            return bitmask;
        }

        public void setBitmask(Object bitmask) {
            this.bitmask = bitmask;
        }

        public Object getJsonbox() {
            return jsonbox;
        }

        public void setJsonbox(Object jsonbox) {
            this.jsonbox = jsonbox;
        }

        public int getCustomOrderID() {
            return customOrderID;
        }

        public void setCustomOrderID(int customOrderID) {
            this.customOrderID = customOrderID;
        }

        public int getCustomID() {
            return customID;
        }

        public void setCustomID(int customID) {
            this.customID = customID;
        }

        public int getShopID() {
            return shopID;
        }

        public void setShopID(int shopID) {
            this.shopID = shopID;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public String getTableID() {
            return tableID;
        }

        public void setTableID(String tableID) {
            this.tableID = tableID;
        }

        public String getTableName() {
            return tableName;
        }

        public void setTableName(String tableName) {
            this.tableName = tableName;
        }

        public int getActivityID() {
            return activityID;
        }

        public void setActivityID(int activityID) {
            this.activityID = activityID;
        }

        public Object getLinkTel() {
            return linkTel;
        }

        public void setLinkTel(Object linkTel) {
            this.linkTel = linkTel;
        }

        public Object getReceiveName() {
            return receiveName;
        }

        public void setReceiveName(Object receiveName) {
            this.receiveName = receiveName;
        }

        public Object getReceiveAddress() {
            return receiveAddress;
        }

        public void setReceiveAddress(Object receiveAddress) {
            this.receiveAddress = receiveAddress;
        }

        public Object getReceiveTime() {
            return receiveTime;
        }

        public void setReceiveTime(Object receiveTime) {
            this.receiveTime = receiveTime;
        }

        public int getOrderType() {
            return orderType;
        }

        public void setOrderType(int orderType) {
            this.orderType = orderType;
        }

        public int getOrderSource() {
            return orderSource;
        }

        public void setOrderSource(int orderSource) {
            this.orderSource = orderSource;
        }

        public int getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(int orderStatus) {
            this.orderStatus = orderStatus;
        }

        public int getPeopleNumber() {
            return peopleNumber;
        }

        public void setPeopleNumber(int peopleNumber) {
            this.peopleNumber = peopleNumber;
        }

        public double getMoney() {
            return money;
        }

        public void setMoney(double money) {
            this.money = money;
        }

        public double getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(double totalPrice) {
            this.totalPrice = totalPrice;
        }

        public double getTableFee() {
            return tableFee;
        }

        public void setTableFee(double tableFee) {
            this.tableFee = tableFee;
        }

        public double getTicketAmount() {
            return ticketAmount;
        }

        public void setTicketAmount(double ticketAmount) {
            this.ticketAmount = ticketAmount;
        }

        public double getPackageFee() {
            return packageFee;
        }

        public void setPackageFee(double packageFee) {
            this.packageFee = packageFee;
        }

        public Object getDeliverFee() {
            return deliverFee;
        }

        public void setDeliverFee(Object deliverFee) {
            this.deliverFee = deliverFee;
        }

        public double getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(double discountAmount) {
            this.discountAmount = discountAmount;
        }

        public double getPlatfomDiscountAmount() {
            return platfomDiscountAmount;
        }

        public void setPlatfomDiscountAmount(double platfomDiscountAmount) {
            this.platfomDiscountAmount = platfomDiscountAmount;
        }

        public int getCoinType() {
            return coinType;
        }

        public void setCoinType(int coinType) {
            this.coinType = coinType;
        }

        public double getShouldPayAmount() {
            return shouldPayAmount;
        }

        public void setShouldPayAmount(double shouldPayAmount) {
            this.shouldPayAmount = shouldPayAmount;
        }

        public double getActualPayAmount() {
            return actualPayAmount;
        }

        public void setActualPayAmount(double actualPayAmount) {
            this.actualPayAmount = actualPayAmount;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public int getIsDelete() {
            return isDelete;
        }

        public void setIsDelete(int isDelete) {
            this.isDelete = isDelete;
        }

        public int getIsPackage() {
            return isPackage;
        }

        public void setIsPackage(int isPackage) {
            this.isPackage = isPackage;
        }

        public Object getSendCoupon() {
            return sendCoupon;
        }

        public void setSendCoupon(Object sendCoupon) {
            this.sendCoupon = sendCoupon;
        }

        public Object getCouponID() {
            return couponID;
        }

        public void setCouponID(Object couponID) {
            this.couponID = couponID;
        }

        public String getCreateUserID() {
            return createUserID;
        }

        public void setCreateUserID(String createUserID) {
            this.createUserID = createUserID;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public Object getModifyUserID() {
            return modifyUserID;
        }

        public void setModifyUserID(Object modifyUserID) {
            this.modifyUserID = modifyUserID;
        }

        public Object getModifyUserName() {
            return modifyUserName;
        }

        public void setModifyUserName(Object modifyUserName) {
            this.modifyUserName = modifyUserName;
        }

        public Object getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(Object modifyDate) {
            this.modifyDate = modifyDate;
        }

        public int getIsExchange() {
            return isExchange;
        }

        public void setIsExchange(int isExchange) {
            this.isExchange = isExchange;
        }

        public Object getExchangeTime() {
            return exchangeTime;
        }

        public void setExchangeTime(Object exchangeTime) {
            this.exchangeTime = exchangeTime;
        }

        public int getShopSerialNumber() {
            return shopSerialNumber;
        }

        public void setShopSerialNumber(int shopSerialNumber) {
            this.shopSerialNumber = shopSerialNumber;
        }

        public String getSerialNumber() {
            return serialNumber;
        }

        public void setSerialNumber(String serialNumber) {
            this.serialNumber = serialNumber;
        }

        public Object getVoucherCode() {
            return voucherCode;
        }

        public void setVoucherCode(Object voucherCode) {
            this.voucherCode = voucherCode;
        }

        public Object getVoucherAmount() {
            return voucherAmount;
        }

        public void setVoucherAmount(Object voucherAmount) {
            this.voucherAmount = voucherAmount;
        }

        public Object getRejectReason() {
            return rejectReason;
        }

        public void setRejectReason(Object rejectReason) {
            this.rejectReason = rejectReason;
        }
    }
}
