package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by lsy on 2018/5/7.
 */

public class FeedListBean {
    private String feedID;
    private String feedName;
    private double feedPrice;
    private int feedSum;
    private int isRetire;//0正常1退菜

    public int getIsRetire() {
        return isRetire;
    }

    public void setIsRetire(int isRetire) {
        this.isRetire = isRetire;
    }

    public String getFeedID() {
        return feedID;
    }

    public void setFeedID(String feedID) {
        this.feedID = feedID;
    }

    public String getFeedName() {
        return feedName;
    }

    public void setFeedName(String feedName) {
        this.feedName = feedName;
    }

    public double getFeedPrice() {
        return feedPrice;
    }

    public void setFeedPrice(double feedPrice) {
        this.feedPrice = feedPrice;
    }

    public int getFeedSum() {
        return feedSum;
    }

    public void setFeedSum(int feedSum) {
        this.feedSum = feedSum;
    }
}
