package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by LuoXueKun
 * on 2018/5/9
 */
public class QrCodeTableResponse extends BaseResponse {

    /**
     * data : {"tableID":39399,"shopID":4540,"areaID":4410,"areaName":"小包厢","tableName":"新3号桌","peopleNum":3,"lowByPeople":null,"lowFree":null,"roomFree":3,"status":1,"printerID":null,"isDelete":0,"koubeiCode":null,"createUserID":"4540","createUserName":"28金钱豹","createDate":1505374499000,"modifyUserID":"AT000064","modifyUserName":"张静","modifyDate":1527243398000,"shopTableID":"39399","shopAreaID":"4410","tableFee":0.1,"dinningPeople":null}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * tableID : 39399
         * shopID : 4540
         * areaID : 4410
         * areaName : 小包厢
         * tableName : 新3号桌
         * peopleNum : 3
         * lowByPeople : null
         * lowFree : null
         * roomFree : 3
         * status : 1
         * printerID : null
         * isDelete : 0
         * koubeiCode : null
         * createUserID : 4540
         * createUserName : 28金钱豹
         * createDate : 1505374499000
         * modifyUserID : AT000064
         * modifyUserName : 张静
         * modifyDate : 1527243398000
         * shopTableID : 39399
         * shopAreaID : 4410
         * tableFee : 0.1
         * dinningPeople : null
         */

        private int tableID;
        private int shopID;
        private int areaID;
        private String areaName;
        private String tableName;
        private int peopleNum;
        private Object lowByPeople;
        private Object lowFree;
        private int roomFree;
        private int status;
        private Object printerID;
        private int isDelete;
        private Object koubeiCode;
        private String createUserID;
        private String createUserName;
        private long createDate;
        private String modifyUserID;
        private String modifyUserName;
        private long modifyDate;
        private String shopTableID;
        private String shopAreaID;
        private double tableFee;
        private Object dinningPeople;

        public int getTableID() {
            return tableID;
        }

        public void setTableID(int tableID) {
            this.tableID = tableID;
        }

        public int getShopID() {
            return shopID;
        }

        public void setShopID(int shopID) {
            this.shopID = shopID;
        }

        public int getAreaID() {
            return areaID;
        }

        public void setAreaID(int areaID) {
            this.areaID = areaID;
        }

        public String getAreaName() {
            return areaName;
        }

        public void setAreaName(String areaName) {
            this.areaName = areaName;
        }

        public String getTableName() {
            return tableName;
        }

        public void setTableName(String tableName) {
            this.tableName = tableName;
        }

        public int getPeopleNum() {
            return peopleNum;
        }

        public void setPeopleNum(int peopleNum) {
            this.peopleNum = peopleNum;
        }

        public Object getLowByPeople() {
            return lowByPeople;
        }

        public void setLowByPeople(Object lowByPeople) {
            this.lowByPeople = lowByPeople;
        }

        public Object getLowFree() {
            return lowFree;
        }

        public void setLowFree(Object lowFree) {
            this.lowFree = lowFree;
        }

        public int getRoomFree() {
            return roomFree;
        }

        public void setRoomFree(int roomFree) {
            this.roomFree = roomFree;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public Object getPrinterID() {
            return printerID;
        }

        public void setPrinterID(Object printerID) {
            this.printerID = printerID;
        }

        public int getIsDelete() {
            return isDelete;
        }

        public void setIsDelete(int isDelete) {
            this.isDelete = isDelete;
        }

        public Object getKoubeiCode() {
            return koubeiCode;
        }

        public void setKoubeiCode(Object koubeiCode) {
            this.koubeiCode = koubeiCode;
        }

        public String getCreateUserID() {
            return createUserID;
        }

        public void setCreateUserID(String createUserID) {
            this.createUserID = createUserID;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public String getModifyUserID() {
            return modifyUserID;
        }

        public void setModifyUserID(String modifyUserID) {
            this.modifyUserID = modifyUserID;
        }

        public String getModifyUserName() {
            return modifyUserName;
        }

        public void setModifyUserName(String modifyUserName) {
            this.modifyUserName = modifyUserName;
        }

        public long getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(long modifyDate) {
            this.modifyDate = modifyDate;
        }

        public String getShopTableID() {
            return shopTableID;
        }

        public void setShopTableID(String shopTableID) {
            this.shopTableID = shopTableID;
        }

        public String getShopAreaID() {
            return shopAreaID;
        }

        public void setShopAreaID(String shopAreaID) {
            this.shopAreaID = shopAreaID;
        }

        public double getTableFee() {
            return tableFee;
        }

        public void setTableFee(double tableFee) {
            this.tableFee = tableFee;
        }

        public Object getDinningPeople() {
            return dinningPeople;
        }

        public void setDinningPeople(Object dinningPeople) {
            this.dinningPeople = dinningPeople;
        }
    }
}
