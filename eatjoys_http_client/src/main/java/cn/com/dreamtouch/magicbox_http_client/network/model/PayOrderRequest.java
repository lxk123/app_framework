package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public class PayOrderRequest extends BaseRequest{
    private String sid;
    private PayParam payOrderStrBody;

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public PayParam getPayOrderStrBody() {
        return payOrderStrBody;
    }

    public void setPayOrderStrBody(PayParam payOrderStrBody) {
        this.payOrderStrBody = payOrderStrBody;
    }
}
