package cn.com.dreamtouch.magicbox_http_client.network.model;

import java.util.List;

/**
 * Created by hu on 2016/8/15.
 */
public class RefundDishes {
    /**
     * {
     detailList =     {
     customOrderDetailID = 4003501;
     dishID = 232507;
     dishNumber = 1;
     };
     orderID = 2267169;
     shopID = 4540;
     tableID = 34796;
     }
     */
    private String orderID;
    private String shopID;
    private String tableID;
    private List<DetailListBean> detailList;

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getShopID() {
        return shopID;
    }

    public void setShopID(String shopID) {
        this.shopID = shopID;
    }

    public String getTableID() {
        return tableID;
    }

    public void setTableID(String tableID) {
        this.tableID = tableID;
    }

    public List<DetailListBean> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<DetailListBean> detailList) {
        this.detailList = detailList;
    }

    public static class DetailListBean {
        private int customOrderDetailID;
        private int dishID;
        private int dishNumber;
        private String remark;

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public int getCustomOrderDetailID() {
            return customOrderDetailID;
        }

        public void setCustomOrderDetailID(int customOrderDetailID) {
            this.customOrderDetailID = customOrderDetailID;
        }

        public int getDishID() {
            return dishID;
        }

        public void setDishID(int dishID) {
            this.dishID = dishID;
        }

        public int getDishNumber() {
            return dishNumber;
        }

        public void setDishNumber(int dishNumber) {
            this.dishNumber = dishNumber;
        }
    }
}
