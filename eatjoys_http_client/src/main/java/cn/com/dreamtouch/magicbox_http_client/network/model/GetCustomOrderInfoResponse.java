package cn.com.dreamtouch.magicbox_http_client.network.model;

import java.util.List;

/**
 * Created by LuoXueKun
 * on 2018/5/9
 */
public class GetCustomOrderInfoResponse  extends  BaseResponse{

    /**
     * data : {"customOrderID":3217878,"customID":-2,"shopID":4540,"shopName":"28金钱豹","tableID":"43575","tableName":"新5号桌","activityID":null,"linkTel":null,"receiveName":null,"receiveAddress":null,"receiveTime":null,"orderType":4,"orderSource":7,"orderStatus":3,"peopleNumber":1,"money":0,"totalPrice":12,"ticketAmount":0,"packageFee":null,"deliverFee":null,"discountAmount":0,"platfomDiscountAmount":0,"coinType":0,"shouldPayAmount":12,"actualPayAmount":0,"remark":"","isDelete":null,"isPackage":0,"sendCoupon":null,"couponID":null,"createUserID":null,"createUserName":"17012345678","createDate":1527660384000,"modifyUserID":null,"modifyUserName":null,"modifyDate":null,"isExchange":0,"exchangeTime":null,"shopSerialNumber":11,"serialNumber":"20180530004540138447","discountMoney":null,"fullCutMoney":null,"tableFee":0,"discountActivity":null,"voucherCode":null,"voucherName":null,"voucherAmount":null,"paymentTypeCode":5,"paymentTypeName":null,"payTime":"2018-05-30 14:06:28","fullCutActivity":null,"customorderdetailDOList":[{"bitmask":0,"jsonbox":null,"customOrderDetailID":6413931,"customOrderID":3217878,"dishID":"325130","dishName":"熊男","dishStatus":2,"sortNumber":0,"price":6,"discountPrice":6,"dishNumber":1,"orginalDishNumber":1,"dishUnitID":null,"dishUnitName":null,"tasteID":null,"tastePrice":0,"detailType":0,"foreignkey":null,"measureUnitName":"份","orderUnitName":null,"tasteName":null,"remark":null,"isDelete":0,"createUserID":"-2","createUserName":"17012345678","createDate":1527660384000,"modifyUserID":null,"modifyUserName":null,"modifyDate":null,"isMustOrder":null,"feedingJson":null,"feedingPrice":null,"isRetire":0,"retireNum":0,"rejectReason":null},{"bitmask":0,"jsonbox":null,"customOrderDetailID":6413932,"customOrderID":3217878,"dishID":"325129","dishName":"碗男","dishStatus":2,"sortNumber":0,"price":6,"discountPrice":6,"dishNumber":1,"orginalDishNumber":1,"dishUnitID":null,"dishUnitName":null,"tasteID":null,"tastePrice":0,"detailType":0,"foreignkey":null,"measureUnitName":"份","orderUnitName":null,"tasteName":null,"remark":null,"isDelete":0,"createUserID":"-2","createUserName":"17012345678","createDate":1527660384000,"modifyUserID":null,"modifyUserName":null,"modifyDate":null,"isMustOrder":null,"feedingJson":null,"feedingPrice":null,"isRetire":0,"retireNum":0,"rejectReason":null}],"voucherVOList":[{"shopID":4540,"voucherCode":"4540_3_vou","voucherName":"美团代金券","voucherStatus":1,"id":3},{"shopID":4540,"voucherCode":"4540_4_vou","voucherName":"百度代金券","voucherStatus":1,"id":6}],"rejectReason":null}
     */

    private DataBean data;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        @Override
        public String toString() {
            return "DataBean{" +
                    "customOrderID=" + customOrderID +
                    ", customID=" + customID +
                    ", shopID=" + shopID +
                    ", shopName='" + shopName + '\'' +
                    ", tableID='" + tableID + '\'' +
                    ", tableName='" + tableName + '\'' +
                    ", activityID=" + activityID +
                    ", linkTel=" + linkTel +
                    ", receiveName=" + receiveName +
                    ", receiveAddress=" + receiveAddress +
                    ", receiveTime=" + receiveTime +
                    ", orderType=" + orderType +
                    ", orderSource=" + orderSource +
                    ", orderStatus=" + orderStatus +
                    ", peopleNumber=" + peopleNumber +
                    ", money=" + money +
                    ", totalPrice=" + totalPrice +
                    ", ticketAmount=" + ticketAmount +
                    ", packageFee=" + packageFee +
                    ", deliverFee=" + deliverFee +
                    ", discountAmount=" + discountAmount +
                    ", platfomDiscountAmount=" + platfomDiscountAmount +
                    ", coinType=" + coinType +
                    ", shouldPayAmount=" + shouldPayAmount +
                    ", actualPayAmount=" + actualPayAmount +
                    ", remark='" + remark + '\'' +
                    ", isDelete=" + isDelete +
                    ", isPackage=" + isPackage +
                    ", sendCoupon=" + sendCoupon +
                    ", couponID=" + couponID +
                    ", createUserID=" + createUserID +
                    ", createUserName='" + createUserName + '\'' +
                    ", createDate=" + createDate +
                    ", modifyUserID=" + modifyUserID +
                    ", modifyUserName=" + modifyUserName +
                    ", modifyDate=" + modifyDate +
                    ", isExchange=" + isExchange +
                    ", exchangeTime=" + exchangeTime +
                    ", shopSerialNumber=" + shopSerialNumber +
                    ", serialNumber='" + serialNumber + '\'' +
                    ", discountMoney=" + discountMoney +
                    ", fullCutMoney=" + fullCutMoney +
                    ", tableFee=" + tableFee +
                    ", discountActivity=" + discountActivity +
                    ", voucherCode=" + voucherCode +
                    ", voucherName=" + voucherName +
                    ", voucherAmount=" + voucherAmount +
                    ", paymentTypeCode=" + paymentTypeCode +
                    ", paymentTypeName=" + paymentTypeName +
                    ", payTime='" + payTime + '\'' +
                    ", fullCutActivity=" + fullCutActivity +
                    ", rejectReason=" + rejectReason +
                    ", customorderdetailDOList=" + customorderdetailDOList +
                    ", voucherVOList=" + voucherVOList +
                    '}';
        }

        /**
         * customOrderID : 3217878
         * customID : -2
         * shopID : 4540
         * shopName : 28金钱豹
         * tableID : 43575
         * tableName : 新5号桌
         * activityID : null
         * linkTel : null
         * receiveName : null
         * receiveAddress : null
         * receiveTime : null
         * orderType : 4
         * orderSource : 7
         * orderStatus : 3
         * peopleNumber : 1
         * money : 0.0
         * totalPrice : 12.0
         * ticketAmount : 0.0
         * packageFee : null
         * deliverFee : null
         * discountAmount : 0.0
         * platfomDiscountAmount : 0.0
         * coinType : 0
         * shouldPayAmount : 12.0
         * actualPayAmount : 0.0
         * remark :
         * isDelete : null
         * isPackage : 0
         * sendCoupon : null
         * couponID : null
         * createUserID : null
         * createUserName : 17012345678
         * createDate : 1527660384000
         * modifyUserID : null
         * modifyUserName : null
         * modifyDate : null
         * isExchange : 0
         * exchangeTime : null
         * shopSerialNumber : 11
         * serialNumber : 20180530004540138447
         * discountMoney : null
         * fullCutMoney : null
         * tableFee : 0.0
         * discountActivity : null
         * voucherCode : null
         * voucherName : null
         * voucherAmount : null
         * paymentTypeCode : 5
         * paymentTypeName : null
         * payTime : 2018-05-30 14:06:28
         * fullCutActivity : null
         * customorderdetailDOList : [{"bitmask":0,"jsonbox":null,"customOrderDetailID":6413931,"customOrderID":3217878,"dishID":"325130","dishName":"熊男","dishStatus":2,"sortNumber":0,"price":6,"discountPrice":6,"dishNumber":1,"orginalDishNumber":1,"dishUnitID":null,"dishUnitName":null,"tasteID":null,"tastePrice":0,"detailType":0,"foreignkey":null,"measureUnitName":"份","orderUnitName":null,"tasteName":null,"remark":null,"isDelete":0,"createUserID":"-2","createUserName":"17012345678","createDate":1527660384000,"modifyUserID":null,"modifyUserName":null,"modifyDate":null,"isMustOrder":null,"feedingJson":null,"feedingPrice":null,"isRetire":0,"retireNum":0,"rejectReason":null},{"bitmask":0,"jsonbox":null,"customOrderDetailID":6413932,"customOrderID":3217878,"dishID":"325129","dishName":"碗男","dishStatus":2,"sortNumber":0,"price":6,"discountPrice":6,"dishNumber":1,"orginalDishNumber":1,"dishUnitID":null,"dishUnitName":null,"tasteID":null,"tastePrice":0,"detailType":0,"foreignkey":null,"measureUnitName":"份","orderUnitName":null,"tasteName":null,"remark":null,"isDelete":0,"createUserID":"-2","createUserName":"17012345678","createDate":1527660384000,"modifyUserID":null,"modifyUserName":null,"modifyDate":null,"isMustOrder":null,"feedingJson":null,"feedingPrice":null,"isRetire":0,"retireNum":0,"rejectReason":null}]
         * voucherVOList : [{"shopID":4540,"voucherCode":"4540_3_vou","voucherName":"美团代金券","voucherStatus":1,"id":3},{"shopID":4540,"voucherCode":"4540_4_vou","voucherName":"百度代金券","voucherStatus":1,"id":6}]
         * rejectReason : null
         */

        private int customOrderID;
        private int customID;
        private int shopID;
        private String shopName;
        private String tableID;
        private String tableName;
        private Object activityID;
        private Object linkTel;
        private Object receiveName;
        private Object receiveAddress;
        private Object receiveTime;
        private int orderType;
        private int orderSource;
        private int orderStatus;
        private int peopleNumber;
        private double money;
        private double totalPrice;
        private double ticketAmount;
        private Object packageFee;
        private Object deliverFee;
        private double discountAmount;
        private double platfomDiscountAmount;
        private int coinType;
        private double shouldPayAmount;
        private double actualPayAmount;
        private String remark;
        private Object isDelete;
        private int isPackage;
        private Object sendCoupon;
        private Object couponID;
        private Object createUserID;
        private String createUserName;
        private long createDate;
        private Object modifyUserID;
        private Object modifyUserName;
        private Object modifyDate;
        private int isExchange;
        private Object exchangeTime;
        private int shopSerialNumber;
        private String serialNumber;
        private Object discountMoney;
        private Object fullCutMoney;
        private double tableFee;
        private Object discountActivity;
        private Object voucherCode;
        private Object voucherName;
        private Object voucherAmount;
        private int paymentTypeCode;
        private Object paymentTypeName;
        private String payTime;
        private Object fullCutActivity;
        private Object rejectReason;
        private List<DetailListBean> customorderdetailDOList;
        private List<VoucherVOListBean> voucherVOList;

        public int getCustomOrderID() {
            return customOrderID;
        }

        public void setCustomOrderID(int customOrderID) {
            this.customOrderID = customOrderID;
        }

        public int getCustomID() {
            return customID;
        }

        public void setCustomID(int customID) {
            this.customID = customID;
        }

        public int getShopID() {
            return shopID;
        }

        public void setShopID(int shopID) {
            this.shopID = shopID;
        }

        public String getShopName() {
            return shopName;
        }

        public void setShopName(String shopName) {
            this.shopName = shopName;
        }

        public String getTableID() {
            return tableID;
        }

        public void setTableID(String tableID) {
            this.tableID = tableID;
        }

        public String getTableName() {
            return tableName;
        }

        public void setTableName(String tableName) {
            this.tableName = tableName;
        }

        public Object getActivityID() {
            return activityID;
        }

        public void setActivityID(Object activityID) {
            this.activityID = activityID;
        }

        public Object getLinkTel() {
            return linkTel;
        }

        public void setLinkTel(Object linkTel) {
            this.linkTel = linkTel;
        }

        public Object getReceiveName() {
            return receiveName;
        }

        public void setReceiveName(Object receiveName) {
            this.receiveName = receiveName;
        }

        public Object getReceiveAddress() {
            return receiveAddress;
        }

        public void setReceiveAddress(Object receiveAddress) {
            this.receiveAddress = receiveAddress;
        }

        public Object getReceiveTime() {
            return receiveTime;
        }

        public void setReceiveTime(Object receiveTime) {
            this.receiveTime = receiveTime;
        }

        public int getOrderType() {
            return orderType;
        }

        public void setOrderType(int orderType) {
            this.orderType = orderType;
        }

        public int getOrderSource() {
            return orderSource;
        }

        public void setOrderSource(int orderSource) {
            this.orderSource = orderSource;
        }

        public int getOrderStatus() {
            return orderStatus;
        }

        public void setOrderStatus(int orderStatus) {
            this.orderStatus = orderStatus;
        }

        public int getPeopleNumber() {
            return peopleNumber;
        }

        public void setPeopleNumber(int peopleNumber) {
            this.peopleNumber = peopleNumber;
        }

        public double getMoney() {
            return money;
        }

        public void setMoney(double money) {
            this.money = money;
        }

        public double getTotalPrice() {
            return totalPrice;
        }

        public void setTotalPrice(double totalPrice) {
            this.totalPrice = totalPrice;
        }

        public double getTicketAmount() {
            return ticketAmount;
        }

        public void setTicketAmount(double ticketAmount) {
            this.ticketAmount = ticketAmount;
        }

        public Object getPackageFee() {
            return packageFee;
        }

        public void setPackageFee(Object packageFee) {
            this.packageFee = packageFee;
        }

        public Object getDeliverFee() {
            return deliverFee;
        }

        public void setDeliverFee(Object deliverFee) {
            this.deliverFee = deliverFee;
        }

        public double getDiscountAmount() {
            return discountAmount;
        }

        public void setDiscountAmount(double discountAmount) {
            this.discountAmount = discountAmount;
        }

        public double getPlatfomDiscountAmount() {
            return platfomDiscountAmount;
        }

        public void setPlatfomDiscountAmount(double platfomDiscountAmount) {
            this.platfomDiscountAmount = platfomDiscountAmount;
        }

        public int getCoinType() {
            return coinType;
        }

        public void setCoinType(int coinType) {
            this.coinType = coinType;
        }

        public double getShouldPayAmount() {
            return shouldPayAmount;
        }

        public void setShouldPayAmount(double shouldPayAmount) {
            this.shouldPayAmount = shouldPayAmount;
        }

        public double getActualPayAmount() {
            return actualPayAmount;
        }

        public void setActualPayAmount(double actualPayAmount) {
            this.actualPayAmount = actualPayAmount;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public Object getIsDelete() {
            return isDelete;
        }

        public void setIsDelete(Object isDelete) {
            this.isDelete = isDelete;
        }

        public int getIsPackage() {
            return isPackage;
        }

        public void setIsPackage(int isPackage) {
            this.isPackage = isPackage;
        }

        public Object getSendCoupon() {
            return sendCoupon;
        }

        public void setSendCoupon(Object sendCoupon) {
            this.sendCoupon = sendCoupon;
        }

        public Object getCouponID() {
            return couponID;
        }

        public void setCouponID(Object couponID) {
            this.couponID = couponID;
        }

        public Object getCreateUserID() {
            return createUserID;
        }

        public void setCreateUserID(Object createUserID) {
            this.createUserID = createUserID;
        }

        public String getCreateUserName() {
            return createUserName;
        }

        public void setCreateUserName(String createUserName) {
            this.createUserName = createUserName;
        }

        public long getCreateDate() {
            return createDate;
        }

        public void setCreateDate(long createDate) {
            this.createDate = createDate;
        }

        public Object getModifyUserID() {
            return modifyUserID;
        }

        public void setModifyUserID(Object modifyUserID) {
            this.modifyUserID = modifyUserID;
        }

        public Object getModifyUserName() {
            return modifyUserName;
        }

        public void setModifyUserName(Object modifyUserName) {
            this.modifyUserName = modifyUserName;
        }

        public Object getModifyDate() {
            return modifyDate;
        }

        public void setModifyDate(Object modifyDate) {
            this.modifyDate = modifyDate;
        }

        public int getIsExchange() {
            return isExchange;
        }

        public void setIsExchange(int isExchange) {
            this.isExchange = isExchange;
        }

        public Object getExchangeTime() {
            return exchangeTime;
        }

        public void setExchangeTime(Object exchangeTime) {
            this.exchangeTime = exchangeTime;
        }

        public int getShopSerialNumber() {
            return shopSerialNumber;
        }

        public void setShopSerialNumber(int shopSerialNumber) {
            this.shopSerialNumber = shopSerialNumber;
        }

        public String getSerialNumber() {
            return serialNumber;
        }

        public void setSerialNumber(String serialNumber) {
            this.serialNumber = serialNumber;
        }

        public Object getDiscountMoney() {
            return discountMoney;
        }

        public void setDiscountMoney(Object discountMoney) {
            this.discountMoney = discountMoney;
        }

        public Object getFullCutMoney() {
            return fullCutMoney;
        }

        public void setFullCutMoney(Object fullCutMoney) {
            this.fullCutMoney = fullCutMoney;
        }

        public double getTableFee() {
            return tableFee;
        }

        public void setTableFee(double tableFee) {
            this.tableFee = tableFee;
        }

        public Object getDiscountActivity() {
            return discountActivity;
        }

        public void setDiscountActivity(Object discountActivity) {
            this.discountActivity = discountActivity;
        }

        public Object getVoucherCode() {
            return voucherCode;
        }

        public void setVoucherCode(Object voucherCode) {
            this.voucherCode = voucherCode;
        }

        public Object getVoucherName() {
            return voucherName;
        }

        public void setVoucherName(Object voucherName) {
            this.voucherName = voucherName;
        }

        public Object getVoucherAmount() {
            return voucherAmount;
        }

        public void setVoucherAmount(Object voucherAmount) {
            this.voucherAmount = voucherAmount;
        }

        public int getPaymentTypeCode() {
            return paymentTypeCode;
        }

        public void setPaymentTypeCode(int paymentTypeCode) {
            this.paymentTypeCode = paymentTypeCode;
        }

        public Object getPaymentTypeName() {
            return paymentTypeName;
        }

        public void setPaymentTypeName(Object paymentTypeName) {
            this.paymentTypeName = paymentTypeName;
        }

        public String getPayTime() {
            return payTime;
        }

        public void setPayTime(String payTime) {
            this.payTime = payTime;
        }

        public Object getFullCutActivity() {
            return fullCutActivity;
        }

        public void setFullCutActivity(Object fullCutActivity) {
            this.fullCutActivity = fullCutActivity;
        }

        public Object getRejectReason() {
            return rejectReason;
        }

        public void setRejectReason(Object rejectReason) {
            this.rejectReason = rejectReason;
        }

        public List<DetailListBean> getCustomorderdetailDOList() {
            return customorderdetailDOList;
        }

        public void setCustomorderdetailDOList(List<DetailListBean> customorderdetailDOList) {
            this.customorderdetailDOList = customorderdetailDOList;
        }

        public List<VoucherVOListBean> getVoucherVOList() {
            return voucherVOList;
        }

        public void setVoucherVOList(List<VoucherVOListBean> voucherVOList) {
            this.voucherVOList = voucherVOList;
        }

        public static class CustomorderdetailDOListBean {
            /**
             * bitmask : 0
             * jsonbox : null
             * customOrderDetailID : 6413931
             * customOrderID : 3217878
             * dishID : 325130
             * dishName : 熊男
             * dishStatus : 2
             * sortNumber : 0
             * price : 6.0
             * discountPrice : 6.0
             * dishNumber : 1.0
             * orginalDishNumber : 1.0
             * dishUnitID : null
             * dishUnitName : null
             * tasteID : null
             * tastePrice : 0.0
             * detailType : 0
             * foreignkey : null
             * measureUnitName : 份
             * orderUnitName : null
             * tasteName : null
             * remark : null
             * isDelete : 0
             * createUserID : -2
             * createUserName : 17012345678
             * createDate : 1527660384000
             * modifyUserID : null
             * modifyUserName : null
             * modifyDate : null
             * isMustOrder : null
             * feedingJson : null
             * feedingPrice : null
             * isRetire : 0
             * retireNum : 0.0
             * rejectReason : null
             */

            private int bitmask;
            private Object jsonbox;
            private int customOrderDetailID;
            private int customOrderID;
            private String dishID;
            private String dishName;
            private int dishStatus;
            private int sortNumber;
            private double price;
            private double discountPrice;
            private double dishNumber;
            private double orginalDishNumber;
            private Object dishUnitID;
            private String dishUnitName;
            private Object tasteID;
            private double tastePrice;
            private int detailType;
            private Object foreignkey;
            private String measureUnitName;
            private Object orderUnitName;
            private String tasteName;
            private Object remark;
            private int isDelete;
            private String createUserID;
            private String createUserName;
            private long createDate;
            private Object modifyUserID;
            private Object modifyUserName;
            private Object modifyDate;
            private Object isMustOrder;
            private Object feedingJson;
            private double feedingPrice;
            private int isRetire;
            private double retireNum;
            private Object rejectReason;

            public int getBitmask() {
                return bitmask;
            }

            public void setBitmask(int bitmask) {
                this.bitmask = bitmask;
            }

            public Object getJsonbox() {
                return jsonbox;
            }

            public void setJsonbox(Object jsonbox) {
                this.jsonbox = jsonbox;
            }

            public int getCustomOrderDetailID() {
                return customOrderDetailID;
            }

            public void setCustomOrderDetailID(int customOrderDetailID) {
                this.customOrderDetailID = customOrderDetailID;
            }

            public int getCustomOrderID() {
                return customOrderID;
            }

            public void setCustomOrderID(int customOrderID) {
                this.customOrderID = customOrderID;
            }

            public String getDishID() {
                return dishID;
            }

            public void setDishID(String dishID) {
                this.dishID = dishID;
            }

            public String getDishName() {
                return dishName;
            }

            public void setDishName(String dishName) {
                this.dishName = dishName;
            }

            public int getDishStatus() {
                return dishStatus;
            }

            public void setDishStatus(int dishStatus) {
                this.dishStatus = dishStatus;
            }

            public int getSortNumber() {
                return sortNumber;
            }

            public void setSortNumber(int sortNumber) {
                this.sortNumber = sortNumber;
            }

            public double getPrice() {
                return price;
            }

            public void setPrice(double price) {
                this.price = price;
            }

            public double getDiscountPrice() {
                return discountPrice;
            }

            public void setDiscountPrice(double discountPrice) {
                this.discountPrice = discountPrice;
            }

            public double getDishNumber() {
                return dishNumber;
            }

            public void setDishNumber(double dishNumber) {
                this.dishNumber = dishNumber;
            }

            public double getOrginalDishNumber() {
                return orginalDishNumber;
            }

            public void setOrginalDishNumber(double orginalDishNumber) {
                this.orginalDishNumber = orginalDishNumber;
            }

            public Object getDishUnitID() {
                return dishUnitID;
            }

            public void setDishUnitID(Object dishUnitID) {
                this.dishUnitID = dishUnitID;
            }

            public String getDishUnitName() {
                return dishUnitName;
            }

            public void setDishUnitName(String dishUnitName) {
                this.dishUnitName = dishUnitName;
            }

            public Object getTasteID() {
                return tasteID;
            }

            public void setTasteID(Object tasteID) {
                this.tasteID = tasteID;
            }

            public double getTastePrice() {
                return tastePrice;
            }

            public void setTastePrice(double tastePrice) {
                this.tastePrice = tastePrice;
            }

            public int getDetailType() {
                return detailType;
            }

            public void setDetailType(int detailType) {
                this.detailType = detailType;
            }

            public Object getForeignkey() {
                return foreignkey;
            }

            public void setForeignkey(Object foreignkey) {
                this.foreignkey = foreignkey;
            }

            public String getMeasureUnitName() {
                return measureUnitName;
            }

            public void setMeasureUnitName(String measureUnitName) {
                this.measureUnitName = measureUnitName;
            }

            public Object getOrderUnitName() {
                return orderUnitName;
            }

            public void setOrderUnitName(Object orderUnitName) {
                this.orderUnitName = orderUnitName;
            }

            public String getTasteName() {
                return tasteName;
            }

            public void setTasteName(String tasteName) {
                this.tasteName = tasteName;
            }

            public Object getRemark() {
                return remark;
            }

            public void setRemark(Object remark) {
                this.remark = remark;
            }

            public int getIsDelete() {
                return isDelete;
            }

            public void setIsDelete(int isDelete) {
                this.isDelete = isDelete;
            }

            public String getCreateUserID() {
                return createUserID;
            }

            public void setCreateUserID(String createUserID) {
                this.createUserID = createUserID;
            }

            public String getCreateUserName() {
                return createUserName;
            }

            public void setCreateUserName(String createUserName) {
                this.createUserName = createUserName;
            }

            public long getCreateDate() {
                return createDate;
            }

            public void setCreateDate(long createDate) {
                this.createDate = createDate;
            }

            public Object getModifyUserID() {
                return modifyUserID;
            }

            public void setModifyUserID(Object modifyUserID) {
                this.modifyUserID = modifyUserID;
            }

            public Object getModifyUserName() {
                return modifyUserName;
            }

            public void setModifyUserName(Object modifyUserName) {
                this.modifyUserName = modifyUserName;
            }

            public Object getModifyDate() {
                return modifyDate;
            }

            public void setModifyDate(Object modifyDate) {
                this.modifyDate = modifyDate;
            }

            public Object getIsMustOrder() {
                return isMustOrder;
            }

            public void setIsMustOrder(Object isMustOrder) {
                this.isMustOrder = isMustOrder;
            }



            public double getFeedingPrice() {
                return feedingPrice;
            }

            public void setFeedingPrice(double feedingPrice) {
                this.feedingPrice = feedingPrice;
            }

            public int getIsRetire() {
                return isRetire;
            }

            public void setIsRetire(int isRetire) {
                this.isRetire = isRetire;
            }

            public double getRetireNum() {
                return retireNum;
            }

            public void setRetireNum(double retireNum) {
                this.retireNum = retireNum;
            }

            public Object getRejectReason() {
                return rejectReason;
            }

            public void setRejectReason(Object rejectReason) {
                this.rejectReason = rejectReason;
            }

            /*--------------------------------------------*/
            private double sumPrice;//总价

            public Object getFeedingJson() {
                return feedingJson;
            }

            public void setFeedingJson(String feedingJson) {
                this.feedingJson = feedingJson;
            }

            public double getSumPrice() {
                return sumPrice;
            }

            public void setSumPrice(double sumPrice) {
                this.sumPrice = sumPrice;
            }

        }

        public static class VoucherVOListBean {
            /**
             * shopID : 4540
             * voucherCode : 4540_3_vou
             * voucherName : 美团代金券
             * voucherStatus : 1
             * id : 3
             */

            private int shopID;
            private String voucherCode;
            private String voucherName;
            private int voucherStatus;
            private int id;

            public int getShopID() {
                return shopID;
            }

            public void setShopID(int shopID) {
                this.shopID = shopID;
            }

            public String getVoucherCode() {
                return voucherCode;
            }

            public void setVoucherCode(String voucherCode) {
                this.voucherCode = voucherCode;
            }

            public String getVoucherName() {
                return voucherName;
            }

            public void setVoucherName(String voucherName) {
                this.voucherName = voucherName;
            }

            public int getVoucherStatus() {
                return voucherStatus;
            }

            public void setVoucherStatus(int voucherStatus) {
                this.voucherStatus = voucherStatus;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }
        }
    }
}
