package cn.com.dreamtouch.magicbox_http_client.network.model;

import java.util.List;

/**
 * Created by LuoXueKun
 * on 2018/5/10
 */
public class GetAllPayMethodResponse  extends  BaseResponse{

    private List<DataBean> data;

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * shopID : 4540
         * paymentMethodCode : 4540_12_pay
         * paymentMethodName : 微信线下支付
         * isEnable : Y
         * id : 442
         */
        private String paymentType;

        private int shopID;
        private String paymentMethodCode;
        private String paymentMethodName;
        private String isEnable;
        private int id;
        public  DataBean(){

        }
        public DataBean(String paymentMethodName, String paymentType) {
            this.paymentMethodName = paymentMethodName;
            this.paymentType = paymentType;
        }

        public int getShopID() {
            return shopID;
        }

        public void setShopID(int shopID) {
            this.shopID = shopID;
        }

        public String getPaymentMethodCode() {
            return paymentMethodCode;
        }

        public void setPaymentMethodCode(String paymentMethodCode) {
            this.paymentMethodCode = paymentMethodCode;
        }

        public String getPaymentMethodName() {
            return paymentMethodName;
        }

        public void setPaymentMethodName(String paymentMethodName) {
            this.paymentMethodName = paymentMethodName;
        }

        public String getIsEnable() {
            return isEnable;
        }

        public void setIsEnable(String isEnable) {
            this.isEnable = isEnable;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }


    }
}
