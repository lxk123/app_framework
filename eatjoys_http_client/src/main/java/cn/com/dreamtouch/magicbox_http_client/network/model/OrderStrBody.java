package cn.com.dreamtouch.magicbox_http_client.network.model;

import java.util.List;

import retrofit2.http.Body;

/**
 * Created by LuoXueKun
 * on 2018/5/8
 */
public class OrderStrBody {
    @Override
    public String toString() {
        return "OrderStrBody{" +
                "actuallyMoney='" + actuallyMoney + '\'' +
                ", orderType=" + orderType +
                ", peopleNumber=" + peopleNumber +
                ", remark='" + remark + '\'' +
                ", shopID=" + shopID +
                ", tableFee='" + tableFee + '\'' +
                ", tableID=" + tableID +
                ", total=" + total +
                ", totalPrice='" + totalPrice + '\'' +
                ", detailList=" + detailList +
                '}';
    }

    /**
     * actuallyMoney : 3
     * detailList : [{"boxFee":"100","dishID":308405,"dishName":"麻辣小龙虾","dishNumber":3,"dishTypeID":29206,"finalPrice":"3","isMustOrder":"0","price":"1"}]
     * orderType : 4
     * peopleNumber : 36
     * remark : 刚刚
     * shopID : 4540
     * tableFee : 0.0
     * tableID : 44162
     * total : 1
     * totalPrice : 3
     */

    private String actuallyMoney;
    private int orderType;
    private int peopleNumber;
    private String remark;
    private int shopID;
    private String tableFee;
    private int tableID;
    private int total;
    private String totalPrice;
    private List<DetailListBean> detailList;

    public String getActuallyMoney() {
        return actuallyMoney;
    }

    public void setActuallyMoney(String actuallyMoney) {
        this.actuallyMoney = actuallyMoney;
    }

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    public int getPeopleNumber() {
        return peopleNumber;
    }

    public void setPeopleNumber(int peopleNumber) {
        this.peopleNumber = peopleNumber;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getShopID() {
        return shopID;
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }

    public String getTableFee() {
        return tableFee;
    }

    public void setTableFee(String tableFee) {
        this.tableFee = tableFee;
    }

    public int getTableID() {
        return tableID;
    }

    public void setTableID(int tableID) {
        this.tableID = tableID;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<DetailListBean> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<DetailListBean> detailList) {
        this.detailList = detailList;
    }

    public static class DetailListBean {
        @Override
        public String toString() {
            return "DetailListBean{" +
                    "boxFee='" + boxFee + '\'' +
                    ", dishID=" + dishID +
                    ", dishName='" + dishName + '\'' +
                    ", dishNumber=" + dishNumber +
                    ", dishTypeID=" + dishTypeID +
                    ", finalPrice='" + finalPrice + '\'' +
                    ", isMustOrder='" + isMustOrder + '\'' +
                    ", price='" + price + '\'' +
                    '}';
        }

        /**
         * boxFee : 100
         * dishID : 308405
         * dishName : 麻辣小龙虾
         * dishNumber : 3
         * dishTypeID : 29206
         * finalPrice : 3
         * isMustOrder : 0
         * price : 1
         */

        private String boxFee;
        private int dishID;
        private String dishName;
        private int dishNumber;
        private int dishTypeID;
        private String finalPrice;
        private String isMustOrder;
        private String price;

        public String getBoxFee() {
            return boxFee;
        }

        public void setBoxFee(String boxFee) {
            this.boxFee = boxFee;
        }

        public int getDishID() {
            return dishID;
        }

        public void setDishID(int dishID) {
            this.dishID = dishID;
        }

        public String getDishName() {
            return dishName;
        }

        public void setDishName(String dishName) {
            this.dishName = dishName;
        }

        public int getDishNumber() {
            return dishNumber;
        }

        public void setDishNumber(int dishNumber) {
            this.dishNumber = dishNumber;
        }

        public int getDishTypeID() {
            return dishTypeID;
        }

        public void setDishTypeID(int dishTypeID) {
            this.dishTypeID = dishTypeID;
        }

        public String getFinalPrice() {
            return finalPrice;
        }

        public void setFinalPrice(String finalPrice) {
            this.finalPrice = finalPrice;
        }

        public String getIsMustOrder() {
            return isMustOrder;
        }

        public void setIsMustOrder(String isMustOrder) {
            this.isMustOrder = isMustOrder;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        /*---------------------------------*/
        private int isRetire;//0正常1退菜
        private int retireNum;//退菜数量
        private String dishUnitName;

        public int getIsRetire() {
            return isRetire;
        }

        public void setIsRetire(int isRetire) {
            this.isRetire = isRetire;
        }

        public int getRetireNum() {
            return retireNum;
        }

        public void setRetireNum(int retireNum) {
            this.retireNum = retireNum;
        }

        public String getDishUnitName() {
            return dishUnitName;
        }

        public void setDishUnitName(String dishUnitName) {
            this.dishUnitName = dishUnitName;
        }
    }
}
