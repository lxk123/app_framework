package cn.com.dreamtouch.magicbox_http_client.network.model;

import android.text.TextUtils;

public class DishSkuEquals {
    private String dishUnit;//规格
    private String dishFeed;//加料
    private String dishCombo;//套餐
    private String dishTaste;//口味

    public String getDishCombo() {
        return dishCombo;
    }

    public void setDishCombo(String dishCombo) {
        this.dishCombo = dishCombo;
    }

    public String getDishUnit() {
        return dishUnit;
    }

    public void setDishUnit(String dishUnit) {
        this.dishUnit = dishUnit;
    }

    public String getDishFeed() {
        return dishFeed;
    }

    public void setDishFeed(String dishFeed) {
        this.dishFeed = dishFeed;
    }

    public String getDishTaste() {
        return dishTaste;
    }

    public void setDishTaste(String dishTaste) {
        this.dishTaste = dishTaste;
    }

    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        DishSkuEquals sku = (DishSkuEquals) obj;
        return TextUtils.equals(dishUnit, sku.dishUnit) && TextUtils.equals(dishTaste, sku.dishTaste)
                && TextUtils.equals(dishFeed, sku.dishFeed) && TextUtils.equals(dishCombo, sku.dishCombo) ;
    }

    @Override
    public String toString() {
        return "DishSkuEquals{" +
                "dishUnit='" + dishUnit + '\'' +
                ", dishFeed='" + dishFeed + '\'' +
                ", dishCombo='" + dishCombo + '\'' +
                ", dishTaste='" + dishTaste + '\'' +
                '}';
    }
}
