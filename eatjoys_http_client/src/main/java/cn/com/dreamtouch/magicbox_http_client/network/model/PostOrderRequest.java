package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public class PostOrderRequest extends BaseRequest{

    private OrderParam orderParam;

    public OrderParam getOrderParam() {
        return orderParam;
    }

    public void setOrderParam(OrderParam orderParam) {
        this.orderParam = orderParam;
    }
    /* private String sid;
      private OrderStrBody orderStrBody;

    @Override
    public String toString() {
        return "PostOrderRequest{" +
                "sid='" + sid + '\'' +
                ", orderStrBody=" + orderStrBody +
                '}';
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }

    public OrderStrBody getOrderStrBody() {
        return orderStrBody;
    }

    public void setOrderStrBody(OrderStrBody orderStrBody) {
        this.orderStrBody = orderStrBody;
    }*/
}
