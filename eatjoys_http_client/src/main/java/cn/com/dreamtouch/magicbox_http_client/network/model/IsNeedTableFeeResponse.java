package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by LuoXueKun
 * on 2018/5/10
 */
public class IsNeedTableFeeResponse extends  BaseResponse {

    /**
     * data : {"isNeed":true,"tableFee":0.01}
     * success : true
     */

    private DataBean data;
    private boolean success;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class DataBean {
        /**
         * isNeed : true
         * tableFee : 0.01
         */

        private boolean isNeed;
        private double tableFee;

        public boolean isIsNeed() {
            return isNeed;
        }

        public void setIsNeed(boolean isNeed) {
            this.isNeed = isNeed;
        }

        public double getTableFee() {
            return tableFee;
        }

        public void setTableFee(double tableFee) {
            this.tableFee = tableFee;
        }
    }
}
