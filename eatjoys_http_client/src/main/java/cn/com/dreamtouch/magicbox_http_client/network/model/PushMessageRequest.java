package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by LuoXueKun
 * on 2018/5/7
 */
public class PushMessageRequest extends BaseRequest{
     private  String shopSerialNumber;
     private  String shopID;

    public String getShopSerialNumber() {
        return shopSerialNumber;
    }

    public void setShopSerialNumber(String shopSerialNumber) {
        this.shopSerialNumber = shopSerialNumber;
    }

    public String getShopID() {
        return shopID;
    }

    public void setShopID(String shopID) {
        this.shopID = shopID;
    }
}
