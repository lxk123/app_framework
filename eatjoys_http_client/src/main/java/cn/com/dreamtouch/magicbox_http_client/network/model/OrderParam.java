package cn.com.dreamtouch.magicbox_http_client.network.model;




import java.util.HashMap;
import java.util.List;

/**
 * 订单参数 bean
 * Created by lsy on 16/8/4.
 */
public class OrderParam {

    private int shopID;
    private int tableID;
    private int peopleNumber;
    private int total;
    private int orderType;
    private String totalPrice;
    private String actuallyMoney;
    private String remark;
    private String tableFee;
    private List<OrderDetailParam> detailList;

    public OrderParam(int shopID, int tableID, int peopleNumber, int total, int orderType, String totalPrice, String actuallyMoney, String remark, String tableFee, List<OrderDetailParam> detailList) {
        this.shopID = shopID;
        this.tableID = tableID;
        this.peopleNumber = peopleNumber;
        this.orderType = orderType;
        this.total = total;
        this.totalPrice = totalPrice;
        this.actuallyMoney = actuallyMoney;
        this.remark = remark;
        this.tableFee = tableFee;
        this.detailList = detailList;
    }

    public int getShopID() {
        return shopID;
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }

    public int getTableID() {
        return tableID;
    }

    public void setTableID(int tableID) {
        this.tableID = tableID;
    }

    public int getPeopleNumber() {
        return peopleNumber;
    }

    public void setPeopleNumber(int peopleNumber) {
        this.peopleNumber = peopleNumber;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getOrderType() {
        return orderType;
    }

    public void setOrderType(int orderType) {
        this.orderType = orderType;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getActuallyMoney() {
        return actuallyMoney;
    }

    public void setActuallyMoney(String actuallyMoney) {
        this.actuallyMoney = actuallyMoney;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTableFee() {
        return tableFee;
    }

    public void setTableFee(String tableFee) {
        this.tableFee = tableFee;
    }

    public List<OrderDetailParam> getDetailList() {
        return detailList;
    }

    public void setDetailList(List<OrderDetailParam> detailList) {
        this.detailList = detailList;
    }

    //订单详细参数
    public static class OrderDetailParam {
        private int dishID;
        private int dishTypeID;
        private String boxFee;
        private String price;
        private String finalPrice;
        private String isMustOrder;
        private String dishUnitID;
        private String tasteID;
        private int dishNumber;
        private String remark;
        private String dishName;
        private HashMap<String,Integer> feedMap;//加料
        private String _id;

        public OrderDetailParam(int dishID, int dishTypeID, String boxFee,
                                String price, String finalPrice, String isMustOrder,
                                String dishUnitID, String tasteID, HashMap<String,Integer> feedList,
                                int dishNumber, String remark, String dishName,String _id) {
            this.dishID = dishID;
            this.dishTypeID = dishTypeID;
            this.boxFee = boxFee;
            this.price = price;
            this.finalPrice = finalPrice;
            this.isMustOrder = isMustOrder;
            this.dishUnitID = dishUnitID;
            this.tasteID = tasteID;
            this.feedMap = feedList;
            this.dishNumber = dishNumber;
            this.remark = remark;
            this.dishName = dishName;
            this._id = _id;
        }

        public String get_id() {
            return _id;
        }

        public void set_id(String _id) {
            this._id = _id;
        }

        public int getDishID() {
            return dishID;
        }

        public void setDishID(int dishID) {
            this.dishID = dishID;
        }

        public int getDishTypeID() {
            return dishTypeID;
        }

        public void setDishTypeID(int dishTypeID) {
            this.dishTypeID = dishTypeID;
        }

        public String getBoxFee() {
            return boxFee;
        }

        public void setBoxFee(String boxFee) {
            this.boxFee = boxFee;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getFinalPrice() {
            return finalPrice;
        }

        public void setFinalPrice(String finalPrice) {
            this.finalPrice = finalPrice;
        }

        public String getIsMustOrder() {
            return isMustOrder;
        }

        public void setIsMustOrder(String isMustOrder) {
            this.isMustOrder = isMustOrder;
        }

        public String getDishUnitID() {
            return dishUnitID;
        }

        public void setDishUnitID(String dishUnitID) {
            this.dishUnitID = dishUnitID;
        }

        public String getTasteID() {
            return tasteID;
        }

        public void setTasteID(String tasteID) {
            this.tasteID = tasteID;
        }

        public int getDishNumber() {
            return dishNumber;
        }

        public void setDishNumber(int dishNumber) {
            this.dishNumber = dishNumber;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public String getDishName() {
            return dishName;
        }

        public void setDishName(String dishName) {
            this.dishName = dishName;
        }

        public HashMap<String, Integer> getFeedMap() {
            return feedMap;
        }

        public void setFeedMap(HashMap<String, Integer> feedMap) {
            this.feedMap = feedMap;
        }
    }
}
