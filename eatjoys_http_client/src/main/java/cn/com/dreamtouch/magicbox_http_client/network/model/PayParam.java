package cn.com.dreamtouch.magicbox_http_client.network.model;

import java.util.List;

/**
 * 支付参数 Bean
 * <p>
 * Created by lsy on 16/8/11.
 */
public class PayParam {
    private int shopID;
    private int orderID;
    private int tableID;
    private double money;
    private String voucherCode;
    private double voucherAmount;
    private String tableFee;
    private List<PayDetailParam> paymentList;

    public PayParam(int shopID, int orderID, int tableID, double money, String voucherCode, double voucherAmount, String tableFee) {
        this.shopID = shopID;
        this.orderID = orderID;
        this.money = money;
        this.tableID = tableID;
        this.voucherCode = voucherCode;
        this.voucherAmount = voucherAmount;
        this.tableFee = tableFee;
    }

    public String getTableFee() {
        return tableFee;
    }

    public void setTableFee(String tableFee) {
        this.tableFee = tableFee;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public double getVoucherAmount() {
        return voucherAmount;
    }

    public void setVoucherAmount(double voucherAmount) {
        this.voucherAmount = voucherAmount;
    }

    public int getShopID() {
        return shopID;
    }

    public void setShopID(int shopID) {
        this.shopID = shopID;
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public int getTableID() {
        return tableID;
    }

    public void setTableID(int tableID) {
        this.tableID = tableID;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public List<PayDetailParam> getPaymentList() {
        return paymentList;
    }

    public void setPaymentList(List<PayDetailParam> paymentList) {
        this.paymentList = paymentList;
    }

    public static class PayDetailParam {

        private String paymentType;
        private double money;
        private String paymentMethod;
        private String authCode;
        private String remark;

        public String getMeituanCouponNo() {
            return meituanCouponNo;
        }

        public void setMeituanCouponNo(String meituanCouponNo) {
            this.meituanCouponNo = meituanCouponNo;
        }

        public double getRebate() {
            return rebate;
        }

        public void setRebate(double rebate) {
            this.rebate = rebate;
        }

        private double rebate;//折扣
        private String meituanCouponNo;//美团劵号
        private String paymentMode;//美团劵号

        public PayDetailParam(String paymentType, double money, String paymentMethod, String authCode, String remark, String meituanCouponNo, double rebate, String paymentMode) {
            this.paymentType = paymentType;
            this.money = money;
            this.paymentMethod = paymentMethod;
            this.authCode = authCode;
            this.remark = remark;
            this.rebate = rebate;
            this.meituanCouponNo = meituanCouponNo;
            this.paymentMode = paymentMode;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getPaymentType() {
            return paymentType;
        }

        public void setPaymentType(String paymentType) {
            this.paymentType = paymentType;
        }

        public double getMoney() {
            return money;
        }

        public void setMoney(double money) {
            this.money = money;
        }

        public String getPaymentMethod() {
            return paymentMethod;
        }

        public void setPaymentMethod(String paymentMethod) {
            this.paymentMethod = paymentMethod;
        }

        public String getAuthCode() {
            return authCode;
        }

        public void setAuthCode(String authCode) {
            this.authCode = authCode;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }
    }
}
