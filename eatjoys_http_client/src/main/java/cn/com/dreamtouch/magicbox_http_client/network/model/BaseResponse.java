package cn.com.dreamtouch.magicbox_http_client.network.model;

/**
 * Created by MuHan on 17/2/20.
 */

public abstract class BaseResponse{
    private String message;
    private int code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
